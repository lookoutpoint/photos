#!/bin/sh
# SPDX-License-Identifier: MIT
rm -rf .mongo

mkdir -p .mongo/data
mkdir -p .mongo/log

echo "Starting mongod..."
mongod --dbpath .mongo/data --logpath .mongo/log/mongod.log --storageEngine wiredTiger --replSet rs0 --fork
mongosh mongo-rs-init.js
