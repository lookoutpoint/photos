// SPDX-License-Identifier: MIT

package pages_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Pages", func() {
	tokensAll := []string{docs.VisTokenAll}

	It("Create, Get", func() {
		Expect(db.Pages.Create(ctx, "")).To(Equal("/"))

		Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("")),
			"GroupBase.SortOrder": PointTo(Equal("")),
		}))

		Expect(db.Pages.Create(ctx, "hello")).To(Equal("/hello/"))

		Expect(db.Pages.Get(ctx, "/hello/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("hello")),
			"GroupBase.SortOrder": PointTo(Equal("hello")),
		}))

		Expect(db.Pages.Create(ctx, "parent/child")).To(Equal("/parent/child/"))

		Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("parent")),
			"GroupBase.SortOrder": PointTo(Equal("parent")),
		}))
		Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("parent/child")),
			"GroupBase.SortOrder": PointTo(Equal("child")),
		}))
	})

	It("Create numerical page", func() {
		Expect(db.Pages.Create(ctx, "10")).To(Equal("/10/"))

		Expect(db.Pages.Get(ctx, "/10/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("10")),
			"GroupBase.SortOrder": PointTo(BeEquivalentTo(10)),
		}))
	})

	Describe("ListChildren", func() {
		It("Ordering of numeric and non-numeric pages", func() {
			_, err := db.Pages.Create(ctx, "key")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Pages.Create(ctx, "101")
			Expect(err).ToNot(HaveOccurred())

			// List from beginning, limit 1
			kws, err := db.Pages.ListChildren(ctx, &apis.GroupListParams{
				ID: "/", ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll},
				Limit: 1,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(kws).To(HaveLen(1))

			Expect(kws[0]).To(MatchGroupBase(Fields{
				"GroupBase.Display": PointTo(Equal("101")),
			}))

			// List relative to first
			kws, err = db.Pages.ListChildren(ctx, &apis.GroupListParams{
				ID: "/", ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll},
				Limit: 1, RefID: "/101/",
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(kws).To(HaveLen(1))

			Expect(kws[0]).To(MatchGroupBase(Fields{
				"GroupBase.Display": PointTo(Equal("key")),
			}))
		})
	})

	Describe("Update", func() {
		It("Display path", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			_, err := db.Pages.Create(ctx, "mystery")
			Expect(err).ToNot(HaveOccurred())

			// Update page
			Expect(db.Pages.Update(ctx, "/mystery/", apis.NewGroupUpdate().SetDisplay("Mystery?!"))).ToNot(HaveOccurred())

			Expect(db.Pages.Get(ctx, "/mystery/", nil, tokensAll)).To(matchDisplayPath("Mystery?!"))

		})
	})

	Describe("SelfVisTokens", func() {
		It("Newly created timeline group", func() {
			Expect(db.Pages.Create(ctx, "/page/")).To(Equal("/page/"))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))

		})

		It("Add and delete", func() {
			Expect(db.Pages.Create(ctx, "/page/")).To(Equal("/page/"))

			// Add
			Expect(db.Pages.AddVisibilityToken(ctx, "/page/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))

			// Delete
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/page/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))
		})

		It("Add and delete recursively", func() {
			Expect(db.Pages.Create(ctx, "/parent/child/")).To(Equal("/parent/child/"))

			// Add to parent, non-recursive
			Expect(db.Pages.AddVisibilityToken(ctx, "/parent/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))
			Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))

			// Delete from parent, recursive
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/parent/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))
			Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))

			// Add to parent, recursive
			Expect(db.Pages.AddVisibilityToken(ctx, "/parent/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))
			Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))

			// Delete from parent, non-recursive
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/parent/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))
			Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))

			// Delete from child, non-recursive
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/parent/child/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/parent/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))
			Expect(db.Pages.Get(ctx, "/parent/child/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
		})
	})

	Describe("RichText", func() {
		It("Update", func() {
			Expect(db.Pages.Create(ctx, "/page/")).To(Equal("/page/"))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": BeNil(),
			}))

			// Update to non-empty text
			Expect(db.Pages.Update(ctx, "/page/", apis.NewGroupUpdate().SetRichText("Hello world"))).ToNot(HaveOccurred())

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": PointTo(Equal("Hello world")),
			}))

			// Update to empty text
			Expect(db.Pages.Update(ctx, "/page/", apis.NewGroupUpdate().SetRichText(""))).ToNot(HaveOccurred())

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": BeNil(),
			}))

		})
	})

	Describe("Delete", func() {
		It("Top level group", func() {
			Expect(db.Pages.Create(ctx, "/page/")).To(Equal("/page/"))
			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1},
			))

			Expect(db.Pages.Delete(ctx, "/page/")).ToNot(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 0},
			))

		})

		It("Child group", func() {
			Expect(db.Pages.Create(ctx, "/page/child/")).To(Equal("/page/child/"))
			Expect(db.Pages.AddVisibilityToken(ctx, "/page/child/", docs.VisTokenPublic, false))

			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1},
			))
			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
			))

			// Fail to delete
			Expect(db.Pages.Delete(ctx, "/page/")).To(HaveOccurred())
			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1},
			))

			// Delete child group
			Expect(db.Pages.Delete(ctx, "/page/child/")).ToNot(HaveOccurred())

			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 0},
			))
			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 0, docs.VisTokenPublic: 0},
			))

			// Delete page group
			Expect(db.Pages.Delete(ctx, "/page/")).ToNot(HaveOccurred())

			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 0, docs.VisTokenPublic: 0},
			))
		})

	})
})
