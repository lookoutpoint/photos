// SPDX-License-Identifier: MIT

package pages

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Pages
type apiImpl struct {
	mdb *mongo.Database
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database) apis.Pages {
	return &apiImpl{mdb: mdb}
}

const collectionName = "pages"

var pageBSONKeys = utils.ExtractBSONKeys(docs.Page{})

// Slugify converts a page to a slug
func Slugify(kw string) string {
	return utils.SlugifyPathLower(utils.SlugifyTokenLower(kw))
}

// Collection returns the pages collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := "/"
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(pageBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the docs for the given page. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, name string) (string, error) {
	opts := &groups.CreateOptions{
		Model: func(base docs.GroupBase) interface{} {
			doc := docs.Page{
				GroupBase: base,
			}
			doc.SelfVisTokens = []string{}
			return doc
		},
	}

	return groups.Create(ctx, api, name, false, opts)
}

// Get returns information about one page id
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Page, error) {
	l := log.WithFields(log.Fields{
		"func": "pages.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(pageBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, options.FindOne().SetProjection(proj))

	var doc docs.Page
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode page document")
		}
		return nil, err
	}

	return &doc, nil
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.Page, error) {
	var results []docs.Page
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	return groups.Update(ctx, api, id, update, nil)
}

func (api *apiImpl) Delete(ctx context.Context, id string) error {
	return groups.DeleteWithSelfVisTokens(ctx, api, id)
}

// AddVisibilityToken to the given page.
func (api *apiImpl) AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error {
	return groups.ModifySelfVisToken(ctx, api, id, token, recurse /*add*/, true)
}

// DeleteVisibilityToken from the given page group.
func (api *apiImpl) DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error {
	return groups.ModifySelfVisToken(ctx, api, id, token, recurse /*add*/, false)
}
