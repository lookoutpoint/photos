// SPDX-License-Identifier: MIT

package database_test

import (
	"context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"go.mongodb.org/mongo-driver/bson"
)

var _ = Describe("Database", func() {
	ctx := context.Background()

	matchIndexName := func(name string) OmegaMatcher {
		return MatchKeys(IgnoreExtras, Keys{"name": Equal(name)})
	}

	BeforeEach(func() {
		clearTestDatabase()
	})

	It("UpdateSchemas", func() {
		// Just check that this can run.
		Expect(db.UpdateSchemas()).ToNot(HaveOccurred())
	})

	It("UpdateIndexes", func() {
		Expect(db.UpdateIndexes()).ToNot(HaveOccurred())

		// Photos
		{
			cursor, err := db.Photos.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("rating_1"),

				matchIndexName("sortKey_1"),
				matchIndexName("folder_1_sortKey_1"),
				matchIndexName("locations_1_sortKey_1"),
				matchIndexName("keywords_1_sortKey_1"),
				matchIndexName("categories_1_sortKey_1"),

				matchIndexName("tsSortKey_1"),
				matchIndexName("folder_1_tsSortKey_1"),
				matchIndexName("locations_1_tsSortKey_1"),
				matchIndexName("keywords_1_tsSortKey_1"),
				matchIndexName("categories_1_tsSortKey_1"),

				matchIndexName("gpsLocation_2dsphere"),
			))
		}

		// Folders
		{
			cursor, err := db.Folders.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
				matchIndexName("descVisTokenCounts.#P_1"),
				matchIndexName("timestamp_1_descVisTokenCounts.#P_1"),
			))
		}

		// Locations
		{
			cursor, err := db.Locations.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
				matchIndexName("descVisTokenCounts.#P_1"),
				matchIndexName("timestamp_1_descVisTokenCounts.#P_1"),
			))
		}

		// Categories
		{
			cursor, err := db.Categories.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
				matchIndexName("related.cat:park_1_depth_1_sortOrder_1__id_1"),
				matchIndexName("related.location_1_depth_1_sortOrder_1__id_1"),
				matchIndexName("descVisTokenCounts.#P_1"),
				matchIndexName("timestamp_1_descVisTokenCounts.#P_1"),
			))
		}

		// Categories - View By Relatd Park
		{
			cursor, err := db.Categories.ViewByRelatedParkCollection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("related.cat:park_1_depth_1_sortOrder_1__id_1"),
				matchIndexName("related.location_1_depth_1_sortOrder_1__id_1"),
			))
		}

		// Keywords
		{
			cursor, err := db.Keywords.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
				matchIndexName("descVisTokenCounts.#P_1"),
				matchIndexName("timestamp_1_descVisTokenCounts.#P_1"),
			))
		}

		// Dates
		{
			cursor, err := db.Dates.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
			))
		}

		// Timeline groups
		{
			cursor, err := db.TimelineGroups.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("depth_1_sortOrder_1__id_1"),
			))
		}

		// Visibility
		{
			cursor, err := db.Visibility.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
				matchIndexName("links.activationValue_1"),
			))
		}

		// Events
		{
			cursor, err := db.Events.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(ConsistOf(
				matchIndexName("_id_"),
			))
		}

		// Check INDEX_VERSION
		res := db.DbPropsCollection().FindOne(ctx, bson.M{"_id": "INDEX_VERSION"})

		var doc map[string]interface{}
		Expect(res.Decode(&doc)).ToNot(HaveOccurred())
		Expect(doc).To(MatchKeys(IgnoreExtras, Keys{
			"version": BeEquivalentTo(18),
		}))

	})

	It("Incremental UpdateIndexes", func() {
		// Drop all indexes on photos and visibility as these are not normally dropped by other tests
		_, err := db.Photos.Collection().Indexes().DropAll(ctx)
		Expect(err).ToNot(HaveOccurred())

		_, err = db.Visibility.Collection().Indexes().DropAll(ctx)
		Expect(err).ToNot(HaveOccurred())

		// ----------------------------------------------------
		// Version 1
		Expect(db.UpdateIndexesTo(1)).ToNot(HaveOccurred())

		// Photos
		{
			cursor, err := db.Photos.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(HaveLen(6))
		}

		// Visibility
		{
			cursor, err := db.Visibility.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(HaveLen(1))
		}

		// Check INDEX_VERSION
		{
			res := db.DbPropsCollection().FindOne(ctx, bson.M{"_id": "INDEX_VERSION"})

			var doc map[string]interface{}
			Expect(res.Decode(&doc)).ToNot(HaveOccurred())
			Expect(doc).To(MatchKeys(IgnoreExtras, Keys{
				"version": BeEquivalentTo(1),
			}))
		}

		// ----------------------------------------------------
		// Version 2
		Expect(db.UpdateIndexesTo(2)).ToNot(HaveOccurred())

		// Photos
		{
			cursor, err := db.Photos.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(HaveLen(6))
		}

		// Visibility
		{
			cursor, err := db.Visibility.Collection().Indexes().List(ctx)
			Expect(err).ToNot(HaveOccurred())

			var idxs []bson.M
			Expect(cursor.All(ctx, &idxs)).ToNot(HaveOccurred())
			Expect(idxs).To(HaveLen(2))
		}

		// Check INDEX_VERSION
		{
			res := db.DbPropsCollection().FindOne(ctx, bson.M{"_id": "INDEX_VERSION"})

			var doc map[string]interface{}
			Expect(res.Decode(&doc)).ToNot(HaveOccurred())
			Expect(doc).To(MatchKeys(IgnoreExtras, Keys{
				"version": BeEquivalentTo(2),
			}))
		}

	})
})
