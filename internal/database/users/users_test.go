// SPDX-License-Identifier: MIT

package users_test

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/users"
)

var _ = Describe("Users", func() {
	BeforeEach(func() {
		clearTestDatabase()
	})

	It("Add users", func() {
		userCount, err := db.Users.GetUserCount(ctx)
		Expect(err).ShouldNot(HaveOccurred())
		Expect(userCount).To(BeNumerically("==", 0))

		err = db.Users.AddUser(ctx, "junior", "secretpassword!")
		Expect(err).ShouldNot(HaveOccurred())

		userCount, err = db.Users.GetUserCount(ctx)
		Expect(err).ShouldNot(HaveOccurred())
		Expect(userCount).To(BeNumerically("==", 1))

		// Should be admin
		Expect(db.Users.IsAdmin(ctx, "junior")).To(BeTrue())
		Expect(db.Users.IsAdmin(ctx, "juniora")).To(BeFalse())

		// Try to add another user (should fail)
		err = db.Users.AddUser(ctx, "senior", "password")
		Expect(err).Should(HaveOccurred())

		userCount, err = db.Users.GetUserCount(ctx)
		Expect(err).ShouldNot(HaveOccurred())
		Expect(userCount).To(BeNumerically("==", 1))

	})

	Describe("Login", func() {
		BeforeEach(func() {
			err := db.Users.AddUser(ctx, "junior", "password!")
			Expect(err).ToNot(HaveOccurred())
		})

		It("Correct password", func() {
			session, err := db.Users.Login(ctx, "junior", "password!")
			Expect(err).ToNot(HaveOccurred())

			session2 := db.Users.GetSession(ctx, session.ID)
			Expect(session2).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"ID":     Equal(session.ID),
				"UserID": Equal(docs.UserID("junior")),
			})))
		})

		It("Incorrect password", func() {
			session, err := db.Users.Login(ctx, "junior", "password")
			Expect(err).To(Equal(apis.ErrUserInvalidPassword))
			Expect(session).To(BeNil())
		})
	})

	Describe("RefreshSession", func() {
		BeforeEach(func() {
			err := db.Users.AddUser(ctx, "junior", "password!")
			Expect(err).ToNot(HaveOccurred())
		})

		It("Refresh valid session", func() {
			session, err := db.Users.Login(ctx, "junior", "password!")
			Expect(err).ToNot(HaveOccurred())
			Expect(db.Users.GetSession(ctx, session.ID)).ToNot(BeNil())

			// Refresh
			Expect(db.Users.RefreshSession(ctx, session.ID)).ToNot(HaveOccurred())
			Expect(db.Users.GetSession(ctx, session.ID)).ToNot(BeNil())

		})

		Describe("Shortened session expiration", func() {
			BeforeEach(func() {
				users.SetSessionDuration(1 * time.Second)
			})

			AfterEach(func() {
				users.SetSessionDuration(0)
			})

			It("Try to refresh expired session", func() {
				session, err := db.Users.Login(ctx, "junior", "password!")
				Expect(err).ToNot(HaveOccurred())
				Expect(db.Users.GetSession(ctx, session.ID)).ToNot(BeNil())

				// Wait for session to expire.
				time.Sleep(2 * time.Second)

				Expect(db.Users.RefreshSession(ctx, session.ID)).To(HaveOccurred())
				Expect(db.Users.GetSession(ctx, session.ID)).To(BeNil())
			})
		})
	})

	It("EndSession", func() {
		err := db.Users.AddUser(ctx, "junior", "password!")
		Expect(err).ToNot(HaveOccurred())

		session, err := db.Users.Login(ctx, "junior", "password!")
		Expect(err).ToNot(HaveOccurred())
		Expect(db.Users.GetSession(ctx, session.ID)).ToNot(BeNil())

		err = db.Users.EndSession(ctx, session.ID)
		Expect(err).ToNot(HaveOccurred())
		Expect(db.Users.GetSession(ctx, session.ID)).To(BeNil())
	})
})
