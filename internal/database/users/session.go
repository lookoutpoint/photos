// SPDX-License-Identifier: MIT

package users

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const sessionCollectionName = "sessions"

func (api *apiImpl) SessionCollection() *mongo.Collection {
	return api.mdb.Collection(sessionCollectionName)
}

// SessionDuration is how long a session before it is considered expired
var SessionDuration = time.Hour * 3

// SetSessionDuration changes the session duration (intended for tests)
func SetSessionDuration(d time.Duration) {
	if d == 0 {
		// Default
		SessionDuration = time.Hour * 3
	} else {
		SessionDuration = d
	}
}

func now() time.Time {
	return time.Now().UTC()
}

func (api *apiImpl) newSession(ctx context.Context, id docs.UserID) (*docs.Session, error) {
	// Session ID is a 24-byte random number
	rawSessionID, err := utils.GenerateSecureRandBase64(24)
	if err != nil {
		return nil, err
	}
	sessionID := docs.SessionID(rawSessionID)

	session := docs.Session{
		ID:     sessionID,
		UserID: id,
		Expiry: now().Add(SessionDuration),
	}
	_, err = api.SessionCollection().InsertOne(ctx, &session)
	if err != nil {
		return nil, err
	}

	return &session, nil
}

// GetSession checks if the given session id is valid and returns the associated Session if it is, otherwise
// returns nil
func (api *apiImpl) GetSession(ctx context.Context, sessionID docs.SessionID) *docs.Session {
	l := log.WithFields(log.Fields{
		"func":      "users.apiImpl.GetSession",
		"sessionID": sessionID,
	})

	// Look for a valid session i.e. has not expired
	res := api.SessionCollection().FindOne(ctx, bson.M{"_id": sessionID, "expiry": bson.M{"$gt": now()}})

	var session docs.Session
	if err := res.Decode(&session); err != nil {
		// If the error is ErrNoDocuments, then this just isn't a valid session.
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not decode session document")
		}
		return nil
	}

	// If a document was found, then the session is found
	return &session
}

// RefreshSession extends the expiry time for the given session
func (api *apiImpl) RefreshSession(ctx context.Context, sessionID docs.SessionID) error {
	l := log.WithFields(log.Fields{
		"func":      "users.apiImpl.RefreshSession",
		"sessionID": sessionID,
	})

	// New expiry time
	newExpiry := now().Add(SessionDuration)

	// Update the session document. Only update non-expired sessions.
	res, err := api.SessionCollection().UpdateOne(ctx,
		bson.M{"_id": sessionID, "expiry": bson.M{"$gt": now()}},
		bson.M{"$set": bson.M{"expiry": newExpiry}},
	)
	if err != nil {
		l.WithError(err).Error("Could not update session document")
		return err
	} else if res.MatchedCount != 1 {
		l.Error("Did not match any session documents")
		return apis.ErrSessionNotFound
	}

	return nil
}

// EndSession deletes the session with the given id
func (api *apiImpl) EndSession(ctx context.Context, sessionID docs.SessionID) error {
	l := log.WithFields(log.Fields{
		"func":      "users.apiImpl.EndSession",
		"sessionID": sessionID,
	})

	_, err := api.SessionCollection().DeleteOne(ctx, bson.M{"_id": sessionID})
	if err != nil {
		l.WithError(err).Error("Could not delete session document")
		return err
	}

	return nil
}

// CleanupExpiredSessions deletes any expired session documents
func (api *apiImpl) CleanupExpiredSessions(ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"func": "users.apiImpl.CleanupExpiredSessions",
	})

	res, err := api.SessionCollection().DeleteMany(ctx, bson.M{"expiry": bson.M{"$lte": now()}})
	if err != nil {
		l.WithError(err).Error("Could not delete expired sessions")
		return err
	}

	l.Infof("Deleted %d sessions", res.DeletedCount)

	return nil
}
