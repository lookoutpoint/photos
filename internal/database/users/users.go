// SPDX-License-Identifier: MIT

package users

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"golang.org/x/crypto/bcrypt"
)

// Implements apis.Users
type apiImpl struct {
	mdb *mongo.Database
}

// NewAPI returns a new apiImpl object for photos
func NewAPI(mdb *mongo.Database) apis.Users {
	return &apiImpl{mdb: mdb}
}

const collectionName = "users"

// Collection returns the user collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// GetUserCount returns the number of users in the collection
func (api *apiImpl) GetUserCount(ctx context.Context) (int64, error) {
	return api.Collection().CountDocuments(ctx, bson.D{})
}

// AddUser adds a new user. There is a current limitation that only one user can be added.
func (api *apiImpl) AddUser(ctx context.Context, id string, password string) error {
	l := log.WithFields(log.Fields{
		"func": "users.apiImpl.AddUser",
	})

	// For now, we only support one user (the admin user), so it's an error if the user count is non-zero
	userCount, err := api.GetUserCount(ctx)
	if err != nil {
		l.WithError(err).Error("Could not get user count")
		return err
	} else if userCount != 0 {
		l.WithField("userCount", userCount).Error("User count is not zero")
		return apis.ErrUserExisting
	}

	// Hash password for storage. This'll also generate a random salt.
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		l.WithError(err).Error("Could not hash password")
		return err
	}

	// Build User document. The newly added user is always Admin role (that's the only role that's currently supported).
	userID := docs.UserID(id)
	hash := string(hashedPassword)
	role := docs.UserRoleAdmin

	user := docs.User{}
	user.ID = &userID
	user.HashedPassword = &hash
	user.Role = &role

	// Add user document
	_, err = api.Collection().InsertOne(ctx, &user)
	if err != nil {
		l.WithError(err).Error("Could not add new user")
		return err
	}

	return nil
}

// Login attempts to make a new login for the user+password combo. Returns a valid session
// if the login is successful, otherwise returns an error.
func (api *apiImpl) Login(ctx context.Context, id string, password string) (*docs.Session, error) {
	l := log.WithFields(log.Fields{
		"func": "users.apiImpl.Login",
		"id":   id,
	})

	// Get user document for the given id.
	res := api.Collection().FindOne(ctx, bson.M{"_id": id}, options.FindOne().SetProjection(bson.M{"hashedPassword": 1}))

	var user docs.User
	if err := res.Decode(&user); err != nil {
		l.WithError(err).Error("Could not find user")
		return nil, err
	}

	if bcrypt.CompareHashAndPassword([]byte(*user.HashedPassword), []byte(password)) != nil {
		return nil, apis.ErrUserInvalidPassword
	}

	// Authentication successful, so create a new session token for the user.
	session, err := api.newSession(ctx, docs.UserID(id))
	if err != nil {
		l.WithError(err).Error("Could not create new session")
		return nil, err
	}

	l.Info("Successfully logged in and created new session")

	return session, nil
}

func (api *apiImpl) IsAdmin(ctx context.Context, id docs.UserID) bool {
	l := log.WithFields(log.Fields{
		"func": "users.apiImpl.IsAdmin",
		"id":   id,
	})

	res := api.Collection().FindOne(ctx,
		bson.M{"_id": id},
		options.FindOne().SetProjection(bson.M{"role": 1}),
	)

	var user docs.User
	if err := res.Decode(&user); err != nil {
		l.WithError(err).Warn("Could not fetch user document")
		return false
	}

	return *user.Role == docs.UserRoleAdmin
}
