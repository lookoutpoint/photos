// SPDX-License-Identifier: MIT

package users_test

import (
	"context"
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

func TestDatabase(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Database/Users Suite")
}

var ctx context.Context
var db *database.Database

const testOutputRootDir = "./.test/output"

var _ = BeforeSuite(func() {
	// Force colors and no timestamps
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: false,
	})

	ctx = context.Background()
	mdb := testutils.NewTestDb()
	db = database.NewWithDb(mdb)

	// Log level (by default only panic-level messages are shown in test mode)
	log.SetLevel(log.PanicLevel)
	level, err := log.ParseLevel(os.Getenv("LOGLEVEL"))
	if err == nil {
		log.SetLevel(level)
	}

	// Set up test output directory
	os.RemoveAll(testOutputRootDir)

	err = os.MkdirAll(testOutputRootDir, os.ModePerm)
	Expect(err).ToNot(HaveOccurred())

})

func clearTestDatabase() {
	testutils.ClearTestDatabase(ctx, db)
}
