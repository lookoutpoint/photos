// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Visibility API
type Visibility interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, name string) (string, error)
	Get(ctx context.Context, id string, projKeys []string) (*docs.VisibilityToken, error)
	GetOrCreate(ctx context.Context, name string, projKeys []string) (*docs.VisibilityToken, error)
	GetByActivationValue(ctx context.Context, actValue string, projKeys []string) (*docs.VisibilityToken, error)
	List(ctx context.Context, projKeys []string) ([]docs.VisibilityToken, error)
	DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, count int) error

	// Initial values only allow setting Redirect and Comment
	CreateLink(ctx context.Context, id string, initial *docs.VisibilityTokenLink) (*docs.VisibilityTokenLink, error)

	// Update only allows changing Redirect, Comment or Disabled
	UpdateLink(ctx context.Context, actValue string, toUpdate *docs.VisibilityTokenLink) error

	// Returns token with ID and the activated link (activation count is not accurate), if the activation value
	// is valid and enabled.
	ActivateLink(ctx context.Context, actValue string) (*docs.VisibilityToken, error)
}

// FilterByVisibilityTokens adds to a filter for the given array of visibility tokens
// There must be at least token in the tokens array.
//
// Assumes the field name is "visTokens".
func FilterByVisibilityTokens(filter bson.M, tokens []string) error {
	numTokens := len(tokens)
	if numTokens == 0 {
		return ErrNoVisibilityTokens
	} else if numTokens == 1 {
		filter["visTokens"] = tokens[0]
	} else {
		filter["visTokens"] = bson.M{"$in": tokens}
	}
	return nil
}
