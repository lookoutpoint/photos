// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Locations API
type Locations interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, loc string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Location, error)
	GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.Location, error)
	GetOrCreate(ctx context.Context, loc string, projKeys []string) (*docs.Location, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.Location, error)
	DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error
	Update(ctx context.Context, id string, update *GroupUpdate) error
}
