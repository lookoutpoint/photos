// SPDX-License-Identifier: MIT

package apis

import (
	"context"
	"errors"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Users API
type Users interface {
	Collection() *mongo.Collection
	SessionCollection() *mongo.Collection
	GetUserCount(ctx context.Context) (int64, error)
	AddUser(ctx context.Context, id string, password string) error
	Login(ctx context.Context, id string, password string) (*docs.Session, error)
	IsAdmin(ctx context.Context, id docs.UserID) bool

	// Sessions
	GetSession(ctx context.Context, sessionID docs.SessionID) *docs.Session
	RefreshSession(ctx context.Context, sessionID docs.SessionID) error
	EndSession(ctx context.Context, sessionID docs.SessionID) error
	CleanupExpiredSessions(ctx context.Context) error
}

// ErrUserExisting means that no new users can be added because at least one user already exists
var ErrUserExisting = errors.New("there is already at least one existing user")

// ErrUserInvalidPassword represents invalid password error
var ErrUserInvalidPassword = errors.New("invalid password")

// ErrSessionNotFound means that there was no session
var ErrSessionNotFound = errors.New("no session found")
