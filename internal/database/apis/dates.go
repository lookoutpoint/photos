// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Dates API
type Dates interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, date string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Date, error)
	GetOrCreate(ctx context.Context, date string, projKeys []string) (*docs.Date, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.Date, error)
	DiffAndUpdate(ctx context.Context, oldID *string, newID *string, oldTokens []string, newTokens []string, count int) error
}
