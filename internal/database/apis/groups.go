// SPDX-License-Identifier: MIT

package apis

import (
	"fmt"

	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
)

// GroupListParams contains parameters for listing group children
type GroupListParams struct {
	Limit          int64            `json:"limit,omitempty"`
	RefID          string           `json:"refId,omitempty"`
	RefOp          list.RefOperator `json:"refOp,omitempty"` // defaults to list.OpGt
	ProjectionKeys []string         `json:"projKeys,omitempty"`
	ID             string           `json:"id,omitempty"`

	// Max relative depth
	// Default is 0, which is the same as 1. Indicates the max level of children
	// to list relative to the depth of ID. A value of -1 means unlimited depth.
	MaxRelDepth int `json:"maxRelDepth,omitempty"`

	// Sort order: ignore depth
	SortIgnoreDepth bool `json:"sortIgnoreDepth,omitempty"`

	// Custom sort key(s) that takes precedence over all other sort orders.
	CustomSortKeys []string `json:"customSortKeys,omitempty"`

	// Name of the custom view to use for the listing. If empty, uses default collection.
	CustomView string `json:"customView,omitempty"`

	// Custom filter. Arbitrary.
	CustomFilter bson.M `json:"customFilter,omitempty"`

	// Visibility tokens. Must have at least one token.
	VisibilityTokens []string `json:"visibilityTokens,omitempty"`
}

// AssignDefaults to parameters
func (params *GroupListParams) AssignDefaults() {
	if len(params.RefOp) == 0 {
		params.RefOp = list.RefOpGt
	}

	if params.MaxRelDepth == 0 {
		params.MaxRelDepth = 1
	}
}

// Validate parameters
func (params *GroupListParams) Validate() error {
	if params.Limit < 0 {
		return fmt.Errorf("invalid limit value %d", params.Limit)
	}

	if len(params.RefID) > 0 && !utils.IsValidSlugPath(params.RefID) {
		return ErrInvalidID
	}

	if err := params.RefOp.Validate(); err != nil {
		return err
	}

	if !utils.IsValidSlugPath(params.ID) {
		return ErrInvalidID
	}

	if params.MaxRelDepth == 0 {
		return fmt.Errorf("MaxRelDepth should not be zero (did not call AssignDefaults?)")
	} else if params.MaxRelDepth < -1 {
		return fmt.Errorf("MaxRelPath is not a valid negative value")
	}

	return nil
}

// GroupUpdate describes the fields that can be updated
type GroupUpdate struct {
	Display         *string      `json:"display,omitempty"`
	SortOrder       *interface{} `json:"sortOrder,omitempty"` // string or number
	MetaDescription *string      `json:"metaDescription,omitempty"`
	RichText        *string      `json:"richText,omitempty"`
}

// NewGroupUpdate creates a new update struct
func NewGroupUpdate() *GroupUpdate {
	return &GroupUpdate{}
}

// SetDisplay sets the display
func (gu *GroupUpdate) SetDisplay(d string) *GroupUpdate {
	gu.Display = &d
	return gu
}

// SetSortOrder sets the sort order
func (gu *GroupUpdate) SetSortOrder(o interface{}) *GroupUpdate {
	gu.SortOrder = &o
	return gu
}

// SetMetaDescription sets the meta description
func (gu *GroupUpdate) SetMetaDescription(d string) *GroupUpdate {
	gu.MetaDescription = &d
	return gu
}

// SetRichtext sets the rich text
func (gu *GroupUpdate) SetRichText(t string) *GroupUpdate {
	gu.RichText = &t
	return gu
}
