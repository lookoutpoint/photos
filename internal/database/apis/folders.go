// SPDX-License-Identifier: MIT

package apis

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Folders API
type Folders interface {
	Collection() *mongo.Collection
	Exists(ctx context.Context, id string) bool
	Create(ctx context.Context, path string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Folder, error)
	GetOrCreate(ctx context.Context, path string, projKeys []string) (*docs.Folder, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.Folder, error)
	DiffAndUpdate(ctx context.Context, oldID *string, newID *string, oldTokens []string, newTokens []string, count int) error
	Update(ctx context.Context, id string, update *GroupUpdate) error

	// Visibility tokens
	AddVisibilityToken(ctx context.Context, id string, target FolderVisibilityTokenTarget, token string) error
	DeleteVisibilityToken(ctx context.Context, id string, target FolderVisibilityTokenTarget, token string) error
}

// ErrFolderNotFound means that a folder could not be found
var ErrFolderNotFound = errors.New("folder not found")

// ErrInvalidFolderPath means invalid path
var ErrInvalidFolderPath = errors.New("invalid folder path")

// FolderVisibilityTokenTarget determines what to target for visibility tokens
type FolderVisibilityTokenTarget int

const (
	// FolderTarget targets just the folder itself
	FolderTarget FolderVisibilityTokenTarget = iota

	// FolderTargetPhotos targets the folder and photos directly in it
	FolderTargetPhotos

	// FolderTargetFullTree targets the folder, photos and sub-folders in its full tree
	FolderTargetFullTree
)

// Validate target
func (target FolderVisibilityTokenTarget) Validate() error {
	switch target {
	case FolderTarget:
	case FolderTargetPhotos:
	case FolderTargetFullTree:
	default:
		return fmt.Errorf("folder target value %v is not valid", target)
	}
	return nil
}
