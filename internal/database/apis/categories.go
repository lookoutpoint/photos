// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Categories API
type Categories interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, catValue string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.CategoryEntry, error)
	GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.CategoryEntry, error)
	GetOrCreate(ctx context.Context, catValue string, projKeys []string) (*docs.CategoryEntry, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.CategoryEntry, error)
	DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error
	Update(ctx context.Context, id string, update *GroupUpdate) error

	UpdateRelated(ctx context.Context, catID string) error
	UpdateCustomViews(ctx context.Context) error

	ViewByRelatedParkCollection() *mongo.Collection
}
