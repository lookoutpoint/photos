// SPDX-License-Identifier: MIT

package apis

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
)

type EventInfo struct {
	Event   string     `json:"event"`
	Object  string     `json:"object,omitempty"`
	Context string     `json:"context,omitempty"`
	Extra   string     `json:"extra,omitempty"`
	Count   int        `json:"count"` // defaults to one if value is zero
	Date    *time.Time `json:"date,omitempty"`
}

type EventGranularity int

const (
	EventByDay EventGranularity = iota
	EventByMonth
	EventByYear
	EventAllTime
)

type EventCountQuery struct {
	StartDate   *time.Time       `json:"startDate,omitempty"`
	EndDate     *time.Time       `json:"endDate,omitempty"`
	Granularity EventGranularity `json:"granularity"`

	Event          *string `json:"event,omitempty"`
	EventRegex     bool    `json:"eventRegex,omitempty"`
	Object         *string `json:"object,omitempty"`
	ObjectRegex    bool    `json:"objectRegex,omitempty"`
	Context        *string `json:"context,omitempty"`
	ContextRegex   bool    `json:"contextRegex,omitempty"`
	Extra          *string `json:"extra,omitempty"`
	ExtraRegex     bool    `json:"extraRegex,omitempty"`
	GroupByEvent   bool    `json:"groupByEvent,omitempty"`
	GroupByObject  bool    `json:"groupByObject,omitempty"`
	GroupByContext bool    `json:"groupByContext,omitempty"`
	GroupByExtra   bool    `json:"groupByExtra,omitempty"`

	Limit int
}

type EventCountResult struct {
	ID struct {
		Date    *time.Time `json:"date,omitempty" bson:"date,omitempty"`
		Event   *string    `json:"event,omitempty" bson:"event,omitempty"`
		Object  *string    `json:"object,omitempty" bson:"object,omitempty"`
		Context *string    `json:"context,omitempty" bson:"context,omitempty"`
		Extra   *string    `json:"extra,omitempty" bson:"extra,omitempty"`
	} `json:"id,omitempty" bson:"_id"`
	Count int `json:"count" bson:"count"`
}

// Events API
type Events interface {
	Collection() *mongo.Collection
	Add(ctx context.Context, events []EventInfo, tokens []string) error
	CountOne(ctx context.Context, event string, object *string, context *string, startDate *time.Time, endDate *time.Time) (int, error)
	Count(ctx context.Context, query EventCountQuery) ([]EventCountResult, error)

	// Schema updates
	ConvertDateToNewFormat(ctx context.Context) error
	ConvertYearMonthDayToDate(ctx context.Context) error
}
