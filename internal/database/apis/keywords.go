// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Keywords API
type Keywords interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, name string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Keyword, error)
	GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.Keyword, error)
	GetOrCreate(ctx context.Context, name string, projKeys []string) (*docs.Keyword, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.Keyword, error)
	DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error
	Update(ctx context.Context, id string, update *GroupUpdate) error
}
