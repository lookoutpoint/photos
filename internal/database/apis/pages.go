// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// Pages API
type Pages interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, name string) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Page, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.Page, error)
	Update(ctx context.Context, id string, update *GroupUpdate) error
	Delete(ctx context.Context, id string) error

	// Visibility tokens
	AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
	DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
}
