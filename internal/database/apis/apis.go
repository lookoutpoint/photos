// SPDX-License-Identifier: MIT

package apis

import (
	"errors"
)

// APIs is the combination of all APIs
type APIs struct {
	Categories     Categories
	Dates          Dates
	Events         Events
	Folders        Folders
	Keywords       Keywords
	Locations      Locations
	Pages          Pages
	Photos         Photos
	TimelineGroups TimelineGroups
	Users          Users
	Visibility     Visibility
}

// ErrInvalidListParams indicates that the list parameters were invalid
var ErrInvalidListParams = errors.New("invalid list params")

// ErrNoVisibilityTokens is an error if there are no visibility tokens provided
var ErrNoVisibilityTokens = errors.New("no visibility tokens provided")

// ErrInvalidID is an error if the provided id is invalid
var ErrInvalidID = errors.New("invalid id")

// ErrInvalidArgs ia generic error for invalid argument values
var ErrInvalidArgs = errors.New("invalid arguments")
