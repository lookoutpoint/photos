// SPDX-License-Identifier: MIT

package apis

import (
	"context"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/mongo"
)

// TimelineGroups API
type TimelineGroups interface {
	Collection() *mongo.Collection
	Create(ctx context.Context, folderID string, name string, filter *docs.PhotoListFilterExpr) (string, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.TimelineGroup, error)
	ListChildren(ctx context.Context, params *GroupListParams) ([]docs.TimelineGroup, error)
	Update(ctx context.Context, id string, update *GroupUpdate) error
	UpdateFilter(ctx context.Context, id string, filter *docs.PhotoListFilterExpr) error
	Sync(ctx context.Context, id string) error
	Delete(ctx context.Context, id string) error

	// Visibility tokens
	AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
	DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error

	// Utilities
	UnwrapFilter(filter *docs.PhotoListFilterExpr) *docs.PhotoListFilterExpr
}
