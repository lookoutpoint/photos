// SPDX-License-Identifier: MIT

package apis

import (
	"context"
	"errors"
	"fmt"
	"io"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"go.mongodb.org/mongo-driver/mongo"
)

// Photos API
type Photos interface {
	Collection() *mongo.Collection
	AddOrUpdate(ctx context.Context, bucketID string, objectID string, objectGenVal uint64, fileSize int, md *photomd.Metadata) (*docs.Photo, error)
	AddOrUpdateAndResize(ctx context.Context, bucketID string, objectID string, objectGenVal uint64) (*docs.Photo, error)
	Delete(ctx context.Context, bucketID string, objectID string) error
	Get(ctx context.Context, id docs.PhotoID, projKeys []string, tokens []string) (*docs.Photo, error)

	// Photo resizing
	ResizeCacheCollection() *mongo.Collection
	GetResizeCacheEntry(ctx context.Context, id docs.PhotoID, maxLength int) *docs.PhotoResizeCacheEntry
	GetResizedImage(ctx context.Context, id docs.PhotoID, maxLength int, tokens []string) (io.ReadCloser, int, bool, error)

	// Listing
	List(ctx context.Context, params *PhotoListParams) ([]docs.Photo, error)
	RandomList(ctx context.Context, params *PhotoRandomListParams) ([]docs.Photo, error)

	// Visibility tokens
	AddInheritedVisibilityToken(ctx context.Context, folderSlugPath string, fullTree bool, token string) error
	DeleteInheritedVisibilityToken(ctx context.Context, folderSlugPath string, fullTree bool, token string) error
}

// ErrPhotoInvalidResizeMaxLength signifies an invalid resize max length
var ErrPhotoInvalidResizeMaxLength = errors.New("invalid resize max length")

// PhotoListParams contains parameters for listing photos
type PhotoListParams struct {
	Limit int64 `json:"limit,omitempty"` // >= 0 (0 means no limit)

	// RefKeyValue expectations for different RefKeys
	// - ListKeySortKey: RefKeyValue should be a string with the sort key
	// - ListKeyTimestamp: RefKeyValue should be a string with the timestamp value (in RFC3339 layout)
	RefKey      PhotoListKey     `json:"refKey,omitempty"` // defaults to ListKeySortKey
	RefKeyValue string           `json:"refKeyValue,omitempty"`
	RefOp       list.RefOperator `json:"refOp,omitempty"` // defaults to list.OpGt

	// Keys to return in photo document.
	ProjectionKeys []string `json:"projKeys,omitempty"`
	// Keys that must exist in photo document.
	ExistKeys []string `json:"existKeys,omitempty"`
	// Photo filter.
	Filter docs.PhotoListFilterExpr `json:"filter"`

	// Visibility tokens. Must have at least one token.
	VisibilityTokens []string `json:"visTokens"`
}

// AssignDefaults to list parameters
func (params *PhotoListParams) AssignDefaults() {
	if len(params.RefKey) == 0 {
		params.RefKey = PhotoListKeySortKey
	}

	if len(params.RefOp) == 0 {
		params.RefOp = list.RefOpGt
	}

	params.Filter.AssignDefaults()
}

// Validate list parameters
func (params *PhotoListParams) Validate() error {
	if params.Limit < 0 {
		return fmt.Errorf("invalid limit value %d", params.Limit)
	}

	if err := params.RefKey.Validate(); err != nil {
		return err
	}

	if err := params.RefOp.Validate(); err != nil {
		return err
	}

	return params.Filter.Validate()
}

// PhotoListKey is an enum for the listing key
type PhotoListKey string

// List keys
const (
	PhotoListKeySortKey   PhotoListKey = "sortKey"
	PhotoListKeyTimestamp PhotoListKey = "timestamp"
)

// Validate photo list key value
func (key PhotoListKey) Validate() error {
	if key == PhotoListKeySortKey || key == PhotoListKeyTimestamp {
		return nil
	}
	return fmt.Errorf("invalid photo list key '%s'", key)
}

// PhotoRandomListParams contains parameters for random list photos
type PhotoRandomListParams struct {
	Count int64 `json:"count"` // > 0

	// Keys to return in photo document.
	ProjectionKeys []string `json:"projKeys,omitempty"`
	// Keys that must exist in photo document.
	ExistKeys []string `json:"existKeys,omitempty"`
	// Photo filter.
	Filter docs.PhotoListFilterExpr `json:"filter"`

	// Visibility tokens. Must have at least one token.
	VisibilityTokens []string `json:"visTokens"`
}

// AssignDefaults to list parameters
func (params *PhotoRandomListParams) AssignDefaults() {
	params.Filter.AssignDefaults()
}

// Validate list parameters
func (params *PhotoRandomListParams) Validate() error {
	if params.Count <= 0 {
		return fmt.Errorf("invalid limit value %d", params.Count)
	}

	return params.Filter.Validate()
}
