// SPDX-License-Identifier: MIT

package dates_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Dates", func() {
	tokensAll := []string{docs.VisTokenAll}

	matchPhotoCount := func(count int, descCount int, token ...string) OmegaMatcher {
		if len(token) > 1 {
			panic("At most one token")
		}

		counts := VisTokenCounts{docs.VisTokenAll: count + 1}
		descCounts := VisTokenCounts{docs.VisTokenAll: descCount}

		if len(token) > 0 {
			counts[token[0]] = count
			descCounts[token[0]] = descCount
		}

		return MatchGroupBasePtrVisTokenCounts(counts, descCounts)
	}

	createDate := func(date string) {
		_, err := db.Dates.Create(ctx, date)
		Expect(err).ToNot(HaveOccurred())
	}

	It("Create, Get, GetOrCreate", func() {
		Expect(db.Dates.Create(ctx, "20200613")).To(Equal("/2020/06/13/"))

		Expect(db.Dates.Get(ctx, "/2020/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display": PointTo(Equal("2020")),
		}))
		Expect(db.Dates.Get(ctx, "/2020/06/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display": PointTo(Equal("2020/06")),
		}))
		Expect(db.Dates.Get(ctx, "/2020/06/13/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display": PointTo(Equal("2020/06/13")),
		}))

		Expect(db.Dates.GetOrCreate(ctx, "20200613", nil)).ToNot(BeNil())

	})

	It("DiffAndUpdate", func() {
		// Simulate adding date
		createDate("20200315")

		date := "/2020/03/15/"
		err := db.Dates.DiffAndUpdate(ctx, nil, &date, nil, tokensAll, 1)
		Expect(err).ToNot(HaveOccurred())

		Expect(db.Dates.Get(ctx, "/2020/", nil, tokensAll)).To(And(
			matchPhotoCount(0, 3),
			MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(1))}),
		))
		Expect(db.Dates.Get(ctx, "/2020/03/", nil, tokensAll)).To(And(
			matchPhotoCount(0, 2),
			MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(2))}),
		))
		Expect(db.Dates.Get(ctx, "/2020/03/15/", nil, tokensAll)).To(And(
			matchPhotoCount(1, 0),
			MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(3))}),
		))

		// Simulate changing dates
		createDate("20201009")

		oldDate := date
		date = "/2020/10/09/"
		err = db.Dates.DiffAndUpdate(ctx, &oldDate, &date, tokensAll, tokensAll, 1)
		Expect(err).ToNot(HaveOccurred())

		Expect(db.Dates.Get(ctx, "/2020/", nil, tokensAll)).To(matchPhotoCount(0, 5))
		Expect(db.Dates.Get(ctx, "/2020/03/", nil, tokensAll)).To(matchPhotoCount(0, 1))
		Expect(db.Dates.Get(ctx, "/2020/03/15/", nil, tokensAll)).To(matchPhotoCount(0, 0))
		Expect(db.Dates.Get(ctx, "/2020/10/", nil, tokensAll)).To(matchPhotoCount(0, 2))
		Expect(db.Dates.Get(ctx, "/2020/10/09/", nil, tokensAll)).To(matchPhotoCount(1, 0))

		// Simulate deleting date
		oldDate = date
		err = db.Dates.DiffAndUpdate(ctx, &oldDate, nil, tokensAll, nil, 1)
		Expect(err).ToNot(HaveOccurred())

		Expect(db.Dates.Get(ctx, "/2020/", nil, tokensAll)).To(matchPhotoCount(0, 4))
		Expect(db.Dates.Get(ctx, "/2020/03/", nil, tokensAll)).To(matchPhotoCount(0, 1))
		Expect(db.Dates.Get(ctx, "/2020/03/15/", nil, tokensAll)).To(matchPhotoCount(0, 0))
		Expect(db.Dates.Get(ctx, "/2020/10/", nil, tokensAll)).To(matchPhotoCount(0, 1))
		Expect(db.Dates.Get(ctx, "/2020/10/09/", nil, tokensAll)).To(matchPhotoCount(0, 0))

	})

})
