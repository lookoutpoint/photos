// SPDX-License-Identifier: MIT

package dates

import (
	"context"
	"regexp"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Dates
type apiImpl struct {
	mdb *mongo.Database
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database) apis.Dates {
	return &apiImpl{mdb: mdb}
}

const collectionName = "photos-dates"

var dateBSONKeys = utils.ExtractBSONKeys(docs.Date{})

var dateStringRegex = regexp.MustCompile("^\\d{8}$")

func isValidDate(s string) bool {
	return dateStringRegex.MatchString(s)
}

// SlugifyDate converts a date string into a slug path
func SlugifyDate(date string) string {
	return utils.SlugifyPath(date[:4], date[4:6], date[6:])
}

// Collection returns the folder collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := "/"
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(dateBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the docs for the given date string. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, date string) (string, error) {
	l := log.WithFields(log.Fields{
		"func": "dates.apiImpl.Create",
		"date": date,
	})

	if !isValidDate(date) {
		l.Error("Invalid date")
		return "", apis.ErrInvalidArgs
	}

	// Display is the same as the slug but without the prefix and trailing slash
	display := SlugifyDate(date)
	display = display[1 : len(display)-1]

	return groups.Create(ctx, api, display, false, nil)
}

// Get returns information about one date id
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Date, error) {
	l := log.WithFields(log.Fields{
		"func": "dates.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(dateBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, &options.FindOneOptions{Projection: proj})

	var doc docs.Date
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode date document")
		}
		return nil, err
	}

	return &doc, nil
}

func (api *apiImpl) GetOrCreate(ctx context.Context, date string, projKeys []string) (*docs.Date, error) {
	l := log.WithFields(log.Fields{
		"func": "dates.apiImpl.GetOrCreate",
		"date": date,
	})

	if !isValidDate(date) {
		l.Error("Invalid date")
		return nil, apis.ErrInvalidArgs
	}

	id := SlugifyDate(date)
	tokensAll := []string{docs.VisTokenAll}

	// Try get
	if d, err := api.Get(ctx, id, projKeys, tokensAll); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing date")
		}
		return d, err
	}

	// Create
	if _, err := api.Create(ctx, date); err != nil {
		l.WithError(err).Error("Could not create date")
		return nil, err
	}

	return api.Get(ctx, id, projKeys, tokensAll)
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.Date, error) {
	var results []docs.Date
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) DiffAndUpdate(ctx context.Context, oldID *string, newID *string, oldTokens []string, newTokens []string, count int) error {
	var oldIDs []string
	var newIDs []string

	if oldID != nil {
		oldIDs = []string{*oldID}
	}

	if newID != nil {
		newIDs = []string{*newID}
	}

	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)
}
