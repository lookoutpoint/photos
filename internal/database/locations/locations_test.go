// SPDX-License-Identifier: MIT

package locations_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Locations", func() {
	tokensAll := []string{docs.VisTokenAll}

	Describe("Create/Get", func() {
		It("Create, Get, GetOrCreate", func() {
			Expect(db.Locations.Create(ctx, "City")).To(Equal("/city/"))
			Expect(db.Locations.Create(ctx, "City, !park: A Park")).To(Equal("/city/a-park/"))

			Expect(db.Locations.Get(ctx, "/city/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display":   PointTo(Equal("City")),
				"Type":                PointTo(BeEquivalentTo(docs.LocationOther)),
				"GroupBase.SortOrder": PointTo(Equal("city")),
			}))
			Expect(db.Locations.Get(ctx, "/city/a-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display":   PointTo(Equal("City,!park:A Park")),
				"Type":                PointTo(BeEquivalentTo(docs.LocationPark)),
				"GroupBase.SortOrder": PointTo(Equal("a park")),
			}))

			Expect(db.Locations.GetOrCreate(ctx, "City,!park:A Park", nil)).ToNot(BeNil())

		})

		It("Create with prefixes", func() {
			Expect(db.Locations.Create(ctx, "abc, !park: x park")).To(Equal("/abc/x-park/"))
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))

			Expect(db.Locations.Create(ctx, "abc, !hike: y trail")).To(Equal("/abc/y-trail/"))
			Expect(db.Locations.Get(ctx, "/abc/y-trail/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!hike:y trail")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationHike)),
				"Park":              BeNil(),
			}))

			Expect(db.Locations.Create(ctx, "abc, !park: z park, !hike:t trail")).To(Equal("/abc/z-park/t-trail/"))
			Expect(db.Locations.Get(ctx, "/abc/z-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:z park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))

			Expect(db.Locations.Get(ctx, "/abc/z-park/t-trail/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:z park,!hike:t trail")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationHike)),
				"Park":              PointTo(Equal("/abc/z-park/")),
				"ParkName":          PointTo(Equal("z park")),
			}))

		})

		It("GetOrCreate(get) change location type (deepest location)", func() {
			// Initial: no park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, x park", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              BeNil(),
			}))

			// Next: with park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, !park: x park", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))
		})

		It("GetOrCreate(get) change location type (ancestor location)", func() {
			// Initial: no park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, x park, blah", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              BeNil(),
			}))

			// Next: with park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, !park: x park, blah", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))
		})

		It("GetOrCreate(create) change location type (ancestor location)", func() {
			// Initial: no park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, x park", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              BeNil(),
			}))

			// Next: with park type
			Expect(db.Locations.GetOrCreate(ctx, "abc, !park: x park, trail", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))
		})

		It("GetOrCreate(create) change location type (multiple ancestor locations)", func() {
			// Initial: no park/hike type
			Expect(db.Locations.GetOrCreate(ctx, "abc, x park, trail", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,x park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              BeNil(),
			}))
			Expect(db.Locations.Get(ctx, "/abc/x-park/trail/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,x park,trail")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              BeNil(),
			}))

			// Next: with park and hike type
			Expect(db.Locations.GetOrCreate(ctx, "abc, !park: X park, !hike: trail, waterfall", nil)).ToNot(BeNil())
			Expect(db.Locations.Get(ctx, "/abc/x-park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:X park")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":              BeNil(),
			}))
			Expect(db.Locations.Get(ctx, "/abc/x-park/trail/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:X park,!hike:trail")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationHike)),
				"Park":              PointTo(Equal("/abc/x-park/")),
				"ParkName":          PointTo(Equal("x park")),
			}))
			Expect(db.Locations.Get(ctx, "/abc/x-park/trail/waterfall/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("abc,!park:X park,!hike:trail,waterfall")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              PointTo(Equal("/abc/x-park/")),
				"ParkName":          PointTo(Equal("x park")),
			}))
		})
	})

	Describe("DiffAndUpdate", func() {
		matchPhotoCount := func(count int, descCount int, token ...string) OmegaMatcher {
			if len(token) > 1 {
				panic("At most one token")
			}

			counts := VisTokenCounts{docs.VisTokenAll: count + 1}
			descCounts := VisTokenCounts{docs.VisTokenAll: descCount}

			if len(token) > 0 {
				counts[token[0]] = count
				descCounts[token[0]] = descCount
			}

			return MatchGroupBasePtrVisTokenCounts(counts, descCounts)
		}

		createLoc := func(loc string) {
			_, err := db.Locations.Create(ctx, loc)
			Expect(err).ToNot(HaveOccurred())
		}

		It("Single level location", func() {
			// Simulate adding location
			createLoc("place")

			loc := "/place/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/place/", nil, tokensAll)).To(And(
				matchPhotoCount(1, 0),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(1))}),
			))

			// Simulate changing location and token
			createLoc("xyz")
			T := "/T/"

			oldLoc := loc
			loc = "/xyz/"
			err = db.Locations.DiffAndUpdate(ctx, []string{oldLoc}, []string{loc}, tokensAll, []string{docs.VisTokenAll, T}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/place/", nil, tokensAll)).To(matchPhotoCount(0, 0))
			Expect(db.Locations.Get(ctx, "/xyz/", nil, tokensAll)).To(matchPhotoCount(1, 0, T))

			// Simulate deleting location
			oldLoc = loc
			err = db.Locations.DiffAndUpdate(ctx, []string{oldLoc}, nil, []string{docs.VisTokenAll, T}, nil, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/place/", nil, tokensAll)).To(matchPhotoCount(0, 0))
			Expect(db.Locations.Get(ctx, "/xyz/", nil, tokensAll)).To(matchPhotoCount(0, 0))

		})

		It("Multi level location", func() {
			// Simulate adding location
			createLoc("country,state,city,park")

			loc := "/country/state/city/park/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 4),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(1))}),
			))
			Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 3),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(2))}),
			))
			Expect(db.Locations.Get(ctx, "/country/state/city/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 2),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(3))}),
			))
			Expect(db.Locations.Get(ctx, "/country/state/city/park/", nil, tokensAll)).To(And(
				matchPhotoCount(1, 0),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(4))}),
			))

			// Simulate changing location
			createLoc("country,stateb,city,park")

			oldLoc := loc
			loc = "/country/stateb/city/park/"
			err = db.Locations.DiffAndUpdate(ctx, []string{oldLoc}, []string{loc}, tokensAll, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCount(0, 7))
			Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/state/city/", nil, tokensAll)).To(matchPhotoCount(0, 1))
			Expect(db.Locations.Get(ctx, "/country/state/city/park/", nil, tokensAll)).To(matchPhotoCount(0, 0))
			Expect(db.Locations.Get(ctx, "/country/stateb/", nil, tokensAll)).To(matchPhotoCount(0, 3))
			Expect(db.Locations.Get(ctx, "/country/stateb/city/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/stateb/city/park/", nil, tokensAll)).To(matchPhotoCount(1, 0))

			// Simulate deleting location
			oldLoc = loc
			err = db.Locations.DiffAndUpdate(ctx, []string{oldLoc}, nil, tokensAll, nil, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCount(0, 6))
			Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/state/city/", nil, tokensAll)).To(matchPhotoCount(0, 1))
			Expect(db.Locations.Get(ctx, "/country/state/city/park/", nil, tokensAll)).To(matchPhotoCount(0, 0))
			Expect(db.Locations.Get(ctx, "/country/stateb/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/stateb/city/", nil, tokensAll)).To(matchPhotoCount(0, 1))
			Expect(db.Locations.Get(ctx, "/country/stateb/city/park/", nil, tokensAll)).To(matchPhotoCount(0, 0))

		})

		It("Independent partial location sequence overlap", func() {
			// Simulate adding location
			createLoc("country,state,asdf,park")

			loc := "/country/state/asdf/park/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCount(0, 4))
			Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCount(0, 3))
			Expect(db.Locations.Get(ctx, "/country/state/asdf/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/state/asdf/park/", nil, tokensAll)).To(matchPhotoCount(1, 0))

			// Simulate adding another location in the same country/state
			createLoc("country,state,blah,park")

			loc = "/country/state/blah/park/"
			err = db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCount(0, 7))
			Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCount(0, 6))
			Expect(db.Locations.Get(ctx, "/country/state/asdf/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/state/asdf/park/", nil, tokensAll)).To(matchPhotoCount(1, 0))
			Expect(db.Locations.Get(ctx, "/country/state/blah/", nil, tokensAll)).To(matchPhotoCount(0, 2))
			Expect(db.Locations.Get(ctx, "/country/state/blah/park/", nil, tokensAll)).To(matchPhotoCount(1, 0))

		})

		It("Special display name: single location", func() {
			createLoc("Canada")

			loc := "/canada/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(And(
				matchPhotoCount(1, 0),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(1)),
					"GroupBase.Display": PointTo(Equal("Canada")),
				}),
			))

		})

		It("Special display name: single location with '/'", func() {
			createLoc("Canada/Kanada")

			loc := "/canada-kanada/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/canada-kanada/", nil, tokensAll)).To(And(
				matchPhotoCount(1, 0),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(1)),
					"GroupBase.Display": PointTo(Equal("Canada/Kanada")),
				}),
			))

		})

		It("Special display name: multi-level location with custom ancestor display name", func() {
			createLoc("Canada,Alberta,Lake Louise,Big Beehive")

			loc := "/canada/alberta/lake-louise/big-beehive/"
			err := db.Locations.DiffAndUpdate(ctx, nil, []string{loc}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 4),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(1)),
					"GroupBase.Display": PointTo(Equal("Canada")),
				}),
			))
			Expect(db.Locations.Get(ctx, "/canada/alberta/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 3),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(2)),
					"GroupBase.Display": PointTo(Equal("Canada,Alberta")),
				}),
			))
			Expect(db.Locations.Get(ctx, "/canada/alberta/lake-louise/", nil, tokensAll)).To(And(
				matchPhotoCount(0, 2),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(3)),
					"GroupBase.Display": PointTo(Equal("Canada,Alberta,Lake Louise")),
				}),
			))
			Expect(db.Locations.Get(ctx, "/canada/alberta/lake-louise/big-beehive/", nil, tokensAll)).To(And(
				matchPhotoCount(1, 0),
				MatchGroupBasePtr(Fields{
					"GroupBase.Depth":   PointTo(Equal(4)),
					"GroupBase.Display": PointTo(Equal("Canada,Alberta,Lake Louise,Big Beehive")),
				}),
			))

		})

	})

	Describe("Update", func() {
		It("Display path", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			_, err := db.Locations.Create(ctx, "country,city")
			Expect(err).ToNot(HaveOccurred())

			// Update country
			Expect(db.Locations.Update(ctx, "/country/", apis.NewGroupUpdate().SetDisplay("Country'x"))).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchDisplayPath("Country'x"))
			Expect(db.Locations.Get(ctx, "/country/city/", nil, tokensAll)).To(matchDisplayPath("Country'x,city"))

			// Update city
			Expect(db.Locations.Update(ctx, "/country/city/", apis.NewGroupUpdate().SetDisplay("'City'"))).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchDisplayPath("Country'x"))
			Expect(db.Locations.Get(ctx, "/country/city/", nil, tokensAll)).To(matchDisplayPath("Country'x,'City'"))

		})

		It("Display path with location prefixes", func() {
			match := func(dp string, ty docs.LocationType) OmegaMatcher {
				return MatchGroupBasePtr(Fields{
					"GroupBase.Display": PointTo(Equal(dp)),
					"Type":              PointTo(BeEquivalentTo(ty)),
				})
			}

			_, err := db.Locations.Create(ctx, "Park")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Get(ctx, "/park/", nil, tokensAll)).To(match("Park", docs.LocationOther))

			Expect(db.Locations.Update(ctx, "/park/", apis.NewGroupUpdate().SetDisplay(" !park: PARK"))).ToNot(HaveOccurred())
			Expect(db.Locations.Get(ctx, "/park/", nil, tokensAll)).To(match("!park:PARK", docs.LocationPark))

			Expect(db.Locations.Update(ctx, "/park/", apis.NewGroupUpdate().SetDisplay(" !hike: HIKE"))).ToNot(HaveOccurred())
			Expect(db.Locations.Get(ctx, "/park/", nil, tokensAll)).To(match("!hike:HIKE", docs.LocationHike))

			Expect(db.Locations.Update(ctx, "/park/", apis.NewGroupUpdate().SetDisplay(" Some Park"))).ToNot(HaveOccurred())
			Expect(db.Locations.Get(ctx, "/park/", nil, tokensAll)).To(match("Some Park", docs.LocationOther))

		})

		It("Change location type to park", func() {
			_, err := db.Locations.Create(ctx, "Country, Park, Playground")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Locations.Update(ctx, "/country/park/", apis.NewGroupUpdate().SetDisplay("!park: Fancy Park"))).ToNot(HaveOccurred())
			Expect(db.Locations.Get(ctx, "/country/park/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Type":     PointTo(BeEquivalentTo(docs.LocationPark)),
				"Park":     BeNil(),
				"ParkName": BeNil(),
			}))
			Expect(db.Locations.Get(ctx, "/country/park/playground/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("Country,!park:Fancy Park,Playground")),
				"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
				"Park":              PointTo(Equal("/country/park/")),
				"ParkName":          PointTo(Equal("fancy park")),
			}))
		})
	})

})
