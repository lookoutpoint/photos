// SPDX-License-Identifier: MIT

package locations

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func AddParkField(ctx context.Context, api apis.Locations) error {
	l := log.WithField("func", "locations.AddParkField")

	bulkModels := utils.NewBulkWriteModels(ctx, api.Collection(), l)

	cursor, err := api.Collection().Find(ctx, bson.M{}, options.Find().SetProjection(bson.M{"_id": 1, "display": 1}))
	if err != nil {
		l.WithError(err).Error("Could not query locations")
		return err
	}

	for cursor.Next(ctx) {
		var loc docs.Location
		if err := cursor.Decode(&loc); err != nil {
			l.WithError(err).Error("Could not decode location")
			return err
		}

		// Get parent park id.
		parentParkInfo := getParentParkInfo(*loc.Display, *loc.ID)
		if parentParkInfo != nil {
			bulkModels.Add(mongo.NewUpdateOneModel().
				SetFilter(bson.M{"_id": *loc.ID}).
				SetUpdate(
					bson.M{
						"$set": bson.M{
							"park":     parentParkInfo.ID,
							"parkName": parentParkInfo.Name,
						},
					},
				))
		}
	}
	if err := cursor.Err(); err != nil {
		l.WithError(err).Error("Could not iterate over cursor")
		return err
	}

	if err := bulkModels.Write(); err != nil {
		l.WithError(err).Error("Could not finish bulk write")
		return err
	}

	return nil

}
