// SPDX-License-Identifier: MIT

package locations

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Locations
type apiImpl struct {
	mdb *mongo.Database
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database) apis.Locations {
	return &apiImpl{mdb: mdb}
}

const collectionName = "photos-locations"
const separator = ","

// Location strings can have prefixes for each part to indicate special properties.
// Examples:
// !park:Park, Country creates a location with id /country/park/ and display Country, Park of type PARK.
//
// These are also part of the display name.
const parkPrefix = "!park:"
const hikePrefix = "!hike:"

var locationBSONKeys = utils.ExtractBSONKeys(docs.Location{})

func isValidLocation(loc string) bool {
	return len(loc) > 0 && strings.Index(loc, ",,") == -1
}

// For a single location path component, strips any prefxies and returns the location type from that prefix.
func stripPrefixAndReturnLocationType(loc string) (string, docs.LocationType) {
	loc = strings.TrimSpace(loc)
	origLen := len(loc)

	loc = strings.TrimPrefix(loc, parkPrefix)
	if len(loc) != origLen {
		return loc, docs.LocationPark
	}

	loc = strings.TrimPrefix(loc, hikePrefix)
	if len(loc) != origLen {
		return loc, docs.LocationHike
	}

	return loc, docs.LocationOther
}

// Applies the location prefix to the given location path component.
func applyLocationTypePrefix(loc string, locType docs.LocationType) string {
	loc = strings.TrimSpace(loc)
	switch locType {
	case docs.LocationPark:
		return parkPrefix + loc
	case docs.LocationHike:
		return hikePrefix + loc
	}
	return loc
}

// For a location path, strips any prefixes and returns the location type for the path, which is based
// on the location path of the most-specific (i.e. last) component.
func stripPrefixesAndReturnLocationType(path []string) ([]string, docs.LocationType) {
	newPath := make([]string, len(path))

	locType := docs.LocationOther
	for i := range path {
		var t docs.LocationType
		newPath[i], t = stripPrefixAndReturnLocationType(path[i])

		if i == len(path)-1 {
			locType = t
		}
	}

	return newPath, locType
}

// Canonicalize the given location display string. Primarily to canonicalize prefixes so that they always appear first (without any spaces)
// in the display string part.
func canonicalizeDisplay(loc string) (string, docs.LocationType) {
	retType := docs.LocationOther

	displayParts := strings.Split(loc, separator)
	for i, part := range displayParts {
		var locType docs.LocationType
		part, locType = stripPrefixAndReturnLocationType(part)
		displayParts[i] = applyLocationTypePrefix(part, locType)

		// The last display string component determines the type for the full location.
		if i == len(displayParts)-1 {
			retType = locType
		}
	}

	return strings.Join(displayParts, separator), retType
}

// Get parent park location id, if any. A park location is not a parent of itself.
type ParkInfo struct {
	ID   string
	Name string
}

func getParentParkInfo(display string, id string) *ParkInfo {
	displayParts := strings.Split(display, separator)
	displayParts = displayParts[:len(displayParts)-1]
	for i, part := range displayParts {
		displayPart, locType := stripPrefixAndReturnLocationType(part)
		if locType == docs.LocationPark {
			var info ParkInfo
			idParts := strings.Split(id, "/")
			info.ID = strings.Join(idParts[:i+2], "/") + "/"
			// The Name field is intended for sorting, so make it case-insensitive
			info.Name = strings.ToLower(displayPart)
			return &info
		}
	}

	return nil
}

func SlugifyPath(path ...string) string {
	path, _ = stripPrefixesAndReturnLocationType(path)
	return utils.SlugifyPathLower(path...)
}

// SlugifyPathString converts a comma-separated path string into a path slug.
// Prefixes are stripped from the id.
func SlugifyPathString(loc string) string {
	return SlugifyPath(strings.Split(loc, separator)...)
}

// Collection returns the folder collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := separator
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(locationBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	// Strip location prefixes from sort order.
	display, _ = stripPrefixAndReturnLocationType(display)
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the docs for the given location string. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, loc string) (string, error) {
	l := log.WithFields(log.Fields{
		"func": "locations.apiImpl.Create",
		"loc":  loc,
	})

	if !isValidLocation(loc) {
		l.Error("Invalid location")
		return "", apis.ErrInvalidArgs
	}

	return groups.Create(ctx, api, loc, false, &groups.CreateOptions{LowerCase: true,
		SlugifyPath: func(displayParts ...string) string {
			// Strips any prefixes.
			return SlugifyPath(displayParts...)
		},
		Model: func(base docs.GroupBase) interface{} {
			var doc docs.Location
			doc.GroupBase = base

			// Canonicalize the display name and also extract the location type.
			newDisplay, locType := canonicalizeDisplay(*base.Display)
			doc.Display = &newDisplay
			doc.Type = &locType

			parkInfo := getParentParkInfo(newDisplay, *base.ID)
			if parkInfo != nil {
				doc.Park = &parkInfo.ID
				doc.ParkName = &parkInfo.Name
			}

			return doc
		},
		ExistingParent: func(ctx context.Context, id string) error {
			l.Tracef("Checking existing parent %+v", id)
			idParts := strings.Split(id, "/")
			if len(idParts) <= 2 {
				// Don't care about root.
				return nil
			}

			// The location string may contain updates for parent location types.
			canonicalDisplay, _ := canonicalizeDisplay(loc)
			canonicalDisplayParts := strings.Split(canonicalDisplay, separator)

			// Get existing group.
			loc, err := api.Get(ctx, id, []string{"display"}, []string{docs.VisTokenAll})
			if err != nil {
				l.WithError(err).Error("Could not get parent location doc")
				return err
			}

			curDisplayParts := strings.Split(*loc.Display, separator)

			if len(canonicalDisplayParts) < len(curDisplayParts) {
				err := fmt.Errorf("Mismatched display part lengths")
				l.WithError(err).Error("Could not update ancestor display name")
				return err
			}
			if len(idParts)-2 != len(curDisplayParts) {
				err := fmt.Errorf("Mismatched id length")
				l.WithError(err).Error("Could not update ancestor display name")
				return err
			}

			for i := range curDisplayParts {
				if curDisplayParts[i] != canonicalDisplayParts[i] {
					// This location has a different display name.
					localId := strings.Join(idParts[:i+2], "/") + "/"
					if err := api.Update(ctx, localId, &apis.GroupUpdate{Display: &canonicalDisplayParts[i]}); err != nil {
						l.WithError(err).WithField("id", localId).Error("Could not update ancestor display name")
						return err
					}
					l.WithField("id", localId).Info("Updated ancestor display name")

					tokensAll := []string{docs.VisTokenAll}
					t, _ := api.Get(ctx, id, nil, tokensAll)
					l.Infof("POST-UPDATE id=%v doc=%+v", id, t)
				}
			}

			return nil
		},
	})

}

// Get returns information about one location id
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Location, error) {
	l := log.WithFields(log.Fields{
		"func": "locations.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(locationBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, &options.FindOneOptions{Projection: proj})

	var doc docs.Location
	if err := res.Decode(&doc); err != nil {
		l.WithError(err).Error("Could not find and decode location document")
		return nil, err
	}

	return &doc, nil
}

// Get returns information about multiple location ids
func (api *apiImpl) GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.Location, error) {
	l := log.WithFields(log.Fields{
		"func": "locations.apiImpl.GetMulti",
		"ids":  ids,
	})

	for _, id := range ids {
		if !utils.IsValidSlugPath(id) {
			l.Error("Invalid id")
			return nil, apis.ErrInvalidID
		}
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(locationBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": bson.M{"$in": ids}}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query location documents")
		return nil, err
	}

	var docs []docs.Location
	if err := cursor.All(ctx, &docs); err != nil {
		l.WithError(err).Error("Could not find and decode location documents")
		return nil, err
	}

	return docs, nil
}

func (api *apiImpl) GetOrCreate(ctx context.Context, loc string, projKeys []string) (*docs.Location, error) {
	l := log.WithFields(log.Fields{
		"func": "locations.apiImpl.GetOrCreate",
		"loc":  loc,
	})

	if !isValidLocation(loc) {
		l.Error("Invalid location")
		return nil, apis.ErrInvalidArgs
	}

	id := SlugifyPathString(loc)
	tokensAll := []string{docs.VisTokenAll}

	// Try get. Always get back display name to check if location type(s) need updating.
	hasDisplay := false
	for _, key := range projKeys {
		if key == "display" {
			hasDisplay = true
			break
		}
	}

	getProjKeys := projKeys
	if !hasDisplay {
		getProjKeys = append(projKeys, "display")
	}

	if d, err := api.Get(ctx, id, getProjKeys, tokensAll); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing location")
			return nil, err
		}

		canonicalDisplay, _ := canonicalizeDisplay(loc)
		if *d.Display == canonicalDisplay {
			// If display was not in original projKeys, remove from the returned document.
			if !hasDisplay {
				d.Display = nil
			}
			return d, nil
		}

		// Display name is different, so location types have changed somewhere in the hierarchy.
		canonicalDisplayParts := strings.Split(canonicalDisplay, separator)
		curDisplayParts := strings.Split(*d.Display, separator)
		idParts := strings.Split(id, "/")
		l.Tracef("canonicalizeDisplayParts=%+v curDisplayParts=%+v idParts=%+v", canonicalDisplayParts, curDisplayParts, idParts)

		if len(canonicalDisplayParts) != len(curDisplayParts) {
			err := fmt.Errorf("Mismatched display part lengths")
			l.WithError(err).Error("Could not update display name")
			return nil, err
		}
		if len(idParts)-2 != len(canonicalDisplayParts) {
			err := fmt.Errorf("Mismatched id length")
			l.WithError(err).Error("Could not update display name")
			return nil, err
		}

		for i := range canonicalDisplayParts {
			if canonicalDisplayParts[i] != curDisplayParts[i] {
				// This location has a different display name.
				localId := strings.Join(idParts[:i+2], "/") + "/"
				if err := api.Update(ctx, localId, &apis.GroupUpdate{Display: &canonicalDisplayParts[i]}); err != nil {
					l.WithError(err).WithField("id", localId).Error("Could not update display name")
					return nil, err
				}
				l.WithField("id", localId).Info("Updated display name")
			}
		}

		d, err = api.Get(ctx, id, projKeys, tokensAll)
		if err != nil {
			l.WithError(err).Error("Could not get location after display update")
			return nil, err
		}

		return d, nil
	}

	// Create
	if _, err := api.Create(ctx, loc); err != nil {
		l.WithError(err).Error("Could not create location")
		return nil, err
	}

	return api.Get(ctx, id, projKeys, tokensAll)
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.Location, error) {
	var results []docs.Location
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error {
	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	l := log.WithFields(log.Fields{
		"func": "locations.Update",
		"id":   id,
	})

	if update.Display != nil {
		canonicalDisplay, _ := canonicalizeDisplay(*update.Display)
		update.Display = &canonicalDisplay
	}

	return groups.Update(ctx, api, id, update, &groups.UpdateOptions{
		PreUpdate: func(ctx context.Context, id string, toSet *bson.M, toUnset *bson.M) error {
			if displayRaw, ok := (*toSet)["display"]; ok {
				display := displayRaw.(string)

				// Update type based on the last display.
				displayParts := strings.Split(display, separator)
				_, locType := stripPrefixAndReturnLocationType(displayParts[len(displayParts)-1])
				(*toSet)["type"] = locType
				l.Tracef("Updating type to %v", locType)

				// Update parent park id too.
				parentParkInfo := getParentParkInfo(display, id)
				if parentParkInfo == nil {
					(*toUnset)["park"] = 1
					(*toUnset)["parkName"] = 1
					l.Tracef("Not a park")
				} else {
					(*toSet)["park"] = parentParkInfo.ID
					(*toSet)["parkName"] = parentParkInfo.Name
					l.Tracef("Updating park to %+v", *parentParkInfo)
				}
			}
			return nil
		},
	})
}
