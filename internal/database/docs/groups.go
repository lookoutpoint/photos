// SPDX-License-Identifier: MIT

package docs

import (
	"time"
)

// GroupBase defines the base fields for a grouping collection. Should be embedded in the overall group-specific document
// with bson:",inline" and json:",inline".
type GroupBase struct {
	ID         *string          `bson:"_id" json:"id,omitempty"`                        // slug path
	Timestamp  *time.Time       `bson:"timestamp,omitempty" json:"timestamp,omitempty"` // last time something was added or modified belonging to the group
	Display    *string          `bson:"display,omitempty" json:"display,omitempty"`     // display path
	Depth      *int             `bson:"depth,omitempty" json:"depth,omitempty"`
	SortOrder  *interface{}     `bson:"sortOrder,omitempty" json:"sortOrder,omitempty"` // string or integer
	CoverPhoto *GroupCoverPhoto `bson:"coverPhoto,omitempty" json:"coverPhoto,omitempty"`

	// Visibility tokens should never be nil. At least an empty array.
	VisTokens          []string       `bson:"visTokens" json:"visTokens,omitempty"`
	VisTokenCounts     map[string]int `bson:"visTokenCounts" json:"visTokenCounts,omitempty"`
	DescVisTokenCounts map[string]int `bson:"descVisTokenCounts" json:"descVisTokenCounts,omitempty"`

	// Content
	MetaDescription *string `bson:"metaDescription,omitempty" json:"metaDescription,omitempty"`
	RichText        *string `bson:"richText,omitempty" json:"richText,omitempty"`
}

func (g GroupBase) GetID() string {
	return *g.ID
}

func (g GroupBase) GetRichText() *string {
	return g.RichText
}

// GroupCoverPhoto describes the current cover photo for a group
type GroupCoverPhoto struct {
	Photo            *PhotoID   `bson:"photo,omitempty" json:"photo,omitempty"`
	UserSelected     *bool      `bson:"userSelected,omitempty" json:"userSelected,omitempty"`
	LastAutoSelected *time.Time `bson:"lastAutoSelected,omitempty" json:"lastAutoSelected,omitempty"`
}
