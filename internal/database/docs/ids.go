// SPDX-License-Identifier: MIT

package docs

import "gitlab.com/lookoutpoint/photos/internal/database/utils"

// These ID types are defined here to prevent circular dependencies when references are needed

// PhotoID is the id type for a photo document
type PhotoID string

// SessionID is the type of a session id
type SessionID string

// UserID is the id type for a user document
type UserID string

// IsValidVisTokenID checks if the id is a valid visibility token id
func IsValidVisTokenID(id string) bool {
	return id == VisTokenAll || id == VisTokenPublic || (utils.IsValidSlugPath(id) && utils.SlugPathDepth(id) == 1)
}
