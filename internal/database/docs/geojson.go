// SPDX-License-Identifier: MIT

package docs

// GeoJSONPoint represents a MongoDB GeoJSON point
// Designed to be added to MongoDB as a document
type GeoJSONPoint struct {
	Type        string    `bson:"type" json:"-"`
	Coordinates []float64 `bson:"coordinates" json:"coordinates"`
}

// NewGeoJSONPoint creates a new GeoJSONPoint object
func NewGeoJSONPoint(latitude float64, longitude float64) *GeoJSONPoint {
	return &GeoJSONPoint{
		Type:        "Point",
		Coordinates: []float64{longitude, latitude},
	}
}

// GetLatitude returns the latitude component of the point
func (pt *GeoJSONPoint) GetLatitude() float64 {
	return pt.Coordinates[1]
}

// GetLongitude returns the longitude component of the point
func (pt *GeoJSONPoint) GetLongitude() float64 {
	return pt.Coordinates[0]
}

// SetLatitude sets the latitude component of the point
func (pt *GeoJSONPoint) SetLatitude(latitude float64) {
	pt.Coordinates[1] = latitude
}

// SetLongitude sets the longitude component of the point
func (pt *GeoJSONPoint) SetLongitude(longitude float64) {
	pt.Coordinates[0] = longitude
}
