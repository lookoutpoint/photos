package docs

import (
	"fmt"
	"reflect"
	"regexp"
	"strings"

	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
)

// PhotoListFilterExpr represents a filter expression
type PhotoListFilterExpr struct {
	// Op defines the logical operation used to join all of the filter clauses
	Op list.LogicalOperator `bson:"op,omitempty" json:"op,omitempty"` // defaults to list.LogicalAnd

	// The filter expression itself can contain nested filter expressions
	Exprs []*PhotoListFilterExpr `bson:"exprs,omitempty" json:"exprs,omitempty"`

	// Property-specific filters
	Folders      []PhotoListFilterFolder      `bson:"folders,omitempty" json:"folders,omitempty"`
	Ratings      []PhotoListFilterRating      `bson:"ratings,omitempty" json:"ratings,omitempty"`
	Keywords     []string                     `bson:"keywords,omitempty" json:"keywords,omitempty"`     // ids
	Locations    []string                     `bson:"locations,omitempty" json:"locations,omitempty"`   // prefix ids
	Categories   []string                     `bson:"categories,omitempty" json:"categories,omitempty"` // ids
	Dates        []PhotoListFilterDate        `bson:"dates,omitempty" json:"dates,omitempty"`
	AspectRatios []PhotoListFilterAspectRatio `bson:"aspectRatios,omitempty" json:"aspectRatios,omitempty"`
	// TODO: GPS location
}

// IsEmpty returns true if there are no filters inside an expression
func (expr *PhotoListFilterExpr) IsEmpty() bool {
	return len(expr.Exprs) == 0 && len(expr.Folders) == 0 && len(expr.Ratings) == 0 && len(expr.Keywords) == 0 &&
		len(expr.Locations) == 0 && len(expr.Categories) == 0 && len(expr.Dates) == 0 && len(expr.AspectRatios) == 0
}

// Clone
func (expr *PhotoListFilterExpr) Clone() *PhotoListFilterExpr {
	clone := &PhotoListFilterExpr{}

	clone.Op = expr.Op

	if len(expr.Exprs) > 0 {
		clone.Exprs = make([]*PhotoListFilterExpr, len(expr.Exprs))
		for i, expr := range expr.Exprs {
			clone.Exprs[i] = expr.Clone()
		}
	}

	if len(expr.Folders) > 0 {
		clone.Folders = make([]PhotoListFilterFolder, 0, len(expr.Folders))
		clone.Folders = append(clone.Folders, expr.Folders...)
	}

	if len(expr.Ratings) > 0 {
		clone.Ratings = make([]PhotoListFilterRating, 0, len(expr.Ratings))
		clone.Ratings = append(clone.Ratings, expr.Ratings...)
	}

	if len(expr.Keywords) > 0 {
		clone.Keywords = make([]string, 0, len(expr.Keywords))
		clone.Keywords = append(clone.Keywords, expr.Keywords...)
	}

	if len(expr.Locations) > 0 {
		clone.Locations = make([]string, 0, len(expr.Locations))
		clone.Locations = append(clone.Locations, expr.Locations...)
	}

	if len(expr.Categories) > 0 {
		clone.Categories = make([]string, 0, len(expr.Categories))
		clone.Categories = append(clone.Categories, expr.Categories...)
	}

	if len(expr.Dates) > 0 {
		clone.Dates = make([]PhotoListFilterDate, 0, len(expr.Dates))
		clone.Dates = append(clone.Dates, expr.Dates...)
	}

	if len(expr.AspectRatios) > 0 {
		clone.AspectRatios = make([]PhotoListFilterAspectRatio, 0, len(expr.AspectRatios))
		clone.AspectRatios = append(clone.AspectRatios, expr.AspectRatios...)
	}

	return clone
}

// AssignDefaults to expression
func (expr *PhotoListFilterExpr) AssignDefaults() {
	if len(expr.Op) == 0 {
		expr.Op = list.LogicalAnd
	}

	for _, subExpr := range expr.Exprs {
		subExpr.AssignDefaults()
	}
}

// Validate expression
func (expr *PhotoListFilterExpr) Validate() error {
	if err := expr.Op.Validate(); err != nil {
		return err
	}

	for _, subExpr := range expr.Exprs {
		if err := subExpr.Validate(); err != nil {
			return err
		}
	}

	for _, folder := range expr.Folders {
		if err := folder.Validate(); err != nil {
			return err
		}
	}

	for _, kw := range expr.Keywords {
		if !utils.IsValidSlugPath(kw) {
			return fmt.Errorf("keyword '%s' is not a valid id", kw)
		}
	}

	for _, cv := range expr.Categories {
		if !utils.IsValidSlugPath(cv) {
			return fmt.Errorf("category '%s' is not a valid id", cv)
		}
	}

	for _, loc := range expr.Locations {
		if !utils.IsValidSlugPath(loc) {
			return fmt.Errorf("location '%s' is not a valid id", loc)
		}
	}

	for _, rating := range expr.Ratings {
		if err := rating.Validate(); err != nil {
			return err
		}
	}

	for _, date := range expr.Dates {
		if err := date.Validate(); err != nil {
			return err
		}
	}

	for _, ar := range expr.AspectRatios {
		if err := ar.Validate(); err != nil {
			return err
		}
	}

	return nil
}

// PhotoListFilterFolder filters by folder path
type PhotoListFilterFolder struct {
	Path     string `bson:"path" json:"path"` // slug path
	FullTree bool   `bson:"fullTree" json:"fullTree"`
}

// Validate folder filter
func (folder PhotoListFilterFolder) Validate() error {
	if !utils.IsValidSlugPath(folder.Path) {
		return fmt.Errorf("Folder filter path '%s' is not a valid id", folder.Path)
	}
	return nil
}

// PhotoListFilterRating filters by photo rating
type PhotoListFilterRating struct {
	Value int                     `bson:"value" json:"value"` // >= 0
	CmpOp list.ComparisonOperator `bson:"cmpOp" json:"cmpOp"`
}

// Validate rating filter
func (rating PhotoListFilterRating) Validate() error {
	if rating.Value < 0 {
		return fmt.Errorf("rating filter value '%d' must be >= 0", rating.Value)
	}

	return rating.CmpOp.Validate()
}

// PhotoListFilterDate filters by date (which really filters by sortKey)
type PhotoListFilterDate struct {
	// yyyy[mm[dd[hh[mm[ss]]]]]
	Value string                  `bson:"value" json:"value"`
	CmpOp list.ComparisonOperator `bson:"cmpOp" json:"cmpOp"`
}

var dateValueRegex = regexp.MustCompile(`^\d{4}|\d{6}|\d{8}|\d{10}|\d{12}|\d{14}$`)

// Validate date filter
func (date PhotoListFilterDate) Validate() error {
	if !dateValueRegex.MatchString(date.Value) {
		return fmt.Errorf("Date filter value '%s' is invalid", date.Value)
	}

	return date.CmpOp.Validate()
}

// PhotoListFilterAspectRatio filters by aspect ratio (photo width/height)
type PhotoListFilterAspectRatio struct {
	Value float32                 `bson:"value" json:"value"`
	CmpOp list.ComparisonOperator `bson:"cmpOp" json:"cmpOp"`
}

// Validate aspect ratio filter
func (ar PhotoListFilterAspectRatio) Validate() error {
	return ar.CmpOp.Validate()
}

// BuildQueryFilter converts a PhotoListFilterExpr into a MongoDB query filter
func (expr *PhotoListFilterExpr) BuildQueryFilter() interface{} {
	clauses := bson.A{}

	for _, subExpr := range expr.Exprs {
		subFilter := subExpr.BuildQueryFilter()
		if subFilter != nil {
			clauses = append(clauses, subFilter)
		}
	}

	clauses = append(clauses, expr.buildFolderFilter()...)
	clauses = append(clauses, expr.buildKeywordFilter()...)
	clauses = append(clauses, expr.buildLocationFilter()...)
	clauses = append(clauses, expr.buildCategoryFilter()...)
	clauses = append(clauses, expr.buildDateFilter()...)
	clauses = append(clauses, expr.buildRatingFilter()...)
	clauses = append(clauses, expr.buildAspectRatiosFilter()...)

	if len(clauses) == 0 {
		return nil
	} else if len(clauses) == 1 {
		return clauses[0]
	} else {
		// Join all clauses
		op := "$" + string(expr.Op)
		return bson.M{op: clauses}
	}
}

func appendInArrayClause(clauses bson.A, key string, values interface{}) bson.A {
	rValues := reflect.ValueOf(values)
	length := rValues.Len()

	if length == 0 {
		return clauses
	}
	if length == 1 {
		return append(clauses, bson.M{key: rValues.Index(0).Interface()})
	}
	return append(clauses, bson.M{key: bson.M{"$in": values}})
}

// Sanitize path by re-slugifying the path
func sanitizeSlugPath(path string) string {
	return utils.SlugifyPath(strings.Split(path, "/")...)
}

func (expr *PhotoListFilterExpr) buildFolderFilter() bson.A {
	clauses := bson.A{}

	// Values for LogicalOr combination when matching against exact folder paths
	orFolders := []string{}

	// Folder
	for _, f := range expr.Folders {
		folderPath := sanitizeSlugPath(f.Path)

		if f.FullTree {
			clauses = append(clauses, bson.M{
				"folder": bson.M{
					"$regex": "^" + folderPath,
				},
			})
		} else {
			if expr.Op == list.LogicalAnd {
				clauses = append(clauses, bson.M{"folder": folderPath})
			} else {
				orFolders = append(orFolders, folderPath)
			}
		}
	}

	clauses = appendInArrayClause(clauses, "folder", orFolders)

	return clauses
}

func (expr *PhotoListFilterExpr) buildKeywordFilter() bson.A {
	clauses := bson.A{}

	orKeywords := []string{}

	for _, kw := range expr.Keywords {
		kwPath := sanitizeSlugPath(kw)

		if expr.Op == list.LogicalAnd {
			clauses = append(clauses, bson.M{"keywords": kwPath})
		} else {
			orKeywords = append(orKeywords, kwPath)
		}
	}

	clauses = appendInArrayClause(clauses, "keywords", orKeywords)

	return clauses
}

func (expr *PhotoListFilterExpr) buildLocationFilter() bson.A {
	clauses := bson.A{}

	for _, loc := range expr.Locations {
		// Input sanitize by slugifying location slug path as this is going into a regex.
		locPath := sanitizeSlugPath(loc)
		clauses = append(clauses, bson.M{
			"locations": bson.M{
				"$regex": "^" + locPath,
			},
		})
	}

	return clauses
}

func (expr *PhotoListFilterExpr) buildCategoryFilter() bson.A {
	clauses := bson.A{}

	for _, cv := range expr.Categories {
		cvPath := sanitizeSlugPath(cv)
		clauses = append(clauses, bson.M{"categories": bson.M{"$regex": "^" + cvPath}})
	}

	return clauses
}

func (expr *PhotoListFilterExpr) buildDateFilter() bson.A {
	clauses := bson.A{}

	for _, dateFilter := range expr.Dates {
		switch dateFilter.CmpOp {
		case list.CmpEq:
			clauses = append(clauses, bson.M{"sortKey": bson.M{"$regex": "^" + dateFilter.Value}})
		case list.CmpNe:
			clauses = append(clauses, bson.M{"sortKey": bson.M{"$not": bson.M{"$regex": "^" + dateFilter.Value}}})
		default:
			op := "$" + string(dateFilter.CmpOp)
			value := dateFilter.Value

			if dateFilter.CmpOp == list.CmpGt || dateFilter.CmpOp == list.CmpLte {
				// Pad to get a 14-character value to get proper filter for Gt and Lte filters.
				// We pad to 14-characters because in the sortKey to encompass the entire date+time.
				for len(value) < 14 {
					value += "9"
				}
			}

			clauses = append(clauses, bson.M{"sortKey": bson.M{op: value}})
		}
	}

	return clauses
}

func (expr *PhotoListFilterExpr) buildRatingFilter() bson.A {
	clauses := bson.A{}

	// Assumption: legal ratings start from 1 and go higher. Zero rating is equivalent as having no rating field.
	for _, ratingFilter := range expr.Ratings {
		if ratingFilter.Value > 0 {
			if ratingFilter.CmpOp == list.CmpLt || ratingFilter.CmpOp == list.CmpLte {
				var op string
				if ratingFilter.CmpOp == list.CmpLt {
					op = "$gte"
				} else {
					op = "$gt"
				}

				clauses = append(clauses, bson.M{"rating": bson.M{"$not": bson.M{op: ratingFilter.Value}}})
			} else {
				op := "$" + string(ratingFilter.CmpOp)
				clauses = append(clauses, bson.M{"rating": bson.M{op: ratingFilter.Value}})
			}
		} else {
			// Zero rating is equivalent as no rating.
			if ratingFilter.CmpOp == list.CmpLt {
				// Lt 0 will match no photos with this filter (expected)
				op := "$" + string(ratingFilter.CmpOp)
				clauses = append(clauses, bson.M{"rating": bson.M{op: 0}})
			} else if ratingFilter.CmpOp == list.CmpNe || ratingFilter.CmpOp == list.CmpGt {
				// Match photos with any rating field.
				clauses = append(clauses, bson.M{"rating": bson.M{"$exists": true}})
			} else if ratingFilter.CmpOp == list.CmpLte || ratingFilter.CmpOp == list.CmpEq {
				// Match photos with no rating field.
				clauses = append(clauses, bson.M{"rating": bson.M{"$exists": false}})
			} else if ratingFilter.CmpOp == list.CmpGte {
				// Gte 0 matches all photos, so no clause is needed
			}
		}
	}

	return clauses
}

func (expr *PhotoListFilterExpr) buildAspectRatiosFilter() bson.A {
	clauses := bson.A{}

	for _, arFilter := range expr.AspectRatios {
		op := "$" + string(arFilter.CmpOp)
		clauses = append(clauses, bson.M{
			"$expr": bson.M{
				op: bson.A{
					bson.M{
						"$divide": bson.A{"$width", "$height"},
					},
					arFilter.Value,
				},
			},
		})
	}

	return clauses
}
