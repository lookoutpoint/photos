// SPDX-License-Identifier: MIT

package docs

import (
	"time"
)

// This file declares all document types stored in the database.

// Type to store self visibility tokens in a document
type SelfVisTokensWrapper struct {
	SelfVisTokens []string `bson:"selfVisTokens" json:"selfVisTokens,omitempty"`
}

func (t SelfVisTokensWrapper) GetSelfVisTokens() []string {
	return t.SelfVisTokens
}

// CategoryEntry is the structure of a category document, which includes categories and category values
type CategoryEntry struct {
	GroupBase `bson:",inline" json:",inline"`

	// Related categories
	Related map[string][]string `bson:"related,omitempty" json:"related,omitempty"`
}

// Date is the structure of a date document
type Date struct {
	GroupBase `bson:",inline" json:",inline"`
}

// Folder is the structure of a folder document
type Folder struct {
	GroupBase            `bson:",inline" json:",inline"`
	SelfVisTokensWrapper `bson:",inline" json:",inline"`
}

// Keyword is the structure of a keyword document
type Keyword struct {
	GroupBase `bson:",inline" json:",inline"`
}

// Page is the structure of a page document
type Page struct {
	GroupBase            `bson:",inline" json:",inline"`
	SelfVisTokensWrapper `bson:",inline" json:",inline"`
}

type LocationType int

const (
	LocationOther LocationType = 0
	LocationHike  LocationType = 1
	LocationPark  LocationType = 2
)

// Location is the structure of a location document
type Location struct {
	GroupBase `bson:",inline" json:",inline"`
	Type      *LocationType `bson:"type" json:"type,omitempty"`
	Park      *string       `bson:"park" json:"park,omitempty"`
	ParkName  *string       `bson:"parkName" json:"parkName,omitempty"`
}

// Photo is the structure of a folder document
type Photo struct {
	ID          *PhotoID   `bson:"_id" json:"id,omitempty"`
	SortKey     *string    `bson:"sortKey" json:"sortKey,omitempty"`
	StoreBucket *string    `bson:"storeBucket" json:"storeBucket,omitempty"`
	StoreObject *string    `bson:"storeObject" json:"storeObject,omitempty"`
	StoreGen    *string    `bson:"storeGen" json:"storeGen,omitempty"` // base64 encoded
	FileSize    *int       `bson:"fileSize" json:"fileSize,omitempty"`
	Folder      *string    `bson:"folder" json:"folder,omitempty"` // slug
	Width       *int       `bson:"width" json:"width,omitempty"`
	Height      *int       `bson:"height" json:"height,omitempty"`
	Timestamp   *time.Time `bson:"timestamp" json:"timestamp,omitempty"`
	TsSortKey   *string    `bson:"tsSortKey" json:"tsSortKey,omitempty"`
	Date        *string    `bson:"date" json:"date,omitempty"`

	// Visibility restrictions

	// A photo either inherits its folder's visibility or has its own visibility tokens
	InheritVisibility *bool `bson:"inheritVisibility,omitempty" json:"inheritVisibility,omitempty"`

	// VisTokens control visibility and account for photo self-visibility and folder visibility.
	// In BSON, don't use omitempty as this field should never be nil (at least an empty array).
	VisTokens []string `bson:"visTokens" json:"visTokens,omitempty"`

	// Optional metadata
	Title       *string    `bson:"title,omitempty" json:"title,omitempty"`
	Description *string    `bson:"description,omitempty" json:"description,omitempty"`
	Keywords    []string   `bson:"keywords,omitempty" json:"keywords,omitempty"`     // slugs
	Categories  []string   `bson:"categories,omitempty" json:"categories,omitempty"` // slugs
	DateTime    *time.Time `bson:"dateTime,omitempty" json:"dateTime,omitempty"`
	Rating      *int32     `bson:"rating,omitempty" json:"rating,omitempty"`

	Copyright   *string `bson:"copyright,omitempty" json:"copyright,omitempty"`
	UsageTerms  *string `bson:"usageTerms,omitempty" json:"usageTerms,omitempty"`
	UsageWebURL *string `bson:"usageWebUrl,omitempty" json:"usageWebUrl,omitempty"`

	Locations   []string      `bson:"locations,omitempty" json:"locations,omitempty"` // slugs
	GPSLocation *GeoJSONPoint `bson:"gpsLocation,omitempty" json:"gpsLocation,omitempty"`
	GPSAltitude *float64      `bson:"gpsAltitude,omitempty" json:"gpsAltitude,omitempty"`

	Make         *string  `bson:"make,omitempty" json:"make,omitempty"`
	Model        *string  `bson:"model,omitempty" json:"model,omitempty"`
	Lens         *string  `bson:"lens,omitempty" json:"lens,omitempty"`
	FNumber      *float64 `bson:"fNumber,omitempty" json:"fNumber,omitempty"`
	FocalLength  *float64 `bson:"focalLength,omitempty" json:"focalLength,omitempty"`
	ISO          *int32   `bson:"iso,omitempty" json:"iso,omitempty"`
	ExposureTime *float64 `bson:"exposureTime,omitempty" json:"exposureTime,omitempty"`
	ExposureBias *float64 `bson:"exposureBias,omitempty" json:"exposureBias,omitempty"`
}

// PhotoResizeCacheEntry is the embedded document structure for one cache entry
type PhotoResizeCacheEntry struct {
	PhotoTimestamp time.Time `bson:"photoTimestamp"`
	FileSize       int       `bson:"fileSize"`
	LastAccess     time.Time `bson:"lastAccess"`
	CacheHit       int       `bson:"cacheHit"`
}

// TimelineGroup describes a timeline grouping of photos
type TimelineGroup struct {
	GroupBase `bson:",inline" json:",inline"`

	SelfVisTokensWrapper `bson:",inline" json:",inline"`

	Filter    *PhotoListFilterExpr `bson:"filter,omitempty" json:"filter,omitempty"`
	StartDate *time.Time           `bson:"startDate,omitempty" json:"startDate,omitempty"`
	EndDate   *time.Time           `bson:"endDate,omitempty" json:"endDate,omitempty"`
}

// Session stores info about a logged-in session
type Session struct {
	ID     SessionID `bson:"_id"`
	UserID UserID    `bson:"userId"`
	Expiry time.Time `bson:"expiry"`
}

// UserRole type
type UserRole string

// UserRoleAdmin role
const UserRoleAdmin UserRole = "admin"

// User is the document type for a user
type User struct {
	ID             *UserID   `bson:"_id" json:"id,omitempty"`
	HashedPassword *string   `bson:"hashedPassword" json:"-"`
	Role           *UserRole `bson:"role" json:"role,omitempty"`
}

// VisibilityToken describes a visibility token
type VisibilityToken struct {
	GroupBase `bson:",inline"`
	Links     []VisibilityTokenLink `bson:"links,omitempty" json:"links,omitempty"`
}

// VisTokenPublic is the virtual token id representing public access (there is no actual VisibilityToken corresponding to this id)
// Its value is picked to specifically to be not a valid slug so that it wouldn't conflict with any created token ids.
const VisTokenPublic = "#P"

// VisTokenAll is the placeholder token value representing "all" tokens. It should exist on all every entity.
// Its value is picked to specifically to be not a valid slug so that it wouldn't conflict with any created token ids.
const VisTokenAll = "*"

// VisibilityTokenLink describes a link for a token
type VisibilityTokenLink struct {
	// Cryptographically-secure activation value
	ActivationValue *string `bson:"activationValue,omitempty" json:"activationValue,omitempty"`
	ActivationCount *int    `bson:"activationCount,omitempty" json:"activationCount,omitempty"`
	Redirect        *string `bson:"redirect,omitempty" json:"redirect,omitempty"`
	Comment         *string `bson:"comment,omitempty" json:"comment,omitempty"`
	Disabled        *bool   `bson:"disabled,omitempty" json:"disabled,omitempty"`
}

// EventEntry stores the count of an event in one time period
const (
	EventDay int = iota
	EventMonth
)

type EventEntry struct {
	Event   *string    `bson:"event,omitempty" json:"event,omitempty"`
	Object  *string    `bson:"object,omitempty" json:"object,omitempty"`
	Context *string    `bson:"context,omitempty" json:"context,omitempty"`
	Count   *int       `bson:"count,omitempty" json:"count,omitempty"`
	Date    *time.Time `bson:"date,omitempty" json:"date,omitempty"`
	Extra   *string    `bson:"extra,omitempty" json:"extra,omitempty"`
}
