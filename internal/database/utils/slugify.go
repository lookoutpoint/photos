// SPDX-License-Identifier: MIT

package utils

import (
	"errors"
	"regexp"
	"strings"
)

// Match spaces and punctuation
var slugifyTokenRegex = regexp.MustCompile("[\\s~`!@#$%^&*()_+=[\\]{}\\\\|:;\"'<>,.?/-]+")

// Match spaces and punctuation but don't match "/" as that is the path separator
var slugifyPathRegex = regexp.MustCompile("[\\s~`!@#$%^&*()_+=[\\]{}\\\\|:;\"'<>,.?-]+")

// SlugifyToken converts the given token into a safe slug string
func SlugifyToken(token string) string {
	return strings.Trim(slugifyTokenRegex.ReplaceAllLiteralString(token, "-"), "-")
}

// SlugifyTokenLower converts the given token into a safe, lower-case slug string
func SlugifyTokenLower(token string) string {
	return strings.ToLower(SlugifyToken(token))
}

// SlugifyPath converts the given path into a safe path with slug tokens. All slug paths begin and end with trailing slash.
func SlugifyPath(path ...string) string {
	slugPath := ""

	// Slugify each token individually first (as a token may have "/", but that doesn't denote a path separator in this context).
	for i, p := range path {
		if i > 0 {
			slugPath += "/"
		}
		slugPath += SlugifyToken(p)
	}

	// Begin slash.
	if len(slugPath) == 0 || slugPath[0] != '/' {
		slugPath = "/" + slugPath
	}
	// Trailing slash.
	if slugPath[len(slugPath)-1] != '/' {
		return slugPath + "/"
	}
	return slugPath
}

// SlugifyPathLower converts the given path into a safe path with lower-case slug tokens
func SlugifyPathLower(path ...string) string {
	return strings.ToLower(SlugifyPath(path...))
}

// ConcatSlugPath creates a slug path by concatenating the given path parts. Does not slugify the individual path parts.
func ConcatSlugPath(path ...string) string {
	slugPath := strings.Join(path, "/")

	// Begin slash.
	if len(slugPath) == 0 || slugPath[0] != '/' {
		slugPath = "/" + slugPath
	}
	// Trailing slash.
	if slugPath[len(slugPath)-1] != '/' {
		return slugPath + "/"
	}
	return slugPath
}

// ConcatSlugPathLower is the same as ConcatSlugPathLower but converts it to lower-case
func ConcatSlugPathLower(path ...string) string {
	return strings.ToLower(ConcatSlugPath(path...))
}

// ErrInvalidSlugPath indicates an invalid slug path
var ErrInvalidSlugPath = errors.New("Invalid slug path")

// IsValidSlugPath checks that the path is valid, which means it must have both a beginning and trailing slash.
// The path must no empty parts (i.e. "//").
func IsValidSlugPath(path string) bool {
	return len(path) > 0 && path[0] == '/' && path[len(path)-1] == '/' && strings.Index(path, "//") == -1
}

// SlugPathDepth returns the depth of the given slug path, which is the number of '/' separators in the path,
// ignoring the trailing slash
func SlugPathDepth(slugPath string) int {
	depth := 0
	for i := 0; i < len(slugPath)-1; i++ {
		if slugPath[i] == '/' {
			depth++
		}
	}
	return depth
}
