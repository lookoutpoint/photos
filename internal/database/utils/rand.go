// SPDX-License-Identifier: MIT

package utils

import (
	"crypto/rand"
	"encoding/base64"
	"io"
)

// GenerateSecureRandBase64 generates a cryptographically secure random number of the given size
// and encodes it in a base64 string.
func GenerateSecureRandBase64(numBytes int) (string, error) {
	num := make([]byte, numBytes)
	_, err := io.ReadFull(rand.Reader, num)
	if err != nil {
		return "", err
	}

	return base64.RawURLEncoding.EncodeToString(num), nil
}
