// SPDX-License-Identifier: MIT

package utils

import (
	"fmt"
	"reflect"
	"strings"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
)

// BSONKeySet holds a set of valid BSON keys
type BSONKeySet map[string]bool

// ExtractBSONKeys returns a map of all the BSON keys tagged on the given type
func ExtractBSONKeys(v interface{}) BSONKeySet {
	keys := BSONKeySet{}

	dupeCheck := func(key string) {
		if _, ok := keys[key]; ok {
			log.Errorf("Duplicate BSON key %s in %+v", key, v)
			panic("Duplicate BSON key")
		}
	}

	t := reflect.TypeOf(v)
	for i := 0; i < t.NumField(); i++ {
		bsonTag, ok := t.Field(i).Tag.Lookup("bson")
		if ok {
			if bsonTag == ",inline" {
				rv := reflect.ValueOf(v)
				rvField := rv.FieldByName(t.Field(i).Name)
				inlineKeys := ExtractBSONKeys(rvField.Interface())

				for ikey := range inlineKeys {
					dupeCheck(ikey)
					keys[ikey] = true
				}
			} else if comma := strings.Index(bsonTag, ","); comma >= 0 {
				tag := bsonTag[:comma]
				dupeCheck(tag)
				keys[tag] = true
			} else {
				dupeCheck(bsonTag)
				keys[bsonTag] = true
			}
		}
	}

	return keys
}

// PrefixBSONKeys adds the given prefix to all keys in the set
func PrefixBSONKeys(keys BSONKeySet, prefix string) BSONKeySet {
	newKeys := BSONKeySet{}

	for key := range keys {
		newKeys[prefix+key] = true
	}

	return newKeys
}

// CombineBSONKeys combines multiple BSONKeySets together. They must not have the same keys.
func CombineBSONKeys(allKeys ...BSONKeySet) BSONKeySet {
	keys := BSONKeySet{}

	dupeCheck := func(key string) {
		if _, ok := keys[key]; ok {
			log.Errorf("Duplicate BSON key %s", key)
			panic("Duplicate BSON key")
		}
	}

	for _, someKeys := range allKeys {
		for key := range someKeys {
			dupeCheck(key)
			keys[key] = true
		}
	}

	return keys
}

// MakeProjectionFromKeys creates a projection document based on the given keys, optionally
// performing validation.
func MakeProjectionFromKeys(validKeys BSONKeySet, keys ...[]string) (bson.M, error) {
	proj := bson.M{}

	for _, keySet := range keys {
		for _, key := range keySet {
			if key == "id" {
				// The id key is stored as _id internally in Mongo
				key = "_id"
			}

			proj[key] = 1
		}
	}

	if validKeys != nil {
		// Validate projection keys
		for key := range proj {
			if _, ok := validKeys[key]; !ok {
				return nil, fmt.Errorf("Invalid key '%v'", key)
			}
		}
	}

	// By default, projections typically return _id even if not specified. In our case, if it's
	// not in the list of keys, don't project it.
	if _, ok := proj["_id"]; !ok {
		proj["_id"] = 0
	}

	return proj, nil
}
