// SPDX-License-Identifier: MIT

package utils

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

// WrapAsTransaction creates or reuses an existing transaction session.
// Returns transaction-ret-val, completed, error; where completed is a booleal indicating if the transaction is done
// (i.e. this is not part of a bigger transaction)
func WrapAsTransaction(ctx context.Context, mdb *mongo.Database, l *log.Entry, transactionCb func(ctx mongo.SessionContext) (interface{}, error)) (interface{}, bool, error) {
	// If the context is already a mongo.SessionContext, use that (i.e. this is part of a bigger transaction)
	if mCtx, ok := ctx.(mongo.SessionContext); ok {
		rv, err := transactionCb(mCtx)
		return rv, false, err
	}

	// Start session and transaction.
	session, err := mdb.Client().StartSession()
	if err != nil {
		l.WithError(err).Error("Could not start session")
		return nil, true, err
	}
	defer session.EndSession(ctx)

	// Count number of attempts for transaction
	cbCount := 0
	wrappedCb := func(ctx mongo.SessionContext) (interface{}, error) {
		cbCount++
		return transactionCb(ctx)
	}

	// Time transaction
	start := time.Now()

	rv, err := session.WithTransaction(ctx, wrappedCb,
		options.Transaction().
			SetReadConcern(readconcern.Majority()).
			SetWriteConcern(writeconcern.Majority()),
	)

	elapsed := time.Since(start)

	if err == nil {
		l.Infof("Completed transaction in %v (# attempts = %v)", elapsed, cbCount)
	}
	return rv, true, err
}
