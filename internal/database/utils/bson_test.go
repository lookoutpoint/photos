// SPDX-License-Identifier: MIT

package utils_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/lookoutpoint/photos/internal/database/utils"
)

var _ = It("ExtractBSONKeys", func() {
	var t struct {
		ID    string   `bson:"_id"`
		Field *float64 `bson:"field"`
		Value *int     `bson:"value,omitempty"`
	}

	keys := ExtractBSONKeys(t)

	Expect(keys).To(HaveLen(3))
	Expect(keys).To(HaveKey("_id"))
	Expect(keys).To(HaveKey("field"))
	Expect(keys).To(HaveKey("value"))
})

var _ = It("PrefixBSONKeys", func() {
	var t struct {
		ID    string   `bson:"_id"`
		Field *float64 `bson:"field"`
		Value *int     `bson:"value,omitempty"`
	}

	keys := PrefixBSONKeys(ExtractBSONKeys(t), "sub.")

	Expect(keys).To(HaveLen(3))
	Expect(keys).To(HaveKey("sub._id"))
	Expect(keys).To(HaveKey("sub.field"))
	Expect(keys).To(HaveKey("sub.value"))
})

var _ = It("CombineBSONKeys", func() {
	var t struct {
		ID    string   `bson:"_id"`
		Field *float64 `bson:"field"`
		Value *int     `bson:"value,omitempty"`
	}
	var s struct {
		A string `bson:"a"`
		B string `bson:"b"`
	}

	keys := CombineBSONKeys(ExtractBSONKeys(t), ExtractBSONKeys(s))

	Expect(keys).To(HaveLen(5))
	Expect(keys).To(HaveKey("_id"))
	Expect(keys).To(HaveKey("field"))
	Expect(keys).To(HaveKey("value"))
	Expect(keys).To(HaveKey("a"))
	Expect(keys).To(HaveKey("b"))
})

var _ = Describe("MakeProjectionFromKeys", func() {
	It("No validation set", func() {
		proj, err := MakeProjectionFromKeys(nil, []string{"a", "keyB"})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(3))
		Expect(proj).To(HaveKeyWithValue("a", 1))
		Expect(proj).To(HaveKeyWithValue("keyB", 1))
		Expect(proj).To(HaveKeyWithValue("_id", 0))

		proj, err = MakeProjectionFromKeys(nil, []string{})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(1))
		Expect(proj).To(HaveKeyWithValue("_id", 0))

		proj, err = MakeProjectionFromKeys(nil, []string{"_id"})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(1))
		Expect(proj).To(HaveKeyWithValue("_id", 1))
	})

	It("With validation set", func() {
		set := BSONKeySet{
			"_id":  true,
			"a":    true,
			"keyB": true,
			"keyC": true,
		}

		proj, err := MakeProjectionFromKeys(set, []string{"a", "keyB"})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(3))
		Expect(proj).To(HaveKeyWithValue("a", 1))
		Expect(proj).To(HaveKeyWithValue("keyB", 1))
		Expect(proj).To(HaveKeyWithValue("_id", 0))

		proj, err = MakeProjectionFromKeys(set, []string{})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(1))
		Expect(proj).To(HaveKeyWithValue("_id", 0))

		proj, err = MakeProjectionFromKeys(set, []string{"_id"})
		Expect(err).ToNot(HaveOccurred())
		Expect(proj).To(HaveLen(1))
		Expect(proj).To(HaveKeyWithValue("_id", 1))

		proj, err = MakeProjectionFromKeys(set, []string{"_id", "keyD", "keyC"})
		Expect(err).To(HaveOccurred())
	})

})
