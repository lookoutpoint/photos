// SPDX-License-Identifier: MIT

package utils

import (
	"context"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type BulkWriteModels struct {
	ctx    context.Context
	coll   *mongo.Collection
	limit  int
	l      *log.Entry
	models []mongo.WriteModel
}

func NewBulkWriteModels(ctx context.Context, coll *mongo.Collection, l *log.Entry) *BulkWriteModels {
	m := new(BulkWriteModels)
	m.ctx = ctx
	m.coll = coll
	m.limit = 100
	m.l = l
	return m
}

func (m *BulkWriteModels) Add(model mongo.WriteModel) error {
	m.models = append(m.models, model)
	if len(m.models) >= m.limit {
		if err := m.Write(); err != nil {
			return err
		}
		m.models = nil
	}
	return nil
}

func (m *BulkWriteModels) Write() error {
	if len(m.models) == 0 {
		return nil
	}

	res, err := m.coll.BulkWrite(m.ctx, m.models, options.BulkWrite().SetOrdered(false))
	if err != nil {
		m.l.WithError(err).Error("Could not do bulk write")
		return err
	}

	m.l.Infof("Bulk wrote %v documents", res.ModifiedCount)
	return nil
}
