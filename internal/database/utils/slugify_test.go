// SPDX-License-Identifier: MIT

package utils_test

import (
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/lookoutpoint/photos/internal/database/utils"
)

var _ = It("SlugifyToken", func() {
	Expect(SlugifyToken("asdf blah")).To(Equal("asdf-blah"))
	Expect(SlugifyToken("Asdf, Blah!")).To(Equal("Asdf-Blah"))
	Expect(SlugifyToken("!asdf, blah")).To(Equal("asdf-blah"))
	Expect(SlugifyToken("St. Anthony's")).To(Equal("St-Anthony-s"))
	Expect(SlugifyToken("`~!@#$%^&*()first _-+=[]{}\\|second:;'\",.<>/?last")).To(Equal("first-second-last"))
})

var _ = It("SlugifyTokenLower", func() {
	Expect(SlugifyTokenLower("asdf blah")).To(Equal("asdf-blah"))
	Expect(SlugifyTokenLower("Asdf, Blah!")).To(Equal("asdf-blah"))
	Expect(SlugifyTokenLower("!asdf, blah")).To(Equal("asdf-blah"))
	Expect(SlugifyTokenLower("St. Anthony's")).To(Equal("st-anthony-s"))
	Expect(SlugifyTokenLower("`~!@#$%^&*()first _-+=[]{}\\|second:;'\",.<>/?last")).To(Equal("first-second-last"))
})

var _ = It("SlugifyPath", func() {
	Expect(SlugifyPath(strings.Split("@Path/To/Crazy, File!", "/")...)).To(Equal("/Path/To/Crazy-File/"))
	Expect(SlugifyPath(strings.Split("/some path/to/crazy, file!`~@#$%^&*()_-+=[]{}\\|:;'\",.<>?", "/")...)).To(Equal("/some-path/to/crazy-file/"))
})

var _ = It("SlugifyPathLower", func() {
	Expect(SlugifyPathLower(strings.Split("@Path/To/Crazy, File!", "/")...)).To(Equal("/path/to/crazy-file/"))
	Expect(SlugifyPathLower(strings.Split("/some path/to/crazy, file!`~@#$%^&*()_-+=[]{}\\|:;'\",.<>?", "/")...)).To(Equal("/some-path/to/crazy-file/"))
})
