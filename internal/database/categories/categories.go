// SPDX-License-Identifier: MIT

package categories

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Categories
type apiImpl struct {
	mdb *mongo.Database
	top *apis.APIs
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database, top *apis.APIs) apis.Categories {
	return &apiImpl{mdb: mdb, top: top}
}

const collectionName = "photos-categories"

// ViewByRelatedPark is the name of the collection that mirrors the
// categories collection but documents are unwound by their "related park category".
const viewByRelatedPark = collectionName + "-view-by-related-park"

var categoryEntryBSONKeys = utils.ExtractBSONKeys(docs.CategoryEntry{})

// CategorySeparator is the string that divdes category components
const CategorySeparator = ":"

func CanonicalizeCategory(category string) string {
	parts := strings.Split(category, CategorySeparator)
	for i, _ := range parts {
		parts[i] = strings.TrimSpace(parts[i])
	}
	return strings.Join(parts, CategorySeparator)
}

// Check if string is a valid category. Must have at least two components, each a non-empty string.
func IsValidCategory(category string) bool {
	parts := strings.Split(CanonicalizeCategory(category), CategorySeparator)
	if len(parts) < 2 {
		return false
	}

	for _, part := range parts {
		if len(part) == 0 {
			return false
		}
	}

	return true
}

// SlugifyToken converts a category or category to a slug
func SlugifyToken(s string) string {
	return utils.SlugifyTokenLower(s)
}

// SlugifyCategory converts a category+value string into a slug.
// The category string should be two tokens separated by the CategorySeparator.
func SlugifyCategory(category string) string {
	parts := strings.Split(category, CategorySeparator)
	return utils.SlugifyPathLower(parts...)
}

// Collection returns the category collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := CategorySeparator
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(categoryEntryBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the docs for the given category. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, category string) (string, error) {
	l := log.WithFields(log.Fields{
		"func":     "categories.apiImpl.Create",
		"category": category,
	})

	if !IsValidCategory(category) {
		l.Error("Invalid category")
		return "", apis.ErrInvalidArgs
	}

	return groups.Create(ctx, api, category, false, &groups.CreateOptions{LowerCase: true})
}

// Get returns information about one category id (can be a category or a category-value)
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.CategoryEntry, error) {
	l := log.WithFields(log.Fields{
		"func": "categories.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(categoryEntryBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, options.FindOne().SetProjection(proj))

	var doc docs.CategoryEntry
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode category entry document")
		}
		return nil, err
	}

	return &doc, nil
}

// Get returns information about multiple category ids
func (api *apiImpl) GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.CategoryEntry, error) {
	l := log.WithFields(log.Fields{
		"func": "categories.apiImpl.GetMulti",
		"ids":  ids,
	})

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(categoryEntryBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": bson.M{"$in": ids}}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query category documents")
		return nil, err
	}

	var docs []docs.CategoryEntry
	if err := cursor.All(ctx, &docs); err != nil {
		l.WithError(err).Error("Could not find and decode category documents")
		return nil, err
	}

	return docs, nil
}

func (api *apiImpl) GetOrCreate(ctx context.Context, category string, projKeys []string) (*docs.CategoryEntry, error) {
	l := log.WithFields(log.Fields{
		"func":     "categories.apiImpl.GetOrCreate",
		"category": category,
	})

	if !IsValidCategory(category) {
		l.Error("Invalid category")
		return nil, apis.ErrInvalidArgs
	}

	id := SlugifyCategory(category)
	tokensAll := []string{docs.VisTokenAll}

	// Try get
	if ce, err := api.Get(ctx, id, projKeys, tokensAll); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing category")
		}
		return ce, err
	}

	// Create
	if _, err := api.Create(ctx, category); err != nil {
		l.WithError(err).Error("Could not create category")
		return nil, err
	}

	return api.Get(ctx, id, projKeys, tokensAll)
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.CategoryEntry, error) {
	var results []docs.CategoryEntry
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error {
	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	return groups.Update(ctx, api, id, update, nil)
}

func (api *apiImpl) UpdateRelated(ctx context.Context, catID string) error {
	l := log.WithFields(log.Fields{
		"func":  "categories.apiImpl.UpdateRelated",
		"catID": catID,
	})

	if !utils.IsValidSlugPath(catID) {
		l.Error("Invalid category id")
		return apis.ErrInvalidID
	}

	// Update the related categories for each category-value.
	filter := bson.M{
		"_id":   bson.M{"$regex": "^" + catID},
		"depth": 2,
	}

	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(bson.M{"_id": 1}))
	if err != nil {
		l.WithError(err).Error("Could not query for category-values")
		return err
	}

	for cursor.Next(ctx) {
		var cv docs.CategoryEntry
		if err := cursor.Decode(&cv); err != nil {
			l.WithError(err).Error("Could not decode document")
			return err
		}

		if err := api.updatedRelatedOne(ctx, *cv.ID); err != nil {
			l.WithError(err).WithField("id", *cv.ID).Error("Could not update related for category-value")
			return err
		}
	}
	if err := cursor.Err(); err != nil {
		l.WithError(err).Error("Could not iterate over cursor")
		return err
	}

	l.Info("Successfully updated category related info")

	return nil
}

func (api *apiImpl) updatedRelatedOne(ctx context.Context, id string) error {
	l := log.WithFields(log.Fields{
		"func": "categories.apiImpl.updateRelatedOne",
		"id":   id,
	})

	related := map[string][]string{}

	// Find related category ids.
	pipeline := bson.A{
		bson.M{"$match": bson.M{"categories": id}},
		bson.M{"$project": bson.M{"_id": 0, "categories": 1}},
		bson.M{"$unwind": "$categories"},
		bson.M{"$group": bson.M{"_id": "$categories"}},
	}

	cursor, err := api.top.Photos.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not execute aggregation pipeline for categories")
		return err
	}

	for cursor.Next(ctx) {
		var result bson.M
		if err := cursor.Decode(&result); err != nil {
			l.WithError(err).Error("Could not category decode result")
			return err
		}

		relatedID := result["_id"].(string)

		// Skip if same as id.
		if relatedID == id {
			continue
		}

		parts := strings.Split(relatedID[1:len(relatedID)-1], "/")
		cat := parts[0]
		value := parts[1]

		key := "cat:" + cat
		related[key] = append(related[key], value)
	}

	// Find related locations.
	pipeline = bson.A{
		bson.M{"$match": bson.M{"categories": id, "locations": bson.M{"$exists": true}}},
		bson.M{"$project": bson.M{"_id": 0, "locations": 1}},
		bson.M{"$unwind": "$locations"},
		bson.M{"$group": bson.M{"_id": "$locations"}},
	}

	cursor, err = api.top.Photos.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not execute aggregation pipeline for locations")
		return err
	}

	for cursor.Next(ctx) {
		var result bson.M
		if err := cursor.Decode(&result); err != nil {
			l.WithError(err).Error("Could not decode location result")
			return err
		}

		locationID := result["_id"].(string)

		related["location"] = append(related["location"], locationID)
	}

	// Set related field.
	update := bson.M{"$set": bson.M{"related": related}}
	_, err = api.Collection().UpdateOne(ctx, bson.M{"_id": id}, update)
	if err != nil {
		l.WithError(err).Error("Could not update related map")
		return err
	}

	return nil
}

func (api *apiImpl) ViewByRelatedParkCollection() *mongo.Collection {
	return api.mdb.Collection(viewByRelatedPark)
}

func (api *apiImpl) UpdateCustomViews(ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"func": "categories.apiImpl.UpdateCustomViews",
	})

	// This view creates a document for each category for each related.cat:park value it has.
	pipeline := bson.A{
		bson.M{"$unwind": bson.M{
			"path":                       "$related.cat:park",
			"preserveNullAndEmptyArrays": true,
		}},
		bson.M{"$addFields": bson.M{
			"_id": bson.M{
				"$cond": bson.A{
					bson.M{"$ne": bson.A{
						bson.M{"$type": "$related.cat:park"},
						"missing",
					}},
					bson.M{
						"$concat": bson.A{"$_id", "@/park/", "$related.cat:park", "/"},
					},
					"$_id",
				},
			},
			"related.cat:park": bson.A{"$related.cat:park"},
			"origID":           "$_id",
		}},
		bson.M{"$out": viewByRelatedPark},
	}

	_, err := api.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline to create ViewByRelatedPark")
		return err
	}

	return nil
}
