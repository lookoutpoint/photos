// SPDX-License-Identifier: MIT

package categories_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Categories", func() {
	tokensAll := []string{docs.VisTokenAll}

	addPhoto := func(objID string, keywords ...string) {
		md := &photomd.Metadata{
			Width:  100,
			Height: 200,
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:02 03:04:05",
			},
			Xmp: photomd.XmpData{
				Subjects: keywords,
			},
		}

		_, err := db.Photos.AddOrUpdate(ctx, "bucket", objID, 100, 100, md)
		Expect(err).ToNot(HaveOccurred())
	}

	Describe("DiffAndUpdate", func() {
		matchCatPhotoCount := func(valueCount int, photoCount int, token ...string) OmegaMatcher {
			if len(token) > 1 {
				panic("At most one token")
			}

			counts := VisTokenCounts{docs.VisTokenAll: valueCount + photoCount}
			if len(token) > 0 {
				counts[token[0]] = photoCount
			}

			return MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 1}, counts)
		}

		matchValPhotoCount := func(count int, token ...string) OmegaMatcher {
			if len(token) > 1 {
				panic("At most one token")
			}

			counts := VisTokenCounts{docs.VisTokenAll: count + 1}
			if len(token) > 0 {
				counts[token[0]] = count
			}

			return MatchGroupBasePtrVisTokenCounts(counts, nil)
		}

		createCV := func(cv string) {
			_, err := db.Categories.Create(ctx, cv)
			Expect(err).ToNot(HaveOccurred())
		}

		It("Create, Get, GetOrCreate", func() {
			Expect(db.Categories.Create(ctx, "cat:value")).To(Equal("/cat/value/"))

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("cat")),
				"GroupBase.Depth":   PointTo(Equal(1)),
			}))
			Expect(db.Categories.Get(ctx, "/cat/value/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("cat:value")),
				"GroupBase.Depth":   PointTo(Equal(2)),
			}))

			Expect(db.Categories.GetOrCreate(ctx, "cat:value", nil)).ToNot(BeNil())

			// Special display names
			Expect(db.Categories.Create(ctx, "Super Hikes:So Awesome Path/Trail!")).To(Equal("/super-hikes/so-awesome-path-trail/"))

			Expect(db.Categories.Get(ctx, "/super-hikes/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("Super Hikes")),
			}))
			Expect(db.Categories.Get(ctx, "/super-hikes/so-awesome-path-trail/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("Super Hikes:So Awesome Path/Trail!")),
			}))

		})

		It("Create multi-level category", func() {
			Expect(db.Categories.Create(ctx, "a:b:c")).To(Equal("/a/b/c/"))

			Expect(db.Categories.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("a")),
			}))
			Expect(db.Categories.Get(ctx, "/a/b/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("a:b")),
			}))
			Expect(db.Categories.Get(ctx, "/a/b/c/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("a:b:c")),
			}))

		})

		It("GetMulti", func() {
			Expect(db.Categories.Create(ctx, "pet:cat")).ToNot(HaveLen(0))
			Expect(db.Categories.Create(ctx, "pet:dog")).ToNot(HaveLen(0))
			Expect(db.Categories.Create(ctx, "color:white")).ToNot(HaveLen(0))

			Expect(db.Categories.GetMulti(ctx, []string{"/pet/cat/", "/color/white/", "/pet/dog/"}, nil, tokensAll)).To(
				ConsistOf(
					MatchGroupBase(Fields{"GroupBase.ID": PointTo(Equal("/pet/cat/"))}),
					MatchGroupBase(Fields{"GroupBase.ID": PointTo(Equal("/pet/dog/"))}),
					MatchGroupBase(Fields{"GroupBase.ID": PointTo(Equal("/color/white/"))}),
				),
			)
		})

		It("Simple category values", func() {
			var cvs []string

			createCV("cat:fur")
			createCV("dog:white")

			// Simulate adding two category values
			cvs = []string{"/cat/fur/", "/dog/white/"}
			err := db.Categories.DiffAndUpdate(ctx, nil, cvs, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(And(
				matchCatPhotoCount(1, 1),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(1))}),
			))
			Expect(db.Categories.Get(ctx, "/cat/fur/", nil, tokensAll)).To(And(
				matchValPhotoCount(1),
				MatchGroupBasePtr(Fields{"GroupBase.Depth": PointTo(Equal(2))}),
			))
			Expect(db.Categories.Get(ctx, "/dog/", nil, tokensAll)).To(matchCatPhotoCount(1, 1))
			Expect(db.Categories.Get(ctx, "/dog/white/", nil, tokensAll)).To(matchValPhotoCount(1))

			// Simulate adding another category value
			createCV("park:local")

			oldCvs := cvs
			cvs = []string{"/park/local/", "/dog/white/", "/cat/fur/"}
			err = db.Categories.DiffAndUpdate(ctx, oldCvs, cvs, tokensAll, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchCatPhotoCount(1, 1))
			Expect(db.Categories.Get(ctx, "/cat/fur/", nil, tokensAll)).To(matchValPhotoCount(1))
			Expect(db.Categories.Get(ctx, "/dog/", nil, tokensAll)).To(matchCatPhotoCount(1, 1))
			Expect(db.Categories.Get(ctx, "/dog/white/", nil, tokensAll)).To(matchValPhotoCount(1))
			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(matchCatPhotoCount(1, 1))
			Expect(db.Categories.Get(ctx, "/park/local/", nil, tokensAll)).To(matchValPhotoCount(1))

			// Simulate renaming category value and adding token
			createCV("park:somewhere")
			createCV("dog:black")
			T := "/T/"

			oldCvs = cvs
			cvs = []string{"/park/somewhere/", "/dog/black/", "/cat/fur/"}
			err = db.Categories.DiffAndUpdate(ctx, oldCvs, cvs, tokensAll, []string{docs.VisTokenAll, T}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchCatPhotoCount(1, 1, T))
			Expect(db.Categories.Get(ctx, "/cat/fur/", nil, tokensAll)).To(matchValPhotoCount(1, T))
			Expect(db.Categories.Get(ctx, "/dog/", nil, tokensAll)).To(matchCatPhotoCount(2, 1, T))
			Expect(db.Categories.Get(ctx, "/dog/white/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/dog/black/", nil, tokensAll)).To(matchValPhotoCount(1, T))
			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(matchCatPhotoCount(2, 1, T))
			Expect(db.Categories.Get(ctx, "/park/local/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/park/somewhere/", nil, tokensAll)).To(matchValPhotoCount(1, T))

			// Simulate removal of all keywords
			oldCvs = cvs
			err = db.Categories.DiffAndUpdate(ctx, oldCvs, nil, []string{docs.VisTokenAll, T}, nil, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchCatPhotoCount(1, 0))
			Expect(db.Categories.Get(ctx, "/cat/fur/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/dog/", nil, tokensAll)).To(matchCatPhotoCount(2, 0))
			Expect(db.Categories.Get(ctx, "/dog/white/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/dog/black/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(matchCatPhotoCount(2, 0))
			Expect(db.Categories.Get(ctx, "/park/local/", nil, tokensAll)).To(matchValPhotoCount(0))
			Expect(db.Categories.Get(ctx, "/park/somewhere/", nil, tokensAll)).To(matchValPhotoCount(0))

		})

		It("Multiple values in same category", func() {
			// First value
			createCV("hike:trail abc")

			err := db.Categories.DiffAndUpdate(ctx, nil, []string{"/hike/trail-abc/"}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/hike/", nil, tokensAll)).To(matchCatPhotoCount(1, 1))
			Expect(db.Categories.Get(ctx, "/hike/trail-abc/", nil, tokensAll)).To(matchValPhotoCount(1))

			// Second value
			createCV("hike:trail xyz")

			err = db.Categories.DiffAndUpdate(ctx, nil, []string{"/hike/trail-xyz/"}, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/hike/", nil, tokensAll)).To(matchCatPhotoCount(2, 2))
			Expect(db.Categories.Get(ctx, "/hike/trail-abc/", nil, tokensAll)).To(matchValPhotoCount(1))
			Expect(db.Categories.Get(ctx, "/hike/trail-xyz/", nil, tokensAll)).To(matchValPhotoCount(1))

		})

	})

	Describe("Update", func() {
		It("Display path", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			_, err := db.Categories.Create(ctx, "cat:value")
			Expect(err).ToNot(HaveOccurred())

			// Update category
			Expect(db.Categories.Update(ctx, "/cat/", apis.NewGroupUpdate().SetDisplay("Cat!!"))).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchDisplayPath("Cat!!"))
			Expect(db.Categories.Get(ctx, "/cat/value/", nil, tokensAll)).To(matchDisplayPath("Cat!!:value"))

			// Update category value
			Expect(db.Categories.Update(ctx, "/cat/value/", apis.NewGroupUpdate().SetDisplay("Value/Val"))).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchDisplayPath("Cat!!"))
			Expect(db.Categories.Get(ctx, "/cat/value/", nil, tokensAll)).To(matchDisplayPath("Cat!!:Value/Val"))

		})
	})

	Describe("UpdateRelated", func() {
		It("Parks and hikes", func() {
			addPhoto("photo1.jpg", "park: a", "hike: h")
			addPhoto("photo2.jpg", "park: a", "hike: h")
			addPhoto("photo3.jpg", "park: a", "hike: i")
			addPhoto("photo4.jpg", "park: b", "hike: j")

			err := db.Categories.UpdateRelated(ctx, "/hike/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/hike/h/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("a"),
				}),
			}))
			Expect(db.Categories.Get(ctx, "/hike/i/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("a"),
				}),
			}))
			Expect(db.Categories.Get(ctx, "/hike/j/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("b"),
				}),
			}))

		})

		It("Parks and hikes with locations", func() {
			addPhoto := func(objID string, location string, keywords ...string) {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Exif: photomd.ExifData{
						photomd.ExifTagDateTimeOriginal: "2020:01:02 03:04:05",
					},
					Xmp: photomd.XmpData{
						Location: location,
						Subjects: keywords,
					},
				}

				_, err := db.Photos.AddOrUpdate(ctx, "bucket", objID, 100, 100, md)
				Expect(err).ToNot(HaveOccurred())
			}

			addPhoto("photo1.jpg", "la", "park: a", "hike: h")
			addPhoto("photo2.jpg", "la", "park: a", "hike: h")
			addPhoto("photo3.jpg", "la", "park: a", "hike: i")
			addPhoto("photo4.jpg", "lb", "park: b", "hike: j")

			err := db.Categories.UpdateRelated(ctx, "/hike/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Categories.Get(ctx, "/hike/h/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("a"),
					"location": ConsistOf("/la/"),
				}),
			}))
			Expect(db.Categories.Get(ctx, "/hike/i/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("a"),
					"location": ConsistOf("/la/"),
				}),
			}))
			Expect(db.Categories.Get(ctx, "/hike/j/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"Related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("b"),
					"location": ConsistOf("/lb/"),
				}),
			}))

		})
	})

	Describe("ListChildren", func() {
		BeforeEach(func() {
			addPhoto("photo1.jpg", "park: a", "hike: x")
			addPhoto("photo2.jpg", "park: a", "hike: x")
			addPhoto("photo3.jpg", "park: a", "hike: y")
			addPhoto("photo4.jpg", "park: b", "hike: h")
			addPhoto("photo5.jpg", "hike: standalone")
			addPhoto("photo6.jpg", "hike: standalone2")

			Expect(db.Categories.UpdateRelated(ctx, "/hike/")).ToNot(HaveOccurred())
			Expect(db.Categories.UpdateCustomViews(ctx)).ToNot(HaveOccurred())
		})

		It("Default sort", func() {
			list, err := db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(5))

			Expect(list[0].ID).To(PointTo(Equal("/hike/h/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/standalone/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/standalone2/")))
			Expect(list[3].ID).To(PointTo(Equal("/hike/x/")))
			Expect(list[4].ID).To(PointTo(Equal("/hike/y/")))

		})

		It("Sort by related park", func() {
			list, err := db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				CustomSortKeys:   []string{"related.cat:park"},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(5))

			Expect(list[0].ID).To(PointTo(Equal("/hike/standalone/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/standalone2/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/x/")))
			Expect(list[3].ID).To(PointTo(Equal("/hike/y/")))
			Expect(list[4].ID).To(PointTo(Equal("/hike/h/")))

			// List relative to standalone, which has no park.
			list, err = db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				RefID:            "/hike/standalone/",
				CustomSortKeys:   []string{"related.cat:park"},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(4))

			Expect(list[0].ID).To(PointTo(Equal("/hike/standalone2/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/x/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/y/")))
			Expect(list[3].ID).To(PointTo(Equal("/hike/h/")))

			// List relative to standalone2, which has no park.
			list, err = db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				RefID:            "/hike/standalone2/",
				CustomSortKeys:   []string{"related.cat:park"},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(3))

			Expect(list[0].ID).To(PointTo(Equal("/hike/x/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/y/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/h/")))

			// List relative to hike x, related to park a.
			list, err = db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				RefID:            "/hike/x/",
				CustomSortKeys:   []string{"related.cat:park"},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(2))

			Expect(list[0].ID).To(PointTo(Equal("/hike/y/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/h/")))

		})

		It("Sort by related park with hike in multiple parks", func() {
			// Add a photo with a hike in multiple parks
			addPhoto("custom.jpg", "park: a", "park: b", "hike: m")

			Expect(db.Categories.UpdateRelated(ctx, "/hike/")).ToNot(HaveOccurred())
			Expect(db.Categories.UpdateCustomViews(ctx)).ToNot(HaveOccurred())

			// List
			list, err := db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				CustomSortKeys:   []string{"related.cat:park"},
				CustomView:       db.Categories.ViewByRelatedParkCollection().Name(),
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(7))

			Expect(list[0].ID).To(PointTo(Equal("/hike/standalone/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/standalone2/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/m/@/park/a/")))
			Expect(list[3].ID).To(PointTo(Equal("/hike/x/@/park/a/")))
			Expect(list[4].ID).To(PointTo(Equal("/hike/y/@/park/a/")))
			Expect(list[5].ID).To(PointTo(Equal("/hike/h/@/park/b/")))
			Expect(list[6].ID).To(PointTo(Equal("/hike/m/@/park/b/")))

			// List relative to the first hike m
			list, err = db.Categories.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/hike/",
				RefID:            "/hike/m/@/park/a/",
				CustomSortKeys:   []string{"related.cat:park"},
				CustomView:       db.Categories.ViewByRelatedParkCollection().Name(),
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(list).To(HaveLen(4))

			Expect(list[0].ID).To(PointTo(Equal("/hike/x/@/park/a/")))
			Expect(list[1].ID).To(PointTo(Equal("/hike/y/@/park/a/")))
			Expect(list[2].ID).To(PointTo(Equal("/hike/h/@/park/b/")))
			Expect(list[3].ID).To(PointTo(Equal("/hike/m/@/park/b/")))
		})

	})
})
