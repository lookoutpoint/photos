// SPDX-License-Identifier: MIT

package folders

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Folders
type apiImpl struct {
	mdb *mongo.Database
	top *apis.APIs
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database, top *apis.APIs) apis.Folders {
	return &apiImpl{mdb: mdb, top: top}
}

const collectionName = "photos-folders"

var folderBSONKeys = utils.ExtractBSONKeys(docs.Folder{})

// A valid path meets the same criteria as a slug path
func isValidPath(path string) bool {
	return utils.IsValidSlugPath(path)
}

// Collection returns the folder collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// Exists checks if a folder exists with the given id
func (api *apiImpl) Exists(ctx context.Context, id string) bool {
	res := api.Collection().FindOne(ctx, bson.M{"_id": id})
	err := res.Err()
	return err == nil
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := "/"
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(folderBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the folder docs for the given folder path. Path must not already exist.
// Returns the folder's id.
func (api *apiImpl) Create(ctx context.Context, path string) (string, error) {
	l := log.WithFields(log.Fields{
		"func": "folders.apiImpl.Create",
		"path": path,
	})

	if !isValidPath(path) {
		l.Error("Invalid path")
		return "", apis.ErrInvalidFolderPath
	}

	// Display path is the path without the trailing "/"
	display := path[:len(path)-1]

	return groups.Create(ctx, api, display, false, &groups.CreateOptions{
		Model: func(base docs.GroupBase) interface{} {
			// Wrap in folder model
			folder := docs.Folder{
				GroupBase: base,
			}
			folder.SelfVisTokens = []string{}

			// Root folder is also public by default. This allows it to be queried immediately.
			if *folder.ID == "/" {
				folder.SelfVisTokens = append(folder.SelfVisTokens, docs.VisTokenPublic)
			}

			return folder
		},
		PostInsert: func(ctx context.Context, model interface{}) error {
			folder := model.(docs.Folder)
			// Apply self visibility tokens
			if err := api.DiffAndUpdate(ctx, nil, folder.ID, nil, folder.SelfVisTokens, 1); err != nil {
				l.WithError(err).WithField("id", *folder.ID).Error("Could not diff and update to add default self visibility tokens")
				return err
			}
			return nil
		},
	})
}

// Get returns the folder information for the given id
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Folder, error) {
	l := log.WithFields(log.Fields{
		"func": "folders.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Build filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Build projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(folderBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	res := api.Collection().FindOne(ctx, filter, &options.FindOneOptions{Projection: proj})

	var doc docs.Folder
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode folder document")
		}
		return nil, err
	}

	return &doc, nil
}

func (api *apiImpl) GetOrCreate(ctx context.Context, path string, projKeys []string) (*docs.Folder, error) {
	l := log.WithFields(log.Fields{
		"func": "folders.apiImpl.GetOrCreate",
		"path": path,
	})

	if !isValidPath(path) {
		l.Error("Invalid path")
		return nil, apis.ErrInvalidFolderPath
	}

	id := utils.SlugifyPath(strings.Split(path, "/")...)
	tokensAll := []string{docs.VisTokenAll}

	// Try get
	if folder, err := api.Get(ctx, id, projKeys, tokensAll); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing folder")
		}
		return folder, err
	}

	// Create
	if _, err := api.Create(ctx, path); err != nil {
		l.WithError(err).Error("Could not create folder")
		return nil, err
	}

	return api.Get(ctx, id, projKeys, tokensAll)
}

// ListChildren returns an array of folder documents for each sub folder of the given folder path.
// The folders are sorted alphabetically by name. If no projection keys are provided (nil), the
// default projection is just the folder id.
func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.Folder, error) {
	var results []docs.Folder
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) DiffAndUpdate(ctx context.Context, oldID *string, newID *string, oldTokens []string, newTokens []string, count int) error {
	var oldIDs []string
	if oldID != nil {
		oldIDs = []string{*oldID}
	}

	var newIDs []string
	if newID != nil {
		newIDs = []string{*newID}
	}

	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	return groups.Update(ctx, api, id, update, nil)
}

// AddVisibilityToken to the given folder and targets. The token is added as a self visibility token on the folder.
func (api *apiImpl) AddVisibilityToken(ctx context.Context, id string, target apis.FolderVisibilityTokenTarget, token string) error {
	l := log.WithFields(log.Fields{
		"func":   "folders.apiImpl.AddVisibilityToken",
		"id":     id,
		"target": target,
	})

	err := target.Validate()
	if err != nil {
		l.WithError(err).Error("Invalid target")
		return err
	}

	// Use a transaction as there are potentially multiple updates to be done and everything should be consistent.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Add token to folder.
		res, err := api.Collection().UpdateOne(
			ctx,
			bson.M{"_id": id},
			bson.M{"$addToSet": bson.M{
				"selfVisTokens": token,
			}},
		)
		if err != nil {
			l.WithError(err).Warn("Could not add self visibility token")
			return nil, err
		} else if res.MatchedCount != 1 {
			l.Warn("Could not find folder")
			return nil, apis.ErrFolderNotFound
		} else if res.ModifiedCount == 1 {
			// Added visibility token.
			// Update via diff and update to mimic addition of a photo with token.
			err = api.DiffAndUpdate(ctx, nil, &id, nil, []string{token}, 1)
			if err != nil {
				l.WithError(err).Warn("Could not diff and update to add token to folder")
				return nil, err
			}
		}

		if target != apis.FolderTarget {
			// Add tokens to photos as inherited visibility tokens.
			err = api.top.Photos.AddInheritedVisibilityToken(ctx, id, target == apis.FolderTargetFullTree, token)
			if err != nil {
				l.WithError(err).Warn("Could not add inherited visibility token to photos")
				return nil, err
			}
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}

// DeleteVisibilityToken from the given folder. The token must be a self-visibility token.
func (api *apiImpl) DeleteVisibilityToken(ctx context.Context, id string, target apis.FolderVisibilityTokenTarget, token string) error {
	l := log.WithFields(log.Fields{
		"func":   "folders.apiImpl.DeleteVisibilityToken",
		"id":     id,
		"target": target,
	})

	err := target.Validate()
	if err != nil {
		l.WithError(err).Error("Invalid target")
		return err
	}

	// Use a transaction as there are potentially multiple updates to be done and everything should be consistent.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Delete token to folder.
		res, err := api.Collection().UpdateOne(
			ctx,
			bson.M{"_id": id},
			bson.M{"$pull": bson.M{
				"selfVisTokens": token,
				// visibilityTokens will be updated via the diff and update
			}},
		)
		if err != nil {
			l.WithError(err).Warn("Could not delete self visibility token")
			return nil, err
		} else if res.MatchedCount != 1 {
			l.Warn("Could not find folder")
			return nil, apis.ErrFolderNotFound
		} else if res.ModifiedCount != 1 {
			// Token not on folder, so do nothing more.
			l.Warn("Token not on folder")
			return nil, nil
		}

		if target != apis.FolderTarget {
			// Delete tokens to photos as inherited visibility token.
			err = api.top.Photos.DeleteInheritedVisibilityToken(ctx, id, target == apis.FolderTargetFullTree, token)
			if err != nil {
				l.WithError(err).Warn("Could not delete inherited visibility token from photos")
				return nil, err
			}
		}

		// Update via diff and update to mimic deletion of token in child.
		err = api.DiffAndUpdate(ctx, &id, nil, []string{token}, nil, 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update to delete token from folder")
			return nil, err
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}
