// SPDX-License-Identifier: MIT

package folders_test

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	gtypes "github.com/onsi/gomega/types"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

func BeCloseToNow() gtypes.GomegaMatcher {
	return BeTemporally("~", time.Now().UTC(), 1*time.Second)
}

var _ = Describe("Folders", func() {
	tokensAll := []string{docs.VisTokenAll}

	BeforeEach(func() {
		clearTestDatabase()
	})

	Describe("Basic create and get", func() {
		It("True root folder", func() {
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())

			// Check folder info
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":                       PointTo(Equal("/")),
				"GroupBase.Display":                  PointTo(Equal("")),
				"GroupBase.Timestamp":                PointTo(BeCloseToNow()),
				"GroupBase.Depth":                    PointTo(Equal(0)),
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"GroupBase.VisTokenCounts":           MatchAllKeys(Keys{docs.VisTokenAll: Equal(1), docs.VisTokenPublic: Equal(1)}),
				"GroupBase.DescVisTokenCounts":       MatchAllKeys(Keys{docs.VisTokenAll: Equal(1), docs.VisTokenPublic: Equal(1)}),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))
		})

		It("Create same folder", func() {
			_, err := db.Folders.Create(ctx, "/root folder/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/root folder/")
			Expect(err).To(HaveOccurred())

			_, err = db.Folders.GetOrCreate(ctx, "/root folder/", nil)
			Expect(err).ToNot(HaveOccurred())

		})

		It("Invalid empty folder name", func() {
			_, err := db.Folders.Create(ctx, "//")
			Expect(err).To(HaveOccurred())
		})

		It("One level folders", func() {
			_, err := db.Folders.Create(ctx, "/root folder/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/another root/")
			Expect(err).ToNot(HaveOccurred())

			// Check folder info
			Expect(db.Folders.Get(ctx, "/root-folder/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":                       PointTo(Equal("/root-folder/")),
				"GroupBase.Timestamp":                PointTo(BeCloseToNow()),
				"GroupBase.Depth":                    PointTo(Equal(1)),
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"GroupBase.Display":                  PointTo(Equal("root folder")),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/another-root/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":                       PointTo(Equal("/another-root/")),
				"GroupBase.Timestamp":                PointTo(BeCloseToNow()),
				"GroupBase.Depth":                    PointTo(Equal(1)),
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"GroupBase.Display":                  PointTo(Equal("another root")),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":                       PointTo(Equal("/")),
				"GroupBase.Timestamp":                PointTo(BeCloseToNow()),
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"GroupBase.Display":                  PointTo(Equal("")),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))

		})

		It("Multi-level folder hierarchy", func() {
			// Create sub folder first, which should create whole chain
			_, err := db.Folders.Create(ctx, "/root suffix/sub folder/Super Trip/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Exists(ctx, "/root-suffix/sub-folder/Super-Trip/")).To(BeTrue())
			Expect(db.Folders.Exists(ctx, "/root-suffix/sub-folder/")).To(BeTrue())
			Expect(db.Folders.Exists(ctx, "/root-suffix/")).To(BeTrue())
			Expect(db.Folders.Exists(ctx, "/")).To(BeTrue())

			// Check folder info
			Expect(db.Folders.Get(ctx, "/root-suffix/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":        PointTo(Equal("/root-suffix/")),
				"GroupBase.Timestamp": PointTo(BeCloseToNow()),
				"GroupBase.Depth":     PointTo(Equal(1)),
				"GroupBase.VisTokens": ConsistOf(docs.VisTokenAll),
				"GroupBase.Display":   PointTo(Equal("root suffix")),
			}))

			Expect(db.Folders.Get(ctx, "/root-suffix/sub-folder/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":        PointTo(Equal("/root-suffix/sub-folder/")),
				"GroupBase.Timestamp": PointTo(BeCloseToNow()),
				"GroupBase.Depth":     PointTo(Equal(2)),
				"GroupBase.VisTokens": ConsistOf(docs.VisTokenAll),
				"GroupBase.Display":   PointTo(Equal("root suffix/sub folder")),
			}))

			Expect(db.Folders.Get(ctx, "/root-suffix/sub-folder/Super-Trip/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID":        PointTo(Equal("/root-suffix/sub-folder/Super-Trip/")),
				"GroupBase.Timestamp": PointTo(BeCloseToNow()),
				"GroupBase.Depth":     PointTo(Equal(3)),
				"GroupBase.VisTokens": ConsistOf(docs.VisTokenAll),
				"GroupBase.Display":   PointTo(Equal("root suffix/sub folder/Super Trip")),
			}))

		})

		It("invalid paths", func() {
			_, err := db.Folders.Create(ctx, "invalid-root-path")
			Expect(err).To(HaveOccurred())

			_, err = db.Folders.Create(ctx, "missing-prefix/")
			Expect(err).To(HaveOccurred())

			Expect(db.Folders.Exists(ctx, "invalid-root-path")).To(BeFalse())
			Expect(db.Folders.Exists(ctx, "missing-prefix/")).To(BeFalse())
		})

	})

	Describe("ListChildren", func() {
		projKeys := []string{"id", "display"}

		It("Empty", func() {
			// Create root folder
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())

			folders, err := db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(0))
		})

		It("One child", func() {
			_, err := db.Folders.Create(ctx, "/My Trips/Wonderful Place/")
			Expect(err).ToNot(HaveOccurred())

			folders, err := db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(1))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/")),
				"GroupBase.Display": PointTo(Equal("My Trips")),
			}))

			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/My-Trips/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(1))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Wonderful-Place/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Wonderful Place")),
			}))

			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/My-Trips/Wonderful-Place/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(0))
		})

		It("Multiple children", func() {
			_, err := db.Folders.Create(ctx, "/My Trips/Park XYZ/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/My Trips/City ABC/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/My Trips/Fun Place/")
			Expect(err).ToNot(HaveOccurred())

			// List root subfolders
			folders, err := db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(1))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/")),
				"GroupBase.Display": PointTo(Equal("My Trips")),
			}))

			// List /My Trips subfolders
			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/My-Trips/",
				ProjectionKeys:   projKeys,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(3))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/City-ABC/")),
				"GroupBase.Display": PointTo(Equal("My Trips/City ABC")),
			}))
			Expect(folders[1]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Fun-Place/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Fun Place")),
			}))
			Expect(folders[2]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Park-XYZ/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Park XYZ")),
			}))

			// List /My Trips subfolders in reverse order
			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/My-Trips/",
				ProjectionKeys:   projKeys,
				RefOp:            list.RefOpLt,
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(3))
			Expect(folders[2]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/City-ABC/")),
				"GroupBase.Display": PointTo(Equal("My Trips/City ABC")),
			}))
			Expect(folders[1]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Fun-Place/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Fun Place")),
			}))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Park-XYZ/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Park XYZ")),
			}))

			// List /My Trips subfolders in reverse order starting after Park XYZ
			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/My-Trips/",
				ProjectionKeys:   projKeys,
				RefOp:            list.RefOpLt,
				RefID:            "/My-Trips/Park-XYZ/",
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(folders).To(HaveLen(2))
			Expect(folders[1]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/City-ABC/")),
				"GroupBase.Display": PointTo(Equal("My Trips/City ABC")),
			}))
			Expect(folders[0]).To(MatchGroupBase(Fields{
				"GroupBase.ID":      PointTo(Equal("/My-Trips/Fun-Place/")),
				"GroupBase.Display": PointTo(Equal("My Trips/Fun Place")),
			}))

		})
	})

	Describe("DiffAndUpdate", func() {
		It("Root folder", func() {
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				nil,
			))

			// Add token
			T := "/T/"
			f := "/"

			Expect(db.Folders.DiffAndUpdate(ctx, nil, &f, nil, []string{T}, 1)).ToNot(HaveOccurred())
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1, T: 1},
				nil,
			))

			// Change token
			T2 := "/T2/"

			Expect(db.Folders.DiffAndUpdate(ctx, &f, &f, []string{T}, []string{T2}, 1)).ToNot(HaveOccurred())
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1, T2: 1},
				nil,
			))

			// Remove token
			Expect(db.Folders.DiffAndUpdate(ctx, &f, nil, []string{T2}, nil, 1)).ToNot(HaveOccurred())
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				nil,
			))

		})

		It("Two folders, two photos, no tokens", func() {
			idA := "/a/"
			idB := "/b/"
			tokens := []string{docs.VisTokenAll}

			_, err := db.Folders.Create(ctx, idA)
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, idB)
			Expect(err).ToNot(HaveOccurred())

			matchPhotoCount := func(count int) OmegaMatcher {
				// +1 for folder's self visibility token
				return MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: count + 1}, nil)
			}

			// Simulate add to A
			err = db.Folders.DiffAndUpdate(ctx, nil, &idA, nil, tokens, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, idA, nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Folders.Get(ctx, idB, nil, tokensAll)).To(matchPhotoCount(0))

			// Simulate add to B
			err = db.Folders.DiffAndUpdate(ctx, nil, &idB, nil, tokens, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, idA, nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Folders.Get(ctx, idB, nil, tokensAll)).To(matchPhotoCount(1))

			// Simulate a move from A to B
			err = db.Folders.DiffAndUpdate(ctx, &idA, &idB, tokens, tokens, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, idA, nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Folders.Get(ctx, idB, nil, tokensAll)).To(matchPhotoCount(2))

			// Simulate deletion from B
			err = db.Folders.DiffAndUpdate(ctx, &idB, nil, tokens, nil, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, idA, nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Folders.Get(ctx, idB, nil, tokensAll)).To(matchPhotoCount(1))

		})

		It("Nested folder add tokens", func() {
			A := "/a/"
			B := "/b/"
			C := "/c/"

			_, err := db.Folders.Create(ctx, "/nested/folder/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				nil,
			))

			// Add tokens
			f := "/nested/folder/"
			err = db.Folders.DiffAndUpdate(ctx, nil, &f, nil, []string{A, B}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 2, A: 1, B: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, A: 1, B: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, A: 1, B: 1},
				nil,
			))

			// Rename token
			err = db.Folders.DiffAndUpdate(ctx, &f, &f, []string{A, B}, []string{C, B}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 2, C: 1, B: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, C: 1, B: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, C: 1, B: 1},
				nil,
			))

			// Delete tokens
			err = db.Folders.DiffAndUpdate(ctx, &f, &f, []string{B, C}, []string{}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1},
			))
			Expect(db.Folders.Get(ctx, "/nested/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				nil,
			))

		})
	})

	Describe("Update", func() {
		Describe("Display path", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			It("Root folder", func() {
				_, err := db.Folders.Create(ctx, "/")
				Expect(err).ToNot(HaveOccurred())

				// Cannot update display path of root folder
				Expect(db.Folders.Update(ctx, "/", apis.NewGroupUpdate().SetDisplay("asdf"))).To(HaveOccurred())

				Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(matchDisplayPath(""))
			})

			It("Folder hierarchy", func() {
				_, err := db.Folders.Create(ctx, "/a/b/c/")
				Expect(err).ToNot(HaveOccurred())

				// Update /a/
				Expect(db.Folders.Update(ctx, "/a/", apis.NewGroupUpdate().SetDisplay("A!!"))).ToNot(HaveOccurred())

				Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(matchDisplayPath(""))
				Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(matchDisplayPath("A!!"))
				Expect(db.Folders.Get(ctx, "/a/b/", nil, tokensAll)).To(matchDisplayPath("A!!/b"))
				Expect(db.Folders.Get(ctx, "/a/b/c/", nil, tokensAll)).To(matchDisplayPath("A!!/b/c"))

				// Update /a/b/
				Expect(db.Folders.Update(ctx, "/a/b/", apis.NewGroupUpdate().SetDisplay("Blah Folder"))).ToNot(HaveOccurred())

				Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(matchDisplayPath(""))
				Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(matchDisplayPath("A!!"))
				Expect(db.Folders.Get(ctx, "/a/b/", nil, tokensAll)).To(matchDisplayPath("A!!/Blah Folder"))
				Expect(db.Folders.Get(ctx, "/a/b/c/", nil, tokensAll)).To(matchDisplayPath("A!!/Blah Folder/c"))

			})
		})

		It("Sort order", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBase(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			_, err := db.Folders.Create(ctx, "/a/")
			Expect(err).ToNot(HaveOccurred())
			_, err = db.Folders.Create(ctx, "/b/")
			Expect(err).ToNot(HaveOccurred())

			// Listing should sort alphabetically
			folders, err := db.Folders.ListChildren(ctx, &apis.GroupListParams{ID: "/",
				ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll}})
			Expect(err).ToNot(HaveOccurred())

			Expect(folders).To(HaveLen(2))
			Expect(folders[0]).To(matchDisplayPath("a"))
			Expect(folders[1]).To(matchDisplayPath("b"))

			// Set sort order on both folders
			Expect(db.Folders.Update(ctx, "/a/", apis.NewGroupUpdate().SetSortOrder(100))).ToNot(HaveOccurred())
			Expect(db.Folders.Update(ctx, "/b/", apis.NewGroupUpdate().SetSortOrder(50))).ToNot(HaveOccurred())

			// Listing should sort by sort order
			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{ID: "/",
				ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll}})
			Expect(err).ToNot(HaveOccurred())

			Expect(folders).To(HaveLen(2))
			Expect(folders[0]).To(matchDisplayPath("b"))
			Expect(folders[1]).To(matchDisplayPath("a"))

			// Remove sort order on both folders
			Expect(db.Folders.Update(ctx, "/a/", apis.NewGroupUpdate().SetSortOrder(0))).ToNot(HaveOccurred())
			Expect(db.Folders.Update(ctx, "/b/", apis.NewGroupUpdate().SetSortOrder(0))).ToNot(HaveOccurred())

			// Listing should sort alphabetically
			folders, err = db.Folders.ListChildren(ctx, &apis.GroupListParams{ID: "/",
				ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll}})
			Expect(err).ToNot(HaveOccurred())

			Expect(folders).To(HaveLen(2))
			Expect(folders[0]).To(matchDisplayPath("a"))
			Expect(folders[1]).To(matchDisplayPath("b"))

		})

		It("Meta description", func() {
			_, err := db.Folders.Create(ctx, "/a/")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Update(ctx, "/a/", apis.NewGroupUpdate().SetMetaDescription("description!"))).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/a/", []string{"metaDescription"}, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.MetaDescription": PointTo(Equal("description!")),
			}))
		})
	})

	Describe("AddVisibilityToken and DeleteVisibilityToken", func() {
		addPhoto := func(objectID string, title string, keywords ...string) docs.PhotoID {
			md := photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    title,
					Subjects: keywords,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
				},
			}

			photo, err := db.Photos.AddOrUpdate(ctx, "", objectID, 0, 100, &md)
			Expect(err).ToNot(HaveOccurred())
			return *photo.ID
		}

		var photo1 docs.PhotoID
		var photo2 docs.PhotoID
		var photo3 docs.PhotoID
		var photo4 docs.PhotoID
		var photo5 docs.PhotoID
		var T string
		var X string

		BeforeEach(func() {
			photo1 = addPhoto("a/x/photo1.jpg", "photo1")
			photo2 = addPhoto("a/x/photo2.jpg", "photo2")
			photo3 = addPhoto("a/y/photo3.jpg", "photo3")
			photo4 = addPhoto("a/y/photo4.jpg", "photo4", "__visibility:x")
			photo5 = addPhoto("a/z/photo5.jpg", "photo4", "__visibility:x")

			X = "/x/"

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			var err error
			T, err = db.Visibility.Create(ctx, "T")
			Expect(err).ToNot(HaveOccurred())
		})

		It("Add public visibility token to root folder (duplicate)", func() {
			Expect(db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Should be no change
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))

		})

		It("Delete non-existent visibility token", func() {
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/", apis.FolderTargetFullTree, T)).ToNot(HaveOccurred())

			// Should be no change
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))

		})

		It("Add visibility token to root folder", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTarget, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1, T: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/", apis.FolderTargetFullTree, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

		It("Add visibility token to /a folder and photos", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/a/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2, T: 1},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, T: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/a/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

		It("Add visibility token to /a folder full tree", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/a/", apis.FolderTargetFullTree, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2, T: 4},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, T: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2, T: 3},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, T: 2},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1, T: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/a/", apis.FolderTargetFullTree, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

		It("Add visibility token to /a/x folder and photos", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/a/x/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2, T: 3},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2, T: 3},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, T: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/a/x/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

		It("Add visibility token to /a/y folder and photos", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/a/y/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2, T: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2, T: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1, T: 2},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens(T))
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/a/y/", apis.FolderTargetFullTree, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

		It("Add visibility token to /a/z folder and photos", func() {
			// Add
			Expect(db.Folders.AddVisibilityToken(ctx, "/a/z/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2, T: 1},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2, T: 1},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1, T: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

			// Delete
			Expect(db.Folders.DeleteVisibilityToken(ctx, "/a/z/", apis.FolderTargetPhotos, T)).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 9, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 8, X: 2},
			))
			Expect(db.Folders.Get(ctx, "/a/x/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/y/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, X: 1},
				nil,
			))
			Expect(db.Folders.Get(ctx, "/a/z/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, X: 1},
				nil,
			))

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo3, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo4, nil, tokensAll)).To(MatchVisibilityTokens(X))
			Expect(db.Photos.Get(ctx, photo5, nil, tokensAll)).To(MatchVisibilityTokens(X))

		})

	})

})
