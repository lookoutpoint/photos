// SPDX-License-Identifier: MIT

package database

import (
	"context"
	"os"
	"strings"
	"sync"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/pages"
	"golang.org/x/sync/errgroup"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/events"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"

	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/categories"
	"gitlab.com/lookoutpoint/photos/internal/database/dates"
	"gitlab.com/lookoutpoint/photos/internal/database/folders"
	"gitlab.com/lookoutpoint/photos/internal/database/keywords"
	"gitlab.com/lookoutpoint/photos/internal/database/locations"
	"gitlab.com/lookoutpoint/photos/internal/database/photos"
	"gitlab.com/lookoutpoint/photos/internal/database/timeline_groups"
	"gitlab.com/lookoutpoint/photos/internal/database/users"
	"gitlab.com/lookoutpoint/photos/internal/database/visibility"
	"gitlab.com/lookoutpoint/photos/internal/utils"
)

// Database interface abstracts database operations
type Database struct {
	Mdb *mongo.Database

	apis.APIs
}

var theDatabase *Database

// Set the database object to use
func Set(db *Database) {
	theDatabase = db
}

// Get the database object to use
func Get() *Database {
	return theDatabase
}

// MongoDatabaseName is the name of the database in MongoDB
var MongoDatabaseName = utils.GetCloudProjectID()

// New returns a new database object that connects to the real database
func New() (*Database, error) {
	db := &Database{}

	ctx := context.Background()

	var err error
	client, err := mongo.NewClient(options.Client().
		ApplyURI(os.Getenv("MONGODB_URI")).
		SetReadConcern(readconcern.Majority()).
		SetWriteConcern(writeconcern.New(writeconcern.WMajority())),
	)
	if err != nil {
		return nil, err
	}

	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}

	db.initDb(client.Database(MongoDatabaseName))

	return db, nil
}

// NewWithDb returns a new database object with the given Mongo database
func NewWithDb(mdb *mongo.Database) *Database {
	db := &Database{}
	db.initDb(mdb)
	return db
}

func (db *Database) initDb(mdb *mongo.Database) {
	db.Mdb = mdb

	db.Categories = categories.NewAPI(mdb, &db.APIs)
	db.Dates = dates.NewAPI(mdb)
	db.Events = events.NewAPI(mdb, &db.APIs)
	db.Folders = folders.NewAPI(mdb, &db.APIs)
	db.Keywords = keywords.NewAPI(mdb)
	db.Locations = locations.NewAPI(mdb)
	db.Pages = pages.NewAPI(mdb)
	db.Photos = photos.NewAPI(mdb, &db.APIs)
	db.TimelineGroups = timeline_groups.NewAPI(mdb, &db.APIs)
	db.Users = users.NewAPI(mdb)
	db.Visibility = visibility.NewAPI(mdb)

	db.createCollections()
}

// Creates empy collections. Necessary for transactions (which cannot create collections during operations).
func (db *Database) createCollections() {
	ctx := context.Background()

	db.Mdb.CreateCollection(ctx, db.Categories.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Categories.ViewByRelatedParkCollection().Name())
	db.Mdb.CreateCollection(ctx, db.Dates.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Events.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Folders.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Keywords.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Locations.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Pages.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Photos.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Photos.ResizeCacheCollection().Name())
	db.Mdb.CreateCollection(ctx, db.TimelineGroups.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Users.Collection().Name())
	db.Mdb.CreateCollection(ctx, db.Users.SessionCollection().Name())
	db.Mdb.CreateCollection(ctx, db.Visibility.Collection().Name())

}

// Name of collection for storing database properties
const dbPropsCollectionName = "db"

// DbPropsCollection is the collection for database properties
func (db *Database) DbPropsCollection() *mongo.Collection {
	return db.Mdb.Collection(dbPropsCollectionName)
}

// Check vis token counts in all collections. If called in checkOnly mode, only does checks otherwise
// will also update vis token counts.
//
// If there are any errors, errors are returned in a map with structure:
// returnedMap[<groupType> + <id>]["visTokenCounts" | "descVisTokenCounts"][<token>] = delta
// where delta is difference between actual count and expected count.
func (db *Database) CheckAndFixVisTokenCounts(ctx context.Context, checkOnly bool) (map[string]map[string]map[string]int, error) {
	l := log.WithField("func", "CheckAndFixVisTokenCounts")

	// Errors may be written to concurrently, so use a mutex.
	errors := map[string]map[string]map[string]int{}
	var errorsMutex sync.Mutex
	var localErrCount map[string]int

	// Runs the aggregation pipeline on the given collection and updates the token counts provided.
	// Assumes that the pipeline produces a set of token documents each consisting of an id and a count,
	// where the count denotes the number of documents associated with that id. The aggregated counts
	// are subtracted from the reference token counts.
	updateRefTokenCounts := func(coll *mongo.Collection, pipeline bson.A, refTokenCounts map[string]int) error {
		l2 := l.WithFields(log.Fields{
			"collection": coll.Name(),
		})

		// Special case: no pipeline to run.
		if len(pipeline) == 0 {
			return nil
		}

		// Run pipeline.
		cursor, err := coll.Aggregate(ctx, pipeline)
		if err != nil {
			l2.WithError(err).Error("Could not run aggregation pipeline")
			return err
		}

		var tokenDocs []struct {
			ID    string `bson:"_id"`
			Count int    `bson:"count"`
		}

		if err := cursor.All(ctx, &tokenDocs); err != nil {
			l2.WithError(err).Error("Could not decode aggregation result")
			return err
		}
		l2.Tracef("Token counts: %+v", tokenDocs)

		// Update reference token counts.
		for _, tokenDoc := range tokenDocs {
			refTokenCounts[tokenDoc.ID] -= tokenDoc.Count
		}

		return nil
	}

	// Processes the token counts check and modifications for a collection. This function will iterate over all
	// documents in the collection and check the visTokenCounts and descVisTokenCounts fields.
	processCollection := func(
		coll *mongo.Collection,
		// Group type name for reporting and debugging.
		groupType string,
		// Returns the pipeline to count the tokens for photos directly in this group.
		getDirectPhotoCountsPipeline func(string) bson.A,
		// Returns the pipeline to count the tokens for photos in this group and descendant groups.
		getDescPhotoCountsPipeline func(string) bson.A,
		// Returns the pipeline to count the tokens for descendant groups.
		getDescGroupCountsPipeline func(string) bson.A,
	) error {
		l2 := l.WithField("collection", coll.Name())

		// For each collection, iterate over each document and check visTokenCounts and descVisTokenCounts.
		// We iterate with the deepest docs first since errors in the deepest docs are likely the root problem
		// (there is otherwise no dependency that the deepest docs need to be processed first).
		findOpts := options.Find()
		findOpts.SetProjection(bson.M{
			// Some groups have selfVisTokens, which are visibility tokens directly applied to the group.
			"selfVisTokens":      1,
			"visTokenCounts":     1,
			"descVisTokenCounts": 1,
		})
		findOpts.SetSort(bson.M{
			"depth": -1,
		})
		cursor, err := coll.Find(ctx, bson.M{}, findOpts)
		if err != nil {
			l2.WithError(err).Error("Could not find docs")
			return err
		}

		for cursor.Next(ctx) {
			var doc struct {
				ID                 string         `bson:"_id"`
				SelfVisTokens      []string       `bson:"selfVisTokens"`
				VisTokenCounts     map[string]int `bson:"visTokenCounts"`
				DescVisTokenCounts map[string]int `bson:"descVisTokenCounts"`
			}
			if err := cursor.Decode(&doc); err != nil {
				l.WithError(err).Error("Could not decode doc")
				return err
			}
			l2.Tracef("Doc: %+v", doc)

			// We have the counts recorded in the document. From this point on, we query photos and groups to validate
			// the count. As we go, the counts are updated by subtracting from the counts in the document. If at the
			// end a token has a non-zero count, then we have a mismatch.

			// Handle:
			// 1. Each document has a self-count with the All token.
			// 2. SelfVisTokens
			doc.VisTokenCounts[docs.VisTokenAll]--
			for _, token := range doc.SelfVisTokens {
				doc.VisTokenCounts[token]--
			}

			// Direct photos.
			if err := updateRefTokenCounts(db.Photos.Collection(), getDirectPhotoCountsPipeline(doc.ID), doc.VisTokenCounts); err != nil {
				l2.WithError(err).Error("Could not get direct photo token counts")
				return err
			}

			// Descendant photos.
			if err := updateRefTokenCounts(db.Photos.Collection(), getDescPhotoCountsPipeline(doc.ID), doc.DescVisTokenCounts); err != nil {
				l2.WithError(err).Error("Could not get descendant photo token counts")
				return err
			}

			// Descendant docs.
			if err := updateRefTokenCounts(coll, getDescGroupCountsPipeline(doc.ID), doc.DescVisTokenCounts); err != nil {
				l2.WithError(err).Error("Could not get descendent doc token counts")
				return err
			}

			// Only report tokens that have a non-zero delta.
			localErrors := map[string]map[string]int{}
			localErrCount = make(map[string]int)
			for token, count := range doc.VisTokenCounts {
				if count != 0 {
					localErrCount[token] = count
				}
			}
			if len(localErrCount) != 0 {
				localErrors["visTokenCounts"] = localErrCount
			}

			localErrCount = make(map[string]int)
			for token, count := range doc.DescVisTokenCounts {
				if count != 0 {
					localErrCount[token] = count
				}
			}
			if len(localErrCount) != 0 {
				localErrors["descVisTokenCounts"] = localErrCount
			}

			if len(localErrors) != 0 {
				errorsMutex.Lock()
				errors["/"+groupType+doc.ID] = localErrors
				errorsMutex.Unlock()
				l.Warnf("Vis token mismatches found: %v id %v = %+v", groupType, doc.ID, localErrors)

				if !checkOnly {
					// Update the counts based on the deltas.
					countUpdates := bson.M{}

					for countType, tokenCounts := range localErrors {
						for token, delta := range tokenCounts {
							countUpdates[countType+"."+token] = -delta
						}
					}

					if _, err := coll.UpdateByID(ctx, doc.ID, bson.M{"$inc": countUpdates}); err != nil {
						l.WithError(err).Error("Could not update vis token counts")
						return err
					}
				}
			}
		}
		if err := cursor.Err(); err != nil {
			l.WithError(err).Error("Could not iterate over docs")
			return err
		}

		return nil
	}

	// Base pipeline for querying photos that match a certain field.
	getPhotoPipeline := func(matchField string, matchValue interface{}) bson.A {
		return bson.A{
			// Match photos per field and value.
			bson.M{
				"$match": bson.M{
					matchField: matchValue,
				},
			},
			// Only need to keep the matchField and visTokens.
			bson.M{
				"$project": bson.M{
					matchField:  1,
					"visTokens": 1,
				},
			},
			// The values of matchField may be an array. The matchValue may match multiple values in that array.
			// Each match is counted separately, so unwind and then rematch (as the array field may also contain other
			// non-matching values).
			bson.M{
				"$unwind": "$" + matchField,
			},
			bson.M{
				"$match": bson.M{
					matchField: matchValue,
				},
			},
			// visTokens is an array and each token is counted separately.
			bson.M{
				"$unwind": "$visTokens",
			},
			// Count by token.
			bson.M{
				"$group": bson.M{
					"_id": "$visTokens",
					"count": bson.M{
						"$sum": 1,
					},
				},
			},
		}
	}

	// Makes a photo pipeline getter. If in descendants mode, will match based on prefix regex on the id.
	makePhotoPipeline := func(matchField string, descendants bool) func(string) bson.A {
		return func(id string) bson.A {
			var matchValue interface{}
			if descendants {
				matchValue = bson.M{"$regex": "^" + id}
			} else {
				matchValue = id
			}
			return getPhotoPipeline(matchField, matchValue)
		}
	}

	// Base pipeline for querying descendant groups of the given id.
	getGroupPipeline := func(id string) bson.A {
		return bson.A{
			// Match descendant groups via prefix regex. This also matches the root group itself.
			bson.M{
				"$match": bson.M{
					"_id": bson.M{
						"$regex": "^" + id,
					},
				},
			},
			// Each descendant group has an implicit All token plus any selfVisTokens.
			bson.M{
				"$project": bson.M{
					"visTokens": bson.M{
						"$concatArrays": bson.A{
							bson.M{
								// Some groups don't have selfVisTokens.
								"$ifNull": bson.A{
									"$selfVisTokens",
									bson.A{},
								},
							},
							bson.A{docs.VisTokenAll},
						},
					},
				},
			},
			// Each token is counted separately.
			bson.M{
				"$unwind": "$visTokens",
			},
			// Count by token.
			bson.M{
				"$group": bson.M{
					"_id": "$visTokens",
					"count": bson.M{
						"$sum": 1,
					},
				},
			},
		}
	}

	// Returns a nil pipeline, which means don't do anything.
	getNilPipeline := func(string) bson.A {
		return nil
	}

	// Process the different collections. Each one will be processed in parallel.
	var taskGroup errgroup.Group

	taskGroup.Go(func() error {
		return processCollection(
			db.Folders.Collection(),
			"folder",
			makePhotoPipeline("folder", false),
			makePhotoPipeline("folder", true),
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		return processCollection(
			db.Keywords.Collection(),
			"keyword",
			makePhotoPipeline("keywords", false),
			makePhotoPipeline("keywords", true),
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		return processCollection(
			db.Categories.Collection(),
			"category",
			makePhotoPipeline("categories", false),
			makePhotoPipeline("categories", true),
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		return processCollection(
			db.Locations.Collection(),
			"location",
			makePhotoPipeline("locations", false),
			makePhotoPipeline("locations", true),
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		return processCollection(
			db.TimelineGroups.Collection(),
			"timeline-groups",
			// Photos are not part of timeline groups
			getNilPipeline,
			getNilPipeline,
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		return processCollection(
			db.Dates.Collection(),
			"date",
			func(id string) bson.A {
				dateValue := strings.Join(strings.Split(id, "/"), "")
				if len(dateValue) != 8 {
					return nil
				}
				return getPhotoPipeline("sortKey", bson.M{"$regex": "^" + dateValue})
			},
			func(id string) bson.A {
				dateValue := strings.Join(strings.Split(id, "/"), "")
				return getPhotoPipeline("sortKey", bson.M{"$regex": "^" + dateValue})
			},
			getGroupPipeline,
		)
	})

	taskGroup.Go(func() error {
		// Visibility tokens are special in that associated photos are counted as part '*', not based on their token id.
		getVisTokenPhotoPipeline := func(matchValue interface{}) bson.A {
			return bson.A{
				bson.M{
					"$match": bson.M{
						"visTokens": matchValue,
					},
				},
				bson.M{
					"$project": bson.M{
						"visTokens": 1,
					},
				},
				bson.M{
					"$unwind": "$visTokens",
				},
				bson.M{
					"$match": bson.M{
						"visTokens": matchValue,
					},
				},
				bson.M{
					"$group": bson.M{
						"_id": "*",
						"count": bson.M{
							"$sum": 1,
						},
					},
				},
			}
		}

		return processCollection(
			db.Visibility.Collection(),
			"visibility",
			func(id string) bson.A {
				return getVisTokenPhotoPipeline(id)
			},
			func(id string) bson.A {
				return getVisTokenPhotoPipeline(bson.M{"$regex": "^" + id})
			},
			getGroupPipeline,
		)
	})

	overallErr := taskGroup.Wait()
	return errors, overallErr

}
