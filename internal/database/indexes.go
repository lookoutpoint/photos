// SPDX-License-Identifier: MIT

package database

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Current index version
// Increment before each push to the remote database
const latestIndexVersion = 18

const indexVersionID = "INDEX_VERSION"

type indexVersionDoc struct {
	ID      string `bson:"_id"`
	Version int    `bson:"version"`
}

var indexUpdaters = map[int]func(ctx context.Context, db *Database) error{
	1:  updateIndexes1,
	2:  updateIndexes2,
	3:  updateIndexes3,
	4:  updateIndexes4,
	5:  updateIndexes5,
	6:  updateIndexes6,
	7:  updateIndexes7,
	8:  updateIndexes8,
	9:  updateIndexes9,
	10: updateIndexes10,
	11: updateIndexes11,
	12: updateIndexes12,
	13: updateIndexes13,
	14: updateIndexes14,
	15: updateIndexes15,
	16: updateIndexes16,
	17: updateIndexes17,
	18: updateIndexes18,
}

// UpdateIndexes if necessary
func (db *Database) UpdateIndexes() error {
	return db.UpdateIndexesTo(latestIndexVersion)
}

// UpdateIndexesTo to a particular version
func (db *Database) UpdateIndexesTo(version int) error {
	l := log.WithFields(log.Fields{
		"func":    "database.UpdateIndexesTo",
		"version": version,
	})

	ctx := context.Background()

	// Check if the index version already exceeds the desired version.
	res := db.DbPropsCollection().FindOne(ctx, bson.M{
		"_id":     indexVersionID,
		"version": bson.M{"$gte": version},
	})
	if err := res.Err(); err == nil {
		// Version already is >=, nothing to do
		return nil
	} else if err != mongo.ErrNoDocuments {
		l.WithError(err).Error("Could not check index version")
		return err
	}

	// Upsert to initialize index version if it does not exist yet.
	_, err := db.DbPropsCollection().UpdateOne(
		ctx,
		bson.M{"_id": indexVersionID},
		bson.M{"$setOnInsert": bson.M{"version": 0}},
		options.Update().SetUpsert(true),
	)
	if err != nil {
		l.WithError(err).Error("Could not initialize index version")
		return err
	}

	// Try to update the index version. Use this as an atomic way to
	// detect if the index version has changed and only one process will do the update.
	res = db.DbPropsCollection().FindOneAndUpdate(
		ctx,
		bson.M{"_id": indexVersionID},
		bson.M{"$set": bson.M{"version": version}},
		options.FindOneAndUpdate().SetProjection(bson.M{"version": 1}),
	)

	prevIndexVersion := indexVersionDoc{Version: 0}
	if err := res.Decode(&prevIndexVersion); err == nil {
		if prevIndexVersion.Version == latestIndexVersion {
			// Matches current version, nothing to do.
			return nil
		}
	} else {
		l.WithError(err).Error("Could not decode index version document")
		return err
	}

	rollback := func(v int) {
		l.Infof("Rolling back index version to %v", v)

		_, err := db.DbPropsCollection().UpdateOne(
			ctx,
			bson.M{"_id": indexVersionID},
			bson.M{"$set": bson.M{"version": v}},
		)
		if err != nil {
			// This is probably very bad
			l.WithError(err).Errorf("Could not rollback index version to %v", v)
		}
	}

	l.Infof("Previous index version: %v", prevIndexVersion.Version)

	// At this point, index version does not match, so recreate indexes.
	for v := prevIndexVersion.Version + 1; v <= version; v++ {
		l.Infof("Applying index creator version %v", v)

		creator, ok := indexUpdaters[v]
		if !ok {
			err := fmt.Errorf("version %v does not have a updater function", v)
			l.WithError(err).Error("Missing creator function")
			rollback(v - 1)
			return err
		}

		if err := creator(ctx, db); err != nil {
			l.WithError(err).WithField("version", v).Error("Updater returned error")
			rollback(v - 1)
			return err
		}

		l.Infof("Successfully applied index updater version %v", v)
	}

	l.Info("Successfully updated indexes")

	return nil
}

// Photo indexes
// groups + sortKey (group = folder, location, keyword, category)
// This is to support photos.List
func updateIndexes1(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes1")

	idxs := db.Photos.Collection().Indexes()
	if _, err := idxs.DropAll(ctx); err != nil {
		l.WithError(err).Error("Could not drop all photos indexes")
		return err
	}

	models := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "folder", Value: 1}, {Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "location", Value: 1}, {Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "keywords", Value: 1}, {Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "categories", Value: 1}, {Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create photos indexes")
		return err
	}

	return nil
}

// Visibility index of links.activationValue
func updateIndexes2(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes2")

	idxs := db.Visibility.Collection().Indexes()
	if _, err := idxs.DropAll(ctx); err != nil {
		l.WithError(err).Error("Could not drop all visibility token indexes")
		return err
	}

	models := []mongo.IndexModel{
		{
			Keys: bson.D{{Key: "links.activationValue", Value: 1}},
			Options: options.Index().SetUnique(true).
				// Unique only if activationValue exists
				SetPartialFilterExpression(bson.M{"links.activationValue": bson.M{"$exists": true}}),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create visibility token indexes")
		return err
	}

	return nil
}

// Photo indexes
// groups + tsSortKey (group = folder, location, keyword, category)
// This is to support photos.List
func updateIndexes3(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes3")

	idxs := db.Photos.Collection().Indexes()

	models := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "folder", Value: 1}, {Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "location", Value: 1}, {Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "keywords", Value: 1}, {Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "categories", Value: 1}, {Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create photos indexes")
		return err
	}

	return nil
}

// Group timestamp and public indexes
// For each group (except date), create two indexes:
// 1. visTokenCounts.#P
// 2. timestamp, visTokenCounts.#P
//
// Both are used to select home group selection
func updateIndexes4(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes4")

	processGroup := func(coll *mongo.Collection) error {
		idxs := coll.Indexes()

		models := []mongo.IndexModel{
			{
				Keys: bson.D{{Key: "visTokenCounts.#P", Value: 1}},
			},
			{
				Keys: bson.D{{Key: "timestamp", Value: -1}, {Key: "visTokenCounts.#P", Value: 1}},
			},
		}

		if _, err := idxs.CreateMany(ctx, models); err != nil {
			l.WithError(err).Error("Could not create indexes")
			return err
		}

		return nil
	}

	if err := processGroup(db.Folders.Collection()); err != nil {
		l.WithError(err).Error("Error with folders")
		return err
	}

	if err := processGroup(db.Locations.Collection()); err != nil {
		l.WithError(err).Error("Error with locations")
		return err
	}

	if err := processGroup(db.Categories.Collection()); err != nil {
		l.WithError(err).Error("Error with categories")
		return err
	}

	if err := processGroup(db.Keywords.Collection()); err != nil {
		l.WithError(err).Error("Error with keywords")
		return err
	}

	return nil
}

// Photo rating index (to support random highlight photo)
func updateIndexes5(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes5")

	idxs := db.Photos.Collection().Indexes()

	models := []mongo.IndexModel{
		{
			Keys: bson.D{{Key: "rating", Value: 1}},
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create photos indexes")
		return err
	}

	return nil
}

// Group timestamp and public indexes
// For each group (except date), create two indexes:
// 1. descVisTokenCounts.#P
// 2. timestamp, descVisTokenCounts.#P
//
// Both are used to select home group selection. This replaces
// the indexes created in updateIndexes4
func updateIndexes6(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes6")

	processGroup := func(coll *mongo.Collection) error {
		idxs := coll.Indexes()

		idxs.DropOne(ctx, "visTokenCounts.#P_1")
		idxs.DropOne(ctx, "timestamp_-1_visTokenCounts.#P_1")

		models := []mongo.IndexModel{
			{
				Keys: bson.D{{Key: "descVisTokenCounts.#P", Value: 1}},
			},
			{
				Keys: bson.D{{Key: "timestamp", Value: 1}, {Key: "descVisTokenCounts.#P", Value: 1}},
			},
		}

		if _, err := idxs.CreateMany(ctx, models); err != nil {
			l.WithError(err).Error("Could not create indexes")
			return err
		}

		return nil
	}

	if err := processGroup(db.Folders.Collection()); err != nil {
		l.WithError(err).Error("Error with folders")
		return err
	}

	if err := processGroup(db.Locations.Collection()); err != nil {
		l.WithError(err).Error("Error with locations")
		return err
	}

	if err := processGroup(db.Categories.Collection()); err != nil {
		l.WithError(err).Error("Error with categories")
		return err
	}

	if err := processGroup(db.Keywords.Collection()); err != nil {
		l.WithError(err).Error("Error with keywords")
		return err
	}

	return nil
}

// Group sort index (sort by sortOrder, id)
// Used for sorting group children
func updateIndexes7(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes7")

	processGroup := func(coll *mongo.Collection) error {
		idxs := coll.Indexes()

		models := []mongo.IndexModel{
			{
				Keys: bson.D{{Key: "sortOrder", Value: 1}, {Key: "_id", Value: 1}},
			},
		}

		if _, err := idxs.CreateMany(ctx, models); err != nil {
			l.WithError(err).Error("Could not create indexes")
			return err
		}

		return nil
	}

	if err := processGroup(db.Folders.Collection()); err != nil {
		l.WithError(err).Error("Error with folders")
		return err
	}

	if err := processGroup(db.Locations.Collection()); err != nil {
		l.WithError(err).Error("Error with locations")
		return err
	}

	if err := processGroup(db.Categories.Collection()); err != nil {
		l.WithError(err).Error("Error with categories")
		return err
	}

	if err := processGroup(db.Keywords.Collection()); err != nil {
		l.WithError(err).Error("Error with keywords")
		return err
	}

	if err := processGroup(db.Dates.Collection()); err != nil {
		l.WithError(err).Error("Error with dates")
		return err
	}

	return nil
}

// Fixup for updateIndexes7 - indexes should be unique
func updateIndexes8(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes8")

	processGroup := func(coll *mongo.Collection) error {
		idxs := coll.Indexes()

		if _, err := idxs.DropOne(ctx, "sortOrder_1__id_1"); err != nil {
			l.WithError(err).Warn("Could not drop existing index")
		}

		models := []mongo.IndexModel{
			{
				Keys:    bson.D{{Key: "sortOrder", Value: 1}, {Key: "_id", Value: 1}},
				Options: options.Index().SetUnique(true),
			},
		}

		if _, err := idxs.CreateMany(ctx, models); err != nil {
			l.WithError(err).Error("Could not create indexes")
			return err
		}

		return nil
	}

	if err := processGroup(db.Folders.Collection()); err != nil {
		l.WithError(err).Error("Error with folders")
		return err
	}

	if err := processGroup(db.Locations.Collection()); err != nil {
		l.WithError(err).Error("Error with locations")
		return err
	}

	if err := processGroup(db.Categories.Collection()); err != nil {
		l.WithError(err).Error("Error with categories")
		return err
	}

	if err := processGroup(db.Keywords.Collection()); err != nil {
		l.WithError(err).Error("Error with keywords")
		return err
	}

	if err := processGroup(db.Dates.Collection()); err != nil {
		l.WithError(err).Error("Error with dates")
		return err
	}

	return nil
}

// Indexes for timeline groups
func updateIndexes9(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes9")

	coll := db.TimelineGroups.Collection()
	idxs := coll.Indexes()

	models := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "sortOrder", Value: 1}, {Key: "_id", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create indexes")
		return err
	}

	return nil
}

// Update group indexes to reflect new sort order for list children:
// depth, sortOrder, id
func updateIndexes10(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes10")

	processGroup := func(coll *mongo.Collection) error {
		idxs := coll.Indexes()

		if _, err := idxs.DropOne(ctx, "sortOrder_1__id_1"); err != nil {
			l.WithError(err).Warn("Could not drop existing index")
		}

		models := []mongo.IndexModel{
			{
				Keys: bson.D{
					{Key: "depth", Value: 1},
					{Key: "sortOrder", Value: 1},
					{Key: "_id", Value: 1},
				},
				Options: options.Index().SetUnique(true),
			},
		}

		if _, err := idxs.CreateMany(ctx, models); err != nil {
			l.WithError(err).Error("Could not create indexes")
			return err
		}

		return nil
	}

	if err := processGroup(db.Folders.Collection()); err != nil {
		l.WithError(err).Error("Error with folders")
		return err
	}

	if err := processGroup(db.Locations.Collection()); err != nil {
		l.WithError(err).Error("Error with locations")
		return err
	}

	if err := processGroup(db.Categories.Collection()); err != nil {
		l.WithError(err).Error("Error with categories")
		return err
	}

	if err := processGroup(db.Keywords.Collection()); err != nil {
		l.WithError(err).Error("Error with keywords")
		return err
	}

	if err := processGroup(db.Dates.Collection()); err != nil {
		l.WithError(err).Error("Error with dates")
		return err
	}

	if err := processGroup(db.TimelineGroups.Collection()); err != nil {
		l.WithError(err).Error("Error with timeline groups")
		return err
	}

	return nil
}

// Update category indexes to reflect customized sort order using related park.
func updateIndexes11(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes11")

	coll := db.Categories.Collection()
	idxs := coll.Indexes()

	models := []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "related.park", Value: 1},
				{Key: "depth", Value: 1},
				{Key: "sortOrder", Value: 1},
				{Key: "_id", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create indexes")
		return err
	}

	return nil
}

// Update related park index.
func updateIndexes12(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes12")

	coll := db.Categories.Collection()
	idxs := coll.Indexes()

	_, err := idxs.DropOne(ctx, "related.park_1_depth_1_sortOrder_1__id_1")
	if err != nil {
		l.WithError(err).Warn("Could not drop previous index")
	}

	models := []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "related.cat:park", Value: 1},
				{Key: "depth", Value: 1},
				{Key: "sortOrder", Value: 1},
				{Key: "_id", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.D{
				{Key: "related.location", Value: 1},
				{Key: "depth", Value: 1},
				{Key: "sortOrder", Value: 1},
				{Key: "_id", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create indexes")
		return err
	}

	return nil
}

// Update related park index on category ViewByRelatedPark collection.
func updateIndexes13(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes13")

	coll := db.Categories.ViewByRelatedParkCollection()
	idxs := coll.Indexes()

	models := []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "related.cat:park", Value: 1},
				{Key: "depth", Value: 1},
				{Key: "sortOrder", Value: 1},
				{Key: "_id", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys: bson.D{
				{Key: "related.location", Value: 1},
				{Key: "depth", Value: 1},
				{Key: "sortOrder", Value: 1},
				{Key: "_id", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create indexes")
		return err
	}

	return nil
}

// Add indexes for events
func updateIndexes14(ctx context.Context, db *Database) error {
	// Conflicts with data structure in updateIndexes15
	// l := log.WithField("func", "database.updateIndexes14")

	// coll := db.Events.Collection()
	// idxs := coll.Indexes()

	// models := []mongo.IndexModel{
	// 	{
	// 		Keys: bson.D{
	// 			{Key: "object", Value: 1},
	// 			{Key: "event", Value: 1},
	// 			{Key: "date", Value: 1},
	// 		},
	// 		Options: options.Index().SetUnique(true),
	// 	},
	// }

	// if _, err := idxs.CreateMany(ctx, models); err != nil {
	// 	l.WithError(err).Error("Could not create indexes")
	// 	return err
	// }

	return nil
}

// Update indexes for events
func updateIndexes15(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes15")

	coll := db.Events.Collection()
	idxs := coll.Indexes()
	if _, err := idxs.DropAll(ctx); err != nil {
		l.WithError(err).Error("Could not drop all events indexes")
		return err
	}

	models := []mongo.IndexModel{
		{
			Keys: bson.D{
				{Key: "event", Value: 1},
				{Key: "object", Value: 1},
				{Key: "context", Value: 1},
				{Key: "date", Value: 1},
			},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create indexes")
		return err
	}

	return nil
}

// Drop indexes for events. Needed to upgrade to schema version 4.
func updateIndexes16(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes16")

	coll := db.Events.Collection()
	idxs := coll.Indexes()
	if _, err := idxs.DropAll(ctx); err != nil {
		l.WithError(err).Error("Could not drop all events indexes")
		return err
	}

	return nil
}

// Update photos to index by locations instead of location
func updateIndexes17(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes17")

	coll := db.Photos.Collection()
	idxs := coll.Indexes()

	_, err := idxs.DropOne(ctx, "location_1_sortKey_1")
	if err != nil {
		l.WithError(err).Warn("Could not drop previous index (sortKey)")
	}

	_, err = idxs.DropOne(ctx, "location_1_tsSortKey_1")
	if err != nil {
		l.WithError(err).Warn("Could not drop previous index (tsSortKey)")
	}

	models := []mongo.IndexModel{
		{
			Keys:    bson.D{{Key: "locations", Value: 1}, {Key: "sortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
		{
			Keys:    bson.D{{Key: "locations", Value: 1}, {Key: "tsSortKey", Value: 1}},
			Options: options.Index().SetUnique(true),
		},
	}

	if _, err := idxs.CreateMany(ctx, models); err != nil {
		l.WithError(err).Error("Could not create photos indexes")
		return err
	}

	return nil
}

// Create geospatial index on photo gpsLocation.
func updateIndexes18(ctx context.Context, db *Database) error {
	l := log.WithField("func", "database.updateIndexes18")

	coll := db.Photos.Collection()
	idxs := coll.Indexes()

	_, err := idxs.DropOne(ctx, "gpsLocation_2dsphere")
	if err != nil {
		l.WithError(err).Warn("Could not drop previous index (gpsLocation_2dsphere)")
	}

	if _, err := idxs.CreateOne(ctx, mongo.IndexModel{Keys: bson.D{{Key: "gpsLocation", Value: "2dsphere"}}}); err != nil {
		l.WithError(err).Error("Could not create photos geospatial index")
		return err
	}

	return nil
}
