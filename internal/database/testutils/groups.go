// SPDX-License-Identifier: MIT

package testutils

import (
	"strings"

	"github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"github.com/onsi/gomega/types"
)

// VisTokenCounts is a map of visibility token values to visibility token counts. Use with MatchGroupBasePtrVisTokenCounts.
type VisTokenCounts map[string]int

// MatchGroupBasePtrVisTokenCounts checks visibility tokens and visibility token counts based on the provided
// self photo counts and descendant photo counts.
func MatchGroupBasePtrVisTokenCounts(counts VisTokenCounts, rawDescCounts VisTokenCounts) types.GomegaMatcher {
	descCounts := VisTokenCounts{}
	for token, count := range rawDescCounts {
		descCounts[token] += count
	}
	for token, count := range counts {
		descCounts[token] += count
	}

	tokens := make([]interface{}, 0, len(descCounts))
	descCountKeys := gstruct.Keys{}
	for token, count := range descCounts {
		if count != 0 { // zero counts should not appear because the field should be deleted
			tokens = append(tokens, token)
			descCountKeys[token] = gomega.Equal(count)
		}
	}

	countKeys := gstruct.Keys{}
	for token, count := range counts {
		if count != 0 { // zero counts should not appear because the field should be deleted
			countKeys[token] = gomega.Equal(count)
		}
	}

	return MatchGroupBasePtr(gstruct.Fields{
		"GroupBase.VisTokens":          gomega.ConsistOf(tokens...),
		"GroupBase.VisTokenCounts":     gstruct.MatchAllKeys(countKeys),
		"GroupBase.DescVisTokenCounts": gstruct.MatchAllKeys(descCountKeys),
	})
}

// MatchGroupBase is a helper function to create a matcher to match a struct with an embedded docs.GroupBase member.
// Also supports other sub-fields with format <Field>.<SubField>.
func MatchGroupBase(addFields ...gstruct.Fields) types.GomegaMatcher {
	subFields := map[string]gstruct.Fields{}
	topFields := gstruct.Fields{}

	for _, fields := range addFields {
		for key, matcher := range fields {
			idx := strings.Index(key, ".")
			if idx >= 0 {
				subField := key[:idx]
				if subFields[subField] == nil {
					subFields[subField] = gstruct.Fields{}
				}
				subFields[subField][key[idx+1:]] = matcher
			} else {
				topFields[key] = matcher
			}
		}
	}

	for key, fields := range subFields {
		topFields[key] = gstruct.MatchFields(gstruct.IgnoreExtras, fields)
	}

	return gstruct.MatchFields(gstruct.IgnoreExtras, topFields)
}

// MatchGroupBasePtr is the same as MatchGroupbase except that it matches gainst a pointer
// to a struct
func MatchGroupBasePtr(addFields ...gstruct.Fields) types.GomegaMatcher {
	return gstruct.PointTo(MatchGroupBase(addFields...))
}
