// SPDX-License-Identifier: MIT

package testutils

import (
	"context"

	"github.com/onsi/gomega"
	"github.com/onsi/gomega/gstruct"
	"github.com/onsi/gomega/types"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// MongoDBURI assumes we're running mongod locally
const MongoDBURI = "mongodb://localhost:27017/?retryWrites=false"

// DatabaseName of the test database
const DatabaseName = "lookoutpoint-test"

// NewTestDb creates a new test database
func NewTestDb() *mongo.Database {
	ctx := context.Background()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(MongoDBURI))
	if err != nil {
		log.WithField("err", err).Fatal("Unable to connect MongoDB client")
	}

	return client.Database(DatabaseName)
}

// ClearTestDatabase clears the provided database
func ClearTestDatabase(ctx context.Context, db *database.Database) {
	// Clear by deleting all documents from collections and dropping all indexes.
	// Not doing a drop because
	// 1. We want to keep the collections (see database initialization where empty collections are created for transactions)
	// 2. Dropping and creating new collections causes WiredTiger storage engine to create a lot of files and hit ulimits
	colls := []*mongo.Collection{
		db.DbPropsCollection(),
		db.Categories.Collection(),
		db.Dates.Collection(),
		db.Events.Collection(),
		db.Folders.Collection(),
		db.Keywords.Collection(),
		db.Locations.Collection(),
		db.Pages.Collection(),
		db.Photos.Collection(),
		db.Photos.ResizeCacheCollection(),
		db.TimelineGroups.Collection(),
		db.Users.Collection(),
		db.Users.SessionCollection(),
		db.Visibility.Collection(),
	}

	for _, coll := range colls {
		if _, err := coll.DeleteMany(ctx, bson.D{}); err != nil {
			log.WithError(err).Warnf("Could not delete %v", coll.Name())
		}

		if _, err := coll.Indexes().DropAll(ctx); err != nil {
			log.WithError(err).Warnf("Could not drop indexes on %v", coll.Name())
		}
	}
}

// MatchVisibilityTokens creates a matcher against a VisibilityTokens field in a pointer to a struct.
// The visibility tokens field is assumed to be a slice and the tokens are matched as a set (i.e. order does not matter).
// Automatically adds a VisTokenAll.
func MatchVisibilityTokens(rawTokens ...string) types.GomegaMatcher {
	tokens := make([]interface{}, 0, len(rawTokens)+1)
	tokens = append(tokens, docs.VisTokenAll)
	for _, t := range rawTokens {
		tokens = append(tokens, t)
	}

	return gstruct.PointTo(gstruct.MatchFields(gstruct.IgnoreExtras, gstruct.Fields{
		"VisTokens": gomega.ConsistOf(tokens...),
	}))
}
