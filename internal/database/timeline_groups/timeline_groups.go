// SPDX-License-Identifier: MIT

package timeline_groups

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.TimelineGroups
type apiImpl struct {
	mdb *mongo.Database
	top *apis.APIs
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database, top *apis.APIs) apis.TimelineGroups {
	return &apiImpl{mdb: mdb, top: top}
}

const collectionName = "photos-timeline-groups"
const folderIDSepToken = ":" // must be a character that doesn't up in normal slug paths but not a regex control character

var timelineGroupBSONKeys = utils.ExtractBSONKeys(docs.TimelineGroup{})

// Collection returns the collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	sep := "|"
	return &sep
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(timelineGroupBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

func getFolderIDFromID(id string) (string, error) {
	// Expected id format: /<mangled-folder-id>/...
	// where <mangled-folder-id> becomes a proper folder id after substituting folderIDSepToken with
	// path separators
	if len(id) == 0 {
		return "", apis.ErrInvalidID
	}

	sep := strings.Index(id[1:], "/") // skip leading "/"
	if sep <= 0 {
		return "", apis.ErrInvalidID
	}

	mangledFolderID := id[1 : sep+1]
	folderID := strings.ReplaceAll(mangledFolderID, folderIDSepToken, "/")

	if !utils.IsValidSlugPath(folderID) {
		return "", apis.ErrInvalidID
	}

	return folderID, nil
}

func wrapFilter(folderID string, filter *docs.PhotoListFilterExpr) *docs.PhotoListFilterExpr {
	// Build filter based on the folder id.
	folderFilter := &docs.PhotoListFilterExpr{
		Folders: []docs.PhotoListFilterFolder{
			{Path: folderID, FullTree: true},
		},
	}

	if filter == nil || filter.IsEmpty() {
		return folderFilter
	}

	// Join the two filters
	return &docs.PhotoListFilterExpr{
		Exprs: []*docs.PhotoListFilterExpr{folderFilter, filter},
	}
}

// UnwrapFilter
func (api *apiImpl) UnwrapFilter(filter *docs.PhotoListFilterExpr) *docs.PhotoListFilterExpr {
	// If there is no filter or there are no expressions, this means filter is
	// just the default folder filter and there is no wrapped filter.
	if filter == nil || len(filter.Exprs) == 0 {
		return nil
	}

	return filter.Exprs[1]
}

// Create the docs for the given timeline group in a folder. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, folderID string, name string, filter *docs.PhotoListFilterExpr) (string, error) {
	l := log.WithFields(log.Fields{
		"func":     "timeline_groups.apiImpl.Create",
		"folderID": folderID,
		"name":     name,
	})

	if !utils.IsValidSlugPath(folderID) {
		l.Error("Folder ID is not a valid slug path")
		return "", apis.ErrInvalidID
	}

	display := folderID
	if len(name) > 0 {
		display += "|" + name
	}

	opts := &groups.CreateOptions{
		SlugifyPath: func(displayParts ...string) string {
			tokens := make([]string, len(displayParts))
			for i, dp := range displayParts {
				if i == 0 {
					tokens[i] = dp
				} else if i == 1 {
					// Folder id. Keep as one entity, so replace its "/" tokens.
					tokens[i] = strings.ReplaceAll(dp, "/", folderIDSepToken)
				} else {
					// For all other tokens, slugify in lower case
					tokens[i] = utils.SlugifyTokenLower(dp)
				}
			}

			return utils.ConcatSlugPath(tokens...)
		},

		Model: func(base docs.GroupBase) interface{} {
			var modelFilter *docs.PhotoListFilterExpr
			if *base.ID != "/" {
				if base.Display != nil && *base.Display == display {
					modelFilter = wrapFilter(folderID, filter)
				} else {
					modelFilter = wrapFilter(folderID, nil)
				}
			}

			doc := docs.TimelineGroup{
				GroupBase: base,
				Filter:    modelFilter,
			}
			doc.SelfVisTokens = []string{}
			return doc
		},
	}

	return groups.Create(ctx, api, display, false, opts)
}

// Get returns information about one timeline group.
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.TimelineGroup, error) {
	l := log.WithFields(log.Fields{
		"func": "timeline_groups.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(timelineGroupBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, options.FindOne().SetProjection(proj))

	var doc docs.TimelineGroup
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode timeline group document")
		}
		return nil, err
	}

	return &doc, nil
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.TimelineGroup, error) {
	var results []docs.TimelineGroup
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	return groups.Update(ctx, api, id, update, nil)
}

func (api *apiImpl) UpdateFilter(ctx context.Context, id string, filter *docs.PhotoListFilterExpr) error {
	l := log.WithFields(log.Fields{
		"func": "timeline_groups.apiImpl.UpdateFilter",
		"id":   id,
	})

	// Extract folder ID, which will also validate proper form for the id
	folderID, err := getFolderIDFromID(id)
	if err != nil {
		l.WithError(err).Error("Invalid id")
		return err
	}

	// Update the document
	updateDoc := bson.M{
		"$set": bson.M{"filter": wrapFilter(folderID, filter)},
	}

	res, err := api.Collection().UpdateOne(ctx, bson.M{"_id": id}, updateDoc)
	if err != nil || res.MatchedCount == 0 {
		l.WithError(err).Error("Could not update filter")
		return err
	}

	// After filter update, re-sync.
	if res.ModifiedCount == 1 {
		return api.Sync(ctx, id)
	}

	return nil
}

func (api *apiImpl) Sync(ctx context.Context, id string) error {
	l := log.WithFields(log.Fields{
		"func": "timeline_groups.apiImpl.Sync",
		"id":   id,
	})

	// Get the filter
	group, err := api.Get(ctx, id, []string{"filter", "coverPhoto"}, []string{docs.VisTokenAll})
	if err != nil || group.Filter == nil {
		l.WithError(err).Error("Could not get timeline group filter")
		return err
	}

	group.Filter.AssignDefaults()

	// Run aggregate pipeline over photos selected by the filter to get min and max photo date times.
	// Write that result into the timeline group.
	pipeline := bson.A{
		// Match all photos against filter (implicit in this match is that vis tokens don't matter)
		bson.M{"$match": group.Filter.BuildQueryFilter()},
		bson.M{"$project": bson.M{"dateTime": 1}},
		bson.M{"$group": bson.M{
			"_id":       id, // for merge
			"sortOrder": bson.M{"$min": bson.M{"$toLong": "$dateTime"}},
			"startDate": bson.M{"$min": "$dateTime"},
			"endDate":   bson.M{"$max": "$dateTime"},
		}},
		bson.M{"$merge": bson.M{
			"into": collectionName,
		}},
	}

	cursor, err := api.top.Photos.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline")
		return err
	}
	cursor.Close(ctx)

	// Reset cover photo if it is not user-selected
	if group.CoverPhoto != nil && (group.CoverPhoto.UserSelected == nil || !*group.CoverPhoto.UserSelected) {
		// Reset auto-selected cover photo
		_, err := api.Collection().UpdateOne(ctx, bson.M{"_id": id}, bson.M{"$unset": bson.M{"coverPhoto": 1}})
		if err != nil {
			l.WithError(err).Error("Could not reset cover photo")
			return err
		}
	}

	return nil
}

func (api *apiImpl) Delete(ctx context.Context, id string) error {
	return groups.DeleteWithSelfVisTokens(ctx, api, id)
}

// AddVisibilityToken to the given timeline group.
func (api *apiImpl) AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error {
	return groups.ModifySelfVisToken(ctx, api, id, token, recurse /*add*/, true)
}

// DeleteVisibilityToken from the given timeline group.
func (api *apiImpl) DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error {
	return groups.ModifySelfVisToken(ctx, api, id, token, recurse /*add*/, false)
}
