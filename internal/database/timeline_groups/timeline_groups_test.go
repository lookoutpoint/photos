// SPDX-License-Identifier: MIT

package timeline_groups_test

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Timeline Groups", func() {
	tokensAll := []string{docs.VisTokenAll}

	addPhoto := func(objectID string, dateTime string) {
		md := &photomd.Metadata{
			Width:  100,
			Height: 200,
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: dateTime,
			},
		}

		Expect(db.Photos.AddOrUpdate(ctx, "bucket", objectID, 0, 1000, md)).ToNot(BeNil())
	}

	folderFilter := func(path string) *docs.PhotoListFilterExpr {
		return &docs.PhotoListFilterExpr{
			Folders: []docs.PhotoListFilterFolder{
				{Path: path, FullTree: true},
			},
		}
	}

	matchDisplayName := func(display string) OmegaMatcher {
		return MatchGroupBasePtr(Fields{
			"GroupBase.Display": PointTo(Equal(display)),
		})
	}

	Describe("Create, Get", func() {
		It("Default filter with no name", func() {
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", nil)).To(Equal("/:some:folder-path:/"))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))

			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("")),
				"GroupBase.Depth":   PointTo(Equal(0)),
				"Filter":            BeNil(),
			}))
		})

		It("Default filter with name", func() {
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "Awesome Days", nil)).To(Equal("/:some:folder-path:/awesome-days/"))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/awesome-days/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/|Awesome Days")),
				"GroupBase.Depth":   PointTo(Equal(2)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))

		})

		It("Custom filter with no name", func() {
			filter := &docs.PhotoListFilterExpr{
				Locations: []string{"/city/"},
			}
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", filter)).To(Equal("/:some:folder-path:/"))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
					"Exprs": ConsistOf(folderFilter("/some/folder-path/"), filter),
				})),
			}))
		})

		It("Custom filter with name", func() {
			filter := &docs.PhotoListFilterExpr{
				Locations: []string{"/city/"},
			}
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "Awesome Days", filter)).To(Equal("/:some:folder-path:/awesome-days/"))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/awesome-days/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/|Awesome Days")),
				"GroupBase.Depth":   PointTo(Equal(2)),
				"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
					"Exprs": ConsistOf(folderFilter("/some/folder-path/"), filter),
				})),
			}))

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))

		})

	})

	Describe("Update", func() {
		It("Update display name of folder timeline group", func() {
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", nil)).To(Equal("/:some:folder-path:/"))
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(matchDisplayName("/some/folder-path/"))

			// Set display name
			Expect(db.TimelineGroups.Update(ctx, "/:some:folder-path:/",
				apis.NewGroupUpdate().SetDisplay("My Awesome Trip!"))).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(matchDisplayName("My Awesome Trip!"))
		})
	})

	Describe("UpdateFilter", func() {
		It("Update with custom filter", func() {
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", nil)).To(Equal("/:some:folder-path:/"))
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(matchDisplayName("/some/folder-path/"))

			filter := &docs.PhotoListFilterExpr{
				Locations: []string{"/city/"},
			}
			Expect(db.TimelineGroups.UpdateFilter(ctx, "/:some:folder-path:/", filter)).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
					"Exprs": ConsistOf(folderFilter("/some/folder-path/"), filter),
				})),
			}))
		})

		It("Update with no filter", func() {
			filter := &docs.PhotoListFilterExpr{
				Locations: []string{"/city/"},
			}
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", filter)).To(Equal("/:some:folder-path:/"))
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
					"Exprs": ConsistOf(folderFilter("/some/folder-path/"), filter),
				})),
			}))

			Expect(db.TimelineGroups.UpdateFilter(ctx, "/:some:folder-path:/", nil)).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))
		})

		It("Update with empty filter", func() {
			filter := &docs.PhotoListFilterExpr{
				Locations: []string{"/city/"},
			}
			Expect(db.TimelineGroups.Create(ctx, "/some/folder-path/", "", filter)).To(Equal("/:some:folder-path:/"))
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
					"Exprs": ConsistOf(folderFilter("/some/folder-path/"), filter),
				})),
			}))

			Expect(db.TimelineGroups.UpdateFilter(ctx, "/:some:folder-path:/", &docs.PhotoListFilterExpr{})).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/:some:folder-path:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/some/folder-path/")),
				"GroupBase.Depth":   PointTo(Equal(1)),
				"Filter":            Equal(folderFilter("/some/folder-path/")),
			}))
		})

	})

	Describe("Sync", func() {
		It("Empty folder", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/")).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/folder/")),
				"StartDate":         BeNil(),
				"EndDate":           BeNil(),
			}))

		})

		It("Folder with photos", func() {
			addPhoto("folder/photo1.jpg", "2020:01:02 01:02:03")
			addPhoto("folder/photo2.jpg", "2020:01:03 11:12:13")

			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/")).ToNot(HaveOccurred())

			startDate := time.Date(2020, time.January, 2, 1, 2, 3, 0, time.UTC)
			endDate := time.Date(2020, time.January, 3, 11, 12, 13, 0, time.UTC)

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display":   PointTo(Equal("/folder/")),
				"GroupBase.SortOrder": PointTo(BeEquivalentTo(startDate.Unix() * 1000)),
				"StartDate":           PointTo(Equal(startDate)),
				"EndDate":             PointTo(Equal(endDate)),
			}))
		})
	})

	Describe("ListChildren", func() {
		It("SortOrder by time", func() {
			addPhoto("folder/photo1.jpg", "2020:01:02 01:02:03")
			addPhoto("folder/photo2.jpg", "2020:01:03 11:12:13")

			Expect(db.TimelineGroups.Create(ctx, "/folder/", "ZZZ", &docs.PhotoListFilterExpr{
				Dates: []docs.PhotoListFilterDate{{Value: "20200102", CmpOp: list.CmpEq}},
			})).ToNot(HaveLen(0))
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/zzz/")).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Create(ctx, "/folder/", "AAA", &docs.PhotoListFilterExpr{
				Dates: []docs.PhotoListFilterDate{{Value: "20200103", CmpOp: list.CmpEq}},
			})).ToNot(HaveLen(0))
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/aaa/")).ToNot(HaveOccurred())

			groups, err := db.TimelineGroups.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/:folder:/",
				ProjectionKeys:   []string{"id"},
				VisibilityTokens: []string{docs.VisTokenAll},
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(groups).To(HaveLen(2))

			Expect(groups[0].ID).To(PointTo(Equal("/:folder:/zzz/")))
			Expect(groups[1].ID).To(PointTo(Equal("/:folder:/aaa/")))

		})

		It("Different depths", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "a|b|c", nil)).ToNot(HaveLen(0))
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "y|z", nil)).ToNot(HaveLen(0))

			// Default depth
			groups, err := db.TimelineGroups.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/:folder:/",
				ProjectionKeys:   []string{"id"},
				VisibilityTokens: []string{docs.VisTokenAll},
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(groups).To(HaveLen(2))

			Expect(groups[0].ID).To(PointTo(Equal("/:folder:/a/")))
			Expect(groups[1].ID).To(PointTo(Equal("/:folder:/y/")))

			// Depth of 2
			groups, err = db.TimelineGroups.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/:folder:/",
				ProjectionKeys:   []string{"id"},
				VisibilityTokens: []string{docs.VisTokenAll},
				MaxRelDepth:      2,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(groups).To(HaveLen(4))

			Expect(groups[0].ID).To(PointTo(Equal("/:folder:/a/")))
			Expect(groups[1].ID).To(PointTo(Equal("/:folder:/y/")))
			Expect(groups[2].ID).To(PointTo(Equal("/:folder:/a/b/")))
			Expect(groups[3].ID).To(PointTo(Equal("/:folder:/y/z/")))

			// Infinite depth
			groups, err = db.TimelineGroups.ListChildren(ctx, &apis.GroupListParams{
				ID:               "/:folder:/",
				ProjectionKeys:   []string{"id"},
				VisibilityTokens: []string{docs.VisTokenAll},
				MaxRelDepth:      -1,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(groups).To(HaveLen(5))

			Expect(groups[0].ID).To(PointTo(Equal("/:folder:/a/")))
			Expect(groups[1].ID).To(PointTo(Equal("/:folder:/y/")))
			Expect(groups[2].ID).To(PointTo(Equal("/:folder:/a/b/")))
			Expect(groups[3].ID).To(PointTo(Equal("/:folder:/y/z/")))
			Expect(groups[4].ID).To(PointTo(Equal("/:folder:/a/b/c/")))

		})
	})

	Describe("SelfVisTokens", func() {
		It("Newly created timeline group", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))

		})

		It("Add and delete", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))

			// Add
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))

			// Delete
			Expect(db.TimelineGroups.DeleteVisibilityToken(ctx, "/:folder:/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
				"SelfVisTokensWrapper.SelfVisTokens": HaveLen(0),
			}))
		})
	})

	Describe("RichText", func() {
		It("Update", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": BeNil(),
			}))

			// Update to non-empty text
			Expect(db.TimelineGroups.Update(ctx, "/:folder:/", apis.NewGroupUpdate().SetRichText("Hello world"))).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": PointTo(Equal("Hello world")),
			}))

			// Update to empty text
			Expect(db.TimelineGroups.Update(ctx, "/:folder:/", apis.NewGroupUpdate().SetRichText(""))).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.RichText": BeNil(),
			}))

		})
	})

	Describe("Delete", func() {
		It("Top level group", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "", nil)).To(Equal("/:folder:/"))
			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1},
			))

			Expect(db.TimelineGroups.Delete(ctx, "/:folder:/")).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 0},
			))

		})

		It("Child group", func() {
			Expect(db.TimelineGroups.Create(ctx, "/folder/", "child", nil)).To(Equal("/:folder:/child/"))
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/child/", docs.VisTokenPublic, false))

			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1},
			))
			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
			))

			// Fail to delete
			Expect(db.TimelineGroups.Delete(ctx, "/:folder:/")).To(HaveOccurred())
			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1},
			))

			// Delete child group
			Expect(db.TimelineGroups.Delete(ctx, "/:folder:/child/")).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 0},
			))
			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 0, docs.VisTokenPublic: 0},
			))

		})

	})

})
