// SPDX-License-Identifier: MIT

package visibility_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = Describe("Visibility", func() {
	BeforeEach(func() {
		clearTestDatabase()
	})

	It("Get non-existent", func() {
		_, err := db.Visibility.Get(ctx, "secret", nil)
		Expect(err).To(HaveOccurred())
	})

	It("GetOrCreate", func() {
		token, err := db.Visibility.GetOrCreate(ctx, "My Secret", nil)
		Expect(err).ToNot(HaveOccurred())
		Expect(token.ID).To(PointTo(Equal("/my-secret/")))

		Expect(db.Visibility.Get(ctx, "/my-secret/", nil)).To(MatchGroupBasePtr(Fields{
			"GroupBase.ID":      PointTo(Equal(*token.ID)),
			"GroupBase.Display": PointTo(Equal("My Secret")),
		}))

		// Query again - should not recreate
		_, err = db.Visibility.GetOrCreate(ctx, "/My Secret", nil)
		Expect(err).ToNot(HaveOccurred())

	})

	Describe("List", func() {
		It("Empty", func() {
			tokens, err := db.Visibility.List(ctx, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(tokens).To(HaveLen(0))
		})

		It("Some tokens", func() {
			Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())
			Expect(db.Visibility.GetOrCreate(ctx, "Private", nil)).ToNot(BeNil())
			Expect(db.Visibility.GetOrCreate(ctx, "private2", nil)).ToNot(BeNil())

			tokens, err := db.Visibility.List(ctx, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(tokens).To(HaveLen(3))

			Expect(tokens[0].ID).To(PointTo(Equal("/private/")))
			Expect(tokens[1].ID).To(PointTo(Equal("/private2/")))
			Expect(tokens[2].ID).To(PointTo(Equal("/xyz/")))
		})
	})

	It("DiffAndUpdate", func() {
		Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())
		Expect(db.Visibility.GetOrCreate(ctx, "abc", nil)).ToNot(BeNil())

		// Simulate add token
		tokens := []string{"/xyz/"}
		Expect(db.Visibility.DiffAndUpdate(ctx, nil, tokens, 1)).ToNot(HaveOccurred())

		Expect(db.Visibility.Get(ctx, "/xyz/", nil)).To(MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 2}, nil))

		// Simulate changing token
		oldTokens := tokens
		tokens = []string{"/abc/"}
		Expect(db.Visibility.DiffAndUpdate(ctx, oldTokens, tokens, 1)).ToNot(HaveOccurred())

		Expect(db.Visibility.Get(ctx, "/xyz/", nil)).To(MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 1}, nil))
		Expect(db.Visibility.Get(ctx, "/abc/", nil)).To(MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 2}, nil))

		// Simulate deleting token
		oldTokens = tokens
		Expect(db.Visibility.DiffAndUpdate(ctx, oldTokens, nil, 1)).ToNot(HaveOccurred())

		Expect(db.Visibility.Get(ctx, "/xyz/", nil)).To(MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 1}, nil))
		Expect(db.Visibility.Get(ctx, "/abc/", nil)).To(MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 1}, nil))

	})

	It("CreateLink", func() {
		Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())

		r := "/"
		c := "comment"
		link, err := db.Visibility.CreateLink(ctx, "/xyz/", &docs.VisibilityTokenLink{Redirect: &r, Comment: &c})
		Expect(err).ToNot(HaveOccurred())
		Expect(link).To(PointTo(MatchFields(IgnoreExtras, Fields{
			"ActivationValue": Not(BeNil()),
			"Redirect":        PointTo(Equal("/")),
			"Comment":         PointTo(Equal("comment")),
		})))

		r = "/folder/"
		c = "hello"
		link2, err := db.Visibility.CreateLink(ctx, "/xyz/", &docs.VisibilityTokenLink{Redirect: &r, Comment: &c})
		Expect(err).ToNot(HaveOccurred())
		Expect(link2).To(PointTo(MatchFields(IgnoreExtras, Fields{
			"ActivationValue": Not(BeNil()),
			"Redirect":        PointTo(Equal("/folder/")),
			"Comment":         PointTo(Equal("hello")),
		})))

		Expect(*link.ActivationValue).ToNot(Equal(*link2.ActivationValue))

		token, err := db.Visibility.Get(ctx, "/xyz/", nil)
		Expect(err).ToNot(HaveOccurred())
		Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
			"Links": ConsistOf(
				MatchFields(IgnoreExtras, Fields{
					"Redirect": PointTo(Equal("/")),
					"Comment":  PointTo(Equal("comment")),
				}),
				MatchFields(IgnoreExtras, Fields{
					"Redirect": PointTo(Equal("/folder/")),
					"Comment":  PointTo(Equal("hello")),
				}),
			),
		})))

	})

	Describe("UpdateLink", func() {
		var linkActValue string

		BeforeEach(func() {
			Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())

			link, err := db.Visibility.CreateLink(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			linkActValue = *link.ActivationValue
		})

		It("Update all fields", func() {
			r := "/dir"
			c := "comment"
			d := false

			Expect(db.Visibility.UpdateLink(ctx, linkActValue,
				&docs.VisibilityTokenLink{Redirect: &r, Comment: &c, Disabled: &d})).ToNot(HaveOccurred())

			token, err := db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect": PointTo(Equal("/dir")),
						"Comment":  PointTo(Equal("comment")),
						"Disabled": PointTo(BeFalse()),
					}),
				),
			})))

		})

		It("Update just one field", func() {
			d := true

			Expect(db.Visibility.UpdateLink(ctx, linkActValue,
				&docs.VisibilityTokenLink{Disabled: &d})).ToNot(HaveOccurred())

			token, err := db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Disabled": PointTo(BeTrue()),
					}),
				),
			})))

		})

		It("Try to update activation value", func() {
			actValue := "asdf"
			d := true

			Expect(db.Visibility.UpdateLink(ctx, linkActValue,
				&docs.VisibilityTokenLink{ActivationValue: &actValue, Disabled: &d})).To(HaveOccurred())

			token, err := db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Not(Equal(actValue))),
					}),
				),
			})))

		})

		It("Try to update non-existing activation value", func() {
			d := true

			Expect(db.Visibility.UpdateLink(ctx, "non-existent-value",
				&docs.VisibilityTokenLink{Disabled: &d})).To(HaveOccurred())

			token, err := db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": HaveLen(1),
			})))

		})

	})

	Describe("ActivateLink", func() {
		var linkActValue string
		var linkActValue2 string

		BeforeEach(func() {
			Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())

			p := "/path/"
			link, err := db.Visibility.CreateLink(ctx, "/xyz/", &docs.VisibilityTokenLink{Redirect: &p})
			Expect(err).ToNot(HaveOccurred())
			linkActValue = *link.ActivationValue

			p = "/blah/"
			link, err = db.Visibility.CreateLink(ctx, "/xyz/", &docs.VisibilityTokenLink{Redirect: &p})
			Expect(err).ToNot(HaveOccurred())
			linkActValue2 = *link.ActivationValue

		})

		It("Activate link 1", func() {
			token, err := db.Visibility.ActivateLink(ctx, linkActValue)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID": PointTo(Equal("/xyz/")),
				"Links": ConsistOf(MatchFields(IgnoreExtras, Fields{
					"Redirect": PointTo(Equal("/path/")),
				})),
			}))

			// Check activation counts
			token, err = db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/path/")),
						"ActivationCount": PointTo(Equal(1)),
					}),
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/blah/")),
						"ActivationCount": BeNil(),
					}),
				),
			})))

		})

		It("Activate link 2", func() {
			token, err := db.Visibility.ActivateLink(ctx, linkActValue2)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID": PointTo(Equal("/xyz/")),
				"Links": ConsistOf(MatchFields(IgnoreExtras, Fields{
					"Redirect": PointTo(Equal("/blah/")),
				})),
			}))

			// Check activation counts
			token, err = db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/path/")),
						"ActivationCount": BeNil(),
					}),
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/blah/")),
						"ActivationCount": PointTo(Equal(1)),
					}),
				),
			})))

		})

		It("Try to activate disabled link", func() {
			d := true
			Expect(db.Visibility.UpdateLink(ctx, linkActValue2, &docs.VisibilityTokenLink{Disabled: &d})).ToNot(HaveOccurred())

			token, err := db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect": PointTo(Equal("/path/")),
						"Disabled": BeNil(),
					}),
					MatchFields(IgnoreExtras, Fields{
						"Redirect": PointTo(Equal("/blah/")),
						"Disabled": PointTo(BeTrue()),
					}),
				),
			})))

			// Try to activate disabled link
			_, err = db.Visibility.ActivateLink(ctx, linkActValue2)
			Expect(err).To(HaveOccurred())

			// Check activation counts
			token, err = db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/path/")),
						"ActivationCount": BeNil(),
					}),
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/blah/")),
						"ActivationCount": BeNil(),
					}),
				),
			})))

			// Activate the non-disabled link. Should work.
			token, err = db.Visibility.ActivateLink(ctx, linkActValue)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID": PointTo(Equal("/xyz/")),
				"Links": ConsistOf(MatchFields(IgnoreExtras, Fields{
					"Redirect": PointTo(Equal("/path/")),
				})),
			}))

			// Check activation counts
			token, err = db.Visibility.Get(ctx, "/xyz/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/path/")),
						"ActivationCount": PointTo(Equal(1)),
					}),
					MatchFields(IgnoreExtras, Fields{
						"Redirect":        PointTo(Equal("/blah/")),
						"ActivationCount": BeNil(),
					}),
				),
			})))
		})
	})

	Describe("GetByActivationValue", func() {
		var linkActValue string

		BeforeEach(func() {
			Expect(db.Visibility.GetOrCreate(ctx, "xyz", nil)).ToNot(BeNil())

			p := "/path/"
			link, err := db.Visibility.CreateLink(ctx, "/xyz/", &docs.VisibilityTokenLink{Redirect: &p})
			Expect(err).ToNot(HaveOccurred())
			linkActValue = *link.ActivationValue
		})

		It("Get", func() {
			Expect(db.Visibility.GetByActivationValue(ctx, linkActValue, nil)).To(MatchGroupBasePtr(Fields{
				"GroupBase.ID": PointTo(Equal("/xyz/")),
			}))
		})

		It("Get disabled link", func() {
			d := true
			Expect(db.Visibility.UpdateLink(ctx, linkActValue, &docs.VisibilityTokenLink{Disabled: &d})).ToNot(HaveOccurred())

			_, err := db.Visibility.GetByActivationValue(ctx, linkActValue, nil)
			Expect(err).To(HaveOccurred())
		})

		It("Get with non-existent activation value", func() {
			_, err := db.Visibility.GetByActivationValue(ctx, linkActValue+"0", nil)
			Expect(err).To(HaveOccurred())
		})
	})

})
