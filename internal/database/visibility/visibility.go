// SPDX-License-Identifier: MIT

package visibility

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Visibility
type apiImpl struct {
	mdb *mongo.Database
}

func isValidName(name string) bool {
	return len(strings.TrimSpace(name)) > 0
}

// IsPublic checks if the public token is in the token array
func IsPublic(tokens []string) bool {
	for _, token := range tokens {
		if token == docs.VisTokenPublic {
			return true
		}
	}
	return false
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database) apis.Visibility {
	return &apiImpl{mdb: mdb}
}

const collectionName = "photos-visibility-tokens"

var tokenBSONKeys = utils.CombineBSONKeys(
	utils.ExtractBSONKeys(docs.VisibilityToken{}),
	utils.PrefixBSONKeys(utils.ExtractBSONKeys(docs.VisibilityTokenLink{}), "links."),
)

// Collection returns the folder collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	// No separator
	return nil
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(tokenBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create a new token. Equivalent id must not already exist.
func (api *apiImpl) Create(ctx context.Context, name string) (string, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.Create",
		"name": name,
	})

	if !isValidName(name) {
		l.Error("Invalid visibility token name")
		return "", apis.ErrInvalidArgs
	}

	return groups.Create(ctx, api, name, false, &groups.CreateOptions{LowerCase: true})
}

// Get returns information about a visibility token
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string) (*docs.VisibilityToken, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(tokenBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Query
	res := api.Collection().FindOne(ctx, bson.M{"_id": id}, options.FindOne().SetProjection(proj))

	var doc docs.VisibilityToken
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode token document")
		}
		return nil, err
	}

	return &doc, nil
}

func (api *apiImpl) GetByActivationValue(ctx context.Context, actValue string, projKeys []string) (*docs.VisibilityToken, error) {
	l := log.WithFields(log.Fields{
		"func":     "visibility.apiImpl.GetByActivationValue",
		"actValue": actValue,
	})

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(tokenBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Link must not be disabled.
	filter := bson.M{
		"links": bson.M{
			"$elemMatch": bson.M{
				"activationValue": actValue,
				"disabled":        bson.M{"$ne": true},
			},
		},
	}

	res := api.Collection().FindOne(ctx, filter, options.FindOne().SetProjection(proj))

	var doc docs.VisibilityToken
	if err := res.Decode(&doc); err != nil {
		l.WithError(err).Error("Could not find and decode token document")
		return nil, err
	}

	return &doc, nil

}

// GetOrCreate will create a new token only if the associated token id does not already exist. Returns docs.VisibilityToken with id and token value.
func (api *apiImpl) GetOrCreate(ctx context.Context, name string, projKeys []string) (*docs.VisibilityToken, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.GetOrCreate",
		"name": name,
	})

	if !isValidName(name) {
		l.Error("Invalid name")
		return nil, apis.ErrInvalidArgs
	}

	id := utils.SlugifyPathLower(name)

	// Try get
	if kw, err := api.Get(ctx, id, projKeys); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing visibility token")
		}
		return kw, err
	}

	// Create
	if _, err := api.Create(ctx, name); err != nil {
		l.WithError(err).Error("Could not create visibility token")
		return nil, err
	}

	return api.Get(ctx, id, projKeys)
}

func (api *apiImpl) List(ctx context.Context, projKeys []string) ([]docs.VisibilityToken, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.List",
	})

	// Build projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(tokenBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Find. Don't list root.
	cursor, err := api.Collection().Find(ctx, bson.M{"_id": bson.M{"$ne": "/"}}, options.Find().SetProjection(proj).SetSort(bson.M{"_id": 1}))
	if err != nil {
		l.WithError(err).Error("Could not get all visibility tokens")
		return nil, err
	}

	// Decode
	tokens := []docs.VisibilityToken{}
	if err := cursor.All(ctx, &tokens); err != nil {
		l.WithError(err).Error("Could not decode documents")
		return nil, err
	}

	return tokens, nil
}

// DiffAndUpdate updates visibility tokens based on a list of old and new tokens. All and Public tokens are skipped.
// Only VisTokenAll is updated in the photo count.
func (api *apiImpl) DiffAndUpdate(ctx context.Context, inOldIDs []string, inNewIDs []string, count int) error {
	var oldIDs []string
	var oldTokens []string
	if inOldIDs != nil {
		oldTokens = []string{docs.VisTokenAll}
		oldIDs = make([]string, 0, len(inOldIDs))
		for _, o := range inOldIDs {
			if o != string(docs.VisTokenAll) && o != string(docs.VisTokenPublic) {
				oldIDs = append(oldIDs, string(o))
			}
		}
	}

	var newIDs []string
	var newTokens []string
	if inNewIDs != nil {
		newTokens = []string{docs.VisTokenAll}
		newIDs = make([]string, 0, len(inNewIDs))
		for _, o := range inNewIDs {
			if o != string(docs.VisTokenAll) && o != string(docs.VisTokenPublic) {
				newIDs = append(newIDs, string(o))
			}
		}
	}

	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)

}

func (api *apiImpl) CreateLink(ctx context.Context, id string, initial *docs.VisibilityTokenLink) (*docs.VisibilityTokenLink, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.CreateLink",
		"id":   id,
	})

	if initial != nil &&
		(initial.ActivationValue != nil || initial.ActivationCount != nil || initial.Disabled != nil) {
		l.Error("Trying to create link with unsupported initial values")
		return nil, apis.ErrInvalidArgs
	}

	// Generate link activation value, which is a 24-byte id.
	actValue, err := utils.GenerateSecureRandBase64(24)
	if err != nil {
		l.WithError(err).Error("Could not generate activation value")
		return nil, err
	}

	// Initialize link fields.
	link := docs.VisibilityTokenLink{
		ActivationValue: &actValue,
	}
	if initial != nil {
		link.Redirect = initial.Redirect
		link.Comment = initial.Comment
	}

	// Insert the link.
	update := bson.M{
		"$push": bson.M{
			"links": link,
		},
	}

	res, err := api.Collection().UpdateOne(ctx, bson.M{"_id": id}, update)
	if err != nil {
		l.WithError(err).Error("Could not push new link")
		return nil, err
	} else if res.ModifiedCount != 1 {
		l.Error("Could not find token with id")
		return nil, apis.ErrInvalidID
	}

	return &link, nil

}

func (api *apiImpl) UpdateLink(ctx context.Context, actValue string, toUpdate *docs.VisibilityTokenLink) error {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.UpdateLink",
	})

	if toUpdate.ActivationValue != nil || toUpdate.ActivationCount != nil {
		l.Error("Trying to update link with unsupported values")
		return apis.ErrInvalidArgs
	}

	// Update
	updateFields := bson.M{}
	if toUpdate.Redirect != nil {
		updateFields["links.$.redirect"] = *toUpdate.Redirect
	}
	if toUpdate.Comment != nil {
		updateFields["links.$.comment"] = *toUpdate.Comment
	}
	if toUpdate.Disabled != nil {
		updateFields["links.$.disabled"] = *toUpdate.Disabled
	}

	update := bson.M{"$set": updateFields}

	// Update link
	filter := bson.M{"links.activationValue": actValue}
	res, err := api.Collection().UpdateOne(ctx, filter, update)
	if err != nil {
		l.WithError(err).Error("Could not update link")
		return err
	} else if res.MatchedCount == 0 {
		l.Error("Could not find link")
		return apis.ErrInvalidID
	}

	return nil
}

func (api *apiImpl) ActivateLink(ctx context.Context, actValue string) (*docs.VisibilityToken, error) {
	l := log.WithFields(log.Fields{
		"func": "visibility.apiImpl.ActivateLink",
	})

	// Activate link by updating its activation count. Link must not be disabled.
	filter := bson.M{
		"links": bson.M{
			"$elemMatch": bson.M{
				"activationValue": actValue,
				"disabled":        bson.M{"$ne": true},
			},
		},
	}
	update := bson.M{"$inc": bson.M{"links.$.activationCount": 1}}

	res := api.Collection().FindOneAndUpdate(ctx, filter, update,
		options.FindOneAndUpdate().SetProjection(bson.M{"_id": 1, "value": 1, "links.$": 1}),
	)

	var token docs.VisibilityToken
	if err := res.Decode(&token); err != nil {
		l.WithError(err).Error("Could not activate link")
		return nil, err
	}

	l.Infof("Activated visibility token %v", *token.ID)

	return &token, nil

}
