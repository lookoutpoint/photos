// SPDX-License-Identifier: MIT

package list

import "fmt"

// RefOperator is a comparison operator for a reference value used for determining
// a range of documents to examine
type RefOperator string

// Operators for reference values. These values are chosen intentionally to match mongodb operators
const (
	RefOpGt  RefOperator = "gt"
	RefOpGte RefOperator = "gte"
	RefOpLt  RefOperator = "lt"
	RefOpLte RefOperator = "lte"
)

// Validate checks that the operator is valid
func (op RefOperator) Validate() error {
	if op == RefOpGt || op == RefOpGte || op == RefOpLt || op == RefOpLte {
		return nil
	}
	return fmt.Errorf("Invalid list operand '%s'", op)
}

// LogicalOperator is a logical operator for joining expressions
type LogicalOperator string

// List logical operators. These values are chosen to intentionally to match mongodb operators
const (
	LogicalAnd LogicalOperator = "and"
	LogicalOr  LogicalOperator = "or"
)

// Validate checks that the operator is valid
func (op LogicalOperator) Validate() error {
	if op == LogicalAnd || op == LogicalOr {
		return nil
	}
	return fmt.Errorf("Invalid list logical op '%s'", op)
}

// ComparisonOperator is a general comparison operator against a value
type ComparisonOperator string

// List comparison operators
const (
	CmpEq  ComparisonOperator = "eq"
	CmpNe  ComparisonOperator = "ne"
	CmpGt  ComparisonOperator = "gt"
	CmpGte ComparisonOperator = "gte"
	CmpLt  ComparisonOperator = "lt"
	CmpLte ComparisonOperator = "lte"
)

// Validate checks that the operator is valid
func (op ComparisonOperator) Validate() error {
	if op == CmpEq || op == CmpNe || op == CmpGt || op == CmpGte || op == CmpLt || op == CmpLte {
		return nil
	}
	return fmt.Errorf("Invalid list comparison op '%s'", op)
}
