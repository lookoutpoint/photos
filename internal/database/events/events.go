// SPDX-License-Identifier: MIT

package events

import (
	"context"
	"errors"
	"net/url"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"

	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Implements apis.Users
type apiImpl struct {
	mdb *mongo.Database
	top *apis.APIs
}

type validationContext struct {
	ctx    context.Context
	apis   *apis.APIs
	tokens []string
	l      *log.Entry
}

type validateEvent func(vctx validationContext, event *apis.EventInfo) bool
type validateString func(vctx validationContext, s string) bool

func makeValidateEvent(validateObject validateString, validateContext validateString) validateEvent {
	return func(vctx validationContext, event *apis.EventInfo) bool {
		// Object and Context must be provided. No Extra is allowed.
		if len(event.Object) == 0 || len(event.Context) == 0 || len(event.Extra) != 0 {
			return false
		}
		return validateObject(vctx, event.Object) && validateContext(vctx, event.Context)
	}
}

func makeValidateEventNoObj(validateContext validateString) validateEvent {
	return func(vctx validationContext, event *apis.EventInfo) bool {
		// Context must be provided. No Object or Extra is allowed.
		if len(event.Object) != 0 || len(event.Context) == 0 || len(event.Extra) != 0 {
			return false
		}
		return validateContext(vctx, event.Context)
	}
}

func validateEmptyEvent(vctx validationContext, event *apis.EventInfo) bool {
	return len(event.Object) == 0 && len(event.Context) == 0 && len(event.Extra) == 0
}

var validButtons = map[string]bool{
	"group/children":      true,
	"group/description":   true,
	"group/highlights":    true,
	"group/info":          true,
	"group/map":           true,
	"group/more":          true,
	"group/order":         true,
	"group/orderByCamera": true,
	"group/orderByUpload": true,
	"group/photos":        true,
	"group/reset":         true,
	"group/timeline":      true,
	"lightbox/close":      true,
	"lightbox/info":       true,
	"lightbox/nav/next":   true,
	"lightbox/nav/prev":   true,
	"lightbox/slideshow":  true,
	"location/hikes":      true,
	"location/parks":      true,
	"menu":                true,
	"park/hikes":          true,
	"scrollToTop":         true,
	"site/theme":          true,
	"timeline/size":       true,
}

func validateButton(vctx validationContext, object string) bool {
	_, ok := validButtons[object]
	return ok
}

var validLinks = map[string]bool{
	"/":                   true,
	"header/logo":         true,
	"home/explore/blogs":  true,
	"home/explore/recent": true,
	"/recent/":            true,
	"/blogs/":             true,
	"/locations/@hikes/":  true,
	"/locations/@parks/":  true,
}

func validateLink(vctx validationContext, object string) bool {
	_, ok := validLinks[object]
	return ok
}

func validateGroupID(vctx validationContext, object string) bool {
	// A valid group id has the following form: /<group-type><id>
	// where id starts with /.
	ok := false
	if strings.HasPrefix(object, "/category/") {
		id := object[len("/category"):]
		_, err := vctx.apis.Categories.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else if strings.HasPrefix(object, "/date/") {
		id := object[len("/date"):]
		_, err := vctx.apis.Dates.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else if strings.HasPrefix(object, "/folder/") {
		id := object[len("/folder"):]
		_, err := vctx.apis.Folders.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else if strings.HasPrefix(object, "/keyword/") {
		id := object[len("/keyword"):]
		_, err := vctx.apis.Keywords.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else if strings.HasPrefix(object, "/location/") {
		id := object[len("/location"):]
		_, err := vctx.apis.Locations.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else if strings.HasPrefix(object, "/timeline/") {
		id := object[len("/timeline"):]
		_, err := vctx.apis.TimelineGroups.Get(vctx.ctx, id, []string{}, vctx.tokens)
		ok = err == nil
	} else {
		// Could be a page.
		_, err := vctx.apis.Pages.Get(vctx.ctx, object, []string{}, vctx.tokens)
		ok = err == nil
	}

	if !ok {
		vctx.l.WithField("object", object).Error("Failed validateGroupID")
	}
	return ok
}

func validatePhotoID(vctx validationContext, object string) bool {
	// A valid photo id has the following form: /photo/<id>
	if !strings.HasPrefix(object, "/photo/") {
		vctx.l.WithField("photoid", object).Error("Failed validatePhotoID")
		return false
	}
	id := docs.PhotoID(object[len("/photo/"):])
	_, err := vctx.apis.Photos.Get(vctx.ctx, id, []string{}, vctx.tokens)
	if err != nil {
		vctx.l.WithError(err).WithField("photoid", object).Error("Failed validatePhotoID")
	}
	return err == nil
}

const MAX_URL = 200

func validateSiteVisitEvent(vctx validationContext, event *apis.EventInfo) bool {
	// No object
	if len(event.Object) != 0 {
		return false
	}

	// Context: must be a valid relative URL
	if len(event.Context) == 0 || event.Context[0] != '/' {
		return false
	}

	_, err := url.ParseRequestURI(event.Context)
	if err != nil {
		return false
	}

	// Unescape URL
	url, err := url.PathUnescape(event.Context)
	if err != nil {
		return false
	}
	event.Context = url

	// Limit URL so that database entry can't grow arbitrarily large
	if len(url) > MAX_URL {
		event.Context = url[:MAX_URL]
	}

	// Limit Extra (referrer) to MAX_URL
	if len(event.Extra) > MAX_URL {
		event.Extra = event.Extra[:MAX_URL]
	}

	return true
}

// Valid fixed contexts.
var validContexts = map[string]bool{
	"blog":                         true,
	"home":                         true,
	"home/explore/blogs":           true,
	"home/explore/categories":      true,
	"home/explore/folders":         true,
	"home/explore/keywords":        true,
	"home/explore/locations":       true,
	"home/explore/recent":          true,
	"home/hlphoto/inline-lightbox": true,
	"home/hlphoto/lightbox":        true,
	"home/top-bar":                 true,
	"menu":                         true,
	"recent":                       true,
}

func validateCommonContext(vctx validationContext, context string) bool {
	// Check fixed contexts.
	if _, ok := validContexts[context]; ok {
		return true
	}

	// Photo ids are also valid contexts.
	if strings.HasPrefix(context, "/photo/") {
		return validatePhotoID(vctx, context)
	}

	// Group ids are also valid contexts.

	// Timeline groups have additional context which can be reduced to the timeline group check.
	if strings.HasPrefix(context, "/timeline-story/") {
		context = "/timeline" + context[len("/timeline-story"):]
	} else if strings.HasPrefix(context, "/timeline-photos/") {
		context = "/timeline" + context[len("/timeline-photos"):]
	}

	return validateGroupID(vctx, context)
}

var validViews = map[string]bool{
	"children": true,
	"photos":   true,
	"timeline": true,
	"parks":    true,
	"hikes":    true,
	"recent":   true,
	"info":     true,
	"map":      true,
}

func validateView(vctx validationContext, view string) bool {
	_, ok := validViews[view]
	return ok
}

func validateLinkClickEvent(vctx validationContext, event *apis.EventInfo) bool {
	// Basic validation.
	v := makeValidateEvent(validateLink, validateCommonContext)
	if v(vctx, event) {
		return true
	}

	// The link may actually be a page, so try to validate against a group click.
	v = makeValidateEvent(validateGroupID, validateCommonContext)
	if v(vctx, event) {
		event.Event = "group/click"
		return true
	}

	return false
}

// List of allowed events. Explicit so that nobody can just inject their own events.
var validEvents = map[string]validateEvent{
	"button/click":             makeValidateEvent(validateButton, validateCommonContext),
	"group/click":              makeValidateEvent(validateGroupID, validateCommonContext),
	"lightbox/show":            makeValidateEventNoObj(validateCommonContext),
	"lightbox/slideshow/photo": makeValidateEventNoObj(validateCommonContext),
	"lightbox/swipe/next":      makeValidateEventNoObj(validateCommonContext),
	"lightbox/swipe/prev":      makeValidateEventNoObj(validateCommonContext),
	"link/click":               validateLinkClickEvent,
	"page/view":                makeValidateEventNoObj(validateCommonContext),
	"page/view-type":           makeValidateEvent(validateView, validateCommonContext),
	"photo/click":              makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/cover":         makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/detail":        makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/home":          makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/lightbox":      makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/thumb":         makeValidateEvent(validatePhotoID, validateCommonContext),
	"photo/view/zoom":          makeValidateEvent(validatePhotoID, validateCommonContext),
	"theme/dark":               validateEmptyEvent,
	"theme/light":              validateEmptyEvent,
	"site/visit":               validateSiteVisitEvent,
}

var ErrInvalidEvent = errors.New("invalid event")

func toEventDate(t time.Time) time.Time {
	// Only keep year, month and day. Use UTC time.
	t = t.UTC()
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
}

// NewAPI returns a new apiImpl object for events
func NewAPI(mdb *mongo.Database, top *apis.APIs) apis.Events {
	return &apiImpl{mdb: mdb, top: top}
}

const collectionName = "events"

// Collection returns the events collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

func (api *apiImpl) Add(ctx context.Context, events []apis.EventInfo, tokens []string) error {
	l := log.WithFields(log.Fields{
		"func": "events.apiImpl.Add",
	})

	// Date for all events added here.
	now := toEventDate(time.Now().UTC())

	vctx := validationContext{
		ctx:    ctx,
		apis:   api.top,
		tokens: tokens,
		l:      l,
	}

	// Best effort: add any valid events
	var err error
	for _, event := range events {
		if event.Count < 0 {
			l.WithField("event", event).Warn("Event with negative count")
			err = ErrInvalidEvent
			continue
		}

		// Validate event.
		validate, ok := validEvents[event.Event]
		if !ok {
			l.WithField("event", event.Event).Error("Unrecogonized event")
			err = ErrInvalidEvent
			continue
		}

		if !validate(vctx, &event) {
			l.WithField("event", event).Error("Event failed validation")
			err = ErrInvalidEvent
			continue
		}

		// Update
		filter := bson.M{
			"event": event.Event,
			"date":  now,
		}
		if len(event.Object) != 0 {
			filter["object"] = event.Object
		} else {
			filter["object"] = bson.M{"$exists": false}
		}
		if len(event.Context) != 0 {
			filter["context"] = event.Context
		} else {
			filter["context"] = bson.M{"$exists": false}
		}
		if event.Date != nil {
			filter["date"] = *event.Date
		}
		if len(event.Extra) != 0 {
			filter["extra"] = event.Extra
		}

		count := event.Count
		if count == 0 {
			count = 1
		}
		update := bson.M{
			"$inc": bson.M{"count": count},
		}

		_, dbErr := api.Collection().UpdateOne(ctx, filter, update, options.Update().SetUpsert(true))
		if dbErr != nil {
			l.WithError(dbErr).WithField("filter", filter).Error("Could not update event counter")
			err = dbErr
			continue
		}
	}

	return err
}

func (api *apiImpl) Count(ctx context.Context, query apis.EventCountQuery) ([]apis.EventCountResult, error) {
	l := log.WithFields(log.Fields{
		"func":  "events.apiImpl.Count",
		"query": query,
	})

	// Build filter.
	filter := bson.M{}

	if query.StartDate != nil || query.EndDate != nil {
		dateFilter := bson.M{}
		if query.StartDate != nil {
			dateFilter["$gte"] = toEventDate(*query.StartDate)
		}
		if query.EndDate != nil {
			dateFilter["$lte"] = toEventDate(*query.EndDate)
		}
		filter["date"] = dateFilter
	}

	if query.Event != nil {
		if query.EventRegex {
			filter["event"] = bson.M{"$regex": *query.Event}

		} else {
			filter["event"] = *query.Event
		}
	}

	if query.Object != nil {
		if query.ObjectRegex {
			filter["object"] = bson.M{"$regex": *query.Object}
		} else {
			filter["object"] = *query.Object
		}
	}

	if query.Context != nil {
		if query.ContextRegex {
			filter["context"] = bson.M{"$regex": *query.Context}
		} else {
			filter["context"] = *query.Context
		}
	}

	if query.Extra != nil {
		if query.ExtraRegex {
			filter["extra"] = bson.M{"$regex": *query.Extra}
		} else {
			filter["extra"] = *query.Extra
		}
	}

	// Grouping id
	groupId := bson.M{}
	if query.Granularity != apis.EventAllTime {
		// Some grouping by date.
		date := bson.M{"year": bson.M{"$year": "$date"}}

		if query.Granularity != apis.EventByYear {
			// Month
			date["month"] = bson.M{"$month": "$date"}
		}

		if query.Granularity == apis.EventByDay {
			// Day
			date["day"] = bson.M{"$dayOfMonth": "$date"}
		}

		groupId["date"] = bson.M{"$dateFromParts": date}
	}

	if query.GroupByEvent {
		groupId["event"] = "$event"
	}
	if query.GroupByObject {
		groupId["object"] = "$object"
	}
	if query.GroupByContext {
		groupId["context"] = "$context"
	}
	if query.GroupByExtra {
		groupId["extra"] = "$extra"
	}

	// Aggregation pipeline.
	pipeline := bson.A{
		// Filter
		bson.M{"$match": filter},
		// Group
		bson.M{
			"$group": bson.M{
				"_id": groupId,
				"count": bson.M{
					"$sum": "$count",
				},
			},
		},
		// Sort
		bson.M{"$sort": bson.D{
			{Key: "count", Value: -1},
			{Key: "_id.event", Value: 1},
			{Key: "_id.object", Value: 1},
			{Key: "_id.context", Value: 1},
			{Key: "_id.extra", Value: 1},
		}},
	}

	if query.Limit > 0 {
		// Limit
		pipeline = append(pipeline, bson.M{
			"$limit": query.Limit,
		})
	}

	// Run pipeline.
	cursor, err := api.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline")
		return nil, err
	}

	var results []apis.EventCountResult
	if err := cursor.All(ctx, &results); err != nil {
		l.WithError(err).Error("Could not retrieve aggregation pipeline results")
		return nil, err
	}

	return results, nil
}

func (api *apiImpl) CountOne(ctx context.Context, event string, object *string, context *string, startDate *time.Time, endDate *time.Time) (int, error) {
	l := log.WithFields(log.Fields{
		"func":    "events.apiImpl.CountOne",
		"event":   event,
		"object":  object,
		"context": context,
	})

	// Build filter.
	filter := bson.M{
		"event": event,
	}
	if object != nil {
		filter["object"] = *object
	}
	if context != nil {
		filter["context"] = *context
	}

	if startDate != nil {
		filter["date"] = bson.M{"$gte": toEventDate(*startDate)}
	}
	if endDate != nil {
		filter["date"] = bson.M{"$lte": toEventDate(*endDate)}
	}

	pipeline := bson.A{
		bson.M{"$match": filter},
		bson.M{"$group": bson.M{
			"_id":   "",
			"count": bson.M{"$sum": "$count"},
		}},
	}

	cursor, err := api.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline")
		return -1, err
	}

	var results []struct {
		Count int `bson:"count"`
	}
	if err := cursor.All(ctx, &results); err != nil {
		l.WithError(err).Error("Could not retrieve aggregation result")
		return -1, err
	}

	// Zero matching events
	if len(results) == 0 {
		return 0, nil
	}

	return results[0].Count, err
}

func (api *apiImpl) ConvertDateToNewFormat(ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"func": "events.apiImpl.ConvertDateToNewFormat",
	})

	// Convert all event entries with a Date field to YearMonth and Day fields.
	filter := bson.M{
		"date": bson.M{"$exists": true},
	}
	proj := bson.M{
		"_id":  1,
		"date": 1,
	}

	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query events")
		return err
	}

	count := 0
	for cursor.Next(ctx) {
		var doc struct {
			ID   string `bson:"_id"`
			Date int    `bson:"date"`
		}
		if err := cursor.Decode(&doc); err != nil {
			l.WithError(err).Error("Could not decode document")
			return err
		}

		update := bson.M{
			"$unset": bson.M{"date": 1, "period": 1},
			"$set": bson.M{
				"yearMonth": doc.Date / 100,
				"day":       doc.Date % 100,
			},
		}

		oid, err := primitive.ObjectIDFromHex(doc.ID)
		if err != nil {
			l.WithError(err).Error("Could not parse object id")
			return err
		}

		_, err = api.Collection().UpdateOne(ctx, bson.M{"_id": oid}, update)
		if err != nil {
			l.WithError(err).Error("Could not update event")
			return err
		}

		count = count + 1
	}
	if err := cursor.Err(); err != nil {
		l.WithError(err).Error("Cursor error")
		return err
	}

	l.Infof("Updated %v events", count)

	return nil
}

func (api *apiImpl) ConvertYearMonthDayToDate(ctx context.Context) error {
	l := log.WithFields(log.Fields{
		"func": "events.apiImpl.ConvertYearMonthDayToDate",
	})

	// Convert all event entries with YearMonth and Day fields to a Date field.
	filter := bson.M{
		"yearMonth": bson.M{"$exists": true},
	}
	proj := bson.M{
		"_id":       1,
		"yearMonth": 1,
		"day":       1,
	}

	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query events")
		return err
	}

	count := 0
	for cursor.Next(ctx) {
		var doc struct {
			ID        string `bson:"_id"`
			YearMonth int    `bson:"yearMonth"`
			Day       int    `bson:"day"`
		}
		if err := cursor.Decode(&doc); err != nil {
			l.WithError(err).Error("Could not decode document")
			return err
		}

		update := bson.M{
			"$unset": bson.M{"yearMonth": 1, "day": 1},
			"$set": bson.M{
				"date": time.Date(doc.YearMonth/100, time.Month(doc.YearMonth%100), doc.Day, 0, 0, 0, 0, time.UTC),
			},
		}

		oid, err := primitive.ObjectIDFromHex(doc.ID)
		if err != nil {
			l.WithError(err).Error("Could not parse object id")
			return err
		}

		_, err = api.Collection().UpdateOne(ctx, bson.M{"_id": oid}, update)
		if err != nil {
			l.WithError(err).Error("Could not update event")
			return err
		}

		count = count + 1
	}
	if err := cursor.Err(); err != nil {
		l.WithError(err).Error("Cursor error")
		return err
	}

	l.Infof("Updated %v events", count)
	return nil
}
