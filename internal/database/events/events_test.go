// SPDX-License-Identifier: MIT

package events_test

import (
	"strconv"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

var _ = Describe("Events", func() {
	var tokensAll = []string{docs.VisTokenAll}

	genPhotoMd := func(location string, dateTime string, keywords ...string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 100,
			Xmp: photomd.XmpData{
				Location: location,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: dateTime,
			},
		}
	}

	var photoID string
	var photoID2 string
	BeforeEach(func() {
		clearTestDatabase()

		photo, err := db.Photos.AddOrUpdate(ctx, "", "a/b/photo.jpg", 0, 0, genPhotoMd("Country", "2020:01:01 15:00:00", "key", "animal: cat"))
		Expect(err).ToNot(HaveOccurred())
		photoID = string(*photo.ID)

		photo, err = db.Photos.AddOrUpdate(ctx, "", "c/d/photo.jpg", 0, 0, genPhotoMd("City", "2021:02:03 17:00:00", "cute", "animal: dog"))
		Expect(err).ToNot(HaveOccurred())
		photoID2 = string(*photo.ID)

		_, err = db.TimelineGroups.Create(ctx, "/a/", "timeline", nil)
		Expect(err).ToNot(HaveOccurred())

	})

	Describe("Add", func() {
		It("button/click", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "button/click", Object: "lightbox/info", Context: "home/hlphoto/lightbox"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "button/click", Object: "lightbox/info", Context: "/folder/a/b/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "button/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(2))

		})

		It("button/click - invalid context", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "button/click", Object: "lightbox/info", Context: "home/hlphoto"}}, tokensAll)
			Expect(err).To(HaveOccurred())
		})

		It("group/click", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "group/click", Object: "/date/2020/01/01/", Context: "/photo/" + photoID}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "group/click", Object: "/location/country/", Context: "/photo/" + photoID}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "group/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(2))

		})

		It("lightbox/show", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "lightbox/show", Context: "home/hlphoto/inline-lightbox"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "lightbox/show", Context: "/keyword/key/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "lightbox/show", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(2))

		})

		It("link/click", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "link/click", Object: "home/explore/blogs", Context: "home"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "link/click", Object: "home/explore/recent", Context: "home"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "link/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(2))

		})

		It("link/click on a page", func() {
			_, err := db.Pages.Create(ctx, "about")
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "link/click", Object: "/about/", Context: "home"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "link/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(0))

			count, err = db.Events.CountOne(ctx, "group/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))
		})

		It("page/view", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "page/view", Context: "/category/animal/cat/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "page/view", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("page/view on a page", func() {
			_, err := db.Pages.Create(ctx, "about")
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "page/view", Context: "/about/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "page/view", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("page/view-type", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "page/view-type", Object: "children", Context: "/folder/a/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "page/view-type", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("photo/click", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "photo/click", Object: "/photo/" + photoID, Context: "/category/animal/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "photo/click", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("photo/view in timeline", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "photo/view/thumb", Object: "/photo/" + photoID, Context: "/timeline/:a:/timeline/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "photo/view/thumb", Object: "/photo/" + photoID, Context: "/timeline-photos/:a:/timeline/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "photo/view/thumb", Object: "/photo/" + photoID, Context: "/timeline-story/:a:/timeline/"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "photo/view/thumb", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(3))

		})

		It("photo/view of private photo with valid vis token", func() {
			photo, err := db.Photos.AddOrUpdate(ctx, "", "a/b/private.jpg", 0, 0, genPhotoMd("Country", "2020:01:01 15:00:00", "__visibility:private", "animal: cat"))
			Expect(err).ToNot(HaveOccurred())
			photoID = string(*photo.ID)

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "photo/view/thumb", Object: "/photo/" + photoID, Context: "/folder/a/b/"}}, []string{"/private/"})
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "photo/view/thumb", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))
		})

		It("theme", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "theme/dark"}}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			count, err := db.Events.CountOne(ctx, "theme/dark", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("error with extra", func() {
			// Three different validation function code paths

			err := db.Events.Add(ctx, []apis.EventInfo{{Event: "theme/dark", Extra: "hello"}}, tokensAll)
			Expect(err).To(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "button/click", Object: "lightbox/info", Context: "home/hlphoto/lightbox", Extra: "hello"}}, tokensAll)
			Expect(err).To(HaveOccurred())

			err = db.Events.Add(ctx, []apis.EventInfo{{Event: "lightbox/show", Context: "home/hlphoto/inline-lightbox", Extra: "hello"}}, tokensAll)
			Expect(err).To(HaveOccurred())
		})

		Describe("site/visit", func() {
			It("basic", func() {
				err := db.Events.Add(ctx, []apis.EventInfo{{Event: "site/visit", Context: "/some/url"}}, tokensAll)
				Expect(err).ToNot(HaveOccurred())

				count, err := db.Events.CountOne(ctx, "site/visit", nil, nil, nil, nil)
				Expect(err).ToNot(HaveOccurred())
				Expect(count).To(Equal(1))

			})

			It("w/ extra", func() {
				err := db.Events.Add(ctx, []apis.EventInfo{{Event: "site/visit", Context: "/some/url", Extra: "referrer"}}, tokensAll)
				Expect(err).ToNot(HaveOccurred())

				count, err := db.Events.CountOne(ctx, "site/visit", nil, nil, nil, nil)
				Expect(err).ToNot(HaveOccurred())
				Expect(count).To(Equal(1))

			})
		})
	})

	Describe("Count", func() {
		makeDate := func(year int, month int, day int) *time.Time {
			t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
			return &t
		}

		BeforeEach(func() {
			err := db.Events.Add(ctx, []apis.EventInfo{
				{Event: "photo/click", Object: "/photo/" + photoID, Context: "/category/animal/", Date: makeDate(2022, 1, 1)},
				{Event: "photo/click", Object: "/photo/" + photoID, Context: "/folder/a/", Date: makeDate(2022, 1, 1)},
				{Event: "photo/click", Object: "/photo/" + photoID2, Context: "/keyword/cute/", Date: makeDate(2021, 1, 2)},
				{Event: "photo/view/thumb", Object: "/photo/" + photoID, Context: "/category/animal/", Date: makeDate(2022, 1, 1)},
				{Event: "photo/view/lightbox", Object: "/photo/" + photoID2, Context: "/category/animal/dog/", Date: makeDate(2022, 2, 3)},
				{Event: "group/click", Object: "/folder/a/b/", Context: "/folder/a/", Date: makeDate(2022, 2, 3)},
				{Event: "group/click", Object: "/folder/a/", Context: "home", Date: makeDate(2022, 1, 1)},
			}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

		})

		Describe("Date", func() {
			It("Start date", func() {
				date := time.Date(2021, 12, 11, 0, 0, 0, 0, time.UTC)
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					StartDate:   &date,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(6),
				}))
			})

			It("End date", func() {
				date := time.Date(2021, 12, 11, 0, 0, 0, 0, time.UTC)
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					EndDate:     &date,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(1),
				}))
			})

			It("Start date with hour", func() {
				date := time.Date(2022, 2, 3, 13, 0, 0, 0, time.UTC)
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					StartDate:   &date,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(2),
				}))
			})

			It("End date with hour", func() {
				date := time.Date(2021, 1, 2, 13, 0, 0, 0, time.UTC)
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					EndDate:     &date,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(1),
				}))
			})
		})

		Describe("Granularity", func() {
			It("All time", func() {
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(7),
				}))
			})

			It("By year", func() {
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventByYear,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(MatchAllElements(
					func(e interface{}) string {
						ee := e.(apis.EventCountResult)
						return strconv.Itoa(ee.ID.Date.Year())
					}, Elements{
						"2021": MatchFields(IgnoreExtras, Fields{"Count": Equal(1)}),
						"2022": MatchFields(IgnoreExtras, Fields{"Count": Equal(6)}),
					},
				))
			})

			It("By month", func() {
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventByMonth,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(MatchAllElements(
					func(e interface{}) string {
						ee := e.(apis.EventCountResult)
						return strconv.Itoa(ee.ID.Date.Year()*100 + int(ee.ID.Date.Month()))
					}, Elements{
						"202101": MatchFields(IgnoreExtras, Fields{"Count": Equal(1)}),
						"202201": MatchFields(IgnoreExtras, Fields{"Count": Equal(4)}),
						"202202": MatchFields(IgnoreExtras, Fields{"Count": Equal(2)}),
					},
				))
			})

			It("By day", func() {
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventByDay,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(MatchAllElements(
					func(e interface{}) string {
						ee := e.(apis.EventCountResult)
						return strconv.Itoa(ee.ID.Date.Year()*10000 + int(ee.ID.Date.Month())*100 + ee.ID.Date.Day())
					}, Elements{
						"20210102": MatchFields(IgnoreExtras, Fields{"Count": Equal(1)}),
						"20220101": MatchFields(IgnoreExtras, Fields{"Count": Equal(4)}),
						"20220203": MatchFields(IgnoreExtras, Fields{"Count": Equal(2)}),
					},
				))
			})
		})

		Describe("Match by event", func() {
			It("Plain event", func() {
				event := "group/click"
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					Event:       &event,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(2),
				}))
			})

			It("Regex event", func() {
				event := "^photo/view/"
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					Event:       &event,
					EventRegex:  true,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(2),
				}))
			})
		})

		It("Group by event", func() {
			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:  apis.EventAllTime,
				GroupByEvent: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(4))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event": PointTo(Equal("photo/click")),
				}),
				"Count": Equal(3),
			}))
			Expect(res[1]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event": PointTo(Equal("group/click")),
				}),
				"Count": Equal(2),
			}))
			Expect(res[2]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event": PointTo(Equal("photo/view/lightbox")),
				}),
				"Count": Equal(1),
			}))
			Expect(res[3]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event": PointTo(Equal("photo/view/thumb")),
				}),
				"Count": Equal(1),
			}))
		})

		Describe("Match by object", func() {
			It("Plain object", func() {
				object := "/photo/" + photoID
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					Object:      &object,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(3),
				}))
			})

			It("Regex object", func() {
				object := "^/photo/"
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					Object:      &object,
					ObjectRegex: true,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(5),
				}))
			})
		})

		It("Group by object", func() {
			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:   apis.EventAllTime,
				GroupByObject: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(4))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Object": PointTo(Equal("/photo/" + photoID)),
				}),
				"Count": Equal(3),
			}))
			Expect(res[1]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Object": PointTo(Equal("/photo/" + photoID2)),
				}),
				"Count": Equal(2),
			}))
			Expect(res[2]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Object": PointTo(Equal("/folder/a/")),
				}),
				"Count": Equal(1),
			}))
			Expect(res[3]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Object": PointTo(Equal("/folder/a/b/")),
				}),
				"Count": Equal(1),
			}))
		})

		Describe("Match by context", func() {
			It("Plain context", func() {
				context := "home"
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity: apis.EventAllTime,
					Context:     &context,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(1),
				}))
			})

			It("Regex context", func() {
				context := "^/folder/"
				res, err := db.Events.Count(ctx, apis.EventCountQuery{
					Granularity:  apis.EventAllTime,
					Context:      &context,
					ContextRegex: true,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(res).To(HaveLen(1))
				Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
					"Count": Equal(2),
				}))
			})
		})

		It("Group by context", func() {
			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:    apis.EventAllTime,
				GroupByContext: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(5))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Context": PointTo(Equal("/category/animal/")),
				}),
				"Count": Equal(2),
			}))
			Expect(res[1]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Context": PointTo(Equal("/folder/a/")),
				}),
				"Count": Equal(2),
			}))
			Expect(res[2]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Context": PointTo(Equal("/category/animal/dog/")),
				}),
				"Count": Equal(1),
			}))
			Expect(res[3]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Context": PointTo(Equal("/keyword/cute/")),
				}),
				"Count": Equal(1),
			}))
			Expect(res[4]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Context": PointTo(Equal("home")),
				}),
				"Count": Equal(1),
			}))
		})
	})

	Describe("site/visit event", func() {
		It("Escaped characters in URL", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{
				{Event: "site/visit", Context: "/folder/to-l%C3%A1trar/"},
			}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:    apis.EventAllTime,
				GroupByEvent:   true,
				GroupByContext: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(1))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event":   PointTo(Equal("site/visit")),
					"Context": PointTo(Equal("/folder/to-látrar/")),
				}),
				"Count": Equal(1),
			}))
		})

		It("Excessively long URL", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{
				{Event: "site/visit", Context: "/" + strings.Repeat("a", 250)},
			}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:    apis.EventAllTime,
				GroupByEvent:   true,
				GroupByContext: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(1))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Event":   PointTo(Equal("site/visit")),
					"Context": PointTo(Equal("/" + strings.Repeat("a", 199))),
				}),
				"Count": Equal(1),
			}))
		})

		It("w/ extra", func() {
			err := db.Events.Add(ctx, []apis.EventInfo{
				{Event: "site/visit", Context: "/folders/", Extra: "https://referrer.com"},
			}, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			res, err := db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:  apis.EventAllTime,
				GroupByExtra: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(1))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Extra": PointTo(Equal("https://referrer.com")),
				}),
				"Count": Equal(1),
			}))

			extra := "referrer"
			res, err = db.Events.Count(ctx, apis.EventCountQuery{
				Granularity:  apis.EventAllTime,
				Extra:        &extra,
				ExtraRegex:   true,
				GroupByExtra: true,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(res).To(HaveLen(1))
			Expect(res[0]).To(MatchFields(IgnoreExtras, Fields{
				"ID": MatchFields(IgnoreExtras, Fields{
					"Extra": PointTo(Equal("https://referrer.com")),
				}),
				"Count": Equal(1),
			}))
		})
	})

})
