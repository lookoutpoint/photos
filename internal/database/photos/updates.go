// SPDX-License-Identifier: MIT

package photos

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func bulkUpdatePhotos(ctx context.Context, api apis.Photos, l *log.Entry, filter bson.M, proj bson.M, updateModel func(photo *docs.Photo) mongo.WriteModel) error {
	// Find all photos
	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query photos")
		return err
	}
	defer cursor.Close(ctx)

	// Update each photo. Batch updates into bulk writes.
	var models []mongo.WriteModel
	writeModels := func() error {
		res, err := api.Collection().BulkWrite(ctx, models, options.BulkWrite().SetOrdered(false))
		if err != nil {
			l.WithError(err).Error("Could not do bulk write")
			return err
		}

		l.Infof("Updated %v photos", res.ModifiedCount)
		models = nil
		return nil
	}

	for cursor.Next(ctx) {
		var photo docs.Photo
		if err := cursor.Decode(&photo); err != nil {
			l.WithError(err).Error("Could not decode photo")
			return err
		}

		models = append(models, updateModel(&photo))

		if len(models) == 100 {
			if err := writeModels(); err != nil {
				return err
			}
		}
	}

	if len(models) > 0 {
		if err := writeModels(); err != nil {
			return err
		}
	}

	return nil
}

// AddMissingTsSortKeys is a schema updater to add missing tsSortKey fields
func AddMissingTsSortKeys(ctx context.Context, api apis.Photos) error {
	l := log.WithField("func", "photos.AddMissingTsSortKeys")

	return bulkUpdatePhotos(ctx, api, l,
		// Find all photos without tsSortKey
		bson.M{"tsSortKey": bson.M{"$exists": false}},
		bson.M{"storeObject": 1, "timestamp": 1},
		func(photo *docs.Photo) mongo.WriteModel {
			return mongo.NewUpdateOneModel().
				SetFilter(bson.M{"_id": *photo.ID}).
				SetUpdate(bson.M{
					"$set": bson.M{
						// Update each photo by generating tsSortKey.
						"tsSortKey": generateSortKey(*photo.Timestamp, *photo.StoreObject, *photo.ID, true),
					},
				})
		},
	)
}

// AddLocationsField is a schema updater to add new Locations field (derived from Location field)
func AddLocationsField(ctx context.Context, api apis.Photos) error {
	l := log.WithField("func", "photos.AddLocationsField")

	return bulkUpdatePhotos(ctx, api, l,
		// Find all photos with location field
		bson.M{
			"location":  bson.M{"$exists": true},
			"locations": bson.M{"$exists": false},
		},
		bson.M{"location": 1},
		func(photo *docs.Photo) mongo.WriteModel {
			return mongo.NewUpdateOneModel().
				SetFilter(bson.M{"_id": *photo.ID}).
				SetUpdate(bson.M{
					"$set": bson.M{
						// Derive Locations field
						// Location is deprecated and removed
						// "locations": bson.A{*photo.Location},
					},
				})
		},
	)
}

// RemoveLocationField is a schema updater to remove the deprecated Location field
func RemoveLocationField(ctx context.Context, api apis.Photos) error {
	l := log.WithField("func", "photos.RemoveLocationField")

	return bulkUpdatePhotos(ctx, api, l,
		// Find all photos with location field
		bson.M{
			"location": bson.M{"$exists": true},
		},
		bson.M{},
		func(photo *docs.Photo) mongo.WriteModel {
			return mongo.NewUpdateOneModel().
				SetFilter(bson.M{"_id": *photo.ID}).
				SetUpdate(bson.M{
					"$unset": bson.M{
						"location": "",
					},
				})
		},
	)
}
