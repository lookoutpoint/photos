// SPDX-License-Identifier: MIT

package photos_test

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
)

var errTestOutputIsDifferent = errors.New("test output file is different than reference")

var testOutputDir = ""

func makeTestOutputPath() string {
	if testOutputDir == "" {
		return testOutputRootDir + "/"
	}
	return testOutputRootDir + "/" + testOutputDir + "/"
}

func setTestOutputDir(relPath string) {
	testOutputDir = relPath
	os.RemoveAll(makeTestOutputPath())

	err := os.MkdirAll(makeTestOutputPath(), os.ModePerm)
	if err != nil {
		panic(err)
	}
}

func compareTestOutputFile(filename string) error {
	fileContents, err := os.ReadFile(makeTestOutputPath() + filename)
	if err != nil {
		return err
	}

	refFileContents, err := ioutil.ReadFile("./reference/" + filename)
	if err != nil {
		return err
	}

	if !bytes.Equal(fileContents, refFileContents) {
		return errTestOutputIsDifferent
	}

	return nil
}

func createTestOutputFile(filename string) *os.File {
	file, err := os.Create(makeTestOutputPath() + filename)
	if err != nil {
		panic(err)
	}
	return file
}
