// SPDX-License-Identifier: MIT

package photos_test

import (
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	"gitlab.com/lookoutpoint/storage"
	teststorage "gitlab.com/lookoutpoint/storage/test"
)

var _ = Describe("Photos", func() {
	const bucketName = "lookoutpoint-dev-photo-master"

	tokensAll := []string{docs.VisTokenAll}

	r := func(n int32, d int32) photomd.Rational {
		return photomd.Rational{Num: n, Den: d}
	}

	ur := func(n uint32, d uint32) photomd.Urational {
		return photomd.Urational{Num: n, Den: d}
	}

	matchPhotoCount := func(count int, descCount int, token ...string) OmegaMatcher {
		if len(token) > 1 {
			panic("Can only specify at most one token")
		}

		counts := VisTokenCounts{docs.VisTokenAll: count + 1}
		descCounts := VisTokenCounts{docs.VisTokenAll: descCount}

		if len(token) > 0 {
			counts[token[0]] = count
			descCounts[token[0]] = descCount
		}

		return MatchGroupBasePtrVisTokenCounts(counts, descCounts)
	}

	// For a group.
	// descCount: number of group descendants (counted as VisTokenAll)
	// token: token for photos
	// tokenCount: number of photos in group
	// tokenDescCount: number of photos in descendant groups
	matchPhotoCountToken := func(descCount int, token string, tokenCount int, tokenDescCount int) OmegaMatcher {
		counts := VisTokenCounts{docs.VisTokenAll: 1 + tokenCount}
		descCounts := VisTokenCounts{docs.VisTokenAll: descCount + tokenDescCount}
		if token != docs.VisTokenAll {
			counts[token] = tokenCount
			descCounts[token] = tokenDescCount
		}

		return MatchGroupBasePtrVisTokenCounts(counts, descCounts)
	}

	BeforeEach(func() {
		clearTestDatabase()
	})

	Describe("AddOrUpdate", func() {
		It("Basic add: root.jpg", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
			}

			// First add
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "root.jpg", 120000000000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"StoreBucket": PointTo(Equal(bucketName)),
				"StoreObject": PointTo(Equal("root.jpg")),
				"StoreGen":    PointTo(Equal("ALCO8BsAAAA")),
				"FileSize":    PointTo(BeNumerically("==", 100)),
				"Folder":      PointTo(Equal("/")),
				"Width":       PointTo(BeNumerically("==", 100)),
				"Height":      PointTo(BeNumerically("==", 200)),
				"DateTime":    PointTo(Equal(time.Date(2020, time.January, 1, 0, 0, 0, 0, time.UTC))),
				"SortKey":     PointTo(Equal("20200101000000root/" + string(*photo.ID))),
				"TsSortKey":   Not(BeNil()),
			}))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 2},
				nil,
			))
			Expect(db.Dates.Get(ctx, "/2020/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenPublic, 0, 1))
			Expect(db.Dates.Get(ctx, "/2020/01/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenPublic, 0, 1))
			Expect(db.Dates.Get(ctx, "/2020/01/01/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenPublic, 1, 0))

			// Change width
			md.Width = 2000

			photo, err = db.Photos.AddOrUpdate(ctx, bucketName, "root.jpg", 120000000001, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"StoreGen": PointTo(Equal("AbCO8BsAAAA")),
				"Width":    PointTo(BeNumerically("==", 2000)),
				"Height":   PointTo(BeNumerically("==", 200)),
			}))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 2},
				nil,
			))

		})

		It("Basic add: in/a/folder/root.jpg", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:02 03:04:05",
				},
			}

			// Add with folder
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "in/a/folder/root.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"StoreBucket": PointTo(Equal(bucketName)),
				"StoreObject": PointTo(Equal("in/a/folder/root.jpg")),
				"StoreGen":    PointTo(Equal("ECcAAAAAAAA")),
				"Folder":      PointTo(Equal("/in/a/folder/")),
				"Width":       PointTo(BeNumerically("==", 100)),
				"Height":      PointTo(BeNumerically("==", 200)),
				"DateTime":    PointTo(Equal(time.Date(2020, time.January, 2, 3, 4, 5, 0, time.UTC))),
				"SortKey":     PointTo(Equal("20200102030405root/" + string(*photo.ID))),
			}))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 4},
			))
			Expect(db.Folders.Get(ctx, "/in/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 3},
			))
			Expect(db.Folders.Get(ctx, "/in/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/in/a/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

		})

		It("Basic add: in/a/special folder/root.jpg", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:02 03:04:05",
				},
			}

			// Add with folder
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "in/a/special folder/root.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"StoreBucket": PointTo(Equal(bucketName)),
				"StoreObject": PointTo(Equal("in/a/special folder/root.jpg")),
				"StoreGen":    PointTo(Equal("ECcAAAAAAAA")),
				"Folder":      PointTo(Equal("/in/a/special-folder/")),
				"Width":       PointTo(BeNumerically("==", 100)),
				"Height":      PointTo(BeNumerically("==", 200)),
				"DateTime":    PointTo(Equal(time.Date(2020, time.January, 2, 3, 4, 5, 0, time.UTC))),
				"SortKey":     PointTo(Equal("20200102030405root/" + string(*photo.ID))),
			}))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 4},
			))
			Expect(db.Folders.Get(ctx, "/in/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 3},
			))
			Expect(db.Folders.Get(ctx, "/in/a/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/in/a/special-folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

		})

		It("More metadata: a/b/c/root.jpg", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:              "root!",
					Location:           "the world, Banff    , Alberta   , Canada",
					Subjects:           []string{"hello", "keyword"},
					Rating:             4,
					UsageTerms:         "Use Terms",
					RightsWebStatement: "http://usage",
				},
				Exif: photomd.ExifData{
					photomd.ExifTagImageDescription:  "describe a root",
					photomd.ExifTagCopyright:         "Copyright Me",
					photomd.ExifTagMake:              "Camera Make",
					photomd.ExifTagModel:             "Camera Model",
					photomd.ExifTagLensModel:         "Lens Model",
					photomd.ExifTagExposureTime:      ur(1, 15),
					photomd.ExifTagFNumber:           ur(71, 10),
					photomd.ExifTagFocalLength:       ur(426, 100),
					photomd.ExifTagISOSpeedRatings:   uint16(250),
					photomd.ExifTagExposureBiasValue: r(-2, 3),
					photomd.ExifTagDateTimeOriginal:  "2019:04:03 20:56:59",
					photomd.ExifTagGPSAltitude:       r(126, 1),
				},
			}

			// Add with folder
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "a/b/c/some picture.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"StoreObject":  PointTo(Equal("a/b/c/some picture.jpg")),
				"Width":        PointTo(BeNumerically("==", 100)),
				"Height":       PointTo(BeNumerically("==", 200)),
				"Title":        PointTo(Equal("root!")),
				"Description":  PointTo(Equal("describe a root")),
				"Folder":       PointTo(Equal("/a/b/c/")),
				"Locations":    ConsistOf("/canada/alberta/banff/the-world/"),
				"Keywords":     ConsistOf("/hello/", "/keyword/"),
				"Rating":       PointTo(BeNumerically("==", 4)),
				"Copyright":    PointTo(Equal("Copyright Me")),
				"UsageTerms":   PointTo(Equal("Use Terms")),
				"UsageWebURL":  PointTo(Equal("http://usage")),
				"Make":         PointTo(Equal("Camera Make")),
				"Model":        PointTo(Equal("Camera Model")),
				"Lens":         PointTo(Equal("Lens Model")),
				"ExposureTime": PointTo(BeNumerically("~", 1.0/15.0)),
				"FNumber":      PointTo(BeNumerically("~", 7.1)),
				"FocalLength":  PointTo(BeNumerically("~", 4.26)),
				"ISO":          PointTo(BeNumerically("==", 250)),
				"ExposureBias": PointTo(BeNumerically("~", -2.0/3.0)),
				"DateTime":     PointTo(Equal(time.Date(2019, time.April, 3, 20, 56, 59, 0, time.UTC))),
				"GPSAltitude":  PointTo(BeNumerically("==", 126)),
				"SortKey":      PointTo(Equal("20190403205659some picture/" + string(*photo.ID))),
			}))

			// Keyword check
			Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(matchPhotoCount(1, 0))
			Expect(db.Keywords.Get(ctx, "/keyword/", nil, tokensAll)).To(matchPhotoCount(1, 0))

		})

		Describe("Location metadata", func() {
			It("Multi-part location", func() {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Xmp: photomd.XmpData{
						Location: "a trail, awesome park",
					},
					Exif: photomd.ExifData{
						photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
					},
				}

				// First add
				photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "root.jpg", 120000000000, 100, md)
				Expect(err).ToNot(HaveOccurred())

				// Check
				Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
					"Locations": ConsistOf("/awesome-park/a-trail/"),
				}))

				Expect(db.Locations.Get(ctx, "/awesome-park/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenPublic, 0, 1))
				Expect(db.Locations.Get(ctx, "/awesome-park/a-trail/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenPublic, 1, 0))
			})

			It("Single location overlaps with state", func() {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Xmp: photomd.XmpData{
						Location: "Ontario",
						State:    "Ontario",
					},
					Exif: photomd.ExifData{
						photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
					},
				}

				// First add
				photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
				Expect(err).ToNot(HaveOccurred())

				// Check
				Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
					"Locations": ConsistOf("/ontario/"),
				}))

				Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			})

			It("Multi-part location overlaps with city, state, country", func() {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Xmp: photomd.XmpData{
						Location: "  Trail  ,   Park  ,    Mega City, Fancy State  ,   Big Country    ",
						City:     "Mega City",
						State:    "Fancy State",
						Country:  "Big Country",
					},
					Exif: photomd.ExifData{
						photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
					},
				}

				// First add
				photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
				Expect(err).ToNot(HaveOccurred())

				// Check
				Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
					"Locations": ConsistOf("/big-country/fancy-state/mega-city/park/trail/"),
				}))

				Expect(db.Locations.Get(ctx, "/big-country/", nil, tokensAll)).To(matchPhotoCountToken(4, docs.VisTokenAll, 0, 1))
				Expect(db.Locations.Get(ctx, "/big-country/fancy-state/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 1))
				Expect(db.Locations.Get(ctx, "/big-country/fancy-state/mega-city/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
				Expect(db.Locations.Get(ctx, "/big-country/fancy-state/mega-city/park/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
				Expect(db.Locations.Get(ctx, "/big-country/fancy-state/mega-city/park/trail/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			})

			Describe("Park locations", func() {
				It("Basic park", func() {
					// First photo without park annotation
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Great Park, Globe",
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/globe/great-park/"),
					}))

					Expect(db.Locations.Get(ctx, "/globe/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/globe/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)

					// Second photo with park annotation
					md = &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!park: Great Park, Globe",
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 10:00:00",
						},
					}

					// Add
					photo, err = db.Photos.AddOrUpdate(ctx, bucketName, "f/photo2.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/globe/great-park/"),
					}))

					Expect(db.Locations.Get(ctx, "/globe/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/globe/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 2, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
				})

				It("Park with nested location", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Falls, !park:Great Park, Globe",
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/globe/great-park/falls/"),
					}))

					Expect(db.Locations.Get(ctx, "/globe/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/globe/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/globe/great-park/falls/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)

				})
			})

			Describe("Special (hike, park, loc) locations", func() {
				It("Basic hike", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!hike:Great Trail, State",
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/state/great-trail/"),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/state/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
				})

				It("Hike in park", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!hike:Great Trail, !park:Great Park, State",
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/state/great-park/great-trail/"),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
				})

				It("Hike in subject", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!park:Great Park, State",
							Subjects: []string{"!hike:Great Trail"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": ConsistOf("/state/great-park/great-trail/"),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)

					// Check that keyword was not created from hike location (although it was a subject).
					Expect(db.Keywords.Get(ctx, "/hike-great-trail/", nil, tokensAll)).Error().Should(HaveOccurred())
				})

				It("Hike@0 in subject", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!park:Great Park, State",
							Subjects: []string{"!hike:@0:Great Trail"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/great-park/",
							"/state/great-park/great-trail/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 1, 1),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)

					// Check that keyword was not created from hike location (although it was a subject).
					Expect(db.Keywords.Get(ctx, "/hike-great-trail/", nil, tokensAll)).Error().Should(HaveOccurred())
				})

				It("Hikes in location and subject", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!hike:Great Trail, !park:Great Park, State",
							Subjects: []string{"!hike:@2:State Trail"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/great-park/great-trail/",
							"/state/state-trail/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/state-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
				})

				It("Multiple hikes only in subjects", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "!park:Great Park, State",
							Subjects: []string{"!hike:@1:State Trail", "!hike:Great Trail", "!hike:@0:Alternative Name", "!hike:@2:World Trail"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/great-park/great-trail/",
							"/state/state-trail/",
							"/state/great-park/alternative-name/",
							"/world-trail/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(4, docs.VisTokenAll, 0, 3))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(2, docs.VisTokenAll, 0, 2),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/great-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/alternative-name/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/state-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/world-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
				})

				It("Custom park location in subject", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Highway, State",
							Subjects: []string{"!park:@1: Great Park"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/highway/",
							"/state/great-park/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/state/highway/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"GroupBase.Display": PointTo(Equal("State,!park:Great Park")),
								"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
				})

				It("Custom park location in subject via !loc", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Highway, State",
							Subjects: []string{"!loc:@1:!park: Great Park"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/highway/",
							"/state/great-park/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/state/highway/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"GroupBase.Display": PointTo(Equal("State,!park:Great Park")),
								"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
				})

				It("Default park, hike and custom location in subject", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "State",
							Subjects: []string{"!hike: Wonder Trail", "!park: Great Park", "!loc:Rest Spot"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/great-park/wonder-trail/rest-spot/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/state/great-park/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(2, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"GroupBase.Display": PointTo(Equal("State,!park:Great Park")),
								"Type":              PointTo(BeEquivalentTo(docs.LocationPark)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/wonder-trail/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(1, docs.VisTokenAll, 0, 1),
							MatchGroupBasePtr(Fields{
								"GroupBase.Display": PointTo(Equal("State,!park:Great Park,!hike:Wonder Trail")),
								"Type":              PointTo(BeEquivalentTo(docs.LocationHike)),
							}),
						),
					)
					Expect(db.Locations.Get(ctx, "/state/great-park/wonder-trail/rest-spot/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"GroupBase.Display": PointTo(Equal("State,!park:Great Park,!hike:Wonder Trail,Rest Spot")),
								"Type":              PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)

				})
			})

			Describe("Custom locations", func() {
				It("Single custom location", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "State",
							Subjects: []string{"!loc:Addition"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/addition/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/state/addition/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)
				})

				It("Single custom location with @0", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "State",
							Subjects: []string{"!loc:@0:Addition"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/state/",
							"/state/addition/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/state/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 1, 1))
					Expect(db.Locations.Get(ctx, "/state/addition/", nil, tokensAll)).To(
						And(
							matchPhotoCountToken(0, docs.VisTokenAll, 1, 0),
							MatchGroupBasePtr(Fields{
								"Type": PointTo(BeEquivalentTo(docs.LocationOther)),
							}),
						),
					)
				})

				It("Custom location with offset", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Town, State, Country",
							Subjects: []string{"!loc:@1:Addition"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/country/state/town/",
							"/country/state/addition/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/country/state/town/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
					Expect(db.Locations.Get(ctx, "/country/state/addition/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
				})

				It("Multiple custom locations with offset", func() {
					md := &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Location: "Town, State, Country",
							Subjects: []string{"!loc:@1:Addition", "!loc:@2:X/ Y"},
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
						},
					}

					// Add
					photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
					Expect(err).ToNot(HaveOccurred())

					// Check
					Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
						"Locations": Equal([]string{
							"/country/state/town/",
							"/country/state/addition/",
							"/country/y/x/",
						}),
					}))

					Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(matchPhotoCountToken(5, docs.VisTokenAll, 0, 3))
					Expect(db.Locations.Get(ctx, "/country/state/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 2))
					Expect(db.Locations.Get(ctx, "/country/state/town/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
					Expect(db.Locations.Get(ctx, "/country/state/addition/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
					Expect(db.Locations.Get(ctx, "/country/y/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
					Expect(db.Locations.Get(ctx, "/country/y/x/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
				})
			})

		})

		Describe("GPS locations", func() {
			It("North-West", func() {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Exif: photomd.ExifData{
						photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
						photomd.ExifTagGPSLatitudeRef:   "N",
						photomd.ExifTagGPSLatitude:      []photomd.Urational{ur(40, 1), ur(463183333, 10000000), ur(0, 1)},
						photomd.ExifTagGPSLongitudeRef:  "W",
						photomd.ExifTagGPSLongitude:     []photomd.Urational{ur(111, 1), ur(571133333, 10000000), ur(0, 1)},
					},
				}

				// Add
				photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "pic.jpg", 10000, 100, md)
				Expect(err).ToNot(HaveOccurred())

				// Check
				Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
					"StoreObject": PointTo(Equal("pic.jpg")),
					"Width":       PointTo(BeNumerically("==", 100)),
					"Height":      PointTo(BeNumerically("==", 200)),
					"GPSLocation": PointTo(MatchAllFields(Fields{
						"Type":        Equal("Point"),
						"Coordinates": Ignore(), // checked below
					})),
					"DateTime": PointTo(Equal(time.Date(2019, time.April, 3, 20, 56, 59, 0, time.UTC))),
					"SortKey":  PointTo(Equal("20190403205659pic/" + string(*photo.ID))),
				}))

				Expect(photo.GPSLocation.Coordinates[0]).To(BeNumerically("~", -111.9518888883))
				Expect(photo.GPSLocation.Coordinates[1]).To(BeNumerically("~", 40.771972221666))
			})

			It("South-East", func() {
				md := &photomd.Metadata{
					Width:  100,
					Height: 200,
					Exif: photomd.ExifData{
						photomd.ExifTagGPSLatitudeRef:  "S",
						photomd.ExifTagGPSLatitude:     []photomd.Urational{ur(36, 1), ur(493608000, 10000000), ur(0, 1)},
						photomd.ExifTagGPSLongitudeRef: "E",
						photomd.ExifTagGPSLongitude:    []photomd.Urational{ur(174, 1), ur(485132000, 10000000), ur(0, 1)},
					},
				}

				// Add
				photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "pic.jpg", 10000, 100, md)
				Expect(err).ToNot(HaveOccurred())

				// Check
				Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
					"StoreObject": PointTo(Equal("pic.jpg")),
					"Width":       PointTo(BeNumerically("==", 100)),
					"Height":      PointTo(BeNumerically("==", 200)),
					"GPSLocation": PointTo(MatchAllFields(Fields{
						"Type":        Equal("Point"),
						"Coordinates": Ignore(), // checked below
					})),
				}))

				Expect(photo.GPSLocation.Coordinates[0]).To(BeNumerically("~", 174.8085533333))
				Expect(photo.GPSLocation.Coordinates[1]).To(BeNumerically("~", -36.82268))
			})

		})

		It("Add with categories and keywords", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
				Xmp: photomd.XmpData{
					Subjects: []string{"key1", "key2", "cat: yellow", "other cat : tree", "key2", "cat: yellow"},
				},
			}

			// First add
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"Keywords":   ConsistOf([]string{"/key1/", "/key2/"}),
				"Categories": ConsistOf([]string{"/cat/yellow/", "/other-cat/tree/"}),
			}))

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/cat/yellow/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Categories.Get(ctx, "/other-cat/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/other-cat/tree/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

		})

		It("Add keywords/categories with punctuation and Chinese characters", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
				Xmp: photomd.XmpData{
					Subjects: []string{"awesome@#$%^&*() place!`", "-_+=[]{}\\|why comma, separated", "hike!: ;\"<,>./?yellow's hollow", "皇,穹宇"},
				},
			}

			// First add
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"Keywords":   ConsistOf([]string{"/why-comma-separated/", "/awesome-place/", "/皇-穹宇/"}),
				"Categories": ConsistOf([]string{"/hike/yellow-s-hollow/"}),
			}))

			Expect(db.Keywords.Get(ctx, "/why-comma-separated/", nil, tokensAll)).To(Not(BeNil()))
			Expect(db.Keywords.Get(ctx, "/awesome-place/", nil, tokensAll)).To(Not(BeNil()))
			Expect(db.Keywords.Get(ctx, "/皇-穹宇/", nil, tokensAll)).To(Not(BeNil()))

			Expect(db.Categories.Get(ctx, "/hike/", nil, tokensAll)).To(Not(BeNil()))
			Expect(db.Categories.Get(ctx, "/hike/yellow-s-hollow/", nil, tokensAll)).To(Not(BeNil()))
		})

		It("Add with ignored keywords and categories", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
				Xmp: photomd.XmpData{
					Subjects: []string{"kw", "__ignore me", "___cat: value", "cat: __ignore_value", "cat: blah"},
				},
			}

			// First add
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "f/photo.jpg", 120000000000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			// Check
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"Keywords":   ConsistOf([]string{"/kw/"}),
				"Categories": ConsistOf([]string{"/cat/blah/"}),
			}))

			Expect(db.Keywords.Get(ctx, "/kw/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Categories.Get(ctx, "/cat/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/cat/blah/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

		})

	})

	Describe("Delete", func() {
		BeforeEach(func() {
			// Need storage for resize cache flow
			s := teststorage.New()
			storage.Set(s)

		})

		It("Delete non-existent", func() {
			err := db.Photos.Delete(ctx, bucketName, "random.jpg")
			Expect(err).ToNot(HaveOccurred())
		})

		It("Basic add and delete: root.jpg", func() {
			md := &photomd.Metadata{Width: 100, Height: 200}

			// Add
			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, "my folder/root.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Photos.Get(ctx, *photo.ID, nil, tokensAll)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			// Delete
			err = db.Photos.Delete(ctx, bucketName, "my folder/root.jpg")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Photos.Get(ctx, *photo.ID, nil, tokensAll)
			Expect(err).To(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, nil))

		})

		It("Add and delete photo with keywords, categories and locations", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "root!",
					Location: "Yummy Restaurant,Banff,  Alberta, Canada ",
					Subjects: []string{"hello", "keyword", "type : hike fest", "moon phase: full"},
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
				},
			}

			// Add
			_, err := db.Photos.AddOrUpdate(ctx, bucketName, "my super/nested folder/picture.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-super/nested-folder/", nil, tokensAll)).To(
				MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 2}, nil),
			)

			Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Keywords.Get(ctx, "/keyword/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Categories.Get(ctx, "/type/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/type/hike-fest/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/moon-phase/full/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/yummy-restaurant/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/04/03/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			// Delete
			err = db.Photos.Delete(ctx, bucketName, "my super/nested folder/picture.jpg")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-super/nested-folder/", nil, tokensAll)).To(
				MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 1}, nil),
			)

			Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Keywords.Get(ctx, "/keyword/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))

			Expect(db.Categories.Get(ctx, "/type/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/type/hike-fest/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/full/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))

			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/yummy-restaurant/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 0))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Dates.Get(ctx, "/2019/04/03/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))

		})

		It("Add and delete photo with visibility token", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "photo",
					Subjects: []string{"__visibility: private"},
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
				},
			}

			tokenPrivate := "/private/"

			// Add
			_, err := db.Photos.AddOrUpdate(ctx, bucketName, "folder/picture.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(matchPhotoCountToken(0, tokenPrivate, 1, 0))
			Expect(db.Visibility.Get(ctx, tokenPrivate, nil)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			// Delete
			err = db.Photos.Delete(ctx, bucketName, "folder/picture.jpg")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(matchPhotoCountToken(0, tokenPrivate, 0, 0))
			Expect(db.Visibility.Get(ctx, tokenPrivate, nil)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))

		})

		It("Add and change photo keywords, categories and locations", func() {
			// Add
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "root!",
					Location: "Yummy Restaurant,Banff,  Alberta, Canada ",
					Subjects: []string{"hello", "keyword", "type : hike fest", "moon phase: full"},
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
				},
			}

			_, err := db.Photos.AddOrUpdate(ctx, bucketName, "my super/nested folder/picture.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-super/nested-folder/", nil, tokensAll)).To(
				MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 2}, nil),
			)

			Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Keywords.Get(ctx, "/keyword/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Categories.Get(ctx, "/type/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/type/hike-fest/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/moon-phase/full/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(matchPhotoCountToken(3, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/yummy-restaurant/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/04/03/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			// Change
			md = &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "new title!",
					Location: "Restaurant, Jasper,  Alberta, Canada ",
					Subjects: []string{"hello", "keyword2", "type2 : hike fest", "moon phase: half-full"},
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2019:05:03 20:56:59",
				},
			}

			_, err = db.Photos.AddOrUpdate(ctx, bucketName, "my super/nested folder/picture.jpg", 10000, 100, md)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Folders.Get(ctx, "/my-super/nested-folder/", nil, tokensAll)).To(
				MatchGroupBasePtrVisTokenCounts(VisTokenCounts{docs.VisTokenAll: 2}, nil),
			)

			Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Keywords.Get(ctx, "/keyword/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Keywords.Get(ctx, "/keyword2/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Categories.Get(ctx, "/type/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/type/hike-fest/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/type2/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/type2/hike-fest/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/", nil, tokensAll)).To(matchPhotoCountToken(2, docs.VisTokenAll, 0, 1))
			Expect(db.Categories.Get(ctx, "/moon-phase/full/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Categories.Get(ctx, "/moon-phase/half-full/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/yummy-restaurant/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/jasper/restaurant/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/jasper/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/alberta/banff/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Locations.Get(ctx, "/canada/alberta/", nil, tokensAll)).To(matchPhotoCountToken(4, docs.VisTokenAll, 0, 1))
			Expect(db.Locations.Get(ctx, "/canada/", nil, tokensAll)).To(matchPhotoCountToken(5, docs.VisTokenAll, 0, 1))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(matchPhotoCountToken(4, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 0))
			Expect(db.Dates.Get(ctx, "/2019/04/03/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 0, 0))
			Expect(db.Dates.Get(ctx, "/2019/05/", nil, tokensAll)).To(matchPhotoCountToken(1, docs.VisTokenAll, 0, 1))
			Expect(db.Dates.Get(ctx, "/2019/05/03/", nil, tokensAll)).To(matchPhotoCountToken(0, docs.VisTokenAll, 1, 0))

		})

	})

	Describe("Visibility tokens", func() {
		addPhoto := func(objectID string, title string, date string, location string, keywords ...string) docs.PhotoID {
			md := photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    title,
					Subjects: keywords,
					Location: location,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: date + " 20:56:59",
				},
			}

			photo, err := db.Photos.AddOrUpdate(ctx, bucketName, objectID, 0, 100, &md)
			Expect(err).ToNot(HaveOccurred())
			return *photo.ID
		}

		var photo1 docs.PhotoID
		var photo2 docs.PhotoID
		var private docs.PhotoID
		tokenA := "/a/"
		tokenPrivate := "/private/"

		BeforeEach(func() {
			photo1 = addPhoto("folder/photo1.jpg", "photo1", "2019:04:13", "Toronto, Ontario", "dog", "park: riverdale")
			photo2 = addPhoto("folder/nested/photo2.jpg", "photo2", "2019:04:15", "Ottawa, Ontario", "rabbit", "park: sunnybrook")
			private = addPhoto("folder/private.jpg", "private", "2019:04:15", "Toronto, Ontario", "__visibility: Private", "dog", "secret", "park: riverdale")

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			_, err := db.Visibility.Create(ctx, "a")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.CheckAndFixVisTokenCounts(ctx, true)).To(HaveLen(0))

		})

		It("Duplicate visibility keyword", func() {
			p := addPhoto("custom.jpg", "custom", "2019:04:13", "__visibility: Private", "__visibility: private")

			Expect(db.Photos.Get(ctx, p, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1, tokenPrivate: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1},
			))
			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3}, nil))

		})

		It("Add inherited visibility tokens (non full tree), then delete", func() {
			// Add
			err := db.Photos.AddInheritedVisibilityToken(ctx, "/folder/", false, tokenA)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens(tokenA))
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, private, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Visibility.Get(ctx, tokenA, nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			// Delete
			err = db.Photos.DeleteInheritedVisibilityToken(ctx, "/folder/", false, tokenA)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, private, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Visibility.Get(ctx, tokenA, nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, nil))

		})

		It("Add inherited visibility tokens (full tree), then delete", func() {
			// Add
			err := db.Photos.AddInheritedVisibilityToken(ctx, "/folder/", true, tokenA)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens(tokenA))
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens(tokenA))
			Expect(db.Photos.Get(ctx, private, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 2},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1},
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 2}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 2}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1, tokenA: 2}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 2}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Visibility.Get(ctx, tokenA, nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3}, nil))

			// Delete (non-full tree)
			err = db.Photos.DeleteInheritedVisibilityToken(ctx, "/folder/", false, tokenA)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens(tokenA))
			Expect(db.Photos.Get(ctx, private, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 0},
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 0}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 0}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 0}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 1}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1, tokenA: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, tokenA: 0}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1, tokenA: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Visibility.Get(ctx, tokenA, nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			// Delete (full tree)
			err = db.Photos.DeleteInheritedVisibilityToken(ctx, "/folder/", true, tokenA)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Photos.Get(ctx, photo1, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, photo2, nil, tokensAll)).To(MatchVisibilityTokens())
			Expect(db.Photos.Get(ctx, private, nil, tokensAll)).To(MatchVisibilityTokens(tokenPrivate))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1},
				VisTokenCounts{docs.VisTokenAll: 2},
			))
			Expect(db.Folders.Get(ctx, "/folder/nested/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2},
				nil,
			))

			Expect(db.Keywords.Get(ctx, "/dog/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Keywords.Get(ctx, "/rabbit/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Categories.Get(ctx, "/park/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Categories.Get(ctx, "/park/riverdale/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Categories.Get(ctx, "/park/sunnybrook/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Locations.Get(ctx, "/ontario/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Locations.Get(ctx, "/ontario/toronto/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))
			Expect(db.Locations.Get(ctx, "/ontario/ottawa/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))

			Expect(db.Dates.Get(ctx, "/2019/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 6, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, VisTokenCounts{docs.VisTokenAll: 5, tokenPrivate: 1}))
			Expect(db.Dates.Get(ctx, "/2019/04/13/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Dates.Get(ctx, "/2019/04/15/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, tokenPrivate: 1}, nil))

			Expect(db.Visibility.Get(ctx, "/private/", nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2}, nil))
			Expect(db.Visibility.Get(ctx, tokenA, nil)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1}, nil))

		})

	})

})
