// SPDX-License-Identifier: MIT

package photos_test

import (
	"context"
	"io"
	"sync"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/storage"
	teststorage "gitlab.com/lookoutpoint/storage/test"
)

var _ = Describe("Resize", func() {
	tokensAll := []string{docs.VisTokenAll}

	Describe("AddOrUpdateAndResize and GetResizedImage", func() {
		var ctx context.Context
		var s storage.Storage

		bucketID := "bucket"

		checkAndCompare := func(imgReader io.ReadCloser, width uint32, height uint32, title string, copyright string, outputFile string) {
			// Save the image and check metadata
			// Need to setup two readers to do this
			rPipe, wPipe := io.Pipe()
			imgTee := io.TeeReader(imgReader, wPipe) // tee so that we can inspect the image in the test too

			wg := sync.WaitGroup{}
			wg.Add(2)

			go func() {
				defer GinkgoRecover()
				defer rPipe.Close()
				defer wg.Done()

				// Check photo metadata
				md, err := photomd.Extract(rPipe)
				Expect(err).ToNot(HaveOccurred())
				io.Copy(io.Discard, rPipe)

				Expect(md.Width).To(BeNumerically("==", width))
				Expect(md.Height).To(BeNumerically("==", height))
				Expect(md.Xmp.Title).To(Equal(title))
				Expect(md.Exif[photomd.ExifTagCopyright]).To(Equal(copyright))
			}()

			go func() {
				defer GinkgoRecover()
				defer wg.Done()

				outFile := createTestOutputFile(outputFile)
				io.Copy(outFile, imgTee)
				outFile.Close()
				wPipe.Close()

				Expect(compareTestOutputFile(outputFile)).To(Succeed())
			}()

			wg.Wait()
			imgReader.Close()
		}

		Describe("nikon1.jpg", func() {
			nikon1Title := "Tunneling Through the Rockies"
			nikon1Copyright := "Copyright 2007 Jason Wong. Some rights reserved. License: https://creativecommons.org/licenses/by-nc-sa/4.0/"

			var nikon1ID docs.PhotoID

			BeforeEach(func() {
				var err error

				setTestOutputDir("resize/AddOrUpdateAndResize-GetResizedImage")

				clearTestDatabase()

				ctx = context.Background()

				// Need storage for cache flow
				s = teststorage.New()
				storage.Set(s)

				// Add nikon1.jpg
				Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/nikon1.jpg", bucketID, "nikon1.jpg")).To(Succeed())

				photo, err := db.Photos.AddOrUpdateAndResize(ctx, bucketID, "nikon1.jpg", 1)
				Expect(err).ToNot(HaveOccurred())

				nikon1ID = *photo.ID
			})

			It("List", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					Filter: docs.PhotoListFilterExpr{
						Folders: []docs.PhotoListFilterFolder{
							{Path: "/"},
						},
					},
					VisibilityTokens: []string{docs.VisTokenAll},
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
			})

			It("Invalid resized size", func() {
				_, _, _, err := db.Photos.GetResizedImage(ctx, nikon1ID, 1, tokensAll)
				Expect(err).To(MatchError(apis.ErrPhotoInvalidResizeMaxLength))
			})

			It("nikon1.jpg - ResizeFillCache", func() {
				// Check each valid length
				imgReader, imgLen, isPublic, err := db.Photos.GetResizedImage(ctx, nikon1ID, 300, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(17468))
				Expect(isPublic).To(Equal(true))
				checkAndCompare(imgReader, 300, 165, nikon1Title, nikon1Copyright, "nikon1-ml-300.jpg")

				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, 600, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(20849))
				checkAndCompare(imgReader, 600, 329, nikon1Title, nikon1Copyright, "nikon1-ml-600.jpg")

				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, 1200, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(33126))
				checkAndCompare(imgReader, 1200, 658, nikon1Title, nikon1Copyright, "nikon1-ml-1200.jpg")

				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, 2400, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(78750))
				checkAndCompare(imgReader, 2400, 1317, nikon1Title, nikon1Copyright, "nikon1-ml-2400.jpg")

				// This length is larger than the original image, so should return the original image.
				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, 4800, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(139882))
				checkAndCompare(imgReader, 3173, 1741, nikon1Title, nikon1Copyright, "nikon1-ml-4800.jpg")

			})

			It("nikon1.jpg - Add and update", func() {
				maxLength := 600 // should be valid

				imgReader, imgLen, _, err := db.Photos.GetResizedImage(ctx, nikon1ID, maxLength, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(20849))
				checkAndCompare(imgReader, 600, 329, nikon1Title, nikon1Copyright, "nikon1-ml-600.jpg")

				// Check resized cache entry
				cacheEntry := db.Photos.GetResizeCacheEntry(ctx, nikon1ID, maxLength)
				Expect(cacheEntry).ToNot(BeNil())
				Expect(cacheEntry.CacheHit).To(Equal(1))

				// Get same resized image again - should hit in the cache
				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, maxLength, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(20849))

				outFile := createTestOutputFile("nikon1-ml-600.jpg")
				io.Copy(outFile, imgReader)
				outFile.Close()
				imgReader.Close()

				Expect(compareTestOutputFile("nikon1-ml-600.jpg")).To(Succeed())

				// Check resized image cache entry - should have tracked one cache hit
				cacheEntry = db.Photos.GetResizeCacheEntry(ctx, nikon1ID, maxLength)
				Expect(cacheEntry).ToNot(BeNil())
				Expect(cacheEntry.CacheHit).To(Equal(2))

				// Simulate an update to the photo by replacing nikon1.jpg with portrait.jpg
				// Image is 1296x1936 pixels
				Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/portrait.jpg", bucketID, "nikon1.jpg")).To(Succeed())
				photo, err := db.Photos.AddOrUpdateAndResize(ctx, bucketID, "nikon1.jpg", 1)
				Expect(err).ToNot(HaveOccurred())
				Expect(*photo.ID).To(Equal(nikon1ID))

				// Get new resized image
				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, maxLength, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(52351))
				checkAndCompare(imgReader, 402, 600, "Totem", "Copyright 2007 Jason Wong.", "portrait-ml-600.jpg")

				// Check resized cache entry - should have one cache hit from the first fetch.
				cacheEntry = db.Photos.GetResizeCacheEntry(ctx, nikon1ID, maxLength)
				Expect(cacheEntry).ToNot(BeNil())
				Expect(cacheEntry.CacheHit).To(Equal(1))

				// Get same resized image again - should hit in the cache
				imgReader, imgLen, _, err = db.Photos.GetResizedImage(ctx, nikon1ID, maxLength, tokensAll)
				Expect(err).ToNot(HaveOccurred())
				Expect(imgLen).To(Equal(52351))

				outFile = createTestOutputFile("portrait-ml-600.jpg")
				io.Copy(outFile, imgReader)
				outFile.Close()
				imgReader.Close()

				Expect(compareTestOutputFile("portrait-ml-600.jpg")).To(Succeed())

				// Check resized cache entry - should have tracked two cache hits
				cacheEntry = db.Photos.GetResizeCacheEntry(ctx, nikon1ID, maxLength)
				Expect(cacheEntry).ToNot(BeNil())
				Expect(cacheEntry.CacheHit).To(Equal(2))

			})
		})

		It("Parallel add and resize photo (test parallel transactions)", func() {
			setTestOutputDir("resize/AddOrUpdateAndResize-GetResizedImage-parallel")

			clearTestDatabase()

			ctx = context.Background()

			// Need storage for cache flow
			s = teststorage.New()
			storage.Set(s)

			// Copy 4 photos into storage
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/photo1.jpg", bucketID, "folder1/photo1.jpg")).To(Succeed())
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/photo2.jpg", bucketID, "folder1/photo2.jpg")).To(Succeed())
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/photo3.jpg", bucketID, "folder2/photo3.jpg")).To(Succeed())
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/photo4.jpg", bucketID, "folder2/photo4.jpg")).To(Succeed())

			// Use go routines to add the photos in parallel.
			wg := sync.WaitGroup{}

			doWork := func(objectID string) {
				defer GinkgoRecover()
				defer wg.Done()

				_, err := db.Photos.AddOrUpdateAndResize(ctx, bucketID, objectID, 1)
				Expect(err).ToNot(HaveOccurred())
			}

			wg.Add(4)
			go doWork("folder1/photo1.jpg")
			go doWork("folder1/photo2.jpg")
			go doWork("folder2/photo3.jpg")
			go doWork("folder2/photo4.jpg")
			wg.Wait()

			// List all photos
			photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
				Filter: docs.PhotoListFilterExpr{
					Folders: []docs.PhotoListFilterFolder{
						{Path: "/", FullTree: true},
					},
				},
				VisibilityTokens: []string{docs.VisTokenAll},
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(4))
		})

	})

})
