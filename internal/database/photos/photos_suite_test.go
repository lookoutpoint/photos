// SPDX-License-Identifier: MIT

package photos_test

import (
	"context"
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/testutils"
	"go.mongodb.org/mongo-driver/mongo"
)

func TestDatabase(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Database/Photos Suite")
}

var ctx context.Context
var mdb *mongo.Database
var db *database.Database

const testOutputRootDir = "./.test/output"

var _ = BeforeSuite(func() {
	// Force colors and no timestamps
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: false,
	})

	// Log level (by default only panic-level messages are shown in test mode)
	log.SetLevel(log.PanicLevel)
	level, err := log.ParseLevel(os.Getenv("LOGLEVEL"))
	if err == nil {
		log.SetLevel(level)
	}

	// Initialize
	ctx = context.Background()
	mdb = testutils.NewTestDb()
	db = database.NewWithDb(mdb)
	Expect(db.UpdateIndexes()).ToNot(HaveOccurred())

	// Set up test output directory
	os.RemoveAll(testOutputRootDir)

	err = os.MkdirAll(testOutputRootDir, os.ModePerm)
	Expect(err).ToNot(HaveOccurred())

})

var _ = AfterEach(func() {
	Expect(db.CheckAndFixVisTokenCounts(ctx, true)).To(HaveLen(0))
})

func clearTestDatabase() {
	testutils.ClearTestDatabase(ctx, db)
}
