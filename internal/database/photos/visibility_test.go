// SPDX-License-Identifier: MIT

package photos_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = Describe("Visibility", func() {
	const bucketName = "lookoutpoint-dev-photo-master"

	tokensAll := []string{docs.VisTokenAll}
	tokensPublic := []string{docs.VisTokenPublic}

	addPhoto := func(objectID string, title string, keywords ...string) *docs.Photo {
		md := photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:01 01:01:00",
			},
		}

		photo, err := db.Photos.AddOrUpdate(ctx, bucketName, objectID, 0, 1000, &md)
		Expect(err).ToNot(HaveOccurred())
		return photo
	}

	listParams := func(folderPath string, visibilityTokens []string) *apis.PhotoListParams {
		return &apis.PhotoListParams{
			Filter: docs.PhotoListFilterExpr{
				Folders: []docs.PhotoListFilterFolder{
					{Path: folderPath, FullTree: true},
				},
			},
			ProjectionKeys:   []string{"id", "title"},
			VisibilityTokens: visibilityTokens,
		}
	}

	BeforeEach(func() {
		clearTestDatabase()
	})

	Describe("Initial add photo", func() {
		It("Add photo with no tokens", func() {
			photo := addPhoto("folder/photo.jpg", "photo1")
			Expect(*photo).To(MatchFields(IgnoreExtras, Fields{
				"InheritVisibility": PointTo(Equal(true)),
				"VisTokens":         ConsistOf(docs.VisTokenAll),
			}))
		})
	})

	Describe("Add photo root folder", func() {
		It("Photo with no tokens", func() {
			addPhoto("photo.jpg", "photo")

			// List all
			photos, err := db.Photos.List(ctx, listParams("/", tokensAll))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photo")))

			// List photos with public token
			photos, err = db.Photos.List(ctx, listParams("/", tokensPublic))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photo")))

		})

		It("Photo with token with public folder", func() {
			addPhoto("photo.jpg", "photo", "__visibility: Limited")
			tokenLimited := "/limited/"

			// Make root folder public, which photo should inherit (if it doesn't have a token)
			Expect(db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTargetFullTree, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// List all
			photos, err := db.Photos.List(ctx, listParams("/", tokensAll))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photo")))

			// List photos with public token
			photos, err = db.Photos.List(ctx, listParams("/", tokensPublic))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))

			// List by token
			photos, err = db.Photos.List(ctx, listParams("/", []string{tokenLimited}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photo")))

			// Check that root folder should have token as well
			folder, err := db.Folders.Get(ctx, "/", nil, tokensAll)
			Expect(err).ToNot(HaveOccurred())
			Expect(folder.VisTokens).To(ConsistOf(docs.VisTokenAll, docs.VisTokenPublic, tokenLimited))

			// Update photo and remove token
			addPhoto("photo.jpg", "photo2")

			// List by token
			photos, err = db.Photos.List(ctx, listParams("/", []string{tokenLimited}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))

			// List photos with public token
			photos, err = db.Photos.List(ctx, listParams("/", tokensPublic))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photo2")))

		})
	})

	Describe("Non-root photos with no tokens", func() {
		BeforeEach(func() {
			addPhoto("folder/photo.jpg", "photo1")
			addPhoto("folder/photo2.jpg", "photo2", "kw")
		})

		It("List with all token", func() {
			photos, err := db.Photos.List(ctx, listParams("/folder/", tokensAll))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0].Title).To(PointTo(Equal("photo1")))
			Expect(photos[1].Title).To(PointTo(Equal("photo2")))
		})

		It("List with public token", func() {
			photos, err := db.Photos.List(ctx, listParams("/folder/", tokensPublic))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))
		})

		It("List with non-applicable visibility token", func() {
			photos, err := db.Photos.List(ctx, listParams("/folder/", []string{"random-token"}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))
		})

	})

	Describe("Non-root photos with tokens", func() {
		token1 := "/set1/"
		token2 := "/set2/"
		token3 := "/set3/"

		BeforeEach(func() {
			addPhoto("root/folderA/photo.jpg", "photoA", "__visibility: Set1")
			addPhoto("root/folderB/photo.jpg", "photoB", "__visibility: Set1", "__visibility: Set2")

		})

		It("Check folder visibility tokens", func() {
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic, token1, token2),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))
			Expect(db.Folders.Get(ctx, "/root/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token1, token2),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/root/folderA/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token1),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/root/folderB/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token1, token2),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))

		})

		It("List with all token", func() {
			photos, err := db.Photos.List(ctx, listParams("/root/", tokensAll))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0].Title).To(PointTo(Equal("photoA")))
			Expect(photos[1].Title).To(PointTo(Equal("photoB")))
		})

		It("List with public token", func() {
			photos, err := db.Photos.List(ctx, listParams("/root/", tokensPublic))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))
		})

		It("List with token1", func() {
			photos, err := db.Photos.List(ctx, listParams("/root/", []string{token1}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0].Title).To(PointTo(Equal("photoA")))
			Expect(photos[1].Title).To(PointTo(Equal("photoB")))
		})

		It("List with token2", func() {
			photos, err := db.Photos.List(ctx, listParams("/root/", []string{token2}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photoB")))
		})

		It("List with token2 and public", func() {
			photos, err := db.Photos.List(ctx, listParams("/root/", []string{token2, docs.VisTokenPublic}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photoB")))
		})

		It("Change visibility tokens", func() {
			addPhoto("root/folderB/photo.jpg", "photoB v2", "__visibility: Set3", "__visibility: Set2")

			// List with token1
			photos, err := db.Photos.List(ctx, listParams("/root/", []string{token1}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photoA")))

			// List with token2
			photos, err = db.Photos.List(ctx, listParams("/root/", []string{token2}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photoB v2")))

			// List with token3
			photos, err = db.Photos.List(ctx, listParams("/root/", []string{token3}))
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("photoB v2")))

			// Check folder tokens
			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, docs.VisTokenPublic, token1, token2, token3),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(docs.VisTokenPublic),
			}))
			Expect(db.Folders.Get(ctx, "/root/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token1, token2, token3),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/root/folderA/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token1),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))
			Expect(db.Folders.Get(ctx, "/root/folderB/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, token2, token3),
				"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
			}))

		})

	})
})
