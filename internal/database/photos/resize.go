// SPDX-License-Identifier: MIT

package photos

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"image"
	"io"
	"os"
	"time"

	gcstorage "cloud.google.com/go/storage"
	"github.com/disintegration/imaging"
	log "github.com/sirupsen/logrus"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"golang.org/x/sync/errgroup"

	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/visibility"
	"gitlab.com/lookoutpoint/photos/internal/utils"
	"gitlab.com/lookoutpoint/storage"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const resizeCacheCollectionName = "photo-resize-cache"

// ResizeCacheCollection returns the cache photo collection reference
func (api *apiImpl) ResizeCacheCollection() *mongo.Collection {
	return api.mdb.Collection(resizeCacheCollectionName)
}

var validResizeMaxLengths = map[int]bool{
	300:  true,
	600:  true,
	1200: true,
	2400: true,
	4800: true,
	9600: true,
}

func (api *apiImpl) isValidResizeMaxLength(maxLength int) bool {
	_, ok := validResizeMaxLengths[maxLength]
	return ok
}

// Generates a resized image such that the max dimension of the resized image is equal to the max length parameter.
// This function will upscale if needed, so be careful if that's not the desire.
func (api *apiImpl) resizeImage(oImg image.Image, maxLength int) image.Image {
	oWidth := oImg.Bounds().Dx()
	oHeight := oImg.Bounds().Dy()

	// Resize the image
	var rWidth, rHeight int // these are the resized dimensions
	if oWidth >= oHeight {
		// Width is the longer dimension, so width will be the max length
		rWidth = maxLength

		// Basic equation here is oHeight * rWidth / oWidth but want to round
		rHeight = (oHeight*rWidth + oWidth/2) / oWidth
	} else {
		// Height is the longer dimension, so height will be the max length
		rHeight = maxLength

		// Basic equation here is oWidth * rHeight / oHeight but want to round
		rWidth = (oWidth*rHeight + oHeight/2) / oHeight
	}

	rImg := imaging.Resize(oImg, rWidth, rHeight, imaging.Lanczos)

	return rImg
}

func (api *apiImpl) getResizeCacheStorageBucket() string {
	bucketID := os.Getenv("LOOKOUTPOINT_PHOTO_RESIZE_CACHE_BUCKET")
	if bucketID != "" {
		return bucketID
	}
	return utils.GetCloudProjectID() + "-photo-resize-cache"
}

func (api *apiImpl) getResizeCacheKey(maxLength int) string {
	return fmt.Sprintf("ml%v", maxLength)
}

// GetResizeCacheEntry returns the cache entry for the given size target
func (api *apiImpl) GetResizeCacheEntry(ctx context.Context, id docs.PhotoID, maxLength int) *docs.PhotoResizeCacheEntry {
	l := log.WithFields(log.Fields{
		"func":      "photos.apiImpl.GetResizeCacheEntry",
		"id":        id,
		"maxLength": maxLength,
	})

	// Try to see if there is a cache entry.
	cacheKey := api.getResizeCacheKey(maxLength)

	res := api.ResizeCacheCollection().FindOne(ctx, bson.M{"_id": id}, &options.FindOneOptions{Projection: bson.M{cacheKey: 1}})

	if rawBson, err := res.DecodeBytes(); err != nil {
		l.Trace("No cache document")
		return nil
	} else if rawCacheEntry, err := rawBson.LookupErr(cacheKey); err != nil {
		l.Trace("No cache entry")
		return nil
	} else {
		// Try to decode the cache entry
		cacheEntry := &docs.PhotoResizeCacheEntry{}
		err = rawCacheEntry.Unmarshal(cacheEntry)
		if err != nil {
			l.WithError(err).Tracef("Could not decode cache entry (%+v)", rawCacheEntry)
			return nil
		}

		return cacheEntry
	}
}

func (api *apiImpl) addResizeCacheEntry(ctx context.Context, id docs.PhotoID, maxLength int, photoTs time.Time, fileSize int) error {
	cacheKey := api.getResizeCacheKey(maxLength)

	// Build document to update cache document. Must use update operators here.
	update := bson.M{
		"$set": bson.M{
			cacheKey + ".photoTimestamp": photoTs,
			cacheKey + ".cacheHit":       0,
			cacheKey + ".fileSize":       fileSize,
		},
		"$currentDate": bson.M{
			cacheKey + ".lastAccess": bson.M{"$type": "date"},
		},
	}

	_, err := api.ResizeCacheCollection().UpdateOne(ctx, bson.M{"_id": id}, update, options.Update().SetUpsert(true))
	return err
}

func (api *apiImpl) updateResizeCacheEntry(ctx context.Context, id docs.PhotoID, maxLength int) error {
	cacheKey := api.getResizeCacheKey(maxLength)

	// Build document to update cache document. Must use update operators here.
	// Update cache hit count and last access date
	update := bson.M{
		"$inc": bson.M{
			cacheKey + ".cacheHit": 1,
		},
		"$currentDate": bson.M{
			cacheKey + ".lastAccess": bson.M{"$type": "date"},
		},
	}

	_, err := api.ResizeCacheCollection().UpdateOne(ctx, bson.M{"_id": id}, update)
	return err
}

func (api *apiImpl) getResizeCacheObjectID(id docs.PhotoID, maxLength int) string {
	return fmt.Sprintf("%v-%v.jpg", id, maxLength)
}

// ErrResizeImageNotReady indicates that resized image is not ready yet
var ErrResizeImageNotReady = errors.New("resized image is not ready")

// GetResizedImage returns a resized image for the given photo id where the max dimension will be <= max length.
// May return a nil ReadCloser if there is no resized image currently available (error = ResizeImageNotReady)
// Returns (ReadCloser, image length in bytes, public boolean flag, error)
func (api *apiImpl) GetResizedImage(ctx context.Context, id docs.PhotoID, maxLength int, tokens []string) (io.ReadCloser, int, bool, error) {
	l := log.WithFields(log.Fields{
		"func":      "photos.apiImpl.GetResizedImage",
		"id":        id,
		"maxLength": maxLength,
	})

	// Get master photo metadata needed for cropping and cache look up
	photo, err := api.Get(ctx, id, []string{"id", "width", "height", "storeBucket", "storeObject", "fileSize", "timestamp", "visTokens"}, tokens)
	if err != nil {
		l.WithError(err).Error("Invalid photo")
		return nil, 0, false, err
	}

	// Validate max length
	if maxLength != 0 && !api.isValidResizeMaxLength(maxLength) {
		l.Error("Invalid max length")
		return nil, 0, false, apis.ErrPhotoInvalidResizeMaxLength
	}

	// Check if we should just use the original image.
	if maxLength == 0 || (maxLength > *photo.Width && maxLength > *photo.Height) {
		l.Info("Using original image")

		r, err := storage.Get().ReaderForObject(ctx, *photo.StoreBucket, *photo.StoreObject)
		if err != nil {
			l.WithError(err).Error("Could not get reader for original image")
			return nil, 0, false, err
		}

		return r, *photo.FileSize, visibility.IsPublic(photo.VisTokens), nil
	}

	// The resized image will be from the cache.
	return api.getResizedImageWithCache(ctx, photo, maxLength, l)
}

func (api *apiImpl) getResizedImageWithCache(ctx context.Context, photo *docs.Photo, maxLength int, l *log.Entry) (io.ReadCloser, int, bool, error) {
	// Try to see if there is a cache entry.
	cacheEntry := api.GetResizeCacheEntry(ctx, *photo.ID, maxLength)

	// Check cache entry timestamp
	if cacheEntry != nil && cacheEntry.PhotoTimestamp != *photo.Timestamp {
		l.Info("Photo timestamp does not match")
		cacheEntry = nil
	}

	if cacheEntry != nil {
		// Retrieve cached image from storage
		cacheObjectID := api.getResizeCacheObjectID(*photo.ID, maxLength)
		objReader, err := storage.Get().ReaderForObject(ctx, api.getResizeCacheStorageBucket(), cacheObjectID)
		if err == nil {
			// Update cache entry
			err := api.updateResizeCacheEntry(ctx, *photo.ID, maxLength)
			if err != nil {
				// Don't consider this a fatal error
				l.WithError(err).Warn("Failed to update resize cache entry, but continuing anyway")
			}

			return objReader, cacheEntry.FileSize, visibility.IsPublic(photo.VisTokens), nil
		}

		// At this point we have failed to get the cached image, so fall through.
		l.WithError(err).Warn("Could not get reader to cached photo")
	}

	// Cache entry is not ready yet.
	return nil, 0, false, ErrResizeImageNotReady
}

func (api *apiImpl) makeJPEGImageReaderWithApp1Segments(imgBytes []byte, app1Segments [][]byte) io.ReadCloser {
	// We assume that imgBytes does not contain any APP1 segments and we will insert the provided
	// APP1 segments immediately after the start of image marker (first two bytes of imgBytes).
	rPipe, wPipe := io.Pipe()

	// Use a goroutine to write the byte segments in the right order
	// Don't check errors here (e.g. if reader closes prematurely)
	// The reader must be closed by the caller.
	go func() {
		defer wPipe.Close()

		// Start of image marker
		wPipe.Write(imgBytes[:2])

		// Write each APP1 segment
		for _, segment := range app1Segments {
			wPipe.Write(segment)
		}

		// Write the rest of the image data
		wPipe.Write(imgBytes[2:])
	}()

	return rPipe
}

type sizeImageLengthMap map[int]int

// Generate all valid resized images for the given photo. Returns a map of (max length, image length in bytes).
func (api *apiImpl) generateResizedImages(ctx context.Context, photoID docs.PhotoID, imgBytes []byte) (sizeImageLengthMap, error) {
	l := log.WithFields(log.Fields{
		"func": "photos.apiImpl.FillResizeCache",
		"id":   photoID,
	})

	// Read the image
	img, err := imaging.Decode(bytes.NewReader(imgBytes))
	if err != nil {
		l.WithError(err).Error("Could not read image")
		return nil, err
	}

	bounds := img.Bounds()
	width := bounds.Dx()
	height := bounds.Dy()

	// Extract the APP1 segments
	app1Segments, err := photomd.ExtractApp1SegmentsWithLog(bytes.NewReader(imgBytes), l)
	if err != nil {
		l.WithError(err).Error("Could not extract APP1 segments")
		return nil, err
	}

	// Generate a resized image for each max length value
	type ResizeResult struct {
		maxLength int
		imgSize   int
	}

	// Use a workgroup with a parallelism limit.
	workGroup, workCtx := errgroup.WithContext(ctx)
	workGroup.SetLimit(2)

	chanResults := make(chan ResizeResult, len(validResizeMaxLengths)) // sized for max number of resized images
	nResized := 0
	for maxLength := range validResizeMaxLengths {
		// Make copy for closure
		maxLength := maxLength
		// At least one of the dimensions must be greater than the max length, otherwise
		// don't need to resize
		if width > maxLength || height > maxLength {
			nResized++
			workGroup.Go(func() error {
				imgSize, err := api.resizeAndSave(workCtx, photoID, maxLength, img, app1Segments, l)
				if err != nil {
					return err
				} else {
					chanResults <- ResizeResult{maxLength, imgSize}
					return nil
				}
			})
		}
	}

	// Wait for all resized images to be done
	err = workGroup.Wait()
	if err != nil {
		l.WithError(err).Error("Some resize failed, undoing resize")
		// Clean up any resized images that were generated
		err := api.deleteResizedImages(ctx, photoID)
		if err != nil {
			l.WithError(err).Warn("Could not clean up resized images")
		}

		return nil, err
	}

	// Populate resized length to image size map
	sizeImgLenMap := sizeImageLengthMap{}
	for i := 0; i < nResized; i++ {
		result := <-chanResults
		sizeImgLenMap[result.maxLength] = result.imgSize
	}

	return sizeImgLenMap, nil
}

// Returns size of resized image in bytes
func (api *apiImpl) resizeAndSave(ctx context.Context, photoID docs.PhotoID, maxLength int, origImg image.Image, app1Segments [][]byte, l *log.Entry) (int, error) {
	// Create a new resized image
	l.WithField("maxLength", maxLength).Info("Resizing image")

	resizedImg := api.resizeImage(origImg, maxLength)

	// Encode the image to a buffer
	buf := &bytes.Buffer{}
	err := imaging.Encode(buf, resizedImg, imaging.JPEG, imaging.JPEGQuality(75))
	if err != nil {
		l.WithError(err).Error("Could not encode jpeg image")
		return 0, err
	}

	imgBytes := buf.Bytes()

	// Save the image by combining the resized image data with the APP1 segments to preserve
	// the original metadata.

	// Calculate the total size of the combined image.
	totalImgLength := len(imgBytes)
	for _, segment := range app1Segments {
		totalImgLength += len(segment)
	}

	// Write the image to the resize cache
	cacheObjectID := api.getResizeCacheObjectID(photoID, maxLength)
	objWriter, err := storage.Get().WriterForObject(ctx, api.getResizeCacheStorageBucket(), cacheObjectID)
	if err != nil {
		l.WithError(err).Error("Could not get writer to resize cache")
		return 0, err
	}

	rImgForCache := api.makeJPEGImageReaderWithApp1Segments(imgBytes, app1Segments)
	defer rImgForCache.Close()

	_, err = io.Copy(objWriter, rImgForCache)
	objWriter.Close()
	rImgForCache.Close()

	if err != nil {
		l.WithError(err).Error("Could not write to resize cache")
		return 0, err
	}

	return totalImgLength, nil
}

func (api *apiImpl) addPhotoToResizeCacheDb(ctx context.Context, photo *docs.Photo, sizeImgLenMap sizeImageLengthMap) error {
	l := log.WithFields(log.Fields{
		"func": "photos.apiImpl.addPhotoToResizeCacheDb",
		"id":   *photo.ID,
	})

	for maxLength, imgLen := range sizeImgLenMap {
		// Add resize cache entry based on the photo that was used to generate this image
		err := api.addResizeCacheEntry(ctx, *photo.ID, maxLength, *photo.Timestamp, imgLen)
		if err != nil {
			l.WithError(err).Error("Could not add resize cache entry")
			return err
		}
	}

	// Mark the photo as resized.
	_, err := api.Collection().UpdateOne(ctx, bson.M{"_id": *photo.ID}, bson.M{"$set": bson.M{"resized": true}})
	if err != nil {
		l.WithError(err).Error("Could not set resized flag")
		return err
	}

	return nil

}

// Best effort delete resize cache entry from database.
func (api *apiImpl) deletePhotoFromResizeCacheDb(ctx context.Context, photoID docs.PhotoID) error {
	l := log.WithFields(log.Fields{
		"func": "photos.apiImpl.deletePhotoFromResizeCacheDb",
		"id":   photoID,
	})

	// Delete entry from resize cache collection.
	_, err := api.ResizeCacheCollection().DeleteOne(ctx, bson.M{"_id": photoID})
	if err != nil {
		l.WithError(err).Warn("Could not delete resize cache entry")
	}

	return err
}

// Best effort delete of all resized images for a specific photo.
func (api *apiImpl) deleteResizedImages(ctx context.Context, photoID docs.PhotoID) error {
	l := log.WithFields(log.Fields{
		"func": "photos.apiImpl.deleteResizedImages",
		"id":   photoID,
	})

	// Delete all possible resized image
	s := storage.Get()
	bucket := api.getResizeCacheStorageBucket()

	var retError error

	for maxLength := range validResizeMaxLengths {
		objectID := api.getResizeCacheObjectID(photoID, maxLength)
		err := s.DeleteObject(ctx, bucket, objectID)
		if err != nil && err != gcstorage.ErrObjectNotExist {
			l.WithError(err).Warnf("Could not delete resize cache object %v", objectID)
			retError = err
		}
	}

	return retError

}
