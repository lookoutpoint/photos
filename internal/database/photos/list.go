// SPDX-License-Identifier: MIT

package photos

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// List photos given some set of query and filter parameters.
func (api *apiImpl) List(ctx context.Context, params *apis.PhotoListParams) ([]docs.Photo, error) {
	l := log.WithFields(log.Fields{
		"func":   "photos.apiImpl.List",
		"params": *params,
	})

	// Param defaults and validation.
	params.AssignDefaults()
	if err := params.Validate(); err != nil {
		l.WithError(err).Error("Invalid list params")
		return nil, apis.ErrInvalidListParams
	}

	// Build filter.
	var filterClauses bson.A
	paramsFilter := params.Filter.BuildQueryFilter()
	if paramsFilter != nil {
		filterClauses = append(filterClauses, paramsFilter)
	}

	// If the options contain a ref key value, add a filter against the ref key.
	if len(params.RefKeyValue) > 0 {
		op := "$" + string(params.RefOp)

		switch params.RefKey {
		case apis.PhotoListKeySortKey:
			filterClauses = append(filterClauses, bson.M{"sortKey": bson.M{op: params.RefKeyValue}})
		case apis.PhotoListKeyTimestamp:
			filterClauses = append(filterClauses, bson.M{"tsSortKey": bson.M{op: params.RefKeyValue}})
		}
	}

	// Add visibility tokens to filter.
	visTokenFilter := bson.M{}
	if err := apis.FilterByVisibilityTokens(visTokenFilter, params.VisibilityTokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	filterClauses = append(filterClauses, visTokenFilter)

	// Add clauses for key existence requirements.
	existsProj, err := utils.MakeProjectionFromKeys(photoBSONKeys, params.ExistKeys)
	if err != nil {
		return nil, apis.ErrInvalidListParams
	}
	delete(existsProj, "_id")

	if len(existsProj) > 0 {
		// existsProj is a map of field -> 0/1
		// Convert this to field -> { $exists -> true }
		for key := range existsProj {
			existsProj[key] = bson.M{"$exists": true}
		}

		filterClauses = append(filterClauses, existsProj)
	}

	// Build projection
	var proj bson.M
	if params.ProjectionKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(photoBSONKeys, params.ProjectionKeys)
		if err != nil {
			return nil, apis.ErrInvalidListParams
		}
	} else {
		// Default projection
		proj = bson.M{"_id": 1}
	}

	// Build find options
	options := options.FindOptions{
		Projection: proj,
	}

	// Determine sort order.
	sortDir := 1
	if params.RefOp == list.RefOpLt || params.RefOp == list.RefOpLte {
		sortDir = -1
	}

	var sortKey string
	switch params.RefKey {
	case apis.PhotoListKeySortKey:
		sortKey = "sortKey"
	case apis.PhotoListKeyTimestamp:
		sortKey = "tsSortKey"
	}
	options.Sort = bson.M{sortKey: sortDir}

	// If options has a position limit, use that value
	if params.Limit > 0 {
		options.Limit = &params.Limit
	}

	// Build overall filter by joining clauses
	var filter interface{}
	if len(filterClauses) == 1 {
		filter = filterClauses[0]
	} else if len(filterClauses) > 1 {
		filter = bson.M{"$and": filterClauses}
	}
	l.Tracef("Filter: %+v", filter)

	// Find using the filter and options
	cursor, err := api.Collection().Find(ctx, filter, &options)
	if err != nil {
		l.WithError(err).Error("Could not query photo listing")
		return nil, err
	}

	// Get results
	photos := []docs.Photo{}
	if err := cursor.All(ctx, &photos); err != nil {
		l.WithError(err).Error("Could not fetch photo documents")
		return nil, err
	}

	return photos, nil
}

// Randomly selects a number of photos given some set of query and filter parameters.
func (api *apiImpl) RandomList(ctx context.Context, params *apis.PhotoRandomListParams) ([]docs.Photo, error) {
	l := log.WithFields(log.Fields{
		"func":   "photos.apiImpl.RandomPick",
		"params": *params,
	})

	// Param defaults and validation.
	params.AssignDefaults()
	if err := params.Validate(); err != nil {
		l.WithError(err).Error("Invalid random params")
		return nil, apis.ErrInvalidListParams
	}

	// Build filter.
	var filterClauses bson.A
	paramsFilter := params.Filter.BuildQueryFilter()
	if paramsFilter != nil {
		filterClauses = append(filterClauses, paramsFilter)
	}

	// Add visibility tokens to filter.
	visTokenFilter := bson.M{}
	if err := apis.FilterByVisibilityTokens(visTokenFilter, params.VisibilityTokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	filterClauses = append(filterClauses, visTokenFilter)

	// Add clauses for key existence requirements.
	existsProj, err := utils.MakeProjectionFromKeys(photoBSONKeys, params.ExistKeys)
	if err != nil {
		return nil, apis.ErrInvalidListParams
	}
	delete(existsProj, "_id")

	if len(existsProj) > 0 {
		// existsProj is a map of field -> 0/1
		// Convert this to field -> { $exists -> true }
		for key := range existsProj {
			existsProj[key] = bson.M{"$exists": true}
		}

		filterClauses = append(filterClauses, existsProj)
	}

	// Build projection
	var proj bson.M
	if params.ProjectionKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(photoBSONKeys, params.ProjectionKeys)
		if err != nil {
			return nil, apis.ErrInvalidListParams
		}
	} else {
		// Default projection
		proj = bson.M{"_id": 1}
	}

	// Build overall filter by joining clauses
	var filter interface{}
	if len(filterClauses) == 1 {
		filter = filterClauses[0]
	} else if len(filterClauses) > 1 {
		filter = bson.M{"$and": filterClauses}
	}
	l.Tracef("Filter: %+v", filter)

	// Build aggregation pipeline
	pipeline := bson.A{
		bson.M{"$match": filter},
		bson.M{"$project": proj},
		bson.M{"$sample": bson.M{"size": params.Count}},
	}

	// Run aggregation pipeline
	cursor, err := api.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not run random pick aggregation pipeline")
		return nil, err
	}

	// Get results
	photos := []docs.Photo{}
	if err := cursor.All(ctx, &photos); err != nil {
		l.WithError(err).Error("Could not fetch photo documents")
		return nil, err
	}

	return photos, nil
}
