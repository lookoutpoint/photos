// SPDX-License-Identifier: MIT

package photos

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"io/ioutil"
	"reflect"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	log "github.com/sirupsen/logrus"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"gitlab.com/lookoutpoint/storage"
)

// Implements apis.Photos
type apiImpl struct {
	mdb *mongo.Database
	top *apis.APIs
}

// NewAPI returns a new apiImpl object for photos
func NewAPI(mdb *mongo.Database, top *apis.APIs) apis.Photos {
	return &apiImpl{
		mdb: mdb,
		top: top,
	}
}

const collectionName = "photos"

var photoBSONKeys = utils.ExtractBSONKeys(docs.Photo{})

func makePhotoID(bucketID string, objectID string) docs.PhotoID {
	idHash := sha1.New()
	idHash.Write([]byte("photo@" + bucketID + ":" + objectID))
	return docs.PhotoID(base64.RawURLEncoding.EncodeToString(idHash.Sum(nil)))
}

// Collection returns the photo collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

func (api *apiImpl) getFolderID(ctx context.Context, objectID string) (string, error) {
	// Full path including folder path starts with '/'. The folder portion of the path is the part up to and including the last '/'
	path := "/" + objectID

	index := strings.LastIndex(path, "/")
	folderPath := path[:index+1]

	// Create the folders if necessary
	folder, err := api.top.Folders.GetOrCreate(ctx, folderPath, []string{"id", "display"})
	if err != nil {
		return "", err
	}

	return *folder.ID, nil
}

// generateSortKey generates the sort key which is GLOBALLY unique across all photos and provides a global ordering of all photos.
// The components of the sort key reflect precedence and also the requirement for uniqueness:
//  1. Photo date time in YYYYMMDDHHMMSS format.
//     The base sorting order is by date-time of the photo.
//  2. Base name of object id without extension.
//     There may be cases where two photos have the same date-time (e.g. rapid consecutive shots), in which case the sequence
//     is generally defined by ordering of the file names. As such, extract the base name (extension is not relevant).
//     This should be sufficient to guarantee the desired sorting order within one folder.
//     Separator: /
//     The goal of the separator is to try to lead to better sorting by base names before considering the id.
//  3. Document ID
//     This is guaranteed (or assumed) to be unique, so including it guarantees uniqueness. This also handles the
//     edge case where the other two fields are not enough to guarantee uniqueness (possibly due to two folders having
//     the same photo or something).
func generateSortKey(ts time.Time, objectID string, id docs.PhotoID, milli bool) string {
	// Component 1: date time
	fmt := "20060102150405"
	if milli {
		fmt += ".000"
	}
	dateTimeStr := ts.Format(fmt)
	if milli {
		dateTimeStr = strings.ReplaceAll(dateTimeStr, ".", "")
	}

	// Component 2: object id base name
	pathSep := strings.LastIndex(objectID, "/")
	baseName := objectID[pathSep+1:]

	// Strip extension if any
	if dot := strings.LastIndex(baseName, "."); dot >= 0 {
		baseName = baseName[:dot]
	}

	return dateTimeStr + baseName + "/" + string(id)
}

func encodeGenerationVal(val uint64) string {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, val)
	return base64.RawURLEncoding.EncodeToString(b)
}

// AddOrUpdate adds/updates photo based on a GCS object path and with metadata
func (api *apiImpl) AddOrUpdate(ctx context.Context, bucketID string, objectID string, objectGenVal uint64, fileSize int, md *photomd.Metadata) (*docs.Photo, error) {
	id := makePhotoID(bucketID, objectID)

	l := log.WithFields(log.Fields{
		"func":   "photos.apiImpl.AddOrUpdate",
		"bucket": bucketID,
		"object": objectID,
		"gen":    objectGenVal,
		"id":     id,
	})
	l.Trace("Adding/updating photo")

	// Build up the folder document.
	var err error

	ts := time.Now().UTC()
	objectGen := encodeGenerationVal(objectGenVal)

	photoDoc := docs.Photo{
		ID:          &id,
		StoreBucket: &bucketID,
		StoreObject: &objectID,
		StoreGen:    &objectGen,
		FileSize:    &fileSize,
		Timestamp:   &ts,
	}

	// Fill other metadata, which also gives us the set of visibility token names to associate with this photo
	mdGroups := fillDocFromMetadata(&photoDoc, md, l)
	l.Tracef("Visibility token names from metadata: %v", mdGroups.VisibilityTokens)

	// Determine if photo inherits visibility or not. A photo inherits visibility if it has no visibility tokens
	// in its metadata.
	inheritVisibility := len(mdGroups.VisibilityTokens) == 0
	photoDoc.InheritVisibility = &inheritVisibility

	// If there is no date-time, use the current time. This shouldn't generally happen.
	// Must do this before the sort key generation.
	if photoDoc.DateTime == nil {
		l.Warn("No photo date time, using current time")
		photoDoc.DateTime = &ts
	}

	// Generate sort keys
	sortKey := generateSortKey(*photoDoc.DateTime, objectID, id, false)
	photoDoc.SortKey = &sortKey

	tsSortKey := generateSortKey(*photoDoc.Timestamp, objectID, id, true) // use milliseconds for timestamp
	photoDoc.TsSortKey = &tsSortKey

	// Add photo and update group counts in a transaction to ensure that the values are in sync. Everything database-related
	// is grouped into the transaction.
	photo, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Folders
		folderID, err := api.getFolderID(ctx, objectID)
		if err != nil {
			l.WithError(err).Warn("Failed to get folder id")
			return nil, err
		}
		photoDoc.Folder = &folderID

		if inheritVisibility {
			// Get self visibility tokens from folder and use those.
			// Don't use visibilityTokens as that contains all derived visibility tokens.
			folder, err := api.top.Folders.Get(ctx, folderID, []string{"selfVisTokens"}, []string{docs.VisTokenAll})
			if err != nil {
				l.WithError(err).WithField("folderID", folderID).Warn("Could not get folder visibility tokens")
				return nil, err
			}

			// The folder's visibility tokens should already include VisTokenAll
			photoDoc.VisTokens = folder.SelfVisTokens
			l.Tracef("Inheriting folder visibility: %v", photoDoc.VisTokens)

			// Also include VisTokenAll
			photoDoc.VisTokens = append(photoDoc.VisTokens, docs.VisTokenAll)
		} else {
			// Create visibility tokens based on the names. Always have VisTokenAll.
			photoDoc.VisTokens = []string{docs.VisTokenAll}

			// Eliminate any duplicates;
			vtIDs := map[string]bool{}
			for _, visTokenName := range mdGroups.VisibilityTokens {
				vtg, err := api.top.Visibility.GetOrCreate(ctx, visTokenName, []string{"id"})
				if err != nil {
					l.WithError(err).WithField("visTokenName", visTokenName).Warn("Could not create visibility token")
					return nil, err
				}
				vtIDs[*vtg.ID] = true
			}

			for vtID := range vtIDs {
				photoDoc.VisTokens = append(photoDoc.VisTokens, vtID)
			}

			l.Tracef("Visibility from metadata: %v", photoDoc.VisTokens)
		}

		// Create and populate various group objects: keywords, categories, locations
		// For keywords and categories, remove any duplicates (multiple keywords that map to the same group id).
		kwIDs := map[string]bool{}
		for _, kw := range mdGroups.Keywords {
			kwg, err := api.top.Keywords.GetOrCreate(ctx, kw, []string{"id"})
			if err != nil {
				l.WithError(err).WithField("kw", kw).Error("Could not get or create keyword")
				return nil, err
			}
			kwIDs[*kwg.ID] = true
		}

		for kwID := range kwIDs {
			photoDoc.Keywords = append(photoDoc.Keywords, kwID)
		}

		cvIDs := map[string]bool{}
		for _, cv := range mdGroups.Categories {
			cvg, err := api.top.Categories.GetOrCreate(ctx, cv, []string{"id"})
			if err != nil {
				l.WithError(err).WithField("cv", cv).Error("Could not get or create category")
				return nil, err
			}
			cvIDs[*cvg.ID] = true
		}

		for cvID := range cvIDs {
			photoDoc.Categories = append(photoDoc.Categories, cvID)
		}

		if len(mdGroups.Locations) > 0 {
			for _, loc := range mdGroups.Locations {
				locg, err := api.top.Locations.GetOrCreate(ctx, strings.Join(loc, ","), []string{"id"})
				if err != nil {
					l.WithError(err).WithField("loc", loc).Error("Could not get or create location")
					return nil, err
				}

				photoDoc.Locations = append(photoDoc.Locations, *locg.ID)
			}
		}

		// Create group for date
		date, err := api.top.Dates.GetOrCreate(ctx, (*photoDoc.SortKey)[:8], []string{"id"})
		if err != nil {
			l.WithError(err).Error("Could not get or create date group")
			return nil, err
		}
		photoDoc.Date = date.ID

		// Upsert the photo document, replacing any existing document
		res := api.Collection().FindOneAndReplace(
			ctx, bson.M{"_id": id}, photoDoc,
			options.FindOneAndReplace().
				SetUpsert(true).
				SetProjection(bson.M{
					"visTokens":  1,
					"folder":     1,
					"keywords":   1,
					"categories": 1,
					"locations":  1,
					"date":       1,
				}),
		)

		var prevPhotoDoc docs.Photo
		newPhoto := false
		if err := res.Decode(&prevPhotoDoc); err != nil {
			if err != mongo.ErrNoDocuments {
				l.WithError(err).Warn("Failed to add photo document")
				return nil, err
			}
			newPhoto = true
		}

		// Post-processing

		// Update photo counts
		var oldVisTokens []string
		var oldFolder *string
		var oldKeywords []string
		var oldCategories []string
		var oldLocations []string
		var oldDate *string

		if !newPhoto {
			oldVisTokens = prevPhotoDoc.VisTokens
			oldFolder = prevPhotoDoc.Folder
			oldKeywords = prevPhotoDoc.Keywords
			oldCategories = prevPhotoDoc.Categories
			oldLocations = prevPhotoDoc.Locations
			oldDate = prevPhotoDoc.Date
		}

		// Helper to keep ids and tokens consistent (both nil or not nil)
		nilify := func(check interface{}, tokens []string) []string {
			if reflect.ValueOf(check).IsNil() {
				return nil
			}
			return tokens
		}

		err = api.top.Folders.DiffAndUpdate(
			ctx, oldFolder, photoDoc.Folder, oldVisTokens, photoDoc.VisTokens, 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update folder")
			return nil, err
		}

		err = api.top.Keywords.DiffAndUpdate(
			ctx, oldKeywords, photoDoc.Keywords,
			nilify(oldKeywords, oldVisTokens), nilify(photoDoc.Keywords, photoDoc.VisTokens), 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update keywords")
			return nil, err
		}

		err = api.top.Categories.DiffAndUpdate(
			ctx, oldCategories, photoDoc.Categories,
			nilify(oldCategories, oldVisTokens), nilify(photoDoc.Categories, photoDoc.VisTokens), 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update categories")
			return nil, err
		}

		err = api.top.Locations.DiffAndUpdate(
			ctx, oldLocations, photoDoc.Locations,
			nilify(oldLocations, oldVisTokens), nilify(photoDoc.Locations, photoDoc.VisTokens), 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update locations")
			return nil, err
		}

		err = api.top.Dates.DiffAndUpdate(
			ctx, oldDate, photoDoc.Date, oldVisTokens, photoDoc.VisTokens, 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update dates")
			return nil, err
		}

		err = api.top.Visibility.DiffAndUpdate(ctx, oldVisTokens, photoDoc.VisTokens, 1)
		if err != nil {
			l.WithError(err).Warn("Could not diff and update visibility tokens")
			return nil, err
		}

		return &photoDoc, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	if err != nil {
		return nil, err
	}

	return photo.(*docs.Photo), nil
}

// AddOrUpdateAndResize adds/updates photo based on a GCS object path and also generates the resized photos
func (api *apiImpl) AddOrUpdateAndResize(ctx context.Context, bucketID string, objectID string, objectGenVal uint64) (*docs.Photo, error) {
	id := makePhotoID(bucketID, objectID)

	l := log.WithFields(log.Fields{
		"func":   "photos.apiImpl.AddOrUpdateAndResizePhoto",
		"bucket": bucketID,
		"object": objectID,
		"gen":    objectGenVal,
		"id":     id,
	})

	// Read in image
	objReader, err := storage.Get().ReaderForObject(ctx, bucketID, objectID)
	if err != nil {
		// Cannot read the object. Return error code and try again later.
		l.WithError(err).Error("Cannot acquire reader on object")
		return nil, err
	}
	defer objReader.Close()

	imgBytes, err := ioutil.ReadAll(objReader)
	if err != nil {
		l.WithError(err).Error("Could not read image data")
		return nil, err
	}

	// Extract metadata
	md, err := photomd.Extract(bytes.NewReader(imgBytes))
	if err != nil {
		l.WithError(err).Error("Could not extract photo metadata")
		return nil, err
	}

	// Resize photos. This performs the computational work but does not record anything in the database (that'll happen
	// in the transaction below).
	sizeImgLenMap, err := api.generateResizedImages(ctx, id, imgBytes)
	if err != nil {
		l.WithError(err).Error("Could not generate resized images")
		return nil, err
	}

	// Add photo and resize in a transaction.
	photo, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Add photo to database.
		photo, err := api.AddOrUpdate(ctx, bucketID, objectID, objectGenVal, len(imgBytes), md)
		if err != nil {
			l.WithError(err).Warn("Could not add/update photo")
			return nil, err
		}

		// Add resized images to database.
		err = api.addPhotoToResizeCacheDb(ctx, photo, sizeImgLenMap)
		if err != nil {
			l.WithError(err).Warn("Could not add resized photos")
			return nil, err
		}

		return photo, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	if err != nil {
		// Database transaction is aborted, so try to clean up generated resized images.
		err := api.deleteResizedImages(ctx, id)
		if err != nil {
			l.WithError(err).Warn("Could not delete resized images on error")
		}

		return nil, err
	}

	return photo.(*docs.Photo), nil
}

// Delete removes the document associated with the given GCS object
func (api *apiImpl) Delete(ctx context.Context, bucketID string, objectID string) error {
	id := makePhotoID(bucketID, objectID)

	l := log.WithFields(log.Fields{
		"func":   "photos.apiImpl.Delete",
		"bucket": bucketID,
		"object": objectID,
		"id":     id,
	})

	// Do the delete and group diff update in a transaction to keep everything in sync.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		res := api.Collection().FindOneAndDelete(ctx, bson.M{"_id": id})

		var photoDoc docs.Photo
		if err := res.Decode(&photoDoc); err != nil {
			if err != mongo.ErrNoDocuments {
				l.WithError(err).Warn("Failed to delete photo document")
				return nil, err
			}
		} else {
			// Update photo count and visibility tokens.
			err := api.top.Folders.DiffAndUpdate(ctx, photoDoc.Folder, nil, photoDoc.VisTokens, nil, 1)
			if err != nil {
				l.WithError(err).Warn("Could not diff and update folder")
				return nil, err
			}

			if photoDoc.Keywords != nil {
				err = api.top.Keywords.DiffAndUpdate(ctx, photoDoc.Keywords, nil, photoDoc.VisTokens, nil, 1)
				if err != nil {
					l.WithError(err).Warn("Could not diff and update keywords")
					return nil, err
				}
			}

			if photoDoc.Categories != nil {
				err = api.top.Categories.DiffAndUpdate(ctx, photoDoc.Categories, nil, photoDoc.VisTokens, nil, 1)
				if err != nil {
					l.WithError(err).Warn("Could not diff and update categories")
					return nil, err
				}
			}

			if photoDoc.Locations != nil {
				err = api.top.Locations.DiffAndUpdate(ctx, photoDoc.Locations, nil, photoDoc.VisTokens, nil, 1)
				if err != nil {
					l.WithError(err).Warn("Could not diff and update locations")
					return nil, err
				}
			}

			err = api.top.Dates.DiffAndUpdate(ctx, photoDoc.Date, nil, photoDoc.VisTokens, nil, 1)
			if err != nil {
				l.WithError(err).Error("Could not diff and update dates")
				return nil, err
			}

			err = api.top.Visibility.DiffAndUpdate(ctx, photoDoc.VisTokens, nil, 1)
			if err != nil {
				l.WithError(err).Warn("Could not diff and update visibility tokens")
				return nil, err
			}

		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	l.Info("Deleted photo")

	// Delete resized cache entry (not a big deal if this fails)
	if err := api.deletePhotoFromResizeCacheDb(ctx, id); err != nil {
		l.WithError(err).Warn("Could not delete photo from resize cache database")
	}

	// Delete resized images
	if err := api.deleteResizedImages(ctx, id); err != nil {
		l.WithError(err).Warn("Could not delete resized images")
	}

	return err
}

// Get returns a Photo document. If no projection is given, all fields are returned.
func (api *apiImpl) Get(ctx context.Context, id docs.PhotoID, projKeys []string, tokens []string) (*docs.Photo, error) {
	l := log.WithFields(log.Fields{
		"func": "photos.apiImpl.Get",
		"id":   id,
	})

	// Build filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Build projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(photoBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	res := api.Collection().FindOne(ctx, filter, &options.FindOneOptions{Projection: proj})

	photo := &docs.Photo{}
	if err := res.Decode(photo); err != nil {
		l.WithError(err).Error("Could not find or query photo document")
		return nil, err
	}

	return photo, nil
}

// See countInheritVisibilityPhotos
type countResult struct {
	ID    string `bson:"_id"`
	Count int    `bson:"count"`
}

type countResults struct {
	Total      []countResult `bson:"total"`
	Folders    []countResult `bson:"folders"`
	Keywords   []countResult `bson:"keywords"`
	Categories []countResult `bson:"categories"`
	Locations  []countResult `bson:"locations"`
	Dates      []countResult `bson:"dates"`
}

// Finds folders and/or sub-folders with photos that are inheriting visibility and meets certain criteria w.r.t. visibility tokens.
// The exact criteria depends if this function is called with add = true or add = false.
//
// add = true
// ----------
// For each folder:
// - Count: the number of photos that need the given token added (i.e. does not already have the token)
//
// add = false
// -----------
// For each folder:
// - Count: the number of photos that need the given token removed (i.e. have the token)
func (api *apiImpl) countInheritVisibilityPhotos(ctx context.Context, folderSlugPath string, fullTree bool, add bool, token string) (*countResults, error) {
	l := log.WithFields(log.Fields{
		"func":           "photos.apiImpl.countInheritVisibilityPhotos",
		"folderSlugPath": folderSlugPath,
		"fullTree":       fullTree,
		"add":            add,
	})

	var folderMatch interface{}
	if fullTree {
		folderMatch = bson.M{"$regex": "^" + folderSlugPath}
	} else {
		folderMatch = folderSlugPath
	}

	var visTokensMatch interface{}
	if add {
		// If adding, find sub-folders with photos that don't have this token
		visTokensMatch = bson.M{"$ne": token}
	} else {
		visTokensMatch = token
	}

	pipeline := []bson.M{
		// Match photos in folders that inherit visibility and match the visibility tokens criteria
		{
			"$match": bson.M{
				"folder":            folderMatch,
				"inheritVisibility": true,
				"visTokens":         visTokensMatch,
			},
		},
		// Project fields
		{
			"$project": bson.M{
				"_id":        false,
				"visTokens":  true,
				"folder":     true,
				"keywords":   true,
				"categories": true,
				"locations":  true,
				"date":       true,
			},
		},
		// Facet per group type
		{
			"$facet": bson.M{
				"total": bson.A{
					bson.M{
						"$group": bson.M{
							"_id":   "",
							"count": bson.M{"$sum": 1},
						},
					},
				},
				"folders": bson.A{
					bson.M{
						"$group": bson.M{
							"_id":   "$folder",
							"count": bson.M{"$sum": 1},
						},
					},
				},
				"keywords": bson.A{
					bson.M{
						"$unwind": "$keywords",
					},
					bson.M{
						"$group": bson.M{
							"_id":   "$keywords",
							"count": bson.M{"$sum": 1},
						},
					},
				},
				"categories": bson.A{
					bson.M{
						"$unwind": "$categories",
					},
					bson.M{
						"$group": bson.M{
							"_id":   "$categories",
							"count": bson.M{"$sum": 1},
						},
					},
				},
				"locations": bson.A{
					bson.M{
						"$unwind": "$locations",
					},
					bson.M{
						"$group": bson.M{
							"_id":   "$locations",
							"count": bson.M{"$sum": 1},
						},
					},
				},
				"dates": bson.A{
					bson.M{
						"$group": bson.M{
							"_id":   "$date",
							"count": bson.M{"$sum": 1},
						},
					},
				},
			},
		},
	}

	// Aggregate
	cursor, err := api.Collection().Aggregate(ctx, pipeline)
	if err != nil {
		l.WithError(err).Error("Could not aggregate")
		return nil, err
	}

	// Decode
	var results []countResults

	err = cursor.All(ctx, &results)
	if err != nil {
		l.WithError(err).Error("Could not fetch results")
		return nil, err
	}

	return &results[0], nil
}

func (api *apiImpl) modifyInheritedVisibilityTokens(ctx context.Context, folderSlugPath string, fullTree bool, add bool, token string) error {
	l := log.WithFields(log.Fields{
		"func":           "photos.apiImpl.modifyInheritedVisibilityTokens",
		"folderSlugPath": folderSlugPath,
		"fullTree":       fullTree,
		"add":            add,
	})

	if !utils.IsValidSlugPath(folderSlugPath) {
		l.WithError(utils.ErrInvalidSlugPath).Error("Invalid")
		return utils.ErrInvalidSlugPath
	}

	// Do all updates in a transaction to keep vis tokens and counts in sync.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Find number of photos in each group type that will be updated.
		counts, err := api.countInheritVisibilityPhotos(ctx, folderSlugPath, fullTree, add, token)
		if err != nil {
			l.WithError(err).Warn("Could not count inherited visibility photos")
			return nil, err
		}
		l.Tracef("Counts: %+v", counts)

		// Build filter to update photos.
		filter := bson.M{
			// Only update photos that are inheriting visibility
			"inheritVisibility": true,
		}

		// Filter by folder, with specific filter depending on fullTree
		if fullTree {
			filter["folder"] = bson.M{"$regex": "^" + folderSlugPath}
		} else {
			filter["folder"] = folderSlugPath
		}

		// Update
		var update bson.M
		if add {
			update = bson.M{
				"$addToSet": bson.M{
					"visTokens": token,
				},
			}
		} else {
			update = bson.M{
				"$pull": bson.M{
					"visTokens": token,
				},
			}
		}

		// Add/delete visibility token.
		res, err := api.Collection().UpdateMany(ctx, filter, update)
		if err != nil {
			l.WithError(err).Warn("Could not add inherited visibility tokens")
			return nil, err
		}

		l.Infof("Matched %v photos", res.MatchedCount)

		// Now update visibility tokens for each group type.

		// Helpers

		tokens := []string{token}

		type SingleIDGroupAPI interface {
			DiffAndUpdate(ctx context.Context, oldID *string, newID *string, oldTokens []string, newTokens []string, count int) error
		}

		updateSingleIDGroup := func(countResults []countResult, api SingleIDGroupAPI, groupType string) error {
			for _, countRes := range countResults {
				id := countRes.ID
				if id == "" { // artifact of aggregation
					continue
				}

				var oldID *string
				var newID *string
				var oldTokens []string
				var newTokens []string
				if add {
					newID = &id
					newTokens = tokens
				} else {
					oldID = &id
					oldTokens = tokens
				}

				// Update counts for visibility tokens
				err = api.DiffAndUpdate(ctx, oldID, newID, oldTokens, newTokens, countRes.Count)
				if err != nil {
					l.WithError(err).WithField(groupType, id).Warnf("Could not diff and update visibility tokens on %v", groupType)
					return err
				}
			}

			return nil
		}

		type MultiIDGroupAPI interface {
			DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error
		}

		updateMultiIDGroup := func(countResults []countResult, api MultiIDGroupAPI, groupType string) error {
			for _, countRes := range countResults {
				if countRes.ID == "" { // artifact of aggregation
					continue
				}

				ids := []string{countRes.ID}

				var oldIDs []string
				var newIDs []string
				var oldTokens []string
				var newTokens []string
				if add {
					newIDs = ids
					newTokens = tokens
				} else {
					oldIDs = ids
					oldTokens = tokens
				}

				// Update counts for visibility tokens
				err = api.DiffAndUpdate(ctx, oldIDs, newIDs, oldTokens, newTokens, countRes.Count)
				if err != nil {
					l.WithError(err).WithField(groupType, ids).Warnf("Could not diff and update visibility tokens on %v", groupType)
					return err
				}
			}

			return nil
		}

		// Updates
		if err := updateSingleIDGroup(counts.Folders, api.top.Folders, "folder"); err != nil {
			return nil, err
		}

		if err := updateMultiIDGroup(counts.Keywords, api.top.Keywords, "keyword"); err != nil {
			return nil, err
		}

		if err := updateMultiIDGroup(counts.Categories, api.top.Categories, "category"); err != nil {
			return nil, err
		}

		if err := updateMultiIDGroup(counts.Locations, api.top.Locations, "location"); err != nil {
			return nil, err
		}

		if err := updateSingleIDGroup(counts.Dates, api.top.Dates, "date"); err != nil {
			return nil, err
		}

		if len(counts.Total) > 0 {
			// Update visibility token group
			var oldVisTokenIDs []string
			var newVisTokenIDs []string

			if add {
				newVisTokenIDs = tokens
			} else {
				oldVisTokenIDs = tokens
			}

			if err := api.top.Visibility.DiffAndUpdate(ctx, oldVisTokenIDs, newVisTokenIDs, counts.Total[0].Count); err != nil {
				l.WithError(err).Warnf("Could not diff and update visibility token")
				return nil, err
			}
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}

// AddInheritedVisibilityTokens adds the given token to photos in given folder slug path.
// Optionally may also apply to photos in sub-folders of the given folder slug path.
func (api *apiImpl) AddInheritedVisibilityToken(ctx context.Context, folderSlugPath string, fullTree bool, token string) error {
	return api.modifyInheritedVisibilityTokens(ctx, folderSlugPath, fullTree, true, token)
}

// DeleteInheritedVisibilityTokens deletes the given token from photos in given folder slug path.
// Optionally may also apply to photos in sub-folders of the given folder slug path.
func (api *apiImpl) DeleteInheritedVisibilityToken(ctx context.Context, folderSlugPath string, fullTree bool, token string) error {
	return api.modifyInheritedVisibilityTokens(ctx, folderSlugPath, fullTree, false, token)
}
