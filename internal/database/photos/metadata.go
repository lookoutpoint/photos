// SPDX-License-Identifier: MIT

package photos

import (
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/categories"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

func getOptionalStringMetadata(val interface{}) *string {
	s, ok := val.(string)
	if !ok || len(s) == 0 {
		return nil
	}

	return &s
}

func getOptionalIntMetadata(val interface{}) *int32 {
	var res int32
	if v, ok := val.(int); ok {
		res = int32(v)
	} else if v, ok := val.(uint); ok {
		res = int32(v)
	} else if v, ok := val.(int32); ok {
		res = v
	} else if v, ok := val.(uint32); ok {
		res = int32(v)
	} else if v, ok := val.(int16); ok {
		res = int32(v)
	} else if v, ok := val.(uint16); ok {
		res = int32(v)
	} else if v, ok := val.(int8); ok {
		res = int32(v)
	} else if v, ok := val.(uint8); ok {
		res = int32(v)
	} else {
		return nil
	}

	if res == 0 {
		return nil
	}

	return &res
}

func getOptionalDateTimeFromString(val interface{}, l *log.Entry) *time.Time {
	s, ok := val.(string)
	if !ok || len(s) == 0 {
		return nil
	}

	t, err := time.Parse("2006:01:02 15:04:05", s)
	if err != nil {
		l.WithError(err).Warnf("Failed to parse time string '%v'", s)
		return nil
	}

	return &t
}

func rationalToFloat(v photomd.Urational) float64 {
	return float64(v.Num) / float64(v.Den)
}

func getOptionalRationalMetadata(val interface{}) *float64 {
	var res float64
	if r, ok := val.(photomd.Rational); ok {
		res = float64(r.Num) / float64(r.Den)
	} else if r, ok := val.(photomd.Urational); ok {
		res = float64(r.Num) / float64(r.Den)
	} else {
		return nil
	}

	return &res
}

func rational3ToDegrees(v []photomd.Urational) float64 {
	deg := rationalToFloat(v[0])
	min := rationalToFloat(v[1])
	sec := rationalToFloat(v[2])
	return deg + (min / 60) + (sec / 3600)
}

func computeGPSLocation(exifMd photomd.ExifData) *docs.GeoJSONPoint {
	var latRef, longRef string
	var lat, long []photomd.Urational

	// Check if we have the required Exif values
	var v interface{}
	var ok bool

	if v, ok = exifMd[photomd.ExifTagGPSLatitudeRef]; !ok {
		return nil
	} else if latRef, ok = v.(string); !ok || (latRef != "N" && latRef != "S") {
		return nil
	}

	if v, ok = exifMd[photomd.ExifTagGPSLongitudeRef]; !ok {
		return nil
	} else if longRef, ok = v.(string); !ok || (longRef != "E" && longRef != "W") {
		return nil
	}

	if v, ok = exifMd[photomd.ExifTagGPSLatitude]; !ok {
		return nil
	} else if lat, ok = v.([]photomd.Urational); !ok || len(lat) != 3 {
		return nil
	}

	if v, ok = exifMd[photomd.ExifTagGPSLongitude]; !ok {
		return nil
	} else if long, ok = v.([]photomd.Urational); !ok || len(long) != 3 {
		return nil
	}

	// We do, compute longitude and latitude
	latitude := rational3ToDegrees(lat)
	longitude := rational3ToDegrees(long)

	if latRef == "S" {
		latitude = -latitude
	}

	if longRef == "W" {
		longitude = -longitude
	}

	return docs.NewGeoJSONPoint(latitude, longitude)
}

type metadataGroups struct {
	Keywords         []string
	Categories       []string
	Locations        [][]string
	VisibilityTokens []string
}

// Visibility tokens are specified as keywords of the form __visibility:<token name>
const visibilityTokenCategory = "__visibility"

// Additional location information can be specified in the subjects.
const hikeSubjectPrefix = "!hike"
const parkSubjectPrefix = "!park"
const locSubjectPrefix = "!loc"

// Keywords start with "__" or "!" are for internal use.
func ignoreKeyword(kw string) bool {
	return strings.HasPrefix(kw, "__") || strings.HasPrefix(kw, "!")
}

// Process subjects array into keywords, categories, and locations.
func processSubjects(subjects []string, groups *metadataGroups, l *log.Entry) {
	var hikes []string
	var parks []string
	var locs []string

	for _, subject := range subjects {
		isKeyword := true

		if categories.IsValidCategory(subject) {
			// This might be a category but could also be a special "category" that translates into something else.
			isKeyword = false
			catParts := strings.Split(categories.CanonicalizeCategory(subject), categories.CategorySeparator)

			// For the special "categories" checked below, their value is the everything left over
			specialValue := strings.Join(catParts[1:], categories.CategorySeparator)

			// Check for visibility token category.
			if catParts[0] == visibilityTokenCategory {
				groups.VisibilityTokens = append(groups.VisibilityTokens, specialValue)
			} else if catParts[0] == hikeSubjectPrefix {
				// Special handling for hikes
				hikes = append(hikes, specialValue)
			} else if catParts[0] == parkSubjectPrefix {
				// Special handling for parks
				parks = append(parks, specialValue)
			} else if catParts[0] == locSubjectPrefix {
				// Special handling for additional locations
				locs = append(locs, specialValue)
			} else {
				// Trim ignored components, starting from the highest ignored part.
				for i, part := range catParts {
					if ignoreKeyword(part) {
						// Only keep parts [0, i)
						catParts = catParts[0:i]
						break
					}
				}

				// If there are at least two parts remaining, make that a category.
				if len(catParts) >= 2 {
					groups.Categories = append(groups.Categories, strings.Join(catParts, categories.CategorySeparator))
				}
			}
		}

		if isKeyword {
			subject := strings.TrimSpace(subject)

			if len(subject) > 0 && !ignoreKeyword(subject) {
				// Just add subject as a keyword
				groups.Keywords = append(groups.Keywords, subject)
			}
		}
	}

	// Sort the group arrays.
	sort.Strings(groups.Keywords)
	sort.Strings(groups.Categories)
	sort.Strings(groups.VisibilityTokens)

	// Custom processing for locations from subjects.
	addSubjectsToLocation(hikes, parks, locs, groups, l)
}

// Custom location values have special syntax [@#:]<name> where # is an index into the base location.
var customSubjectRegex = regexp.MustCompile(`^(?:@(\d+):)?(.+)$`)

func addSubjectsToLocation(hikes []string, parks []string, locs []string, groups *metadataGroups, l *log.Entry) {
	l.Tracef("Hikes: %+v", hikes)
	l.Tracef("Parks: %+v", parks)
	l.Tracef("Locs: %+v", locs)

	if len(groups.Locations) == 0 {
		// If there is no location at all right now, initialize an empty default location.
		groups.Locations = append(groups.Locations, []string{})
	}

	// The base location to use for relative locations below (those that use indices). This is a copy of the default location.
	baseLoc := groups.Locations[0]
	l.Tracef("Base loc: %+v", baseLoc)

	// The location values below may have an index specified at the beginning: "@#:" where # is the index.
	// - If no index value is specified, the default location is modified.
	// - If an index is specified, always creates a new location relative to the base location and index.
	//
	// Process things in order of highest in hierarchy to lowest (parks, hikes, locs) in case they all modify the default location.

	// Custom parks.
	for _, park := range parks {
		// Park name has syntax [@#:]<park>
		parts := customSubjectRegex.FindStringSubmatch(park)
		parkName := parkSubjectPrefix + ":" + parts[2]

		if len(parts[1]) == 0 {
			// Modifies default location.
			groups.Locations[0] = append([]string{parkName}, groups.Locations[0]...)
			l.Tracef("Added park to default location: %+v", groups.Locations[0])
		} else {
			// Add new location, using subset of base location starting from index locIndex.
			locIndex := 0
			if len(parts[1]) > 0 {
				locIndex, _ = strconv.Atoi(parts[1])
			}

			if locIndex <= len(baseLoc) {
				newLoc := append([]string{parkName}, baseLoc[locIndex:]...)
				groups.Locations = append(groups.Locations, newLoc)
			} else {
				l.Errorf("Park value '%v' references invalid location index", park)
			}
		}

	}

	// Custom hikes.
	for _, hike := range hikes {
		// Hike name has syntax [@#:]<hike>
		parts := customSubjectRegex.FindStringSubmatch(hike)
		hikeName := hikeSubjectPrefix + ":" + parts[2]

		if len(parts[1]) == 0 {
			// Modifies default location.
			groups.Locations[0] = append([]string{hikeName}, groups.Locations[0]...)
			l.Tracef("Added hike to default location: %+v", groups.Locations[0])
		} else {
			// Add new location, using subset of base location starting from index locIndex.
			locIndex := 0
			if len(parts[1]) > 0 {
				locIndex, _ = strconv.Atoi(parts[1])
			}

			if locIndex <= len(baseLoc) {
				newLoc := append([]string{hikeName}, baseLoc[locIndex:]...)
				groups.Locations = append(groups.Locations, newLoc)
			} else {
				l.Errorf("Hike value '%v' references invalid location index", hike)
			}
		}
	}

	// Custom locations.
	for _, loc := range locs {
		// Location name has syntax [@#:]<loc>
		// where <loc> may have '/' as delimiters
		parts := customSubjectRegex.FindStringSubmatch(loc)
		newLoc := strings.Split(parts[2], "/")

		if len(parts[1]) == 0 {
			// Modifies default location.
			groups.Locations[0] = append(newLoc, groups.Locations[0]...)
			l.Tracef("Added location to default location: %+v", groups.Locations[0])
		} else {
			// Add new location, using subset of base location starting from index locIndex.
			locIndex := 0
			if len(parts[1]) > 0 {
				locIndex, _ = strconv.Atoi(parts[1])
			}

			if locIndex <= len(baseLoc) {
				newLoc = append(newLoc, baseLoc[locIndex:]...)
				groups.Locations = append(groups.Locations, newLoc)
			} else {
				l.Errorf("Custom location value '%v' references invalid location index", loc)
			}
		}
	}

	// If the default location is empty, trim it.
	if len(groups.Locations[0]) == 0 {
		groups.Locations = groups.Locations[1:]
	}

}

// Parse location metadata. Splits location into components and add to Locations array in order given in the metadata.
func parseLocationMetadata(xmp photomd.XmpData, groups *metadataGroups) {
	location := getOptionalStringMetadata(xmp.Location)

	if location != nil {
		// Split location as a comma-separated string. First string is most specific location.
		locations := strings.Split(*location, ",")

		// Trim each location component.
		var locArray []string
		for _, loc := range locations {
			loc = strings.TrimSpace(loc)
			if len(loc) > 0 {
				locArray = append(locArray, loc)
			}
		}

		if len(locArray) > 0 {
			groups.Locations = append(groups.Locations, locArray)
		}
	}
}

// Reverses each location so that the first element is the highest-level (least-specific) location component.
func reorderLocations(groups *metadataGroups) {
	for _, locArray := range groups.Locations {
		for i, j := 0, len(locArray)-1; i < j; i, j = i+1, j-1 {
			locArray[i], locArray[j] = locArray[j], locArray[i]
		}
	}
}

// Parse keyword, categories and location metadata.
// Returns an array of visibility token names, which may appear in keywords.
func parseGroups(md *photomd.Metadata, l *log.Entry) *metadataGroups {
	var groups metadataGroups

	// Parse location first, which may be referenced by certain subjects.
	parseLocationMetadata(md.Xmp, &groups)

	// Process subjects.
	processSubjects(md.Xmp.Subjects, &groups, l)

	// Post-process locations.
	reorderLocations(&groups)

	return &groups
}

// Fills photo document from photo metadata. Returns selected extracted metadata.
func fillDocFromMetadata(photoDoc *docs.Photo, md *photomd.Metadata, l *log.Entry) *metadataGroups {
	photoDoc.Width = &md.Width
	photoDoc.Height = &md.Height

	// All other data attributes are optional depend if the metadata value exists
	photoDoc.Title = getOptionalStringMetadata(md.Xmp.Title)
	photoDoc.Description = getOptionalStringMetadata(md.Exif[photomd.ExifTagImageDescription])
	photoDoc.DateTime = getOptionalDateTimeFromString(md.Exif[photomd.ExifTagDateTimeOriginal], l)
	photoDoc.Rating = getOptionalIntMetadata(md.Xmp.Rating)

	photoDoc.Copyright = getOptionalStringMetadata(md.Exif[photomd.ExifTagCopyright])
	photoDoc.UsageTerms = getOptionalStringMetadata(md.Xmp.UsageTerms)
	photoDoc.UsageWebURL = getOptionalStringMetadata(md.Xmp.RightsWebStatement)

	photoDoc.GPSLocation = computeGPSLocation(md.Exif)
	photoDoc.GPSAltitude = getOptionalRationalMetadata(md.Exif[photomd.ExifTagGPSAltitude])

	photoDoc.Make = getOptionalStringMetadata(md.Exif[photomd.ExifTagMake])
	photoDoc.Model = getOptionalStringMetadata(md.Exif[photomd.ExifTagModel])
	photoDoc.Lens = getOptionalStringMetadata(md.Exif[photomd.ExifTagLensModel])
	photoDoc.FNumber = getOptionalRationalMetadata(md.Exif[photomd.ExifTagFNumber])
	photoDoc.FocalLength = getOptionalRationalMetadata(md.Exif[photomd.ExifTagFocalLength])
	photoDoc.ISO = getOptionalIntMetadata(md.Exif[photomd.ExifTagISOSpeedRatings])
	photoDoc.ExposureTime = getOptionalRationalMetadata(md.Exif[photomd.ExifTagExposureTime])
	photoDoc.ExposureBias = getOptionalRationalMetadata(md.Exif[photomd.ExifTagExposureBiasValue])

	return parseGroups(md, l)
}
