// SPDX-License-Identifier: MIT

package photos_test

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "github.com/onsi/gomega/gstruct"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
)

var _ = Describe("List", func() {
	const bucketName = "lookoutpoint-dev-photo-master"

	tokensAll := []string{docs.VisTokenAll}

	addPhoto := func(objectID string, md *photomd.Metadata) {
		Expect(db.Photos.AddOrUpdate(ctx, bucketName, objectID, 0, 1000, md)).ToNot(BeNil())
	}

	baseMd := func(title string, order int) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title: title,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: fmt.Sprintf("%d:01:01 01:00:00", 2020+order),
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()
	})

	Describe("List", func() {
		Describe("Folder filter", func() {
			md := baseMd

			It("Empty folder", func() {
				_, err := db.Folders.Create(ctx, "/folder/")
				Expect(err).ToNot(HaveOccurred())

				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					Filter: docs.PhotoListFilterExpr{
						Op: list.LogicalAnd,
						Folders: []docs.PhotoListFilterFolder{
							{Path: "/folder/"},
						},
					},
					VisibilityTokens: tokensAll,
				})
				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(0))
			})

			Describe("Direct folder", func() {
				// Photo sort keys (in order):
				// 20210101010000DSC_100/lTepZOFPvXtfkGcw6WsomMPP_tw
				// 20220101010000DSC_101/QnSclOvO9ONf5o21xKI8obHJ5Xk
				// 20240101010000DSC_256/C1213CKYZ6A2xJJCrp6TpjpKxkQ
				// 20250101010000DSC_257/CSAant_bbZ8ZNG3qzJNl6Alrvt0
				// 20260101010000DSC_300/_9xOu1vfcbVh_x0-IiosG-ItXGg
				BeforeEach(func() {
					addPhoto("folder/DSC_256.jpg", md("Exposure 1", 4))
					addPhoto("folder/DSC_257.jpg", md("Exposure 2", 5))
					addPhoto("folder/DSC_100.jpg", md("First Photo", 1))
					addPhoto("folder/DSC_101.jpg", md("Second Photo", 2))
					addPhoto("folder/DSC_300.jpg", md("Last Photo", 6))
					addPhoto("other folder/DSC_400.jpg", md("Somewhere Else", 3))
				})

				It("Simple forward list", func() {
					// List all photos in folder
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"sortKey", "title", "width", "height"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					})
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0]).To(MatchFields(IgnoreExtras, Fields{
						"Title": PointTo(Equal("First Photo")),
					}))
					Expect(photos[1].Title).To(PointTo(Equal("Second Photo")))
					Expect(photos[2].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[3].Title).To(PointTo(Equal("Exposure 2")))
					Expect(photos[4].Title).To(PointTo(Equal("Last Photo")))

					// List all photos in other folder
					photos, err = db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/other-folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					})
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("Somewhere Else")))

				})

				It("Paginate with pages of two", func() {
					params := &apis.PhotoListParams{
						Limit:          2,
						ProjectionKeys: []string{"title", "sortKey"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					}

					// First page
					photos, err := db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("First Photo")))
					Expect(photos[1].Title).To(PointTo(Equal("Second Photo")))

					// Second page
					params.RefKeyValue = *photos[1].SortKey
					photos, err = db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 2")))

					// Third page
					params.RefKeyValue = *photos[1].SortKey
					photos, err = db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("Last Photo")))
				})

				It("Query second photo and next", func() {
					params := &apis.PhotoListParams{
						Limit:          2,
						RefKeyValue:    "20220101010000DSC_101/QnSclOvO9ONf5o21xKI8obHJ5Xk",
						ProjectionKeys: []string{"title", "sortKey"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					}

					// Not inclusive
					params.RefOp = list.RefOpGt
					photos, err := db.Photos.List(ctx, params)

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 2")))

					// Inclusive
					params.RefOp = list.RefOpGte
					photos, err = db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Second Photo")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 1")))
				})

				It("Query fourth photo and previous", func() {
					params := &apis.PhotoListParams{
						Limit:          2,
						RefKeyValue:    "20250101010000DSC_257/CSAant_bbZ8ZNG3qzJNl6Alrvt0",
						ProjectionKeys: []string{"title", "sortKey"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					}

					// Not inclusive
					params.RefOp = list.RefOpLt
					photos, err := db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[1].Title).To(PointTo(Equal("Second Photo")))

					// Inclusive
					params.RefOp = list.RefOpLte
					photos, err = db.Photos.List(ctx, params)
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 2")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 1")))

				})

				It("Timestamp order", func() {
					// List all photos in folder. Should be in order in which they were added.
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						RefKey:         apis.PhotoListKeyTimestamp,
						ProjectionKeys: []string{"title", "tsSortKey"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					})
					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 2")))
					Expect(photos[2].Title).To(PointTo(Equal("First Photo")))
					Expect(photos[3].Title).To(PointTo(Equal("Second Photo")))
					Expect(photos[4].Title).To(PointTo(Equal("Last Photo")))

				})

			})

			Describe("Multi-level hierarchy", func() {
				BeforeEach(func() {
					addPhoto("folder/subfolder/DSC_256.jpg", md("Exposure 1", 0))
					addPhoto("folder/DSC_257.jpg", md("Exposure 2", 0))
				})

				It("Direct folder", func() {
					// List photos directly in folder
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/"},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 2")))
				})

				It("Full tree folder", func() {
					// List photos in full tree in folder
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Op: list.LogicalOr,
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/folder/", FullTree: true},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("Exposure 1")))
					Expect(photos[1].Title).To(PointTo(Equal("Exposure 2")))
				})
			})

			Describe("Multi-value filter", func() {
				BeforeEach(func() {
					addPhoto("a/b/c/p1.jpg", md("P1", 1))
					addPhoto("a/b/c/p3.jpg", md("P3", 3))
					addPhoto("a/x/y/p2.jpg", md("P2", 2))
				})

				It("AND: same hierarchical path", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/a/b/c/"},
								{Path: "/a/b/", FullTree: true},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("P1")))
					Expect(photos[1].Title).To(PointTo(Equal("P3")))
				})

				It("AND: disjoint paths", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/a/b/c/"},
								{Path: "/a/x/y/"},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(0))
				})

				It("OR: disjoint paths", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Op: list.LogicalOr,
							Folders: []docs.PhotoListFilterFolder{
								{Path: "/a/b/c/"},
								{Path: "/a/x/y/"},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("P1")))
					Expect(photos[1].Title).To(PointTo(Equal("P2")))
					Expect(photos[2].Title).To(PointTo(Equal("P3")))
				})

			})

		})

		Describe("Keyword filter", func() {
			md := func(title string, order int, keywords ...string) *photomd.Metadata {
				md := baseMd(title, order)
				md.Xmp.Subjects = keywords
				return md
			}

			BeforeEach(func() {
				addPhoto("apple.jpg", md("apple", 1, "fruit", "apple", "red"))
				addPhoto("banana.jpg", md("banana", 2, "fruit", "banana", "yellow"))
				addPhoto("brocolli.jpg", md("brocolli", 3, "vegetable", "brocolli", "green"))
				addPhoto("strawberry.jpg", md("strawberry", 4, "fruit", "strawberry", "red"))
				addPhoto("beet.jpg", md("beet", 5, "vegetable", "beet", "red"))
			})

			It("Single keyword", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Keywords: []string{"/fruit/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("apple")))
				Expect(photos[1].Title).To(PointTo(Equal("banana")))
				Expect(photos[2].Title).To(PointTo(Equal("strawberry")))

			})

			It("AND: Multiple keywords", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Keywords: []string{"/red/", "/fruit/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0].Title).To(PointTo(Equal("apple")))
				Expect(photos[1].Title).To(PointTo(Equal("strawberry")))

			})

			It("OR: Multiple keywords", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Op:       list.LogicalOr,
						Keywords: []string{"/vegetable/", "/yellow/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("banana")))
				Expect(photos[1].Title).To(PointTo(Equal("brocolli")))
				Expect(photos[2].Title).To(PointTo(Equal("beet")))
			})

		})

		Describe("Category filter", func() {
			md := func(title string, order int, keywords ...string) *photomd.Metadata {
				md := baseMd(title, order)
				md.Xmp.Subjects = keywords
				return md
			}

			BeforeEach(func() {
				addPhoto("apple.jpg", md("apple", 1, "fruit: apple", "color: red"))
				addPhoto("banana.jpg", md("banana", 2, "fruit: banana", "color: yellow"))
				addPhoto("brocolli.jpg", md("brocolli", 3, "vegetable: brocolli", "color: green"))
				addPhoto("strawberry.jpg", md("strawberry", 4, "fruit: strawberry", "color: red"))
				addPhoto("beet.jpg", md("beet", 5, "vegetable: beet", "color: red"))
			})

			It("Single category", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Categories: []string{"/fruit/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("apple")))
				Expect(photos[1].Title).To(PointTo(Equal("banana")))
				Expect(photos[2].Title).To(PointTo(Equal("strawberry")))

			})

			It("Single category value", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Categories: []string{"/color/red/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("apple")))
				Expect(photos[1].Title).To(PointTo(Equal("strawberry")))
				Expect(photos[2].Title).To(PointTo(Equal("beet")))

			})

			It("AND: Multiple categories", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Categories: []string{"/fruit/", "/color/yellow/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("banana")))

			})

			It("OR: Multiple categories", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Op:         list.LogicalOr,
						Categories: []string{"/color/yellow/", "/vegetable/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("banana")))
				Expect(photos[1].Title).To(PointTo(Equal("brocolli")))
				Expect(photos[2].Title).To(PointTo(Equal("beet")))
			})

		})

		Describe("Location-related filter", func() {
			md := func(title string, order int, locations string) *photomd.Metadata {
				md := baseMd(title, order)
				md.Xmp.Location = locations
				return md
			}

			BeforeEach(func() {
				addPhoto("1.jpg", md("1", 1, "Trinity park, Xyz Town"))
				addPhoto("2.jpg", md("2", 1, "Xyz Town, District Nine"))
			})

			It("Single location", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Locations: []string{"/xyz-town/trinity-park/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("1")))

				photos, err = db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Op:        list.LogicalOr,
						Locations: []string{"/district-nine/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("2")))

			})

			It("AND: Multiple locations", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Locations: []string{"/district-nine/", "/district-nine/xyz-town/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("2")))

			})

			It("OR: Multiple locations", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Op:        list.LogicalOr,
						Locations: []string{"/district-nine/", "/xyz-town/"},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0].Title).To(PointTo(Equal("1")))
				Expect(photos[1].Title).To(PointTo(Equal("2")))

			})
		})

		Describe("Date filter", func() {
			md := func(title string, dateTime string) *photomd.Metadata {
				md := baseMd(title, 0)
				md.Exif[photomd.ExifTagDateTimeOriginal] = dateTime
				return md
			}

			BeforeEach(func() {
				addPhoto("1.jpg", md("1", "2020:01:02 01:02:03"))
				addPhoto("2.jpg", md("2", "2020:01:03 01:02:03"))
				addPhoto("3.jpg", md("3", "2020:01:03 11:12:13"))
				addPhoto("4.jpg", md("4", "2021:12:15 01:02:03"))
				addPhoto("5.jpg", md("5", "2021:12:25 01:02:03"))
				addPhoto("6.jpg", md("6", "2020:11:10 01:02:03"))
				addPhoto("7.jpg", md("7", "2020:11:11 01:02:03"))
			})

			Describe("Year", func() {
				It("Eq", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020", CmpOp: list.CmpEq},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
					Expect(photos[3].Title).To(PointTo(Equal("6")))
					Expect(photos[4].Title).To(PointTo(Equal("7")))
				})

				It("Ne", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020", CmpOp: list.CmpNe},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("4")))
					Expect(photos[1].Title).To(PointTo(Equal("5")))
				})

				It("Gt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020", CmpOp: list.CmpGt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("4")))
					Expect(photos[1].Title).To(PointTo(Equal("5")))
				})

				It("Gte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020", CmpOp: list.CmpGte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(7))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
					Expect(photos[3].Title).To(PointTo(Equal("6")))
					Expect(photos[4].Title).To(PointTo(Equal("7")))
					Expect(photos[5].Title).To(PointTo(Equal("4")))
					Expect(photos[6].Title).To(PointTo(Equal("5")))
				})

				It("Lt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2021", CmpOp: list.CmpLt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
					Expect(photos[3].Title).To(PointTo(Equal("6")))
					Expect(photos[4].Title).To(PointTo(Equal("7")))
				})

				It("Lte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020", CmpOp: list.CmpLte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
					Expect(photos[3].Title).To(PointTo(Equal("6")))
					Expect(photos[4].Title).To(PointTo(Equal("7")))
				})

			})

			Describe("Year and month", func() {
				It("Eq", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011", CmpOp: list.CmpEq},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("6")))
					Expect(photos[1].Title).To(PointTo(Equal("7")))
				})

				It("Ne", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202001", CmpOp: list.CmpNe},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(4))
					Expect(photos[0].Title).To(PointTo(Equal("6")))
					Expect(photos[1].Title).To(PointTo(Equal("7")))
					Expect(photos[2].Title).To(PointTo(Equal("4")))
					Expect(photos[3].Title).To(PointTo(Equal("5")))
				})

				It("Gt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011", CmpOp: list.CmpGt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("4")))
					Expect(photos[1].Title).To(PointTo(Equal("5")))
				})

				It("Gte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011", CmpOp: list.CmpGte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(4))
					Expect(photos[0].Title).To(PointTo(Equal("6")))
					Expect(photos[1].Title).To(PointTo(Equal("7")))
					Expect(photos[2].Title).To(PointTo(Equal("4")))
					Expect(photos[3].Title).To(PointTo(Equal("5")))
				})

				It("Lt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011", CmpOp: list.CmpLt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
				})

				It("Lte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011", CmpOp: list.CmpLte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
					Expect(photos[3].Title).To(PointTo(Equal("6")))
					Expect(photos[4].Title).To(PointTo(Equal("7")))
				})

			})

			Describe("Year, month and day", func() {
				It("Eq", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20201111", CmpOp: list.CmpEq},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("7")))
				})

				It("Ne", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20200103", CmpOp: list.CmpNe},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(5))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("6")))
					Expect(photos[2].Title).To(PointTo(Equal("7")))
					Expect(photos[3].Title).To(PointTo(Equal("4")))
					Expect(photos[4].Title).To(PointTo(Equal("5")))
				})

				It("Gt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20201110", CmpOp: list.CmpGt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("7")))
					Expect(photos[1].Title).To(PointTo(Equal("4")))
					Expect(photos[2].Title).To(PointTo(Equal("5")))
				})

				It("Gte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20211215", CmpOp: list.CmpGte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("4")))
					Expect(photos[1].Title).To(PointTo(Equal("5")))
				})

				It("Lt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20200103", CmpOp: list.CmpLt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
				})

				It("Lte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20200103", CmpOp: list.CmpLte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
				})

			})

			Describe("Time", func() {
				It("Eq", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020010311", CmpOp: list.CmpEq},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("3")))
				})

				It("Ne", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20200103010203", CmpOp: list.CmpNe},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(6))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("3")))
					Expect(photos[2].Title).To(PointTo(Equal("6")))
					Expect(photos[3].Title).To(PointTo(Equal("7")))
					Expect(photos[4].Title).To(PointTo(Equal("4")))
					Expect(photos[5].Title).To(PointTo(Equal("5")))
				})

				It("Gt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202011100102", CmpOp: list.CmpGt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("7")))
					Expect(photos[1].Title).To(PointTo(Equal("4")))
					Expect(photos[2].Title).To(PointTo(Equal("5")))
				})

				It("Gte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "20211215010204", CmpOp: list.CmpGte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(1))
					Expect(photos[0].Title).To(PointTo(Equal("5")))
				})

				It("Lt", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "202001030103", CmpOp: list.CmpLt},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(2))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
				})

				It("Lte", func() {
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						ProjectionKeys: []string{"title"},
						Filter: docs.PhotoListFilterExpr{
							Dates: []docs.PhotoListFilterDate{
								{Value: "2020010311", CmpOp: list.CmpLte},
							},
						},
						VisibilityTokens: tokensAll,
					})

					Expect(err).ToNot(HaveOccurred())
					Expect(photos).To(HaveLen(3))
					Expect(photos[0].Title).To(PointTo(Equal("1")))
					Expect(photos[1].Title).To(PointTo(Equal("2")))
					Expect(photos[2].Title).To(PointTo(Equal("3")))
				})

			})

		})

		Describe("Rating filter", func() {
			md := func(title string, order int, rating int) *photomd.Metadata {
				md := baseMd(title, order)
				md.Xmp.Rating = rating
				return md
			}

			BeforeEach(func() {
				addPhoto("0.jpg", baseMd("0", 0)) // no rating
				addPhoto("1.jpg", md("1", 1, 1))
				addPhoto("2.jpg", md("2", 2, 2))
				addPhoto("3.jpg", md("3", 3, 3))
				addPhoto("4.jpg", md("4", 4, 4))
				addPhoto("5.jpg", md("5", 5, 5))
			})

			It("Eq", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 5, CmpOp: list.CmpEq},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("5")))
			})

			It("Eq 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpEq},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
			})

			It("Ne", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 4, CmpOp: list.CmpNe},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(5))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
				Expect(photos[1].Title).To(PointTo(Equal("1")))
				Expect(photos[2].Title).To(PointTo(Equal("2")))
				Expect(photos[3].Title).To(PointTo(Equal("3")))
				Expect(photos[4].Title).To(PointTo(Equal("5")))
			})

			It("Ne 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpNe},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(5))
				Expect(photos[0].Title).To(PointTo(Equal("1")))
				Expect(photos[1].Title).To(PointTo(Equal("2")))
				Expect(photos[2].Title).To(PointTo(Equal("3")))
				Expect(photos[3].Title).To(PointTo(Equal("4")))
				Expect(photos[4].Title).To(PointTo(Equal("5")))
			})

			It("Gt", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 4, CmpOp: list.CmpGt},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("5")))
			})

			It("Gt 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpNe},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(5))
				Expect(photos[0].Title).To(PointTo(Equal("1")))
				Expect(photos[1].Title).To(PointTo(Equal("2")))
				Expect(photos[2].Title).To(PointTo(Equal("3")))
				Expect(photos[3].Title).To(PointTo(Equal("4")))
				Expect(photos[4].Title).To(PointTo(Equal("5")))
			})

			It("Gte", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 3, CmpOp: list.CmpGte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("3")))
				Expect(photos[1].Title).To(PointTo(Equal("4")))
				Expect(photos[2].Title).To(PointTo(Equal("5")))
			})

			It("Gte 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpGte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(6))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
				Expect(photos[1].Title).To(PointTo(Equal("1")))
				Expect(photos[2].Title).To(PointTo(Equal("2")))
				Expect(photos[3].Title).To(PointTo(Equal("3")))
				Expect(photos[4].Title).To(PointTo(Equal("4")))
				Expect(photos[5].Title).To(PointTo(Equal("5")))
			})

			It("Lt", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 3, CmpOp: list.CmpLt},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
				Expect(photos[1].Title).To(PointTo(Equal("1")))
				Expect(photos[2].Title).To(PointTo(Equal("2")))
			})

			It("Lt 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpLt},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(0))
			})

			It("Lte", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 2, CmpOp: list.CmpLte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
				Expect(photos[1].Title).To(PointTo(Equal("1")))
				Expect(photos[2].Title).To(PointTo(Equal("2")))
			})

			It("Lte 0", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Ratings: []docs.PhotoListFilterRating{
							{Value: 0, CmpOp: list.CmpLte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
			})

		})

		Describe("Aspect ratio filter", func() {
			md := func(title string, order int, width int, height int) *photomd.Metadata {
				md := baseMd(title, order)
				md.Width = width
				md.Height = height
				return md
			}

			BeforeEach(func() {
				addPhoto("square.jpg", md("square", 0, 100, 100))
				addPhoto("wide.jpg", md("wide", 1, 200, 100))
				addPhoto("tall.jpg", md("tall", 2, 50, 100))
			})

			It("Eq", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpEq},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("square")))
			})

			It("Ne", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpNe},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0].Title).To(PointTo(Equal("wide")))
				Expect(photos[1].Title).To(PointTo(Equal("tall")))
			})

			It("Gt", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpGt},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("wide")))
			})

			It("Gte", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpGte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0].Title).To(PointTo(Equal("square")))
				Expect(photos[1].Title).To(PointTo(Equal("wide")))
			})

			It("Lt", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpLt},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("tall")))
			})

			It("Lte", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 2, CmpOp: list.CmpLte},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("square")))
				Expect(photos[1].Title).To(PointTo(Equal("wide")))
				Expect(photos[2].Title).To(PointTo(Equal("tall")))
			})

		})

		Describe("Sub-expressions", func() {
			md := func(title string, order int, rating int) *photomd.Metadata {
				md := baseMd(title, order)
				md.Xmp.Rating = rating
				return md
			}

			BeforeEach(func() {
				addPhoto("0.jpg", baseMd("0", 0)) // no rating
				addPhoto("folder/1.jpg", md("1", 1, 1))
				addPhoto("xyz/2.jpg", md("2", 2, 2))
				addPhoto("abc/def/3.jpg", md("3", 3, 3))
			})

			It("Folder or rating", func() {
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Op: list.LogicalOr,
						Exprs: []*docs.PhotoListFilterExpr{
							{
								Ratings: []docs.PhotoListFilterRating{
									{Value: 1, CmpOp: list.CmpLte},
								},
							},
							{
								Folders: []docs.PhotoListFilterFolder{
									{Path: "/abc/", FullTree: true},
								},
							},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0].Title).To(PointTo(Equal("0")))
				Expect(photos[1].Title).To(PointTo(Equal("1")))
				Expect(photos[2].Title).To(PointTo(Equal("3")))
			})
		})

		It("List with gps coordinates existence", func() {
			// Add a photo with GPS coordinates.
			m := baseMd("gps", 0)
			m.Exif[photomd.ExifTagGPSLatitude] = []photomd.Urational{{Num: 10, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
			m.Exif[photomd.ExifTagGPSLongitude] = []photomd.Urational{{Num: 20, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
			m.Exif[photomd.ExifTagGPSLatitudeRef] = "N"
			m.Exif[photomd.ExifTagGPSLongitudeRef] = "E"
			addPhoto("gps.jpg", m)

			photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
				ProjectionKeys:   []string{"title"},
				ExistKeys:        []string{"gpsLocation"},
				VisibilityTokens: tokensAll,
			})

			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].Title).To(PointTo(Equal("gps")))
		})

	})

	Describe("Random pick", func() {
		Describe("Folder, rating and aspect ratio filter", func() {
			md := func(title string, order int, width int, height int, rating int) *photomd.Metadata {
				md := baseMd(title, order)
				md.Width = width
				md.Height = height
				md.Xmp.Rating = rating
				return md
			}

			BeforeEach(func() {
				addPhoto("folder/square.jpg", md("square", 0, 100, 100, 4))
				addPhoto("folder/wide.jpg", md("wide", 1, 200, 100, 5))
				addPhoto("tall.jpg", md("tall", 2, 50, 100, 4))
			})

			It("Random pick", func() {
				photos, err := db.Photos.RandomList(ctx, &apis.PhotoRandomListParams{
					Count:          1,
					ProjectionKeys: []string{"title"},
					Filter: docs.PhotoListFilterExpr{
						Folders: []docs.PhotoListFilterFolder{
							{Path: "/folder/"},
						},
						AspectRatios: []docs.PhotoListFilterAspectRatio{
							{Value: 1, CmpOp: list.CmpGte},
						},
						Ratings: []docs.PhotoListFilterRating{
							{Value: 4, CmpOp: list.CmpEq},
						},
					},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("square")))
			})

			It("Random pick with gps coordinates", func() {
				// Add a photo with GPS coordinates.
				m := md("gps", 3, 100, 100, 4)
				m.Exif[photomd.ExifTagGPSLatitude] = []photomd.Urational{{Num: 10, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
				m.Exif[photomd.ExifTagGPSLongitude] = []photomd.Urational{{Num: 20, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
				m.Exif[photomd.ExifTagGPSLatitudeRef] = "N"
				m.Exif[photomd.ExifTagGPSLongitudeRef] = "E"
				addPhoto("gps.jpg", m)

				photos, err := db.Photos.RandomList(ctx, &apis.PhotoRandomListParams{
					Count:            1,
					ProjectionKeys:   []string{"title"},
					ExistKeys:        []string{"gpsLocation"},
					VisibilityTokens: tokensAll,
				})

				Expect(err).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0].Title).To(PointTo(Equal("gps")))
			})
		})

	})

})
