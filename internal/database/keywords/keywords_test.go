// SPDX-License-Identifier: MIT

package keywords_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

var _ = BeforeEach(func() {
	clearTestDatabase()
})

var _ = Describe("Keywords", func() {
	tokensAll := []string{docs.VisTokenAll}

	It("Create, Get, GetOrCreate", func() {
		Expect(db.Keywords.Create(ctx, "hello")).To(Equal("/hello/"))

		Expect(db.Keywords.Get(ctx, "/hello/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("hello")),
			"GroupBase.SortOrder": PointTo(Equal("hello")),
		}))

		Expect(db.Keywords.GetOrCreate(ctx, "Hello World", nil)).To(MatchGroupBasePtr(Fields{
			"GroupBase.ID":      PointTo(Equal("/hello-world/")),
			"GroupBase.Display": PointTo(Equal("Hello World")),
		}))
	})

	It("Create numerical keyword", func() {
		Expect(db.Keywords.Create(ctx, "10")).To(Equal("/10/"))

		Expect(db.Keywords.Get(ctx, "/10/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
			"GroupBase.Display":   PointTo(Equal("10")),
			"GroupBase.SortOrder": PointTo(BeEquivalentTo(10)),
		}))
	})

	It("GetMulti", func() {
		Expect(db.Keywords.Create(ctx, "hello")).ToNot(HaveLen(0))
		Expect(db.Keywords.Create(ctx, "world")).ToNot(HaveLen(0))

		Expect(db.Keywords.GetMulti(ctx, []string{"/world/", "/hello/"}, nil, tokensAll)).To(
			ConsistOf(
				MatchGroupBase(Fields{"GroupBase.ID": PointTo(Equal("/hello/"))}),
				MatchGroupBase(Fields{"GroupBase.ID": PointTo(Equal("/world/"))}),
			),
		)
	})

	Describe("DiffAndUpdate", func() {
		matchPhotoCount := func(count int, token ...string) OmegaMatcher {
			if len(token) > 1 {
				panic("At most one token")
			}

			counts := VisTokenCounts{docs.VisTokenAll: count + 1}
			if len(token) > 0 {
				counts[token[0]] = count
			}

			return MatchGroupBasePtrVisTokenCounts(counts, nil)
		}

		createKw := func(name string) {
			_, err := db.Keywords.Create(ctx, name)
			Expect(err).ToNot(HaveOccurred())
		}

		It("Simple slugs", func() {
			var kws []string

			// Simulate adding two new keywords
			createKw("a")
			createKw("b")

			kws = []string{"/a/", "/b/"}
			err := db.Keywords.DiffAndUpdate(ctx, nil, kws, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1))

			// Simulate adding another keyword
			createKw("c")

			oldKws := kws
			kws = []string{"/a/", "/b/", "/c/"}
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, kws, tokensAll, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(1))

			// Simulate renaming a keyword
			createKw("x")

			oldKws = kws
			kws = []string{"/x/", "/b/", "/c/"}
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, kws, tokensAll, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/x/", nil, tokensAll)).To(matchPhotoCount(1))

			// Simulate removal of all keywords
			oldKws = kws
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, nil, tokensAll, nil, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/x/", nil, tokensAll)).To(matchPhotoCount(0))

		})

		It("Change vis tokens", func() {
			var kws []string

			// Simulate adding two new keywords
			createKw("a")
			createKw("b")

			kws = []string{"/a/", "/b/"}
			err := db.Keywords.DiffAndUpdate(ctx, nil, kws, nil, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1))

			// Simulate adding another keyword and adding a token
			createKw("c")
			T := "/T/"

			oldKws := kws
			kws = []string{"/a/", "/b/", "/c/"}
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, kws, tokensAll, []string{docs.VisTokenAll, T}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(1, T))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1, T))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(1, T))

			// Simulate renaming a keyword and changing token
			createKw("x")
			T2 := "/T2/"

			oldKws = kws
			kws = []string{"/x/", "/b/", "/c/"}
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, kws, []string{docs.VisTokenAll, T}, []string{docs.VisTokenAll, T2}, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(1, T2))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(1, T2))
			Expect(db.Keywords.Get(ctx, "/x/", nil, tokensAll)).To(matchPhotoCount(1, T2))

			// Simulate removing a keyword and removing token
			oldKws = kws
			kws = []string{"/x/", "/c/"}
			err = db.Keywords.DiffAndUpdate(ctx, oldKws, kws, []string{docs.VisTokenAll, T2}, tokensAll, 1)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/a/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/b/", nil, tokensAll)).To(matchPhotoCount(0))
			Expect(db.Keywords.Get(ctx, "/c/", nil, tokensAll)).To(matchPhotoCount(1))
			Expect(db.Keywords.Get(ctx, "/x/", nil, tokensAll)).To(matchPhotoCount(1))

		})

	})

	Describe("ListChildren", func() {
		It("Ordering of numeric and non-numeric keywords", func() {
			_, err := db.Keywords.Create(ctx, "key")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Keywords.Create(ctx, "101")
			Expect(err).ToNot(HaveOccurred())

			// List from beginning, limit 1
			kws, err := db.Keywords.ListChildren(ctx, &apis.GroupListParams{
				ID: "/", ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll},
				Limit: 1,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(kws).To(HaveLen(1))

			Expect(kws[0]).To(MatchGroupBase(Fields{
				"GroupBase.Display": PointTo(Equal("101")),
			}))

			// List relative to first
			kws, err = db.Keywords.ListChildren(ctx, &apis.GroupListParams{
				ID: "/", ProjectionKeys: []string{"display"}, VisibilityTokens: []string{docs.VisTokenAll},
				Limit: 1, RefID: "/101/",
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(kws).To(HaveLen(1))

			Expect(kws[0]).To(MatchGroupBase(Fields{
				"GroupBase.Display": PointTo(Equal("key")),
			}))
		})
	})

	Describe("Update", func() {
		It("Display path", func() {
			matchDisplayPath := func(dp string) OmegaMatcher {
				return MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal(dp))})
			}

			_, err := db.Keywords.Create(ctx, "mystery")
			Expect(err).ToNot(HaveOccurred())

			// Update keyword
			Expect(db.Keywords.Update(ctx, "/mystery/", apis.NewGroupUpdate().SetDisplay("Mystery?!"))).ToNot(HaveOccurred())

			Expect(db.Keywords.Get(ctx, "/mystery/", nil, tokensAll)).To(matchDisplayPath("Mystery?!"))

		})
	})

})
