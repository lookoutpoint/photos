// SPDX-License-Identifier: MIT

package keywords

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implements apis.Keywords
type apiImpl struct {
	mdb *mongo.Database
}

// NewAPI creates a new apiImpl object that will use the provided db
func NewAPI(mdb *mongo.Database) apis.Keywords {
	return &apiImpl{mdb: mdb}
}

const collectionName = "photos-keywords"

var keywordBSONKeys = utils.ExtractBSONKeys(docs.Keyword{})

func isValidName(name string) bool {
	return len(strings.TrimSpace(name)) > 0
}

// Slugify converts a keyword to a slug
func Slugify(kw string) string {
	return utils.SlugifyPathLower(utils.SlugifyTokenLower(kw))
}

// Collection returns the folder collection reference
func (api *apiImpl) Collection() *mongo.Collection {
	return api.mdb.Collection(collectionName)
}

// For GroupAPI
func (api *apiImpl) Separator() *string {
	// No separator
	return nil
}

// For GroupAPI
func (api *apiImpl) MakeProjectionFromKeys(keys []string) (interface{}, error) {
	return utils.MakeProjectionFromKeys(keywordBSONKeys, keys)
}

// For GroupAPI
func (api *apiImpl) DisplayToSortOrder(display string) interface{} {
	return groups.DefaultDisplayToSortOrder(display)
}

// Create the docs for the given keyword. Must not already exist.
// Returns the id.
func (api *apiImpl) Create(ctx context.Context, name string) (string, error) {
	l := log.WithFields(log.Fields{
		"func": "keywords.apiImpl.Create",
		"name": name,
	})

	if !isValidName(name) {
		l.Error("Invalid keyword")
		return "", apis.ErrInvalidArgs
	}

	return groups.Create(ctx, api, name, false, &groups.CreateOptions{LowerCase: true})
}

// Get returns information about one keyword id
func (api *apiImpl) Get(ctx context.Context, id string, projKeys []string, tokens []string) (*docs.Keyword, error) {
	l := log.WithFields(log.Fields{
		"func": "keywords.apiImpl.Get",
		"id":   id,
	})

	if !utils.IsValidSlugPath(id) {
		l.Error("Invalid id")
		return nil, apis.ErrInvalidID
	}

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(keywordBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": id}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	res := api.Collection().FindOne(ctx, filter, options.FindOne().SetProjection(proj))

	var doc docs.Keyword
	if err := res.Decode(&doc); err != nil {
		if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not find and decode keyword document")
		}
		return nil, err
	}

	return &doc, nil
}

// Get returns information about multiple keyword ids
func (api *apiImpl) GetMulti(ctx context.Context, ids []string, projKeys []string, tokens []string) ([]docs.Keyword, error) {
	l := log.WithFields(log.Fields{
		"func": "keywords.apiImpl.GetMulti",
		"ids":  ids,
	})

	// Projection
	var proj bson.M
	if projKeys != nil {
		var err error
		proj, err = utils.MakeProjectionFromKeys(keywordBSONKeys, projKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return nil, err
		}
	}

	// Filter
	filter := bson.M{"_id": bson.M{"$in": ids}}
	if err := apis.FilterByVisibilityTokens(filter, tokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens to filter")
		return nil, err
	}

	// Query
	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(proj))
	if err != nil {
		l.WithError(err).Error("Could not query keyword documents")
		return nil, err
	}

	var docs []docs.Keyword
	if err := cursor.All(ctx, &docs); err != nil {
		l.WithError(err).Error("Could not find and decode keyword documents")
		return nil, err
	}

	return docs, nil
}

func (api *apiImpl) GetOrCreate(ctx context.Context, name string, projKeys []string) (*docs.Keyword, error) {
	l := log.WithFields(log.Fields{
		"func": "keywords.apiImpl.GetOrCreate",
		"name": name,
	})

	if !isValidName(name) {
		l.Error("Invalid name")
		return nil, apis.ErrInvalidArgs
	}

	id := Slugify(name)
	tokensAll := []string{docs.VisTokenAll}

	// Try get
	if kw, err := api.Get(ctx, id, projKeys, tokensAll); err != mongo.ErrNoDocuments {
		if err != nil {
			l.WithError(err).Error("Could not get existing keyword")
		}
		return kw, err
	}

	// Create
	if _, err := api.Create(ctx, name); err != nil {
		l.WithError(err).Error("Could not create keyword")
		return nil, err
	}

	return api.Get(ctx, id, projKeys, tokensAll)
}

func (api *apiImpl) ListChildren(ctx context.Context, params *apis.GroupListParams) ([]docs.Keyword, error) {
	var results []docs.Keyword
	err := groups.ListChildren(ctx, api, params, &results)
	return results, err
}

func (api *apiImpl) DiffAndUpdate(ctx context.Context, oldIDs []string, newIDs []string, oldTokens []string, newTokens []string, count int) error {
	return groups.UpdateGroupPhotoCountsAndVisTokens(ctx, api, oldIDs, newIDs, oldTokens, newTokens, count)
}

func (api *apiImpl) Update(ctx context.Context, id string, update *apis.GroupUpdate) error {
	return groups.Update(ctx, api, id, update, nil)
}
