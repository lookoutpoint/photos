// SPDX-License-Identifier: MIT

package database_test

import (
	"context"
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/testutils"
)

func TestDatabase(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Database Suite")
}

var ctx context.Context
var db *database.Database

var _ = BeforeSuite(func() {
	// Force colors and no timestamps
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: false,
	})

	ctx = context.Background()
	mdb := testutils.NewTestDb()
	db = database.NewWithDb(mdb)

	// Log level (by default only panic-level messages are shown in test mode)
	log.SetLevel(log.PanicLevel)
	level, err := log.ParseLevel(os.Getenv("LOGLEVEL"))
	if err == nil {
		log.SetLevel(level)
	}
})

func clearTestDatabase() {
	testutils.ClearTestDatabase(ctx, db)
}
