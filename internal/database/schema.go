// SPDX-License-Identifier: MIT

package database

import (
	"context"
	"fmt"

	"gitlab.com/lookoutpoint/photos/internal/database/locations"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/photos"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Current schema version
// Increment before each push to the remote database
const latestSchemaVersion = 9

const schemaVersionID = "SCHEMA_VERSION"

type schemaVersionDoc struct {
	ID      string `bson:"_id"`
	Version int    `bson:"version"`
}

var schemaUpdaters = map[int]func(ctx context.Context, db *Database) error{
	1: updateSchemas1,
	2: updateSchemas2,
	// Reuse updateSchemas2 as schema 3 changes GroupBase.SortOrder from integer to string,
	// so can reuse the same update logic
	3: updateSchemas2,
	4: updateSchemas4,
	5: updateSchemas5,
	6: updateSchemas6,
	7: updateSchemas7,
	8: updateSchemas8,
	// Reuse updateSchemeas8 to update Park field using the updated logic
	9: updateSchemas8,
}

// GetSchemaVersion returns (cur, latest) schema version
func (db *Database) GetSchemaVersion(ctx context.Context) (int, int, error) {
	res := db.DbPropsCollection().FindOne(ctx, bson.M{"_id": schemaVersionID})

	var ver schemaVersionDoc
	if err := res.Decode(&ver); err == nil {
		return ver.Version, latestSchemaVersion, nil
	} else if err == mongo.ErrNoDocuments {
		// If there is no schema version in the database, interpret this as a fresh database
		// and so is already up-to-date with the latest schema
		return latestSchemaVersion, latestSchemaVersion, nil
	} else {
		return -1, -1, err
	}
}

// UpdateSchemas if necessary
func (db *Database) UpdateSchemas() error {
	return db.UpdateSchemasTo(latestSchemaVersion)
}

// UpdateSchemasTo to a particular version
func (db *Database) UpdateSchemasTo(version int) error {
	l := log.WithFields(log.Fields{
		"func":    "database.UpdateSchemasTo",
		"version": version,
	})

	ctx := context.Background()

	// Check if the schema version already exceeds the desired version.
	res := db.DbPropsCollection().FindOne(ctx, bson.M{
		"_id":     schemaVersionID,
		"version": bson.M{"$gte": version},
	})
	if err := res.Err(); err == nil {
		// Version already is >=, nothing to do
		return nil
	} else if err != mongo.ErrNoDocuments {
		l.WithError(err).Error("Could not check schema version")
		return err
	}

	// Upsert to initialize schema version if it does not exist yet.
	_, err := db.DbPropsCollection().UpdateOne(
		ctx,
		bson.M{"_id": schemaVersionID},
		bson.M{"$setOnInsert": bson.M{"version": 0}},
		options.Update().SetUpsert(true),
	)
	if err != nil {
		l.WithError(err).Error("Could not initialize schema version")
		return err
	}

	// Try to update the schema version. Use this as an atomic way to
	// detect if the schema version has changed and only one process will do the update.
	res = db.DbPropsCollection().FindOneAndUpdate(
		ctx,
		bson.M{"_id": schemaVersionID},
		bson.M{"$set": bson.M{"version": version}},
		options.FindOneAndUpdate().SetProjection(bson.M{"version": 1}),
	)

	prevSchemaVersion := schemaVersionDoc{Version: 0}
	if err := res.Decode(&prevSchemaVersion); err == nil {
		if prevSchemaVersion.Version == latestSchemaVersion {
			// Matches current version, nothing to do.
			return nil
		}
	} else {
		l.WithError(err).Error("Could not decode schema version document")
		return err
	}

	rollback := func(v int) {
		l.Infof("Rolling back schema version to %v", v)

		_, err := db.DbPropsCollection().UpdateOne(
			ctx,
			bson.M{"_id": schemaVersionID},
			bson.M{"$set": bson.M{"version": v}},
		)
		if err != nil {
			// This is probably very bad
			l.WithError(err).Errorf("Could not rollback schema version to %v", v)
		}
	}

	l.Infof("Previous schema version: %v", prevSchemaVersion.Version)

	// At this point, schema version does not match, so recreate schemaes.
	for v := prevSchemaVersion.Version + 1; v <= version; v++ {
		l.Infof("Applying schema creator version %v", v)

		creator, ok := schemaUpdaters[v]
		if !ok {
			err := fmt.Errorf("version %v does not have a updater function", v)
			l.WithError(err).Error("Missing creator function")
			rollback(v - 1)
			return err
		}

		// Run schema upgrade in transaction.
		_, _, err := utils.WrapAsTransaction(ctx, db.Mdb, l, func(ctx mongo.SessionContext) (interface{}, error) {
			err := creator(ctx, db)
			return nil, err
		})

		if err != nil {
			l.WithError(err).WithField("version", v).Error("Updater returned error")
			rollback(v - 1)
			return err
		}

		l.Infof("Successfully applied schema updater version %v", v)
	}

	l.Info("Successfully updated schemas")

	return nil
}

// Photo schema update: add tsSortKey field
func updateSchemas1(ctx context.Context, db *Database) error {
	return photos.AddMissingTsSortKeys(ctx, db.Photos)
}

// Group schema update: add sortOrder, inferred from last segment of display name
func updateSchemas2(ctx context.Context, db *Database) error {
	l := log.WithField("func", "updateSchemas2")

	sep := "/"
	if err := groups.AddSortOrder(ctx, db.Folders.Collection(), &sep); err != nil {
		l.WithError(err).Error("Could not add sort order to folders")
		return err
	}

	if err := groups.AddSortOrder(ctx, db.Keywords.Collection(), nil); err != nil {
		l.WithError(err).Error("Could not add sort order to keywords")
		return err
	}

	sep = ":"
	if err := groups.AddSortOrder(ctx, db.Categories.Collection(), &sep); err != nil {
		l.WithError(err).Error("Could not add sort order to cateogories")
		return err
	}

	sep = ","
	if err := groups.AddSortOrder(ctx, db.Locations.Collection(), &sep); err != nil {
		l.WithError(err).Error("Could not add sort order to locations")
		return err
	}

	sep = "/"
	if err := groups.AddSortOrder(ctx, db.Dates.Collection(), &sep); err != nil {
		l.WithError(err).Error("Could not add sort order to dates")
		return err
	}

	// Intentionally skipping timeline groups

	return nil
}

// Events schema update
func updateSchemas4(ctx context.Context, db *Database) error {
	return db.Events.ConvertDateToNewFormat(ctx)
}

// Events schema update
func updateSchemas5(ctx context.Context, db *Database) error {
	return db.Events.ConvertYearMonthDayToDate(ctx)
}

// Add photo.Locations field
func updateSchemas6(ctx context.Context, db *Database) error {
	return photos.AddLocationsField(ctx, db.Photos)
}

// Remove photo.Location field
func updateSchemas7(ctx context.Context, db *Database) error {
	return photos.RemoveLocationField(ctx, db.Photos)
}

// Add location.Park field
func updateSchemas8(ctx context.Context, db *Database) error {
	return locations.AddParkField(ctx, db.Locations)
}
