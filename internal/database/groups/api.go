// SPDX-License-Identifier: MIT

package groups

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"go.mongodb.org/mongo-driver/mongo"
)

// GroupBasicAPI define general methods that all groups must support
type GroupBasicAPI interface {
	Collection() *mongo.Collection
	Separator() *string
	MakeProjectionFromKeys(keys []string) (interface{}, error)

	// Converts a display string to a sort order.
	DisplayToSortOrder(display string) interface{}
}

// Default implementation for DisplayToSortOrder. If the display string is an integer,
// use the integer value as the sort order. Otherwise just use the lowercase string (case-insensitive sort order).
func DefaultDisplayToSortOrder(display string) interface{} {
	display = strings.TrimSpace(display)
	if i, err := strconv.Atoi(display); err == nil {
		return i
	} else {
		return strings.ToLower(display)
	}
}

type GroupSelfVisTokensInterface interface {
	GetID() string
	GetSelfVisTokens() []string
}

type GroupSelfVisTokensAPI[G GroupSelfVisTokensInterface] interface {
	GroupBasicAPI
	ListChildren(ctx context.Context, params *apis.GroupListParams) ([]G, error)
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*G, error)
	AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
	DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
}
