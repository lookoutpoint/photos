// SPDX-License-Identifier: MIT

package groups

import (
	"context"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// AddSortOrder adds sort order to all groups in the given collection
// OUTDATED: should use GroupAPI.DisplayToSortOrder
func AddSortOrder(ctx context.Context, coll *mongo.Collection, displaySep *string) error {
	l := log.WithFields(log.Fields{
		"func":       "groups.AddSortOrder",
		"collection": coll.Name(),
	})

	// Find all groups
	cursor, err := coll.Find(ctx, bson.M{}, options.Find().SetProjection(bson.M{"display": 1}))
	if err != nil {
		l.WithError(err).Error("Could not query groups")
		return err
	}
	defer cursor.Close(ctx)

	// Update each group by inferring sort order. Batch updates into bulk writes.
	var models []mongo.WriteModel
	writeModels := func() error {
		res, err := coll.BulkWrite(ctx, models, options.BulkWrite().SetOrdered(false))
		if err != nil {
			l.WithError(err).Error("Could not do bulk write")
			return err
		}

		l.Infof("Updated %v groups", res.ModifiedCount)
		models = nil
		return nil
	}

	for cursor.Next(ctx) {
		var group docs.GroupBase
		if err := cursor.Decode(&group); err != nil {
			l.WithError(err).Error("Could not decode group")
			return err
		}

		// Infer sort order. Use same logic as in group creation.
		display := *group.Display
		if displaySep != nil {
			idx := strings.LastIndex(display, *displaySep)
			if idx >= 0 {
				display = display[idx+1:]
			}
		}

		sortOrder := DefaultDisplayToSortOrder(display)

		models = append(models, mongo.NewUpdateOneModel().
			SetFilter(bson.M{"_id": *group.ID}).
			SetUpdate(bson.M{
				"$set": bson.M{
					"sortOrder": sortOrder,
				},
			}),
		)

		if len(models) == 100 {
			if err := writeModels(); err != nil {
				return err
			}
		}
	}

	if len(models) > 0 {
		if err := writeModels(); err != nil {
			return err
		}
	}

	return nil
}
