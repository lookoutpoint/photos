// SPDX-License-Identifier: MIT

package groups

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func newBase(api GroupBasicAPI, id string, displayParts []string, displaySep string) docs.GroupBase {
	display := strings.Join(displayParts, displaySep)
	ts := time.Now()
	depth := utils.SlugPathDepth(id)

	// Determine initial sort order value. The default case is to use the string value of the display part.
	// However if the last part of the display string is an integer, use the numerical
	// value as the sort order.
	var sortOrder interface{}
	if len(displayParts) >= 1 {
		sortOrder = api.DisplayToSortOrder(displayParts[len(displayParts)-1])
	} else {
		sortOrder = ""
	}

	return docs.GroupBase{
		ID:                 &id,
		Display:            &display,
		Timestamp:          &ts,
		Depth:              &depth,
		SortOrder:          &sortOrder,
		VisTokens:          []string{},
		VisTokenCounts:     map[string]int{},
		DescVisTokenCounts: map[string]int{},
	}
}

// CreateOptions allows for extensions for how Create works
type CreateOptions struct {
	LowerCase      bool
	SlugifyPath    func(displayParts ...string) string
	Model          func(base docs.GroupBase) interface{}
	PostInsert     func(ctx context.Context, model interface{}) error
	ExistingParent func(ctx context.Context, id string) error
}

func splitAndCollapse(display string, sep *string) []string {
	var rawDisplayParts []string

	if sep != nil {
		// Split the display into its parts based on the group's separator.
		rawDisplayParts = strings.Split(display, *sep)
	} else {
		rawDisplayParts = []string{display}
	}

	// Collapse consecutive separators into one. Equivalently, remove parts that are the empty string
	// (except for the first one, which is a placeholder for the root).
	displayParts := []string{}
	for i, part := range rawDisplayParts {
		if i == 0 || len(part) > 0 {
			displayParts = append(displayParts, part)
		}
	}

	return displayParts
}

// Create creates the any group for the given path. Path must not already exist.
// Returns the id.
func Create(ctx context.Context, api GroupBasicAPI, display string, upsert bool, opts *CreateOptions) (string, error) {
	l := log.WithFields(log.Fields{
		"func":           "groups.Create",
		"collectionName": api.Collection().Name(),
		"display":        display,
		"upsert":         upsert,
	})

	sep := api.Separator()

	// Split the display into its parts based on the group's separator.
	displayParts := splitAndCollapse(display, sep)

	// If the first display part is not an empty string, add one. This is a placeholder for the root.
	if displayParts[0] != "" {
		displayParts = append([]string{""}, displayParts...)
	}

	// Turn the display parts into an id.
	var id string
	if opts != nil && opts.SlugifyPath != nil {
		id = opts.SlugifyPath(displayParts...)
	} else if opts != nil && opts.LowerCase {
		id = utils.SlugifyPathLower(displayParts...)
	} else {
		id = utils.SlugifyPath(displayParts...)
	}

	// If upserting, if the id already exists, don't do anything more.
	if upsert {
		res := api.Collection().FindOne(ctx, bson.M{"_id": id}, options.FindOne().SetProjection(bson.M{"_id": 1}))
		if err := res.Err(); err == nil {
			// Group already exists. Call optional callback on existing parent.
			if opts != nil && opts.ExistingParent != nil {
				if err := opts.ExistingParent(ctx, id); err != nil {
					l.WithError(err).Error("Error while processing existing parent")
					return "", err
				}
			}
			return id, nil
		} else if err != mongo.ErrNoDocuments {
			l.WithError(err).Error("Could not check if group already exists")
			return "", err
		}
	}

	// Create the entire group chain (including parents) in a transaction.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.Collection().Database(), l, func(ctx mongo.SessionContext) (interface{}, error) {
		// If there is no separator, then there is only one display part (excluding the root).
		// For the joins below, just use the empty string in such cases.
		var joinSep string
		if sep != nil {
			joinSep = *sep
		}

		// Create parent first.
		if len(displayParts) > 1 {
			// Use upsert as parent may already exist.
			if _, err := Create(ctx, api, strings.Join(displayParts[:len(displayParts)-1], joinSep), true, opts); err != nil {
				l.WithError(err).Warn("Could not create parent group")
				return nil, err
			}
		}

		// Build group model. Apply extension model if there is any.
		var group interface{}
		group = newBase(api, id, displayParts[1:], joinSep) // don't use root part in display string
		if opts != nil && opts.Model != nil {
			group = opts.Model(group.(docs.GroupBase))
		}

		// Insert this group.
		if _, err := api.Collection().InsertOne(ctx, group); err != nil {
			l.WithError(err).Warn("Could not insert new group")
			return nil, err
		}

		// Post-insert processing
		// Update "photo" counts to add an "all" token for the self-group. This token will never be removed until this group is deleted.
		if err := UpdateGroupPhotoCountsAndVisTokens(ctx, api, nil, []string{id}, nil, []string{docs.VisTokenAll}, 1); err != nil {
			l.WithError(err).Warn("Could not add all vis token")
			return nil, err
		}

		// Custom post-insertion processing
		if opts != nil && opts.PostInsert != nil {
			if err := opts.PostInsert(ctx, group); err != nil {
				l.WithError(err).Warn("Error during post-insert")
				return nil, err
			}
		}

		return nil, nil
	})
	if err != nil {
		if transComplete {
			l.WithError(err).Error("Could not complete transaction")
		}
		return "", err
	}

	l.Infof("Created id = '%v'", id)

	return id, nil
}

// UpdateGroupPhotoCountsAndVisTokens updates a set of group documents based on the
// diff of ids. Also updates the visibility token photo counts in each affected
// group document.
func UpdateGroupPhotoCountsAndVisTokens(
	ctx context.Context,
	api GroupBasicAPI,
	oldIDs []string, newIDs []string,
	oldTokens []string, newTokens []string,
	count int,
) error {
	l := log.WithFields(log.Fields{
		"func":           "docs.UpdateGroupPhotoCountsAndVisTokens",
		"collectionName": api.Collection().Name(),
		"oldIDs":         oldIDs,
		"newIDs":         newIDs,
	})

	if count == 0 {
		return nil
	}

	// Sanity checks
	if (oldIDs == nil) != (oldTokens == nil) {
		err := errors.New("inconsistent old ids and tokens")
		l.WithError(err).Errorf("Umm... this shouldn't happen; oldIDs: %v oldTokens: %v", oldIDs == nil, oldTokens == nil)
		return err
	}

	if (newIDs == nil) != (newTokens == nil) {
		err := errors.New("inconsistent new ids and tokens")
		l.WithError(err).Errorf("Umm... this shouldn't happen; newIDs: %v newTokens: %v", newIDs == nil, newTokens == nil)
		return err
	}

	// ID diff
	idDelta := map[string]int{}
	for _, id := range oldIDs {
		if !utils.IsValidSlugPath(id) {
			l.WithError(utils.ErrInvalidSlugPath).WithField("id", id).Error("Invalid id")
			return utils.ErrInvalidSlugPath
		}
		idDelta[id]--
	}
	for _, id := range newIDs {
		if !utils.IsValidSlugPath(id) {
			l.WithError(utils.ErrInvalidSlugPath).WithField("id", id).Error("Invalid id")
			return utils.ErrInvalidSlugPath
		}
		idDelta[id]++
	}

	// Vis token diff
	visTokenDelta := map[string]int{}
	for _, token := range oldTokens {
		if !docs.IsValidVisTokenID(token) {
			l.WithError(utils.ErrInvalidSlugPath).WithField("token", token).Error("Invalid token")
			return utils.ErrInvalidSlugPath
		}
		visTokenDelta[token]--
	}
	for _, token := range newTokens {
		if !docs.IsValidVisTokenID(token) {
			l.WithError(utils.ErrInvalidSlugPath).WithField("token", token).Error("Invalid token")
			return utils.ErrInvalidSlugPath
		}
		visTokenDelta[token]++
	}

	visTokensToAdd := []string{}
	visTokensToDelete := []string{}

	for token, delta := range visTokenDelta {
		if delta == -1 {
			visTokensToDelete = append(visTokensToDelete, token)
		} else if delta == 1 {
			visTokensToAdd = append(visTokensToAdd, token)
		} else if delta != 0 {
			l.WithField("delta", delta).Warn("Invalid delta value for token - skipping")
		}
	}

	// Do all updates in a transaction as the count updates should be all-or-nothing.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.Collection().Database(), l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Accumulate update models: provided models and for additions
		var updateModels []mongo.WriteModel

		// Iterate over id diff and update each id as appropriate
		for id, delta := range idDelta {
			if delta == -1 {
				if len(oldTokens) > 0 {
					// Delete counts for all old vis tokens
					err := deleteFromPhotoGroup(ctx, api.Collection(), id, oldTokens, count, false)
					if err != nil {
						l.WithError(err).WithField("id", id).Warn("Could not delete old visibility tokens from group")
						return nil, err
					}
					l.Tracef("Delete count (%v) for %v for tokens %v", count, id, oldTokens)
				}
			} else if delta == 1 {
				if len(newTokens) > 0 {
					// Increment counts for all new vis tokens
					updateModels = append(updateModels, modelForAddToPhotoGroup(id, newTokens, count, false)...)
					l.Tracef("Add count (%v) for %v for tokens %v", count, id, newTokens)
				}
			} else if delta == 0 {
				if len(visTokensToAdd) > 0 {
					// Increment counts for net new vis tokens
					updateModels = append(updateModels, modelForAddToPhotoGroup(id, visTokensToAdd, count, false)...)
					l.Tracef("Add count (%v) for %v for tokens %v", count, id, visTokensToAdd)
				}

				if len(visTokensToDelete) > 0 {
					// Delete counts for net old vis tokens
					err := deleteFromPhotoGroup(ctx, api.Collection(), id, visTokensToDelete, count, false)
					if err != nil {
						l.WithError(err).WithField("id", id).Warn("Could not delete net old visibility tokens from group")
						return nil, err
					}
					l.Tracef("Delete count (%v) for %v for tokens %v", count, id, visTokensToDelete)
				}
			} else {
				l.WithField("delta", delta).WithField("id", id).Warn("Invalid delta value - skipping")
			}
		}

		if len(updateModels) > 0 {
			// Apply update models in bulk write.
			res, err := api.Collection().BulkWrite(ctx, updateModels, options.BulkWrite().SetOrdered(true))
			if err != nil {
				l.WithError(err).Warn("Could not do bulk write to update models")
				return nil, err
			}

			l.Infof("Update models matched %v, modified %v, upserted %v", res.MatchedCount, res.ModifiedCount, res.UpsertedCount)
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}

func modelForAddToPhotoGroup(slugPath string, tokens []string, count int, descendant bool) []mongo.WriteModel {
	if len(tokens) == 0 {
		return nil
	}

	// Build update doc
	inc := bson.M{}
	for _, token := range tokens {
		if !descendant {
			inc["visTokenCounts."+string(token)] = count
		}
		inc["descVisTokenCounts."+string(token)] = count
	}

	update := bson.M{
		"$currentDate": bson.M{"timestamp": true},
		"$inc":         inc,
		"$addToSet":    bson.M{"visTokens": bson.M{"$each": tokens}},
	}

	// Build filter
	filter := bson.M{"_id": slugPath}

	// Make update model
	models := []mongo.WriteModel{
		mongo.NewUpdateOneModel().SetFilter(filter).SetUpdate(update),
	}

	// Create model to update parent id, if there is a parent
	sep := strings.LastIndex(slugPath[:len(slugPath)-1], "/")
	if sep >= 0 {
		models = append(models, modelForAddToPhotoGroup(slugPath[:sep+1], tokens, count, true)...)
	}

	return models
}

func deleteFromPhotoGroup(ctx context.Context, collection *mongo.Collection, slugPath string, tokens []string, count int, descendant bool) error {
	l := log.WithFields(log.Fields{
		"func":           "docs.deleteFromPhotoGroup",
		"collectionName": collection.Name(),
		"slugPath":       slugPath,
	})

	if len(tokens) == 0 {
		return nil
	}

	// Build update doc
	inc := bson.M{}
	for _, token := range tokens {
		if !descendant {
			inc["visTokenCounts."+string(token)] = -count
		}
		inc["descVisTokenCounts."+string(token)] = -count
	}

	update := bson.M{
		"$inc": inc,
	}

	options := options.FindOneAndUpdate().
		SetReturnDocument(options.After).
		SetProjection(bson.M{"visTokenCounts": 1, "descVisTokenCounts": 1})

	// Update and get back updated photo counts
	res := collection.FindOneAndUpdate(ctx, bson.M{"_id": slugPath}, update, options)

	var doc docs.GroupBase
	if err := res.Decode(&doc); err != nil {
		l.WithError(err).Warn("Could not decode updated group document")
		return err
	}

	// Check if the photo count for any tokens are zero, in which case that token needs to be removed from the group
	// visibility tokens.
	for _, token := range tokens {
		if doc.DescVisTokenCounts[token] < 0 || doc.VisTokenCounts[token] < 0 {
			l.Warn("Token has negative photo count")
			l.WithField("token", token).Trace("Token has negative photo count")
		}

		if doc.DescVisTokenCounts[token] == 0 {
			l.Tracef("Full delete of token %v", token)

			// Full delete (this implies that VisTokenPhotoCounts[token] == 0 too)
			filter := bson.M{
				"_id": slugPath,
				// To protect against race conditions (e.g. the photo count being changed between the previous update and this delete),
				// filter for the zero value
				"descVisTokenCounts." + string(token): 0,
			}

			update := bson.M{
				"$pull": bson.M{"visTokens": token},
				"$unset": bson.M{
					"descVisTokenCounts." + string(token): 1,
					"visTokenCounts." + string(token):     1,
				},
			}

			_, err := collection.UpdateOne(ctx, filter, update)
			if err != nil {
				l.WithError(err).Error("Could not perform full token delete")
				return err
			}
		} else if doc.VisTokenCounts[token] == 0 {
			l.Tracef("Partial delete of token %v", token)

			// Partial delete. Only remove the visTokenCounts field.
			filter := bson.M{
				"_id": slugPath,
				// To protect against race conditions (e.g. the photo count being changed between the previous update and this delete),
				// filter for the zero value
				"visTokenCounts." + string(token): 0,
			}

			update := bson.M{
				"$unset": bson.M{
					"visTokenCounts." + string(token): 1,
				},
			}

			_, err := collection.UpdateOne(ctx, filter, update)
			if err != nil {
				l.WithError(err).Error("Could not perform partial token delete")
				return err
			}
		}
	}

	// Apply delete to parent, if there is a parent
	sep := strings.LastIndex(slugPath[:len(slugPath)-1], "/")
	if sep >= 0 {
		return deleteFromPhotoGroup(ctx, collection, slugPath[:sep+1], tokens, count, true)
	}

	return nil
}

// ListChildren returns an array of documents for each child of the given id. Children are sorted by id.
// If no projection keys are provided (nil), the default projection is just the folder id.
func ListChildren(ctx context.Context, api GroupBasicAPI, params *apis.GroupListParams, results interface{}) error {
	l := log.WithFields(log.Fields{
		"func":           "groups.ListChildren",
		"collectionName": api.Collection().Name(),
		"params":         *params,
	})

	// Param defaults and validation.
	params.AssignDefaults()
	if err := params.Validate(); err != nil {
		l.WithError(err).Error("Invalid list params")
		return apis.ErrInvalidListParams
	}

	// Depth filter
	depth := utils.SlugPathDepth(params.ID)

	var depthFilter interface{}
	if params.MaxRelDepth == -1 {
		// Unlimited depth
		depthFilter = bson.M{"$gte": depth + 1}
	} else if params.MaxRelDepth == 1 {
		// One level of depth, so filter for the next depth level
		depthFilter = depth + 1
	} else {
		// Depth range.
		depthFilter = bson.M{
			"$gte": depth + 1,
			"$lte": depth + params.MaxRelDepth,
		}
	}

	// Query for all groups with the id prefix and a depth one more than the id
	filter := bson.M{
		"_id": bson.M{
			"$regex": "^" + params.ID,
		},
		"depth": depthFilter,
	}

	// Apply visibility tokens to filter.
	if err := apis.FilterByVisibilityTokens(filter, params.VisibilityTokens); err != nil {
		l.WithError(err).Error("Could not apply visibility tokens")
		return err
	}

	// Build projection
	var proj interface{}
	if params.ProjectionKeys != nil {
		var err error
		proj, err = api.MakeProjectionFromKeys(params.ProjectionKeys)
		if err != nil {
			l.WithError(err).Error("Could not make projection from keys")
			return err
		}
	} else {
		// Default projection
		proj = bson.M{"_id": 1}
	}

	// Sort direction
	sortDir := 1
	if params.RefOp == list.RefOpLt || params.RefOp == list.RefOpLte {
		sortDir = -1
	}

	coll := api.Collection()
	if len(params.CustomView) > 0 {
		l.Tracef("Using custom view %s", params.CustomView)
		coll = api.Collection().Database().Collection(params.CustomView)
	}

	// Apply reference id and operator if specified.
	if len(params.RefID) > 0 {
		refOp := "$" + string(params.RefOp)

		// For reference id, also use its sort order
		res := coll.FindOne(ctx, bson.M{"_id": params.RefID}, options.FindOne().SetProjection(bson.M{"depth": 1, "sortOrder": 1}))

		var refGroup docs.GroupBase
		if err := res.Decode(&refGroup); err != nil {
			l.WithError(err).Error("Could not query reference group")
			return err
		}

		// Per the sort order, sort by depth, then sortOrder and then by id.
		sortOrderOp := "$gt"
		if sortDir == -1 {
			sortOrderOp = "$lt"
		}

		_, refSortOrderIsString := (*refGroup.SortOrder).(string)

		var sortOrderFilters bson.A

		if !params.SortIgnoreDepth {
			sortOrderFilters = bson.A{
				// depth
				bson.M{
					"depth": bson.M{"$gt": *refGroup.Depth},
				},
				// depth, sortOrder
				bson.M{
					"depth":     *refGroup.Depth,
					"sortOrder": bson.M{sortOrderOp: *refGroup.SortOrder},
				},
				// depth, sortOrder, id
				bson.M{
					"depth":     *refGroup.Depth,
					"sortOrder": *refGroup.SortOrder,
					"_id":       bson.M{refOp: params.RefID},
				},
			}
		} else {
			sortOrderFilters = bson.A{
				// sortOrder
				bson.M{
					"sortOrder": bson.M{sortOrderOp: *refGroup.SortOrder},
				},
				// sortOrder, id
				bson.M{
					"sortOrder": *refGroup.SortOrder,
					"_id":       bson.M{refOp: params.RefID},
				},
			}
		}

		// MongoDB type order: number, string
		if sortDir == 1 && !refSortOrderIsString {
			// Sort order comparison is against number, so also accept strings (which come after numbers)
			filter := bson.M{
				"sortOrder": bson.M{"$type": "string"},
			}
			if !params.SortIgnoreDepth {
				filter["depth"] = *refGroup.Depth
			}
			sortOrderFilters = append(sortOrderFilters, filter)
		} else if sortDir == -1 && refSortOrderIsString {
			// Sort order comparison is against string, so also accept numbers (which come before strings)
			filter := bson.M{
				"sortOrder": bson.M{"$type": "number"},
			}
			if !params.SortIgnoreDepth {
				filter["depth"] = *refGroup.Depth
			}
			sortOrderFilters = append(sortOrderFilters, filter)
		}

		// Apply custom sort keys, if any. Do this in reverse order because the first key should have the most precendence
		// and the loop logic puts the last key processed as the most precedence.
		for i := range params.CustomSortKeys {
			sortKey := params.CustomSortKeys[len(params.CustomSortKeys)-1-i]

			// First add a clause to compare against custom sort key.

			// Get custom sort key value. Use aggregation pipeline to rename field for easy access.
			pipeline := bson.A{
				bson.M{"$match": bson.M{"_id": params.RefID}},
				bson.M{"$project": bson.M{
					"_id":   0,
					"value": "$" + sortKey,
				}},
			}

			cursor, err := coll.Aggregate(ctx, pipeline)
			if err != nil {
				l.WithError(err).Error("Could not get run aggregation pipeline for custom sort key value")
				return err
			}

			var result []struct {
				Value interface{} `bson:"value"`
			}
			if err := cursor.All(ctx, &result); err != nil {
				l.WithError(err).Error("Could not get custom sort key value")
				return err
			}

			// Extract the value.
			// If the value is an array, just use the first value. Note that this essentially chooses an
			// ordering as there is no ordering defined if the custom key is a multi-value array.
			var customSortKeyValue interface{}
			if len(result) == 1 {
				r := reflect.ValueOf(result[0].Value)
				switch r.Kind() {
				case reflect.Slice:
					customSortKeyValue = r.Index(0).Interface()

				case reflect.Invalid:
					customSortKeyValue = nil

				default:
					customSortKeyValue = result[0].Value
				}
			}

			// For each existing sort order filter clause, add a constraint
			// that the custom sort key is equal. This also applies to previously added
			// custom keys, which is why we go in reverse order to get the right
			// precedence.
			for i := range sortOrderFilters {
				clause := sortOrderFilters[i].(bson.M)
				if customSortKeyValue == nil {
					clause[sortKey] = bson.M{"$exists": false}
				} else {
					clause[sortKey] = customSortKeyValue
				}
			}

			// Add sort order filter clause to filter by custom sort key.
			customFilter := bson.M{}
			if sortDir == 1 {
				if customSortKeyValue == nil {
					customFilter[sortKey] = bson.M{"$exists": true}
				} else {
					customFilter[sortKey] = bson.M{sortOrderOp: customSortKeyValue}
				}
			} else {
				customFilter[sortKey] = bson.M{sortOrderOp: customSortKeyValue}
			}
			sortOrderFilters = append(sortOrderFilters, customFilter)
		}

		filter = bson.M{
			"$and": bson.A{
				// Use existing filter
				filter,
				bson.M{
					"$or": sortOrderFilters,
				},
			},
		}
	}

	// Build FindOptions
	options := options.Find().SetProjection(proj)

	// Sort by custom sort keys (if any), depth, sortOrder and then id.
	sortOrder := bson.D{}

	for _, sortKey := range params.CustomSortKeys {
		sortOrder = append(sortOrder, bson.E{Key: sortKey, Value: sortDir})
	}
	if !params.SortIgnoreDepth {
		sortOrder = append(sortOrder, bson.E{Key: "depth", Value: 1})
	}
	sortOrder = append(sortOrder,
		bson.E{Key: "sortOrder", Value: sortDir},
		bson.E{Key: "_id", Value: sortDir},
	)

	options.SetSort(sortOrder)

	if params.Limit > 0 {
		options.SetLimit(params.Limit)
	}

	// Add custom filter, if any.
	if len(params.CustomFilter) > 0 {
		filter = bson.M{"$and": bson.A{
			params.CustomFilter,
			filter,
		}}
	}

	// Query
	cursor, err := coll.Find(ctx, filter, options)
	if err != nil {
		l.WithError(err).Error("Failed to query child documents")
		return err
	}

	// Fetch all results
	if err := cursor.All(ctx, results); err != nil {
		l.WithError(err).Error("Failed to fetch child documents")
		return err
	}

	return nil
}

// UpdateOptions allows for extensions for how Update works
type UpdateOptions struct {
	PreUpdate func(ctx context.Context, id string, toSet *bson.M, toUnset *bson.M) error
}

// Update group
func Update(ctx context.Context, api GroupBasicAPI, id string, update *apis.GroupUpdate, opts *UpdateOptions) error {
	if update == nil {
		return nil
	}

	l := log.WithFields(log.Fields{
		"func":           "groups.Update",
		"collectionName": api.Collection().Name(),
		"id":             id,
	})

	sep := api.Separator()

	// Update target group.
	updateSet := bson.M{}
	updateUnset := bson.M{}

	// Display
	var oldDisplay string
	var newDisplay string
	if update.Display != nil {
		if id == "/" {
			l.Error("Cannot update display path of root")
			return apis.ErrInvalidID
		}

		// Validate that the new display path does not have any invalid characters (i.e. separator).
		if sep != nil && strings.Contains(*update.Display, *sep) {
			l.Error("Update display string has invalid characters")
			return apis.ErrInvalidArgs
		}

		// Get current display path.
		res := api.Collection().FindOne(ctx, bson.M{"_id": id}, options.FindOne().SetProjection(bson.M{"display": 1}))

		var doc docs.GroupBase
		if err := res.Decode(&doc); err != nil {
			l.WithError(err).Error("Could not get group document")
			return err
		}

		oldDisplay = *doc.Display

		// Build new display path by replacing the last segment.
		newDisplay = *update.Display
		if sep != nil {
			sepIdx := strings.LastIndex(oldDisplay, *sep)
			if sepIdx >= 0 {
				newDisplay = oldDisplay[:sepIdx+1] + *update.Display
			}
		}

		updateSet["display"] = newDisplay

		// Rederive the sort order
		updateSet["sortOrder"] = api.DisplayToSortOrder(*update.Display)
	}

	// Sort order
	if update.SortOrder != nil {
		sortOrder := *update.SortOrder

		// Must be int or string
		if _, ok := sortOrder.(int); ok {
		} else if _, ok := sortOrder.(string); ok {
		} else if sortOrderF, ok := sortOrder.(float64); ok {
			sortOrder = int(sortOrderF)
		} else {
			err := fmt.Errorf("sort order is not int or string (%t)", sortOrder)
			l.WithError(err).Error("Bad sort order")
			return err
		}

		updateSet["sortOrder"] = sortOrder
	}

	// Meta description
	if update.MetaDescription != nil {
		desc := strings.TrimSpace(*update.MetaDescription)

		if len(desc) == 0 {
			updateUnset["metaDescription"] = 1
		} else {
			updateSet["metaDescription"] = desc
		}
	}

	// Rich text
	if update.RichText != nil {
		text := *update.RichText

		if len(text) == 0 {
			updateUnset["richText"] = 1
		} else {
			updateSet["richText"] = text
		}
	}

	if len(updateSet)+len(updateUnset) == 0 {
		// Nothing to do.
		return nil
	}

	// Apply custom updates if specified.
	if opts != nil && opts.PreUpdate != nil {
		if err := opts.PreUpdate(ctx, id, &updateSet, &updateUnset); err != nil {
			l.WithError(err).Error("Pre-update hook failed")
			return err
		}
	}

	updateDoc := bson.M{}
	if len(updateSet) != 0 {
		updateDoc["$set"] = updateSet
	}
	if len(updateUnset) != 0 {
		updateDoc["$unset"] = updateUnset
	}

	res, err := api.Collection().UpdateOne(ctx, bson.M{"_id": id}, updateDoc)
	if err != nil {
		l.WithError(err).Error("Could not update group")
		return err
	} else if res.MatchedCount == 0 {
		l.Error("Could not find document to update")
		return fmt.Errorf("no document to update")
	} else if res.ModifiedCount == 0 {
		// Nothing else to do.
		return nil
	}

	// Group has been updated. If there is a new display path, update all child groups in the folder tree.
	if update.Display == nil {
		return nil
	}

	return updateChildrenDisplay(ctx, api, id, oldDisplay, newDisplay, opts)
}

func updateChildrenDisplay(ctx context.Context, api GroupBasicAPI, id string, oldDisplay string, newDisplay string, opts *UpdateOptions) error {
	l := log.WithFields(log.Fields{
		"func":           "groups.updateChildrenDisplay",
		"collectionName": api.Collection().Name(),
		"id":             id,
	})

	// Find all child groups (full recursive search).
	filter := bson.M{
		"_id":   bson.M{"$regex": "^" + id},
		"depth": bson.M{"$gt": utils.SlugPathDepth(id)},
	}

	cursor, err := api.Collection().Find(ctx, filter, options.Find().SetProjection(bson.M{"display": 1}))
	if err != nil {
		l.WithError(err).Error("Could not get child groups")
		return err
	}
	defer cursor.Close(ctx)

	// Iterate over each group, update display path. Accumulate update models for a bulk write.
	oldDisplayLen := len(oldDisplay)
	var models []mongo.WriteModel

	for cursor.Next(ctx) {
		var child docs.GroupBase
		if err := cursor.Decode(&child); err != nil {
			l.WithError(err).Error("Could not decode sub-folder")
			return err
		}

		// Assume that the group's display path starts with oldDisplay. Replace it with newDisplay
		childNewDisplay := newDisplay + (*child.Display)[oldDisplayLen:]
		toSet := bson.M{"display": childNewDisplay}
		toUnset := bson.M{}

		// Apply hooks if any.
		if opts != nil && opts.PreUpdate != nil {
			if err := opts.PreUpdate(ctx, *child.ID, &toSet, &toUnset); err != nil {
				l.WithError(err).Error("Pre-update hook failed")
				return err
			}
		}

		models = append(models, mongo.NewUpdateOneModel().
			SetFilter(bson.M{"_id": *child.ID}).
			SetUpdate(bson.M{"$set": toSet, "$unset": toUnset}),
		)
	}
	if err := cursor.Err(); err != nil {
		l.WithError(err).Error("Could not iterate over child groups")
		return err
	}

	// Bulk write.
	if len(models) > 0 {
		res, err := api.Collection().BulkWrite(ctx, models, options.BulkWrite().SetOrdered(false))
		if err != nil {
			l.WithError(err).Error("Could not do bulk write")
			return err
		}

		l.Infof("Updated %v children", res.ModifiedCount)
	}

	return nil
}

// Find empty groups, which are groups with no descendants except itself.
func FindEmptyGroups(ctx context.Context, coll *mongo.Collection) ([]string, error) {
	l := log.WithFields(log.Fields{
		"func":           "groups.FindEmptyGroups",
		"collectionName": coll.Name(),
	})

	cursor, err := coll.Find(ctx, bson.M{"descVisTokenCounts.*": 1}, options.Find().SetProjection(bson.M{"_id": 1}))
	if err != nil {
		l.WithError(err).Error("Could not find empty groups")
		return nil, err
	}

	var docs []struct {
		ID string `bson:"_id"`
	}
	if err = cursor.All(ctx, &docs); err != nil {
		l.WithError(err).Error("Could not fetch results")
		return nil, err
	}

	ids := []string{}
	for _, doc := range docs {
		ids = append(ids, doc.ID)
	}

	return ids, nil
}

func deleteEmptyGroup(ctx context.Context, coll *mongo.Collection, id string) error {
	l := log.WithFields(log.Fields{
		"func":           "groups.deleteEmptyGroup",
		"collectionName": coll.Name(),
		"id":             id,
	})

	// Wrap everything in a transaction.
	_, transComplete, err := utils.WrapAsTransaction(ctx, coll.Database(), l, func(ctx mongo.SessionContext) (interface{}, error) {
		res := coll.FindOneAndDelete(ctx, bson.M{"_id": id, "descVisTokenCounts.*": 1},
			options.FindOneAndDelete().SetProjection(bson.M{"descVisTokenCounts": 1}))
		if err := res.Err(); err != nil {
			l.WithError(err).Error("Could not delete empty group")
			return nil, err
		}

		var doc docs.GroupBase
		if err := res.Decode(&doc); err != nil {
			l.Error("Could not decode empty group")
			return nil, err
		}

		// Now delete from parent group.
		sep := strings.LastIndex(id[:len(id)-1], "/")
		if sep >= 0 {
			// Gather all tokens applied on the group.
			tokens := make([]string, 0, len(doc.DescVisTokenCounts))
			for k := range doc.DescVisTokenCounts {
				tokens = append(tokens, k)
			}

			// Delete group from parent..
			err := deleteFromPhotoGroup(ctx, coll, id[:sep+1], tokens, 1, true)
			if err != nil {
				l.WithError(err).Error("Could not delete from parent group")
				return nil, err
			}
		}
		return nil, nil
	})
	if !transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}

// Delete given empty groups. Returns array of ids that were and were not deleted.
func DeleteEmptyGroups(ctx context.Context, coll *mongo.Collection, ids []string) ([]string, []string) {
	l := log.WithFields(log.Fields{
		"func":           "groups.DeleteEmptyGroups",
		"collectionName": coll.Name(),
	})

	// Delete each group separately.
	var success []string
	var fail []string
	for _, id := range ids {
		if err := deleteEmptyGroup(ctx, coll, id); err != nil {
			l.WithError(err).Warnf("Could not delete empty group %s", id)
			fail = append(fail, id)
		} else {
			success = append(success, id)
		}
	}

	return success, fail
}
