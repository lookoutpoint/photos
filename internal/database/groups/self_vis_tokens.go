// SPDX-License-Identifier: MIT

package groups

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func DeleteWithSelfVisTokens[G GroupSelfVisTokensInterface](ctx context.Context, api GroupSelfVisTokensAPI[G], id string) error {
	l := log.WithFields(log.Fields{
		"func":           "groups.DeleteWithSelfVisTokens",
		"collectionName": api.Collection().Name(),
		"id":             id,
	})

	// Can only delete the group if it has no children.
	children, err := api.ListChildren(ctx, &apis.GroupListParams{
		ID:               id,
		Limit:            1,
		ProjectionKeys:   []string{"id"},
		VisibilityTokens: []string{docs.VisTokenAll},
	})
	if err != nil {
		l.WithError(err).Error("Could not list children")
		return err
	}

	if len(children) > 0 {
		l.Error("Group has children - cannot delete")
		return fmt.Errorf("cannot delete group with children")
	}

	// Delete as a transaction
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.Collection().Database(), l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Get tokens.
		group, err := api.Get(ctx, id, []string{"selfVisTokens"}, []string{docs.VisTokenAll})
		if err != nil {
			l.WithError(err).Error("Could not get group")
			return nil, err
		}

		tokens := []string{docs.VisTokenAll}
		tokens = append(tokens, (*group).GetSelfVisTokens()...)

		if err := UpdateGroupPhotoCountsAndVisTokens(ctx, api, []string{id}, nil, tokens, nil, 1); err != nil {
			l.WithError(err).Error("Could not update token count")
			return nil, err
		}

		// Delete group.
		if _, err := api.Collection().DeleteOne(ctx, bson.M{"_id": id}); err != nil {
			l.WithError(err).Error("Could not delete group")
			return nil, err
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}

func ModifySelfVisToken[G GroupSelfVisTokensInterface](ctx context.Context, api GroupSelfVisTokensAPI[G], id string, token string, recurse bool, add bool) error {
	l := log.WithFields(log.Fields{
		"func":           "groups.ModifySelfVisToken",
		"collectionName": api.Collection().Name(),
		"id":             id,
		"add":            add,
	})

	// Use a transaction as there are potentially multiple updates to be done and everything should be consistent.
	_, transComplete, err := utils.WrapAsTransaction(ctx, api.Collection().Database(), l, func(ctx mongo.SessionContext) (interface{}, error) {
		// Add/delete token to group.
		var op string
		if add {
			op = "$addToSet"
		} else {
			op = "$pull"
		}

		res, err := api.Collection().UpdateOne(
			ctx,
			bson.M{"_id": id},
			bson.M{op: bson.M{
				"selfVisTokens": token,
			}},
		)
		if err != nil {
			l.WithError(err).Warn("Could not update self visibility token")
			return nil, err
		} else if res.MatchedCount != 1 {
			l.Warn("Could not find group")
			return nil, apis.ErrInvalidID
		} else if res.ModifiedCount == 1 {
			// Updated visibility token.
			// Update via diff and update to mimic addition/deletion of a photo with token.
			var err error
			if add {
				err = UpdateGroupPhotoCountsAndVisTokens(ctx, api, nil, []string{id}, nil, []string{token}, 1)
			} else {
				err = UpdateGroupPhotoCountsAndVisTokens(ctx, api, []string{id}, nil, []string{token}, nil, 1)
			}

			if err != nil {
				l.WithError(err).Warn("Could not diff and update on group")
				return nil, err
			}
		}

		if recurse {
			// List children and apply to each.
			children, err := api.ListChildren(ctx, &apis.GroupListParams{
				ID:               id,
				MaxRelDepth:      -1,
				ProjectionKeys:   []string{"id"},
				VisibilityTokens: []string{docs.VisTokenAll},
			})
			if err != nil {
				l.WithError(err).Error("Could not list children")
				return nil, err
			}

			for _, child := range children {
				err := ModifySelfVisToken(ctx, api, child.GetID(), token, false, add)
				if err != nil {
					return nil, err
				}
			}
		}

		return nil, nil
	})
	if err != nil && transComplete {
		l.WithError(err).Error("Could not complete transaction")
	}

	return err
}
