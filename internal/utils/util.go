// SPDX-License-Identifier: MIT

package utils

import (
	"os"

	log "github.com/sirupsen/logrus"
)

// GetEnv looks up an env var and uses a default if it doesn't exist
func GetEnv(key string, defaultVal string) string {
	val, success := os.LookupEnv(key)
	if success {
		return val
	}
	log.Warnf("Env var %v not found - using default value", key)
	return defaultVal
}

var cloudProjectID = GetEnv("GOOGLE_CLOUD_PROJECT", "lookoutpoint-local")

// GetCloudProjectID returns the Google cloud project id from env var
func GetCloudProjectID() string {
	return cloudProjectID
}
