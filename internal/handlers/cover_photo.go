// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"math/rand"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Cover photo info which contains enough to determine how to display the photo
type coverPhotoInfo struct {
	ID       docs.PhotoID `json:"id"`
	StoreGen string       `json:"storeGen"`
	Width    int          `json:"width"`
	Height   int          `json:"height"`
}

// Use an odd number to prevent modulo behavior
var coverPhotoAutoSelectionDuration = 29 * time.Hour

type coverPhotoOptions struct {
	MinAspectRatio float32
	MaxAspectRatio float32
}

func getGroupCoverPhotoInfo(ctx context.Context, collection *mongo.Collection, doc *docs.GroupBase, groupFilter *docs.PhotoListFilterExpr, tokens []string, opts *coverPhotoOptions) (*coverPhotoInfo, error) {
	l := log.WithFields(log.Fields{
		"func":           "getGroupCoverPhotoInfo",
		"collectionName": collection.Name(),
		"id":             *doc.ID,
	})

	// Set default options
	if opts == nil {
		opts = &coverPhotoOptions{}
	}
	if opts.MinAspectRatio == 0 {
		opts.MinAspectRatio = 9.0 / 16.0
	}
	if opts.MaxAspectRatio == 0 {
		opts.MaxAspectRatio = 16.0 / 9.0
	}

	db := database.Get()

	// Cover photo config. Extract fields to get some sensible defaults
	var photoID *docs.PhotoID
	var userSelected = false
	var lastAutoSelected = time.Unix(0, 0)

	if doc.CoverPhoto != nil {
		photoID = doc.CoverPhoto.Photo
		if doc.CoverPhoto.UserSelected != nil {
			userSelected = *doc.CoverPhoto.UserSelected
		}
		if doc.CoverPhoto.LastAutoSelected != nil {
			lastAutoSelected = *doc.CoverPhoto.LastAutoSelected
		}
	}

	if photoID != nil && (userSelected || time.Now().UTC().Add(-coverPhotoAutoSelectionDuration).Before(lastAutoSelected)) {
		// Either user-selected cover photo or last auto-selected photo is still within time range.
		photo, err := db.Photos.Get(ctx, *photoID, []string{"storeGen", "width", "height"}, tokens)
		if err == nil {
			return &coverPhotoInfo{
				ID:       *photoID,
				StoreGen: *photo.StoreGen,
				Width:    *photo.Width,
				Height:   *photo.Height,
			}, nil
		}

		// Fall-back to auto-selection
		if userSelected {
			l.WithField("id", *photoID).Warn("Unable to use user-selected cover photo - falling back to auto-selection")
		} else {
			l.WithField("id", *photoID).Warn("Unable to use last-selected cover photo - falling back to auto-selection")
		}
	}

	l.Trace("Trying to auto-select cover photo")

	// At this point, do auto-selection.
	// If the filter matches any public photos, select from public photos. Otherwise, select based on the
	// given visibility tokens.
	listParams := apis.PhotoListParams{
		Limit:            1,
		ProjectionKeys:   []string{"id"},
		Filter:           *groupFilter,
		VisibilityTokens: []string{docs.VisTokenPublic},
	}

	publicPhotos, err := db.Photos.List(ctx, &listParams)
	if err != nil {
		l.WithError(err).Error("Could not check for public photos")
		return nil, err
	}

	// These are the tokens to use.
	var tokensFilter []string
	if len(publicPhotos) > 0 {
		tokensFilter = append(tokensFilter, docs.VisTokenPublic)
	} else {
		tokensFilter = tokens
	}

	// Helper to randomly pick a photo with an optional rating and/or optional aspect ratio filter
	randomPick := func(rating int, aspectRatio bool) (*docs.Photo, error) {
		params := apis.PhotoRandomListParams{
			Count:            1,
			ProjectionKeys:   []string{"id", "storeGen", "width", "height"},
			VisibilityTokens: tokensFilter,
			Filter: docs.PhotoListFilterExpr{
				Exprs: []*docs.PhotoListFilterExpr{groupFilter},
			},
		}

		if rating != 0 {
			params.Filter.Ratings = []docs.PhotoListFilterRating{{Value: rating, CmpOp: list.CmpEq}}
		}
		if aspectRatio {
			params.Filter.AspectRatios = []docs.PhotoListFilterAspectRatio{
				{Value: opts.MaxAspectRatio, CmpOp: list.CmpLte},
				{Value: opts.MinAspectRatio, CmpOp: list.CmpGte},
			}
		}

		photos, err := db.Photos.RandomList(ctx, &params)
		if err != nil {
			l.WithFields(log.Fields{"rating": rating, "aspectRatio": aspectRatio}).WithError(err).Error("Could not do random list")
			return nil, err
		}

		if len(photos) == 0 {
			return nil, nil
		}
		return &photos[0], nil
	}

	type configParams struct {
		rating      int
		aspectRatio bool
	}
	configs := []configParams{
		{5, true},
		{4, true},
		{3, true},
		{0, true},
		{0, false},
	}

	for _, config := range configs {
		photo, err := randomPick(config.rating, config.aspectRatio)
		if err != nil {
			return nil, err
		}

		if photo != nil {
			l.Infof("Auto-selected cover photo with rating=%v aspectRatio=%v", config.rating, config.aspectRatio)

			// Found a photo. Use this one.

			// Store it in the database for next use.
			// For lastAutoSelected time, add a fudge factor so that if there are a batch of cover photo selections
			// (as may happen when initially nothing has a cover photo), spread out their "expiry" so we don't have
			// to find new ones all at the same time next time.
			adjustment := rand.Int63n(int64(coverPhotoAutoSelectionDuration/4)) - int64(coverPhotoAutoSelectionDuration/8)
			adjustedTime := time.Now().UTC().Add(time.Duration(adjustment))

			_, err := collection.UpdateOne(ctx, bson.M{"_id": *doc.ID},
				bson.M{
					"$set": bson.M{
						"coverPhoto.photo":            *photo.ID,
						"coverPhoto.userSelected":     false,
						"coverPhoto.lastAutoSelected": adjustedTime,
					},
				},
			)
			if err != nil {
				// Don't treat as a hard error as we have the photo
				l.WithError(err).Warn("Could not update with new auto-selected cover photo")
			}

			return &coverPhotoInfo{
				*photo.ID,
				*photo.StoreGen,
				*photo.Width,
				*photo.Height,
			}, nil
		}
	}

	return nil, nil
}
