// SPDX-License-Identifier: MIT

package handlers

import "time"

const (
	shortTimeout = 10 * time.Second
	longTimeout  = 60 * time.Second
)
