// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Folder", func() {
	var db *database.Database
	var ctx context.Context

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		db = database.Get()
	})

	Describe("FolderInfo", func() {
		genPhotoMd := func(w int, h int, title string, location string, dateTime string) *photomd.Metadata {
			return &photomd.Metadata{
				Width:  w,
				Height: h,
				Xmp: photomd.XmpData{
					Title:    title,
					Location: location,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: dateTime,
				},
			}
		}

		addRating := func(md *photomd.Metadata, rating int) *photomd.Metadata {
			md.Xmp.Rating = rating
			return md
		}

		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		type Response map[string]interface{}

		It("Non-existent folder", func() {
			request := makeRequest("/hidden-folder/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(404))

		})

		It("Empty root folder", func() {
			// Create root folder.
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())
			makeAllPhotosPublic(ctx)

			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Should have no sub-folders.
			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))

		})

		It("With non-public sub folders", func() {
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())
			makeAllPhotosPublic(ctx)

			// Make a sub-folder in the database.
			_, err = db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			// Check root folder
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))

		})

		It("Meta description and rich text", func() {
			_, err := db.Folders.Create(ctx, "/a/")
			Expect(err).ToNot(HaveOccurred())

			err = db.Folders.Update(ctx, "/a/", apis.NewGroupUpdate().SetDisplay("A").SetMetaDescription("DESC").SetRichText("RICH"))
			Expect(err).ToNot(HaveOccurred())

			err = db.Folders.AddVisibilityToken(ctx, "/a/", apis.FolderTarget, docs.VisTokenPublic)
			Expect(err).ToNot(HaveOccurred())

			// Check
			request := makeRequest("/a/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":         Equal("A"),
				"public":          Equal(true),
				"metaDescription": Equal("DESC"),
				"richText":        Equal("RICH"),
				"hasChildren":     Equal(false),
				"hasPhotos":       BeEquivalentTo(0),
			}))
		})

		It("Sub folders", func() {
			// Make a few folders in the database.
			_, err := db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/b/c/")
			Expect(err).ToNot(HaveOccurred())

			// Make all folders public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/z/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())
			Expect(db.Folders.AddVisibilityToken(ctx, "/b/c/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Check root folder
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(0),
			}))

			// Check folder "b", which is in subFolder[0]
			request = makeRequest("/b/")
			respRec = httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal("b"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(0),
			}))

		})

		It("In admin session", func() {
			// Create admin session
			adminSession := createAdminUserAndSession(ctx)

			// Make a few folders in the database.
			_, err := db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/b/c/")
			Expect(err).ToNot(HaveOccurred())

			// Make all folders public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/z/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())
			Expect(db.Folders.AddVisibilityToken(ctx, "/b/c/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Check root folder
			request := addSessionToRequest(makeRequest("/"), adminSession)
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":       Equal(""),
				"public":        Equal(true),
				"sortOrder":     BeEquivalentTo(""),
				"visTokens":     ConsistOf(string(docs.VisTokenAll), string(docs.VisTokenPublic)),
				"selfVisTokens": ConsistOf(string(docs.VisTokenPublic)),
				"hasChildren":   Equal(true),
				"hasPhotos":     BeEquivalentTo(0),
			}))

		})

		It("In admin session and sort order", func() {
			// Create admin session
			adminSession := createAdminUserAndSession(ctx)

			// Make a folder in the database and set a sort order.
			_, err := db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			err = db.Folders.Update(ctx, "/z/", apis.NewGroupUpdate().SetSortOrder(100))
			Expect(err).ToNot(HaveOccurred())

			// Make folder public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/z/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Check folder
			request := addSessionToRequest(makeRequest("/z/"), adminSession)
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":       Equal("z"),
				"public":        Equal(true),
				"sortOrder":     BeEquivalentTo(100),
				"visTokens":     ConsistOf(string(docs.VisTokenAll), string(docs.VisTokenPublic)),
				"selfVisTokens": ConsistOf(string(docs.VisTokenPublic)),
				"hasChildren":   Equal(false),
				"hasPhotos":     BeEquivalentTo(0),
			}))
		})

		It("Subfolders with cover photos", func() {
			folderCoverPhoto := addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", addRating(genPhotoMd(2000, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00"), 5))
			addPhotoAndMockResize(ctx, "folder/DSC_100.jpg", genPhotoMd(300, 4000, "First Photo", "Toronto", "2020:01:01 10:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_101.jpg", genPhotoMd(400, 5000, "Second Photo", "Tokyo", "2020:01:01 10:01:00"))
			addPhotoAndMockResize(ctx, "xyz/DSC_300.jpg", genPhotoMd(500, 6000, "London", "London", "2020:01:01 20:01:00"))
			xyzCoverPhoto := addPhotoAndMockResize(ctx, "xyz/DSC_400.jpg", addRating(genPhotoMd(8000, 6000, "Somewhere Else", "Atlanta", "2020:01:02 11:01:00"), 4))

			// Check root folder
			request := makeRequestCoverPhoto("/")
			respRec := httptest.NewRecorder()

			FolderInfo(respRec, request)

			// Sub-folders aren't public
			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))

			// Make all folders public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/folder/", apis.FolderTargetPhotos, docs.VisTokenPublic)).ToNot(HaveOccurred())
			Expect(db.Folders.AddVisibilityToken(ctx, "/xyz/", apis.FolderTargetPhotos, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Root
			request = makeRequestCoverPhoto("/")
			respRec = httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display": Equal(""),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal(string(*folderCoverPhoto.ID)),
					"storeGen": Ignore(),
					"width":    BeNumerically("==", 2000),
					"height":   BeNumerically("==", 2000),
				}),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS),
			}))

			// folder
			request = makeRequestCoverPhoto("/folder/")
			respRec = httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display": Equal("folder"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal(string(*folderCoverPhoto.ID)),
					"storeGen": Ignore(),
					"width":    BeNumerically("==", 2000),
					"height":   BeNumerically("==", 2000),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS),
			}))

			// xyz
			request = makeRequestCoverPhoto("/xyz/")
			respRec = httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display": Equal("xyz"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal(string(*xyzCoverPhoto.ID)),
					"storeGen": Ignore(),
					"width":    BeNumerically("==", 8000),
					"height":   BeNumerically("==", 6000),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS),
			}))

			// Check root folder, this time asking for no cover photos
			request = makeRequest("/")
			respRec = httptest.NewRecorder()

			FolderInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS),
			}))

		})

		Describe("Meta-properties", func() {
			Describe("Has children, no photos", func() {
				It("Public", func() {
					_, err := db.Folders.Create(ctx, "/a/")
					Expect(err).ToNot(HaveOccurred())

					err = db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTarget, docs.VisTokenPublic)
					Expect(err).ToNot(HaveOccurred())

					err = db.Folders.AddVisibilityToken(ctx, "/a/", apis.FolderTarget, docs.VisTokenPublic)
					Expect(err).ToNot(HaveOccurred())

					// Check /a/
					request := makeRequest("/a/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasChildren": Equal(false),
						"hasPhotos":   BeEquivalentTo(0),
					}))

					// Check /
					request = makeRequest("/")
					respRec = httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasChildren": Equal(true),
						"hasPhotos":   BeEquivalentTo(0),
					}))
				})

				It("Non-public child", func() {
					// `/a/` is not public, so not considered child of root for the FolderInfo request
					_, err := db.Folders.Create(ctx, "/a/")
					Expect(err).ToNot(HaveOccurred())

					err = db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTarget, docs.VisTokenPublic)
					Expect(err).ToNot(HaveOccurred())

					// Check /
					request := makeRequest("/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasChildren": Equal(false),
						"hasPhotos":   BeEquivalentTo(0),
					}))
				})
			})

			Describe("has*Photos", func() {
				md := func(rating int, gps bool) *photomd.Metadata {
					md := genPhotoMd(1, 1, "a", "a", "2020:01:01 00:00:00")
					if rating > 0 {
						md = addRating(md, rating)
					}
					if gps {
						md = addGpsMetadata(md)
					}
					return md
				}

				It("Highlight and mappable", func() {
					addPhotoAndMockResize(ctx, "f/p.jpgg", md(5, true))
					makeAllPhotosPublic(ctx)

					// Check
					request := makeRequest("/f/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasPhotos": BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS | HAS_MAPPABLE_PHOTOS),
					}))
				})

				It("Non-highlight and mappable", func() {
					addPhotoAndMockResize(ctx, "f/p.jpgg", md(0, true))
					makeAllPhotosPublic(ctx)

					// Check
					request := makeRequest("/f/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasPhotos": BeEquivalentTo(HAS_PHOTOS | HAS_MAPPABLE_PHOTOS),
					}))
				})

				It("Highlight and non-mappable", func() {
					addPhotoAndMockResize(ctx, "f/p.jpgg", md(4, false))
					makeAllPhotosPublic(ctx)

					// Check
					request := makeRequest("/f/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasPhotos": BeEquivalentTo(HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS),
					}))
				})

				It("Non-highlight and non-mappable", func() {
					addPhotoAndMockResize(ctx, "f/p.jpgg", md(2, false))
					makeAllPhotosPublic(ctx)

					// Check
					request := makeRequest("/f/")
					respRec := httptest.NewRecorder()

					FolderInfo(respRec, request)

					Expect(respRec.Code).To(Equal(200))

					var resp Response
					Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
					Expect(resp).To(MatchKeys(IgnoreExtras, Keys{
						"hasPhotos": BeEquivalentTo(HAS_PHOTOS),
					}))
				})
			})
		})
	})

	Describe("FolderChildren", func() {
		genPhotoMd := func(w int, h int, title string, location string, dateTime string) *photomd.Metadata {
			return &photomd.Metadata{
				Width:  w,
				Height: h,
				Xmp: photomd.XmpData{
					Title:    title,
					Location: location,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: dateTime,
				},
			}
		}

		addRating := func(md *photomd.Metadata, rating int) *photomd.Metadata {
			md.Xmp.Rating = rating
			return md
		}

		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		type Response []map[string]interface{}

		It("Non-existent folder", func() {
			request := makeRequest("/hidden-folder/")
			respRec := httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(0))
		})

		It("Empty root folder", func() {
			// Create root folder.
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())
			makeAllPhotosPublic(ctx)

			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Should have no sub-folders.
			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(0))

		})

		It("With non-public sub folders", func() {
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())
			makeAllPhotosPublic(ctx)

			// Make a sub-folder in the database.
			_, err = db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			// Check root folder
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(0))

		})

		It("With public sub folders", func() {
			// Make a few folders in the database.
			_, err := db.Folders.Create(ctx, "/z/")
			Expect(err).ToNot(HaveOccurred())

			_, err = db.Folders.Create(ctx, "/b/c/")
			Expect(err).ToNot(HaveOccurred())

			// Make all folders public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/z/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())
			Expect(db.Folders.AddVisibilityToken(ctx, "/b/c/", apis.FolderTarget, docs.VisTokenPublic)).ToNot(HaveOccurred())

			// Check root folder
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())

			// Expect in alphabetical order
			Expect(resp).To(HaveLen(2))
			Expect(resp[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/b/"),
				"display": Equal("b"),
				"public":  Equal(true),
			}))
			Expect(resp[1]).To(MatchAllKeys(Keys{
				"id":      Equal("/z/"),
				"display": Equal("z"),
				"public":  Equal(true),
			}))

			// Check folder "b"
			request = makeRequest("/b/")
			respRec = httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())

			Expect(resp).To(HaveLen(1))
			Expect(resp[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/b/c/"),
				"display": Equal("b/c"),
				"public":  Equal(true),
			}))

		})

		It("Subfolders with cover photos", func() {
			folderCoverPhoto := addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", addRating(genPhotoMd(2000, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00"), 5))
			addPhotoAndMockResize(ctx, "folder/DSC_100.jpg", genPhotoMd(300, 4000, "First Photo", "Toronto", "2020:01:01 10:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_101.jpg", genPhotoMd(400, 5000, "Second Photo", "Tokyo", "2020:01:01 10:01:00"))
			addPhotoAndMockResize(ctx, "xyz/DSC_300.jpg", genPhotoMd(500, 6000, "London", "London", "2020:01:01 20:01:00"))
			xyzCoverPhoto := addPhotoAndMockResize(ctx, "xyz/DSC_400.jpg", addRating(genPhotoMd(8000, 6000, "Somewhere Else", "Atlanta", "2020:01:02 11:01:00"), 4))

			// Check root folder
			request := makeRequestCoverPhoto("/")
			respRec := httptest.NewRecorder()

			FolderChildren(respRec, request)

			// Sub-folders aren't public
			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(0))

			// Make all folders public.
			Expect(db.Folders.AddVisibilityToken(ctx, "/folder/", apis.FolderTargetPhotos, docs.VisTokenPublic)).ToNot(HaveOccurred())
			Expect(db.Folders.AddVisibilityToken(ctx, "/xyz/", apis.FolderTargetPhotos, docs.VisTokenPublic)).ToNot(HaveOccurred())

			request = makeRequestCoverPhoto("/")
			respRec = httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())

			Expect(resp).To(HaveLen(2))
			Expect(resp[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/folder/"),
				"display": Equal("folder"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal(string(*folderCoverPhoto.ID)),
					"storeGen": Ignore(),
					"width":    BeNumerically("==", 2000),
					"height":   BeNumerically("==", 2000),
				}),
			}))
			Expect(resp[1]).To(MatchAllKeys(Keys{
				"id":      Equal("/xyz/"),
				"display": Equal("xyz"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal(string(*xyzCoverPhoto.ID)),
					"storeGen": Ignore(),
					"width":    BeNumerically("==", 8000),
					"height":   BeNumerically("==", 6000),
				}),
			}))

			// Check root folder, this time asking for no cover photos
			request = makeRequest("/")
			respRec = httptest.NewRecorder()

			FolderChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())

			Expect(resp).To(HaveLen(2))
			Expect(resp[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/folder/"),
				"display": Equal("folder"),
				"public":  Equal(true),
			}))
			Expect(resp[1]).To(MatchAllKeys(Keys{
				"id":      Equal("/xyz/"),
				"display": Equal("xyz"),
				"public":  Equal(true),
			}))

		})

	})

	Describe("Update", func() {
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestAndSortOrder := func(id string, displayPath string, sortOrder interface{}) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v", "sortOrder": %v }`, id, displayPath, sortOrder))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("Try to change root folder", func() {
			_, err := db.Folders.Create(ctx, "/")
			Expect(err).ToNot(HaveOccurred())

			// No admin
			request := makeRequest("/", "Title")
			respRec := httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			// With admin
			request = addSessionToRequest(makeRequest("/", "Title"), adminSession)
			respRec = httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(400))

		})

		It("Update folder display path", func() {
			_, err := db.Folders.Create(ctx, "/x/y/z/")
			Expect(err).ToNot(HaveOccurred())

			// With admin
			request := addSessionToRequest(makeRequest("/x/y/", "Why?"), adminSession)
			respRec := httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/x/y/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("x/Why?"))}),
			)
		})

		It("Update folder sort order", func() {
			_, err := db.Folders.Create(ctx, "/x/y/z/")
			Expect(err).ToNot(HaveOccurred())

			// With admin (number)
			request := addSessionToRequest(makeRequestAndSortOrder("/x/y/", "Why?", 101), adminSession)
			respRec := httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/x/y/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.Display":   PointTo(Equal("x/Why?")),
					"GroupBase.SortOrder": PointTo(BeEquivalentTo(101)),
				}),
			)

			// With admin (string)
			request = addSessionToRequest(makeRequestAndSortOrder("/x/y/", "Why?", "\"blah\""), adminSession)
			respRec = httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/x/y/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.Display":   PointTo(Equal("x/Why?")),
					"GroupBase.SortOrder": PointTo(Equal("blah")),
				}),
			)

		})

		It("Update folder meta description", func() {
			makeRequest := func(id string, richText string) *http.Request {
				payload := strings.NewReader(
					fmt.Sprintf(`{ "id": "%v", "richText": "%v" }`, id, richText))
				return httptest.NewRequest("POST", "/", payload)
			}

			_, err := db.Folders.Create(ctx, "/x/y/z/")
			Expect(err).ToNot(HaveOccurred())

			// With admin
			request := addSessionToRequest(makeRequest("/x/y/", "Describe Why?"), adminSession)
			respRec := httptest.NewRecorder()

			FolderUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/x/y/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.Display":  PointTo(Equal("x/y")),
					"GroupBase.RichText": PointTo(Equal("Describe Why?")),
				}),
			)

		})
	})

	Describe("Visibility tokens", func() {
		var adminSession *docs.Session
		var token string
		tokensAll := []string{docs.VisTokenAll}

		genPhotoMd := func(title string) *photomd.Metadata {
			return &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title: title,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
			}
		}

		makeRequest := func(mode string, folder string, target string, token string, session *docs.Session) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "mode": "%v", "folder": "%v", "target": "%v", "token": "%v" }`, mode, folder, target, token))
			request := httptest.NewRequest("POST", "/", payload)
			return addSessionToRequest(request, session)
		}

		BeforeEach(func() {
			var err error

			// Admin session
			adminSession = createAdminUserAndSession(ctx)

			// Add token
			tokenDoc, err := db.Visibility.GetOrCreate(ctx, "Private", nil)
			Expect(err).ToNot(HaveOccurred())
			token = *tokenDoc.ID

			// Add photos
			addPhotoAndMockResize(ctx, "folder/photo1.jpg", genPhotoMd("Photo 1"))
			addPhotoAndMockResize(ctx, "folder/photo2.jpg", genPhotoMd("Photo 2"))
			addPhotoAndMockResize(ctx, "xyz/photo3.jpg", genPhotoMd("Photo 3"))
		})

		It("Add visibility token without admin", func() {
			request := makeRequest("add", "/", "folder", "/private/", nil)
			respRec := httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Add private visibility token to folder", func() {
			request := makeRequest("add", "/folder/", "folder", "/private/", adminSession)
			respRec := httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, token: 1}, nil,
			))
		})

		It("Add public visibility token to photos", func() {
			request := makeRequest("add", "/folder/", "photos", docs.VisTokenPublic, adminSession)
			respRec := httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, docs.VisTokenPublic: 3}, nil,
			))
		})

		It("Add public visibility token to full tree", func() {
			request := makeRequest("add", "/", "fulltree", docs.VisTokenPublic, adminSession)
			respRec := httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 1, docs.VisTokenPublic: 1},
				VisTokenCounts{docs.VisTokenAll: 5, docs.VisTokenPublic: 3},
			))
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, docs.VisTokenPublic: 2}, nil,
			))
			Expect(db.Folders.Get(ctx, "/xyz/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 2, docs.VisTokenPublic: 1}, nil,
			))

		})

		It("Add and delete visibility token", func() {
			request := makeRequest("add", "/folder/", "photos", "/private/", adminSession)
			respRec := httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, token: 3}, nil,
			))

			// Try to delete without admin session
			request = makeRequest("delete", "/folder/", "folder", "/private/", nil)
			respRec = httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, token: 3}, nil,
			))

			// Delete with admin session
			request = makeRequest("delete", "/folder/", "folder", "/private/", adminSession)
			respRec = httptest.NewRecorder()

			FolderVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).To(MatchGroupBasePtrVisTokenCounts(
				VisTokenCounts{docs.VisTokenAll: 3, token: 2}, nil,
			))

		})

	})

	Describe("FolderListEmpty and FolderDeleteEmpty", func() {
		var err error
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		genPhotoMd := func(title string) *photomd.Metadata {
			return &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title: title,
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 00:00:00",
				},
			}
		}

		makeListRequest := func(session *docs.Session) *http.Request {
			request := httptest.NewRequest("GET", "/", nil)
			return addSessionToRequest(request, session)
		}

		makeDeleteRequest := func(ids []string, session *docs.Session) *http.Request {
			json, err := json.Marshal(ids)
			Expect(err).ToNot(HaveOccurred())

			request := httptest.NewRequest("POST", "/", bytes.NewReader(json))
			return addSessionToRequest(request, session)
		}

		type DeleteResponse struct {
			Deleted    []string `json:"deleted"`
			NotDeleted []string `json:"notDeleted"`
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)

			// Add photo
			addPhotoAndMockResize(ctx, "folder/photo1.jpg", genPhotoMd("Photo 1"))

			// Create empty sub-folders
			_, err := db.Folders.Create(ctx, "/folder/subA/subB/")
			Expect(err).ToNot(HaveOccurred())

			// Add tokens to sub-folders
			db.Folders.AddVisibilityToken(ctx, "/folder/subA/subB/", apis.FolderTargetFullTree, docs.VisTokenPublic)
			db.Folders.AddVisibilityToken(ctx, "/folder/subA/", apis.FolderTargetFullTree, docs.VisTokenPublic)
		})

		It("Without admin session", func() {
			// List
			request := makeListRequest(nil)
			respRec := httptest.NewRecorder()

			FolderListEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			// Delete
			request = makeDeleteRequest([]string{"/folder/subA/subB/"}, nil)
			respRec = httptest.NewRecorder()

			FolderDeleteEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			// Check that folders are still there
			Expect(db.Folders.Get(ctx, "/folder/subA/subB/", nil, tokensAll)).ToNot(BeNil())
			Expect(db.Folders.Get(ctx, "/folder/subA/", nil, tokensAll)).ToNot(BeNil())
		})

		It("List and delete", func() {
			// List
			request := makeListRequest(adminSession)
			respRec := httptest.NewRecorder()

			FolderListEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var ids []string
			Expect(json.Unmarshal(readResponseBody(respRec), &ids)).ToNot(HaveOccurred())
			Expect(ids).To(HaveLen(1))
			Expect(ids[0]).To(Equal("/folder/subA/subB/"))

			// Delete
			request = makeDeleteRequest(ids, adminSession)
			respRec = httptest.NewRecorder()

			FolderDeleteEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var delResp DeleteResponse
			Expect(json.Unmarshal(readResponseBody(respRec), &delResp)).ToNot(HaveOccurred())
			Expect(delResp.Deleted).To(HaveLen(1))
			Expect(delResp.Deleted[0]).To(Equal("/folder/subA/subB/"))
			Expect(delResp.NotDeleted).To(HaveLen(0))

			// Check folders
			_, err = db.Folders.Get(ctx, "/folder/subA/subB/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
			Expect(db.Folders.Get(ctx, "/folder/subA/", nil, tokensAll)).ToNot(BeNil())
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).ToNot(BeNil())

			// List
			request = makeListRequest(adminSession)
			respRec = httptest.NewRecorder()

			FolderListEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			ids = []string{}
			Expect(json.Unmarshal(readResponseBody(respRec), &ids)).ToNot(HaveOccurred())
			Expect(ids).To(HaveLen(1))
			Expect(ids[0]).To(Equal("/folder/subA/"))

			// Delete
			ids = append(ids, "/folder/") // invalid group for deletion
			request = makeDeleteRequest(ids, adminSession)
			respRec = httptest.NewRecorder()

			FolderDeleteEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			delResp = DeleteResponse{}
			Expect(json.Unmarshal(readResponseBody(respRec), &delResp)).ToNot(HaveOccurred())
			Expect(delResp.Deleted).To(HaveLen(1))
			Expect(delResp.Deleted[0]).To(Equal("/folder/subA/"))
			Expect(delResp.NotDeleted).To(HaveLen(1))
			Expect(delResp.NotDeleted[0]).To(Equal("/folder/"))

			// Check folders
			_, err = db.Folders.Get(ctx, "/folder/subA/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
			Expect(db.Folders.Get(ctx, "/folder/", nil, tokensAll)).ToNot(BeNil())

			// List
			request = makeListRequest(adminSession)
			respRec = httptest.NewRecorder()

			FolderListEmpty(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			ids = []string{}
			Expect(json.Unmarshal(readResponseBody(respRec), &ids)).ToNot(HaveOccurred())
			Expect(ids).To(HaveLen(0))
		})
	})

})
