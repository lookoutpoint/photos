// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strconv"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

// Add some placeholder GPS location metadata
func addGpsMetadata(md *photomd.Metadata) *photomd.Metadata {
	md.Exif[photomd.ExifTagGPSLatitude] = []photomd.Urational{{Num: 10, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
	md.Exif[photomd.ExifTagGPSLongitude] = []photomd.Urational{{Num: 20, Den: 1}, {Num: 0, Den: 1}, {Num: 0, Den: 1}}
	md.Exif[photomd.ExifTagGPSLatitudeRef] = "N"
	md.Exif[photomd.ExifTagGPSLongitudeRef] = "E"
	return md
}

var _ = Describe("Photo", func() {
	var db *database.Database
	var ctx context.Context

	genPhotoMd := func(w int, h int, title string, location string, dateTime string, keywords ...string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  w,
			Height: h,
			Xmp: photomd.XmpData{
				Title:    title,
				Location: location,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: dateTime,
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		db = database.Get()
	})

	Describe("PhotoGetMetadata", func() {
		makeRequest := func(id docs.PhotoID) *http.Request {
			return httptest.NewRequest("GET", "/"+string(id), nil)
		}

		It("Get metadata", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:              "root!",
					Location:           "the world, Banff, Alberta, Canada",
					Subjects:           []string{"hello", "keyword", "cat:value", "fruit:apple"},
					Rating:             4,
					UsageTerms:         "Use Terms",
					RightsWebStatement: "http://usage",
				},
				Exif: photomd.ExifData{
					photomd.ExifTagImageDescription: "describe a root",
					photomd.ExifTagCopyright:        "Copyright Me",
					photomd.ExifTagMake:             "Camera Make",
					photomd.ExifTagModel:            "Camera Model",
					photomd.ExifTagLensModel:        "Lens Model",
					photomd.ExifTagISOSpeedRatings:  uint16(250),
					photomd.ExifTagDateTimeOriginal: "2019:04:03 20:56:59",
				},
			}

			photo, err := db.Photos.AddOrUpdate(ctx, "bucket", "folder/photo.jpg", 1, 2000, md)
			Expect(err).ToNot(HaveOccurred())

			request := makeRequest(*photo.ID)
			respRec := httptest.NewRecorder()

			PhotoGetMetadata(respRec, request)

			// Photo is not public
			Expect(respRec.Code).To(Equal(400))

			// Make public
			Expect(db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTargetFullTree, docs.VisTokenPublic)).ToNot(HaveOccurred())

			request = makeRequest(*photo.ID)
			respRec = httptest.NewRecorder()

			PhotoGetMetadata(respRec, request)

			// Photo is now public
			Expect(respRec.Code).To(Equal(200))

			var response map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"id":          Equal("nGzA54ET4te8hF4rjp0gY10_EhA"),
				"sortKey":     Equal("20190403205659photo/nGzA54ET4te8hF4rjp0gY10_EhA"),
				"tsSortKey":   Not(HaveLen(0)),
				"storeGen":    Equal("AQAAAAAAAAA"),
				"width":       BeEquivalentTo(100),
				"height":      BeEquivalentTo(200),
				"date":        Equal("/2019/04/03/"),
				"folder":      Equal([]interface{}{"/folder/", "folder"}),
				"title":       Equal("root!"),
				"description": Equal("describe a root"),
				"keywords": ConsistOf(
					Equal([]interface{}{"/hello/", "hello"}),
					Equal([]interface{}{"/keyword/", "keyword"}),
				),
				"categories": ConsistOf(
					Equal([]interface{}{"/cat/value/", "cat:value"}),
					Equal([]interface{}{"/fruit/apple/", "fruit:apple"}),
				),
				"dateTime":    Equal("2019-04-03T20:56:59Z"),
				"rating":      BeEquivalentTo(4),
				"copyright":   Equal("Copyright Me"),
				"usageTerms":  Equal("Use Terms"),
				"usageWebUrl": Equal("http://usage"),
				"locations":   Equal([]interface{}{[]interface{}{"/canada/alberta/banff/the-world/", "Canada,Alberta,Banff,the world"}}),
				"make":        Equal("Camera Make"),
				"model":       Equal("Camera Model"),
				"lens":        Equal("Lens Model"),
				"iso":         BeEquivalentTo(250),
				"public":      Equal(true),
			}))

		})

	})

	Describe("PhotoList: for folders", func() {
		makeRequestWithKey := func(folder string, limit *int, refSortKey *string, fullTree *bool, refKey *string, mapping bool) *http.Request {
			var params struct {
				Limit       *int                     `json:"limit,omitempty"`
				RefKey      *string                  `json:"refKey,omitempty"`
				RefKeyValue *string                  `json:"refKeyValue,omitempty"`
				Filter      docs.PhotoListFilterExpr `json:"filter"`
				Mapping     bool                     `json:"mapping,omitempty"`
			}

			params.Limit = limit
			params.RefKey = refKey
			params.RefKeyValue = refSortKey
			params.Filter.Folders = []docs.PhotoListFilterFolder{{Path: folder}}
			if fullTree != nil {
				params.Filter.Folders[0].FullTree = *fullTree
			}
			params.Mapping = mapping

			json, err := json.Marshal(params)
			Expect(err).ToNot(HaveOccurred())

			return httptest.NewRequest("POST", "/", bytes.NewReader(json))
		}

		makeRequest := func(folder string, limit *int, refSortKey *string, fullTree *bool) *http.Request {
			return makeRequestWithKey(folder, limit, refSortKey, fullTree, nil /*mapping*/, false)
		}

		makeMappingRequest := func(folder string, limit *int, refSortKey *string, fullTree *bool) *http.Request {
			return makeRequestWithKey(folder, limit, refSortKey, fullTree, nil /*mapping*/, true)
		}

		It("Non-existent folder", func() {
			request := makeRequest("/not-a-folder/", nil, nil, nil)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(0))
		})

		It("Listing in direct folder", func() {
			addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", genPhotoMd(100, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_257.jpg", genPhotoMd(200, 3000, "Exposure 2", "Boston", "2020:01:01 15:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_100.jpg", genPhotoMd(300, 4000, "First Photo", "Toronto", "2020:01:01 10:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_101.jpg", genPhotoMd(400, 5000, "Second Photo", "Tokyo", "2020:01:01 10:01:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_300.jpg", genPhotoMd(500, 6000, "Last Photo", "London", "2020:01:01 20:01:00"))
			addPhotoAndMockResize(ctx, "other-folder/DSC_400.jpg", genPhotoMd(500, 6000, "Somewhere Else", "Atlanta", "2020:01:02 11:01:00"))
			makeAllPhotosPublic(ctx)

			// Initial listing of three
			limit := 3
			request := makeRequest("/folder/", &limit, nil, nil)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(3))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":     Equal("First Photo"),
				"locations": Equal([]interface{}{[]interface{}{"/toronto/", "Toronto"}}),
				"width":     BeNumerically("==", 300),
				"height":    BeNumerically("==", 4000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))
			Expect(photos[1]).To(MatchAllKeys(Keys{
				"title":     Equal("Second Photo"),
				"locations": Equal([]interface{}{[]interface{}{"/tokyo/", "Tokyo"}}),
				"width":     BeNumerically("==", 400),
				"height":    BeNumerically("==", 5000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))
			Expect(photos[2]).To(MatchAllKeys(Keys{
				"title":     Equal("Exposure 1"),
				"locations": Equal([]interface{}{[]interface{}{"/new-york-city/", "New York City"}}),
				"width":     BeNumerically("==", 100),
				"height":    BeNumerically("==", 2000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))

			// Do next listing of three
			sortKey := photos[2]["sortKey"].(string)
			request = makeRequest("/folder/", &limit, &sortKey, nil)
			respRec = httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos = []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":     Equal("Exposure 2"),
				"locations": Equal([]interface{}{[]interface{}{"/boston/", "Boston"}}),
				"width":     BeNumerically("==", 200),
				"height":    BeNumerically("==", 3000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))
			Expect(photos[1]).To(MatchAllKeys(Keys{
				"title":     Equal("Last Photo"),
				"locations": Equal([]interface{}{[]interface{}{"/london/", "London"}}),
				"width":     BeNumerically("==", 500),
				"height":    BeNumerically("==", 6000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))

		})

		It("Listing in full tree folder", func() {
			addPhotoAndMockResize(ctx, "folder/subfolder/DSC_256.jpg", genPhotoMd(100, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00"))
			addPhotoAndMockResize(ctx, "folder/DSC_257.jpg", genPhotoMd(200, 3000, "Exposure 2", "Boston", "2020:01:01 15:00:00"))
			makeAllPhotosPublic(ctx)

			// Initial listing of three
			fullTree := true
			request := makeRequest("/folder/", nil, nil, &fullTree)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":     Equal("Exposure 1"),
				"locations": Equal([]interface{}{[]interface{}{"/new-york-city/", "New York City"}}),
				"width":     BeNumerically("==", 100),
				"height":    BeNumerically("==", 2000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))
			Expect(photos[1]).To(MatchAllKeys(Keys{
				"title":     Equal("Exposure 2"),
				"locations": Equal([]interface{}{[]interface{}{"/boston/", "Boston"}}),
				"width":     BeNumerically("==", 200),
				"height":    BeNumerically("==", 3000),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))
		})

		It("Listing by timestamp", func() {
			addPhotoAndMockResize(ctx, "folder/DSC_257.jpg", genPhotoMd(200, 3000, "Exposure 2", "Boston", "2020:01:01 15:00:00"))
			makeAllPhotosPublic(ctx)

			key := "timestamp"
			request := makeRequestWithKey("/folder/", nil, nil, nil, &key, false)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":     Equal("Exposure 2"),
				"locations": Equal([]interface{}{[]interface{}{"/boston/", "Boston"}}),
				"width":     BeNumerically("==", 200),
				"height":    BeNumerically("==", 3000),
				"public":    Equal(true),
				"id":        Ignore(),
				"tsSortKey": Ignore(),
				"storeGen":  Ignore(),
			}))
		})

		It("Listing with more location fields", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "Photo",
					Location: "Park, Toronto, Ontario  , Canada ",
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 16:00:00",
				},
			}

			addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", md)
			makeAllPhotosPublic(ctx)

			request := makeRequest("/folder/", nil, nil, nil)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":     Equal("Photo"),
				"locations": Equal([]interface{}{[]interface{}{"/canada/ontario/toronto/park/", "Canada,Ontario,Toronto,Park"}}),
				"width":     BeNumerically("==", 100),
				"height":    BeNumerically("==", 200),
				"public":    Equal(true),
				"id":        Ignore(),
				"sortKey":   Ignore(),
				"storeGen":  Ignore(),
			}))

		})

		It("Listing with multiple location values", func() {
			md := &photomd.Metadata{
				Width:  100,
				Height: 200,
				Xmp: photomd.XmpData{
					Title:    "Photo",
					Location: "Location1, XYZ",
					Subjects: []string{"!loc:@0:Location2", "!loc:@0:Location3", "!loc:@1:Location4"},
				},
				Exif: photomd.ExifData{
					photomd.ExifTagDateTimeOriginal: "2020:01:01 16:00:00",
				},
			}

			addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", md)
			makeAllPhotosPublic(ctx)

			_, err := db.Locations.Get(ctx, "/xyz/location1/location2/", nil, []string{docs.VisTokenPublic})
			Expect(err).ToNot(HaveOccurred())

			request := makeRequest("/folder/", nil, nil, nil)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title": Equal("Photo"),
				// Should be first location
				"locations": Equal([]interface{}{
					[]interface{}{"/xyz/location1/", "XYZ,Location1"},
				}),
				"width":    BeNumerically("==", 100),
				"height":   BeNumerically("==", 200),
				"public":   Equal(true),
				"id":       Ignore(),
				"sortKey":  Ignore(),
				"storeGen": Ignore(),
			}))

		})

		It("Listing in mappable mode", func() {
			addPhotoAndMockResize(ctx, "folder/subfolder/DSC_256.jpg", addGpsMetadata(genPhotoMd(100, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00")))
			addPhotoAndMockResize(ctx, "folder/DSC_257.jpg", addGpsMetadata(genPhotoMd(200, 3000, "Exposure 2", "Boston", "2020:01:01 15:00:00")))
			makeAllPhotosPublic(ctx)

			// Initial listing of three
			fullTree := true
			request := makeMappingRequest("/folder/", nil, nil, &fullTree)
			respRec := httptest.NewRecorder()

			PhotoList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0]).To(MatchAllKeys(Keys{
				"title":       Equal("Exposure 1"),
				"width":       BeNumerically("==", 100),
				"height":      BeNumerically("==", 2000),
				"gpsLocation": Ignore(),
				"id":          Ignore(),
				"sortKey":     Ignore(),
				"storeGen":    Ignore(),
			}))
			Expect(photos[1]).To(MatchAllKeys(Keys{
				"title":       Equal("Exposure 2"),
				"width":       BeNumerically("==", 200),
				"height":      BeNumerically("==", 3000),
				"gpsLocation": Ignore(),
				"id":          Ignore(),
				"sortKey":     Ignore(),
				"storeGen":    Ignore(),
			}))
		})

		Describe("Listing with some restricted visibility photos", func() {
			var privateActValue string
			var superPrivateActValue string

			BeforeEach(func() {
				addPhotoAndMockResize(ctx, "folder/DSC_256.jpg", genPhotoMd(100, 2000, "Exposure 1", "New York City", "2020:01:01 15:00:00"))
				addPhotoAndMockResize(ctx, "folder/DSC_257.jpg", genPhotoMd(200, 3000, "Exposure 2", "Boston", "2020:01:01 15:00:00", "__visibility: private"))
				addPhotoAndMockResize(ctx, "folder/DSC_258.jpg", genPhotoMd(300, 4000, "Exposure 3", "Washinton", "2020:01:01 15:00:00", "__visibility: super private"))
				makeAllPhotosPublic(ctx)

				link, err := db.Visibility.CreateLink(ctx, "/private/", nil)
				Expect(err).ToNot(HaveOccurred())
				privateActValue = *link.ActivationValue

				link, err = db.Visibility.CreateLink(ctx, "/super-private/", nil)
				Expect(err).ToNot(HaveOccurred())
				superPrivateActValue = *link.ActivationValue

			})

			It("Public", func() {
				request := makeRequest("/folder/", nil, nil, nil)
				respRec := httptest.NewRecorder()

				PhotoList(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				photos := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(1))
				Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 1"),
					"public": Equal(true),
				}))
			})

			It("Vis token: private", func() {
				request := makeRequest("/folder/", nil, nil, nil)
				request.Header.Set("cookie", CookieVisTokenValues+"="+privateActValue)
				respRec := httptest.NewRecorder()

				PhotoList(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				photos := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 1"),
					"public": Equal(true),
				}))
				Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 2"),
					"public": Equal(false),
				}))

			})

			It("Vis token: super private", func() {
				request := makeRequest("/folder/", nil, nil, nil)
				request.Header.Set("cookie", CookieVisTokenValues+"="+superPrivateActValue)
				respRec := httptest.NewRecorder()

				PhotoList(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				photos := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(2))
				Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 1"),
					"public": Equal(true),
				}))
				Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 3"),
					"public": Equal(false),
				}))

			})

			It("Vis token: private + super private", func() {
				request := makeRequest("/folder/", nil, nil, nil)
				request.Header.Set("cookie", CookieVisTokenValues+"="+privateActValue+","+superPrivateActValue)
				respRec := httptest.NewRecorder()

				PhotoList(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				photos := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
				Expect(photos).To(HaveLen(3))
				Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 1"),
					"public": Equal(true),
				}))
				Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 2"),
					"public": Equal(false),
				}))
				Expect(photos[2]).To(MatchKeys(IgnoreExtras, Keys{
					"title":  Equal("Exposure 3"),
					"public": Equal(false),
				}))
			})

		})

	})

	Describe("PhotoRandomMappable", func() {
		md := func(title string, rating int, gps bool) *photomd.Metadata {
			md := genPhotoMd(100, 200, title, "", "2020:01:01 01:00:00")
			md.Xmp.Rating = rating
			if gps {
				addGpsMetadata(md)
			}
			return md
		}
		addPhoto := func(objectID string, md *photomd.Metadata) {
			Expect(db.Photos.AddOrUpdate(ctx, "test-bucket", objectID, 0, 1000, md)).ToNot(BeNil())
		}

		makeRequest := func(count int64, folder *string) *http.Request {
			var params struct {
				Count  int64                    `json:"count"`
				Filter docs.PhotoListFilterExpr `json:"filter"`
			}

			params.Count = count
			if folder != nil {
				params.Filter.Folders = []docs.PhotoListFilterFolder{{Path: *folder}}
			}

			json, err := json.Marshal(params)
			Expect(err).ToNot(HaveOccurred())

			return httptest.NewRequest("POST", "/", bytes.NewReader(json))
		}

		BeforeEach(func() {
			addPair := func(val int) {
				// Filename and title are based on given value.
				s := strconv.Itoa(val)
				// Folder name is based on val/2
				f := strconv.Itoa(val / 2)
				addPhoto("f"+f+"/"+s+".jpg", md(s, val, false))
				addPhoto("f"+f+"/"+s+"gps.jpg", md(s+"gps", val, true))
			}
			for rating := 0; rating <= 5; rating++ {
				addPair(rating)
			}
			makeAllPhotosPublic(ctx)
		})

		It("Random 1", func() {
			request := makeRequest(1, nil)

			respRec := httptest.NewRecorder()

			PhotoRandomMappable(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("5gps"),
			}))
		})

		It("Random 3", func() {
			request := makeRequest(3, nil)

			respRec := httptest.NewRecorder()

			PhotoRandomMappable(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(3))
			Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("5gps"),
			}))
			Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("4gps"),
			}))
			Expect(photos[2]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("3gps"),
			}))
		})

		It("Random 10", func() {
			request := makeRequest(10, nil)

			respRec := httptest.NewRecorder()

			PhotoRandomMappable(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(6))
			Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("5gps"),
			}))
			Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("4gps"),
			}))
			Expect(photos[2]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("3gps"),
			}))
			Expect(photos[3]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("2gps"),
			}))
			Expect(photos[4]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("1gps"),
			}))
			Expect(photos[5]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("0gps"),
			}))
		})

		It("Random 10 - folder f1", func() {
			folder := "/f1/"
			request := makeRequest(10, &folder)

			respRec := httptest.NewRecorder()

			PhotoRandomMappable(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(2))
			Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("3gps"),
			}))
			Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("2gps"),
			}))
		})

		It("Random 3 with many rating 4 photos", func() {
			addPhoto("f1/gps_a.jpg", md("gps_a", 4, true))
			addPhoto("f1/gps_b.jpg", md("gps_b", 4, true))
			addPhoto("f1/gps_c.jpg", md("gps_c", 4, true))
			makeAllPhotosPublic(ctx)

			request := makeRequest(3, nil)

			respRec := httptest.NewRecorder()

			PhotoRandomMappable(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			photos := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &photos)).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(3))
			Expect(photos[0]).To(MatchKeys(IgnoreExtras, Keys{
				"title": Equal("5gps"),
			}))
			Expect(photos[1]).To(MatchKeys(IgnoreExtras, Keys{
				"title": BeElementOf([]string{"4gps", "gps_a", "gps_b", "gps_c"}),
			}))
			Expect(photos[2]).To(MatchKeys(IgnoreExtras, Keys{
				"title": BeElementOf([]string{"4gps", "gps_a", "gps_b", "gps_c"}),
			}))
			Expect(photos[1]["title"]).ToNot(Equal(photos[2]["title"]))
		})
	})
})
