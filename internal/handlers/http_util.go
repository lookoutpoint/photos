// SPDX-License-Identifier: MIT

package handlers

import (
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

func decodeJSONBody(req *http.Request, obj interface{}) error {
	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()
	return dec.Decode(obj)
}

func renderJSON(resp http.ResponseWriter, obj interface{}, l *log.Entry) {
	js, err := json.Marshal(obj)
	if err != nil {
		l.WithError(err).Error("Failed to marshal object to JSON")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.Header().Set("Content-Type", "application/json")
	resp.Write(js)
}

func renderErrorToJSON(resp http.ResponseWriter, err error, l *log.Entry) {
	var msg struct {
		Error string `json:"error"`
	}
	msg.Error = err.Error()

	renderJSON(resp, msg, l)
}

func handlerRecover(resp http.ResponseWriter, l *log.Entry) {
	if r := recover(); r != nil {
		l.Errorf("Recovered from %v - failing request", r)
		resp.WriteHeader(http.StatusInternalServerError)
	}
}
