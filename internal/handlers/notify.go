// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/storage"

	log "github.com/sirupsen/logrus"
)

// NotifyMasterStorageUpdates handles storage notifications from the photo master bucket
func NotifyMasterStorageUpdates(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.NotifyMasterStorageUpdates",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Check authorization
	auth, err := GetOrCreateAuth()
	if err != nil {
		l.WithError(err).Error("Could not get or create auth")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	if !auth.CheckServiceAccountAuthorization(ctx, req) {
		l.Error("Failed service account auth check")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Parse for relevant storage notification details
	var notification struct {
		Message struct {
			Attributes struct {
				EventType        string  `json:"eventType"`
				ObjectID         string  `json:"objectId"`
				ObjectGen        string  `json:"objectGeneration"`
				BucketID         string  `json:"bucketId"`
				OverwrittenByGen *string `json:"overwrittenByGeneration"`
				OverwroteGen     *string `json:"overwroteGeneration"`
			}
		}
	}
	decoder := json.NewDecoder(req.Body)
	if err := decoder.Decode(&notification); err != nil {
		l.WithError(err).Error("Could not decode body")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	attrs := notification.Message.Attributes
	l = l.WithFields(log.Fields{
		"bucketID":         attrs.BucketID,
		"objectID":         attrs.ObjectID,
		"objectGeneration": attrs.ObjectGen,
	})

	if attrs.ObjectID[len(attrs.ObjectID)-1] == '/' {
		// This is a folder, there's nothing to do.
		l.Info("Folder - nothing to do")
		resp.WriteHeader(http.StatusOK)
		return
	}

	switch attrs.EventType {
	case storage.EventFinalize:
		// New or updated object.
		l.Info("Finalize event")

		// Create a task to handle it. The task allows rate limiting to be applied to these events.
		err = createFinalizePhotoTask(attrs.BucketID, attrs.ObjectID, attrs.ObjectGen)
		if err != nil {
			l.WithError(err).Error("Could not create task")
			resp.WriteHeader(http.StatusInternalServerError)
		} else {
			resp.WriteHeader(http.StatusOK)
		}
		return

	case storage.EventDelete:
		// If there is an overwritten by generation field, then don't do anything as the photo will be replaced
		// in a finalize event.
		if attrs.OverwrittenByGen != nil {
			l.Info("Ignoring Delete event as overwritten-by-generation set")
			break
		}

		l.Info("Delete event")

		// Create a task to handle it. The task allows rate limiting to be applied to these events.
		err = createDeletePhotoTask(attrs.BucketID, attrs.ObjectID)
		if err != nil {
			l.WithError(err).Error("Could not create task")
			resp.WriteHeader(http.StatusInternalServerError)
		} else {
			resp.WriteHeader(http.StatusOK)
		}
		return
	}

	// Anything that reaches this point is a no-op and so acknowledge the message
	resp.WriteHeader(http.StatusOK)
}

// NotifyFinalizePhotoTask is the handler for finalizing photo tasks
func NotifyFinalizePhotoTask(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.NotifyFinalizePhotoTask",
		"url":  req.URL,
	})

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 3*time.Minute)
	defer cancel()

	// Some basic validation
	taskName := req.Header.Get("X-Appengine-Taskname")
	if taskName == "" {
		l.Error("Invalid task: No X-Appengine-Taskname request header found")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Extract the request body for further task details.
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		l.WithError(err).Error("Could not read task body")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	task, err := decodeFinalizePhotoTask(body)
	if err != nil {
		l.WithError(err).Error("Could not decode task body")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Translate object generation into an integer.
	objectGen, err := strconv.ParseUint(task.ObjectGen, 10, 64)
	if err != nil {
		l.WithError(err).Error("Could not parse object generation value")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Add/update photo and generate resized versions too
	_, err = database.Get().Photos.AddOrUpdateAndResize(ctx, task.BucketID, task.ObjectID, objectGen)
	if err != nil {
		l.WithError(err).WithField("task", task).Error("Could not add/update and resize photo")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Success!
	resp.WriteHeader(http.StatusOK)
}

// NotifyDeletePhotoTask is the handler for deleting photo tasks
func NotifyDeletePhotoTask(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.NotifyDeletePhotoTask",
		"url":  req.URL,
	})

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 3*time.Minute)
	defer cancel()

	// Some basic validation
	taskName := req.Header.Get("X-Appengine-Taskname")
	if taskName == "" {
		l.Error("Invalid task: No X-Appengine-Taskname request header found")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Extract the request body for further task details.
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		l.WithError(err).Error("Could not read task body")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	task, err := decodeDeletePhotoTask(body)
	if err != nil {
		l.WithError(err).Error("Could not decode task body")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Delete photo
	err = database.Get().Photos.Delete(ctx, task.BucketID, task.ObjectID)
	if err != nil {
		l.WithError(err).WithField("task", task).Error("Could not delete photo")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Success!
	resp.WriteHeader(http.StatusOK)
}
