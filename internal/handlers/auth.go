// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"log"
	"net/http"
	"regexp"
	"strings"

	oidc "github.com/coreos/go-oidc"
	"gitlab.com/lookoutpoint/photos/internal/utils"
)

// Auth interface for authorization checks. This interface is here to allow for testing
// to substitute something different.
type Auth interface {
	CheckServiceAccountAuthorization(ctx context.Context, req *http.Request) bool
}

// The one Auth object
var theAuth Auth = nil

// SetAuth sets the Auth object to use
func SetAuth(auth Auth) {
	theAuth = auth
}

// GetOrCreateAuth gets the Auth object to use. Creates it if it does not exist.
func GetOrCreateAuth() (Auth, error) {
	if theAuth == nil {
		var err error
		theAuth, err = NewAuth()
		return theAuth, err
	}
	return theAuth, nil
}

// The real Auth implementation intended to be used in the cloud
type authImpl struct {
	serviceAccountEmail *regexp.Regexp
	provider            *oidc.Provider
	tokenVerifier       *oidc.IDTokenVerifier
}

var tokenAudience = utils.GetEnv("AUTH_AUDIENCE", "")

// NewAuth creates a new real implementation of the Auth interface
func NewAuth() (Auth, error) {
	var err error
	auth := &authImpl{}

	project := utils.GetCloudProjectID()
	auth.serviceAccountEmail = regexp.MustCompile(`^internal@` + project + `\.iam\.gserviceaccount\.com$`)

	auth.provider, err = oidc.NewProvider(context.Background(), "https://accounts.google.com")
	if err != nil {
		return nil, err
	}

	auth.tokenVerifier = auth.provider.Verifier(&oidc.Config{ClientID: tokenAudience})

	return auth, nil
}

func (auth authImpl) CheckServiceAccountAuthorization(ctx context.Context, req *http.Request) bool {
	// Start with Authorization header
	authHeaderArray, ok := req.Header["Authorization"]
	if !ok {
		log.Printf("Error: Missing Authorization header\nHeaders: %+v", req.Header)
		return false
	}

	if len(authHeaderArray) != 1 {
		log.Printf("Error: More than one Authorization value: %s", authHeaderArray)
		return false
	}

	authHeader := authHeaderArray[0]

	spaceIndex := strings.Index(authHeader, " ")
	if spaceIndex == -1 {
		log.Printf("Error: Unrecognized Authorization header: %s", authHeader)
		return false
	}

	token := authHeader[spaceIndex+1:]

	idToken, err := auth.tokenVerifier.Verify(ctx, token)
	if err != nil {
		log.Printf("Error: failed id token verification (%s)", err)
		return false
	}

	var claims struct {
		Email string
	}
	if err := idToken.Claims(&claims); err != nil {
		log.Printf("Error: failed to get claims from id token (%s)", err)
		return false
	}

	if auth.serviceAccountEmail.FindStringIndex(claims.Email) == nil {
		log.Printf("Error: id token email incorrect (email: %s)", claims.Email)
		return false
	}

	return true
}
