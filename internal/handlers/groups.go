// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"reflect"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/groups"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/visibility"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type groupAPI interface {
	Collection() *mongo.Collection
}

type groupAPIListChildren[G any] interface {
	groupAPI
	ListChildren(ctx context.Context, params *apis.GroupListParams) ([]G, error)
}

type groupWithCoverPhoto struct {
	docs.GroupBase `json:",inline"`
	Public         bool            `json:"public"`
	CoverPhoto     *coverPhotoInfo `json:"coverPhoto,omitempty"`
}

// stdBuildChildResponse builds a child response using the GroupBase
func stdBuildChildResponse(ctx context.Context, child interface{}, public bool, coverPhoto *coverPhotoInfo) (interface{}, error) {
	var response groupWithCoverPhoto
	response.GroupBase = reflect.ValueOf(child).FieldByName("GroupBase").Interface().(docs.GroupBase)
	response.Public = public
	response.CoverPhoto = coverPhoto

	return response, nil
}

// groupChildren returns info about the children of the given group id
func groupChildren(resp http.ResponseWriter, req *http.Request, api groupAPI,
	buildFilter func(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error),
	listChildren func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error),
	buildChildResponse func(ctx context.Context, child interface{}, public bool, coverPhoto *coverPhotoInfo) (interface{}, error),
	getAspectRatioRange func(ctx context.Context, child interface{}) (float32, float32),
) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupChildren",
		"collectionName": api.Collection().Name(),
		"url":            req.URL,
	})
	defer handlerRecover(resp, l) // in case reflect raises an error below

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// The path is in the JSON body payload.
	var reqData struct {
		// Matches fields in GroupListParams
		Limit           int64            `json:"limit,omitempty"`
		RefID           string           `json:"refId,omitempty"`
		RefOp           list.RefOperator `json:"refOp,omitempty"` // defaults to list.OpGt
		ID              string           `json:"id,omitempty"`
		MaxRelDepth     int              `json:"maxRelDepth,omitempty"`
		SortIgnoreDepth bool             `json:"sortIgnoreDepth,omitempty"`

		CoverPhoto bool `json:"coverPhoto"`

		Extension map[string]interface{} `json:"extension,omitempty"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Request visibility tokens.
	visTokens := getVisibilityTokensFromRequest(ctx, req)

	docKeys := []string{"id", "display", "visTokens"}
	if reqData.CoverPhoto {
		docKeys = append(docKeys, "coverPhoto")
	}

	// Query the database for the children.
	children, err := listChildren(ctx, &apis.GroupListParams{
		ID:               reqData.ID,
		Limit:            reqData.Limit,
		RefID:            reqData.RefID,
		RefOp:            reqData.RefOp,
		MaxRelDepth:      reqData.MaxRelDepth,
		SortIgnoreDepth:  reqData.SortIgnoreDepth,
		ProjectionKeys:   docKeys,
		VisibilityTokens: visTokens, // must respect visibility tokens
	}, reqData.Extension)
	if err != nil {
		l.WithError(err).Error("Could not list children")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Children should be a slice where each element contains an embedded GroupBase object
	childrenVal := reflect.ValueOf(children)
	response := make([]interface{}, childrenVal.Len())

	for i := range response {
		childVal := childrenVal.Index(i)
		childBaseVal := childVal.FieldByName("GroupBase")
		childBase := childBaseVal.Interface().(docs.GroupBase)

		public := visibility.IsPublic(childBase.VisTokens)
		childBaseVal.FieldByName("VisTokens").SetLen(0) // don't put vis tokens in the response

		var coverPhoto *coverPhotoInfo
		if reqData.CoverPhoto && buildFilter != nil {
			childFilter, err := buildFilter(ctx, *childBase.ID)
			if err != nil {
				l.WithError(err).WithField("id", *childBase.ID).Error("Could not build filter for child")
				resp.WriteHeader(http.StatusInternalServerError)
				return
			}

			var coverPhotoOpts *coverPhotoOptions
			if getAspectRatioRange != nil {
				min, max := getAspectRatioRange(ctx, childVal.Interface())
				coverPhotoOpts = &coverPhotoOptions{
					MinAspectRatio: min,
					MaxAspectRatio: max,
				}
			}

			coverPhoto, err = getGroupCoverPhotoInfo(
				ctx, api.Collection(), &childBase, childFilter, visTokens, coverPhotoOpts)
			if err != nil {
				// Don't treat as fatal error
				l.WithError(err).WithField("id", *childBase.ID).Warn("Could not get cover photo for child")
			}

			// Don't want to serialize the raw cover photo info
			coverPhotoVal := childBaseVal.FieldByName("CoverPhoto")
			coverPhotoVal.Set(reflect.Zero(coverPhotoVal.Type()))
		}

		response[i], err = buildChildResponse(ctx, childVal.Interface(), public, coverPhoto)
		if err != nil {
			l.WithError(err).WithField("id", *childBase.ID).Error("Could not build response")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	renderJSON(resp, response, l)
}

type groupDeleteAPI interface {
	groupAPI
	Delete(ctx context.Context, id string) error
}

func groupDelete(resp http.ResponseWriter, req *http.Request, api groupDeleteAPI) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupDelete",
		"url":            req.URL,
		"collectionName": api.Collection().Name(),
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		ID string `json:"id"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Delete
	err := api.Delete(ctx, reqData.ID)
	if err != nil {
		l.WithError(err).Error("Could not delete group")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// No data on response
	resp.WriteHeader(http.StatusNoContent)
}

type groupSelfVisTokensAPI[G groups.GroupSelfVisTokensInterface] interface {
	Collection() *mongo.Collection
	Get(ctx context.Context, id string, projKeys []string, tokens []string) (*G, error)
	AddVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
	DeleteVisibilityToken(ctx context.Context, id string, token string, recurse bool) error
}

func groupVisibilityToken[G groups.GroupSelfVisTokensInterface](resp http.ResponseWriter, req *http.Request, api groupSelfVisTokensAPI[G]) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupVisibilityToken",
		"url":            req.URL,
		"collectionName": api.Collection().Name(),
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		Mode    string `json:"mode"`
		ID      string `json:"id"`
		Token   string `json:"token"`
		Recurse bool   `json:"recurse"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)
	db := database.Get()

	// Validate id
	if _, err := api.Get(ctx, reqData.ID, []string{"id"}, []string{docs.VisTokenAll}); err != nil {
		l.Error("Group does not exist")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validate token
	var token string
	if reqData.Token == string(docs.VisTokenPublic) {
		token = docs.VisTokenPublic
	} else {
		tokenDoc, err := db.Visibility.Get(ctx, reqData.Token, []string{"id"})
		if err != nil {
			l.WithError(err).Error("Could not get visibility token")
			resp.WriteHeader(http.StatusBadRequest)
			return
		}
		token = *tokenDoc.ID
	}

	// Do visibility token action.
	if reqData.Mode == "add" {
		if err := api.AddVisibilityToken(ctx, reqData.ID, token, reqData.Recurse); err != nil {
			l.WithError(err).Error("Could not add visibility token")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else if reqData.Mode == "delete" {
		if err := api.DeleteVisibilityToken(ctx, reqData.ID, token, reqData.Recurse); err != nil {
			l.WithError(err).Error("Could not delete visibility token")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		l.Error("Invalid mode")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l.Info("Modified visibility token")
	resp.WriteHeader(http.StatusNoContent)
}

type groupSelection struct {
	Recent []docs.GroupBase
	Other  []docs.GroupBase
}

// A group must have this many public token counts in order to be selected
var homeGroupCountThreshold = 10

// SetHomeGroupCountThreshold changes the threshold (intended for testing)
func SetHomeGroupCountThreshold(t int) {
	homeGroupCountThreshold = t
}

func selectHomeGroups(ctx context.Context, api groupAPI, numRecent int, numTotal int) (groupSelection, error) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.selectHomeGroups",
		"collectionName": api.Collection().Name(),
	})

	var result groupSelection

	proj := bson.M{"display": 1, "coverPhoto": 1}

	// Select recently updated groups (within the past two weeks). Select up to numRecent.
	cursor, err := api.Collection().Aggregate(ctx, bson.A{
		bson.M{"$match": bson.M{
			"_id":                   bson.M{"$ne": "/"}, // never select root
			"descVisTokenCounts.#P": bson.M{"$gte": homeGroupCountThreshold},
			"timestamp":             bson.M{"$gte": time.Now().UTC().Add(-14 * 24 * time.Hour)}, // 2 weeks
		}},
		bson.M{"$project": proj},
		bson.M{"$sample": bson.M{"size": numRecent}},
	})
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline for recent groups")
		return result, err
	}

	if err := cursor.All(ctx, &result.Recent); err != nil {
		l.WithError(err).Error("Could not query cursor for recent groups")
		return result, err
	}

	// Randomly select the rest of the groups.
	recentIDs := []string{"/"} // never select root
	for _, r := range result.Recent {
		recentIDs = append(recentIDs, *r.ID)
	}

	matchFilter := bson.M{
		"_id":                   bson.M{"$not": bson.M{"$in": recentIDs}},
		"descVisTokenCounts.#P": bson.M{"$gte": homeGroupCountThreshold},
	}

	cursor, err = api.Collection().Aggregate(ctx, bson.A{
		bson.M{"$match": matchFilter},
		bson.M{"$project": proj},
		bson.M{"$sample": bson.M{"size": numTotal - len(result.Recent)}},
	})
	if err != nil {
		l.WithError(err).Error("Could not run aggregation pipeline for other groups")
		return result, err
	}

	if err := cursor.All(ctx, &result.Other); err != nil {
		l.WithError(err).Error("Could not query cursor for other groups")
		return result, err
	}

	return result, nil
}

type groupSelectionWithCoverPhoto struct {
	Recent []groupWithCoverPhoto `json:"recent"`
	Other  []groupWithCoverPhoto `json:"other"`
}

func toGroupWithCoverPhoto(ctx context.Context, api groupAPI, groups []docs.GroupBase,
	buildFilter func(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error)) ([]groupWithCoverPhoto, error) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.toGroupSelectionWithCoverPhoto",
		"collectionName": api.Collection().Name(),
	})

	result := make([]groupWithCoverPhoto, 0, len(groups))
	for _, group := range groups {
		filter, err := buildFilter(ctx, *group.ID)
		if err != nil {
			l.WithError(err).Error("Could not build filter")
			return nil, err
		}

		coverPhoto, err := getGroupCoverPhotoInfo(ctx, api.Collection(), &group, filter, []string{docs.VisTokenPublic}, nil)
		if err != nil {
			l.WithError(err).WithField("id", *group.ID).Error("Could not get group cover photo")
			return nil, err
		}

		result = append(result, groupWithCoverPhoto{
			GroupBase:  group,
			Public:     true,
			CoverPhoto: coverPhoto,
		})
	}

	return result, nil
}

// HomeGroups returns a selection of groups for listing on the home page
func HomeGroups(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.HomeGroups",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Request parameters.
	var reqData struct {
		NumRecent int `json:"recent"`
		NumTotal  int `json:"total"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Check limits.
	if reqData.NumRecent > 8 || reqData.NumTotal > 8 || reqData.NumRecent > reqData.NumTotal {
		l.Error("Bad parameters")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	db := database.Get()

	var respData struct {
		Folders    groupSelectionWithCoverPhoto `json:"folders"`
		Locations  groupSelectionWithCoverPhoto `json:"locations"`
		Categories groupSelectionWithCoverPhoto `json:"categories"`
		Keywords   groupSelectionWithCoverPhoto `json:"keywords"`
	}

	processGroup := func(api groupAPI,
		buildFilter func(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error)) (groupSelectionWithCoverPhoto, error) {
		ll := l.WithField("type", api.Collection().Name())

		var result groupSelectionWithCoverPhoto

		groups, err := selectHomeGroups(ctx, api, reqData.NumRecent, reqData.NumTotal)
		if err != nil {
			ll.WithError(err).Error("Could not select groups")
			return result, err
		}

		result.Recent, err = toGroupWithCoverPhoto(ctx, api, groups.Recent, buildFilter)
		if err != nil {
			ll.WithError(err).Error("Could not process recent groups")
			return result, err
		}

		result.Other, err = toGroupWithCoverPhoto(ctx, api, groups.Other, buildFilter)
		if err != nil {
			ll.WithError(err).Error("Could not process other groups")
			return result, err
		}

		return result, nil
	}

	var err error

	respData.Folders, err = processGroup(db.Folders, buildFolderFilter)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData.Locations, err = processGroup(db.Locations, buildLocationFilter)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData.Categories, err = processGroup(db.Categories, buildCategoryFilter)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	respData.Keywords, err = processGroup(db.Keywords, buildKeywordFilter)
	if err != nil {
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	renderJSON(resp, respData, l)
}

type groupUpdateAPI interface {
	groupAPI
	Update(ctx context.Context, id string, update *apis.GroupUpdate) error
}

// groupUpdate updates a group. Must be admin.
func groupUpdate(resp http.ResponseWriter, req *http.Request, api groupUpdateAPI) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupUpdate",
		"collectionName": api.Collection().Name(),
		"url":            req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		ID               string `json:"id"`
		apis.GroupUpdate `json:",inline"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Do update
	err := api.Update(ctx, reqData.ID, &reqData.GroupUpdate)
	if err != nil {
		l.WithError(err).Error("Could not update group")
		if err == apis.ErrInvalidID {
			resp.WriteHeader(http.StatusBadRequest)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Success
	resp.WriteHeader(http.StatusNoContent)
}

// Bit flags for groupInfo `hasPhotos` field
const (
	// Group has photos
	HAS_PHOTOS = 1 << iota
	// Group has highlight photos (rating >= 4)
	HAS_HIGHLIGHT_PHOTOS
	// Group has mappable photos (photos with `gpsLocation` field)
	HAS_MAPPABLE_PHOTOS
)

// groupInfo returns info about the given group path.
//
// The following GroupBase fields are in the response, if they are not empty:
// - display
// - metaDescription
// - richText
// The `get` callback may provide additional fields beyond these.
//
// The following derived fields are also provided:
// - public
// - coverPhoto (if requested)
//
// The following meta-fields are included that provide more information
// about other properties of the group (children, photos):
// - hasChildren
// - hasPhotos: photo bit flags
//
// Admin-only fields:
// - sortOrder
// - visTokens
func groupInfo[G any](resp http.ResponseWriter, req *http.Request, api groupAPIListChildren[G],
	get func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error),
	buildFilter func(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error),
	getAspectRatioRange func(ctx context.Context, doc interface{}) (float32, float32),
) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupInfo",
		"collectionName": api.Collection().Name(),
		"url":            req.URL,
	})
	defer handlerRecover(resp, l) // in case reflect raises an error below

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// The path is in the JSON body payload.
	var reqData struct {
		ID         string `json:"id"`
		CoverPhoto bool   `json:"coverPhoto"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	isAdminSession := IsAdminSession(ctx, req)

	// Request visibility tokens.
	visTokens := getVisibilityTokensFromRequest(ctx, req)

	keys := []string{"id", "display", "visTokens", "metaDescription", "richText"}
	if reqData.CoverPhoto {
		keys = append(keys, "coverPhoto")
	}
	if isAdminSession {
		// For admin, also return sortOrder
		keys = append(keys, "sortOrder")
	}

	// Query
	group, err := get(ctx, reqData.ID, keys, visTokens, isAdminSession)
	if err != nil {
		l.WithError(err).Error("Could not get folder info")
		if err == mongo.ErrNoDocuments {
			resp.WriteHeader(http.StatusNotFound)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Extract GroupBase for further processing
	groupBaseVal := reflect.ValueOf(group).Elem().FieldByName("GroupBase")
	groupBase := groupBaseVal.Interface().(docs.GroupBase)

	// Build response for group
	if !isAdminSession {
		// If not admin, don't put visibility tokens in response
		groupBaseVal.FieldByName("VisTokens").Set(reflect.ValueOf(([]string)(nil)))
	}

	// Cover photo
	var coverPhoto *coverPhotoInfo
	if reqData.CoverPhoto && buildFilter != nil {
		filter, err := buildFilter(ctx, *groupBase.ID)
		if err != nil {
			l.WithError(err).Error("Could not build filter")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}

		var coverPhotoOpts *coverPhotoOptions
		if getAspectRatioRange != nil {
			min, max := getAspectRatioRange(ctx, group)
			coverPhotoOpts = &coverPhotoOptions{
				MinAspectRatio: min,
				MaxAspectRatio: max,
			}
		}

		coverPhoto, err = getGroupCoverPhotoInfo(
			ctx, api.Collection(), &groupBase, filter, visTokens, coverPhotoOpts)
		if err != nil {
			// Don't treat as fatal error
			l.WithError(err).Warn("Could not get cover photo")
		}

		// Don't serialize original cover photo info
		groupBaseVal.FieldByName("CoverPhoto").Set(reflect.ValueOf((*docs.GroupCoverPhoto)(nil)))
	}
	groupBaseVal.FieldByName("ID").Set(reflect.ValueOf((*string)(nil))) // don't serialize id

	// Convert group to JSON and back so that it can be stored in the response
	// struct. The fields in the group are not known, so it's not possible to directly
	// embed the field in the response struct.
	group = reflect.ValueOf(group).Elem().Interface()
	groupJSON, err := json.Marshal(group)
	if err != nil {
		l.WithError(err).Error("Could not json marshal group")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Convert back to a a map to insert the non-GroupBase fields.
	var response map[string]any
	if err := json.Unmarshal(groupJSON, &response); err != nil {
		l.WithError(err).Error("Could not unmarshal")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	response["public"] = visibility.IsPublic(groupBase.VisTokens)
	if coverPhoto != nil {
		response["coverPhoto"] = coverPhoto
	}

	// hasChildren
	children, err := api.ListChildren(ctx, &apis.GroupListParams{
		Limit:            1,
		ID:               reqData.ID,
		VisibilityTokens: visTokens,
	})
	if err != nil {
		l.WithError(err).Error("Could not check for children")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
	response["hasChildren"] = len(children) > 0

	// has*Photos: check for existence of certain photo attributes
	photoFlags := 0
	if buildFilter != nil {
		baseFilter, err := buildFilter(ctx, reqData.ID)
		if err != nil {
			l.WithError(err).Error("Could not build photo filter")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Happy path: has highlight and mappable photo
		db := database.Get()
		photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
			Limit:            1,
			VisibilityTokens: visTokens,
			ExistKeys:        []string{"gpsLocation"},
			Filter: docs.PhotoListFilterExpr{
				Exprs:   []*docs.PhotoListFilterExpr{baseFilter},
				Ratings: []docs.PhotoListFilterRating{{Value: 4, CmpOp: list.CmpGte}},
			},
		})
		if err != nil {
			l.WithError(err).Error("Could not list mappable highlight photo")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
		if len(photos) > 0 {
			photoFlags = HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS | HAS_MAPPABLE_PHOTOS
		} else {
			//  Try just highlight photos.
			photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
				Limit:            1,
				VisibilityTokens: visTokens,
				Filter: docs.PhotoListFilterExpr{
					Exprs:   []*docs.PhotoListFilterExpr{baseFilter},
					Ratings: []docs.PhotoListFilterRating{{Value: 4, CmpOp: list.CmpGte}},
				},
			})
			if err != nil {
				l.WithError(err).Error("Could not list highlight photo")
				resp.WriteHeader(http.StatusInternalServerError)
				return
			}
			if len(photos) > 0 {
				// Has photos and highlights, but no mappable
				photoFlags = HAS_PHOTOS | HAS_HIGHLIGHT_PHOTOS
			} else {
				//  Try just mappable photos.
				photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
					Limit:            1,
					VisibilityTokens: visTokens,
					ExistKeys:        []string{"gpsLocation"},
					Filter:           *baseFilter,
				})
				if err != nil {
					l.WithError(err).Error("Could not list mappable photo")
					resp.WriteHeader(http.StatusInternalServerError)
					return
				}
				if len(photos) > 0 {
					// Has mappable photo, but no highlights
					photoFlags = HAS_PHOTOS | HAS_MAPPABLE_PHOTOS
				} else {
					//  Try just any photos.
					photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
						Limit:            1,
						VisibilityTokens: visTokens,
						Filter:           *baseFilter,
					})
					if err != nil {
						l.WithError(err).Error("Could not list any photo")
						resp.WriteHeader(http.StatusInternalServerError)
						return
					}
					if len(photos) > 0 {
						photoFlags = HAS_PHOTOS
					}
				}
			}
		}
	}
	response["hasPhotos"] = photoFlags

	renderJSON(resp, response, l)
}

// List all empty groups. Must be admin.
func groupListEmpty(resp http.ResponseWriter, req *http.Request, coll *mongo.Collection) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupListEmpty",
		"collectionName": coll.Name(),
		"url":            req.URL,
	})

	// Validate HTTP method
	if req.Method != "GET" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	ids, err := groups.FindEmptyGroups(ctx, coll)
	if err != nil {
		l.WithError(err).Error("Could not find empty groups")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	renderJSON(resp, ids, l)
}

func groupDeleteEmpty(resp http.ResponseWriter, req *http.Request, coll *mongo.Collection) {
	l := log.WithFields(log.Fields{
		"func":           "handlers.groupDeleteEmpty",
		"collectionName": coll.Name(),
		"url":            req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Body should be an array of ids.
	var ids []string
	if err := decodeJSONBody(req, &ids); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	var response struct {
		Deleted    []string `json:"deleted"`
		NotDeleted []string `json:"notDeleted"`
	}
	response.Deleted, response.NotDeleted = groups.DeleteEmptyGroups(ctx, coll, ids)

	renderJSON(resp, response, l)
}
