// SPDX-License-Identifier: MIT

package handlers

import "net/http"

// CookieSessionID is the session id cookie
const CookieSessionID = "sid"

// CookieVisTokenValues is the visibility token values cookie
const CookieVisTokenValues = "vtv"

// For secure cookies, this variable determines the same site mode.
// Default to strict mode unless localhost is allowed (for dev purposes).
var cookieSameSite = func() http.SameSite {
	if AllowLocalhost() {
		return http.SameSiteNoneMode
	}
	return http.SameSiteStrictMode
}()
