// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/users"
)

func setSessionIDCookie(resp http.ResponseWriter, session *docs.Session) {
	http.SetCookie(resp, &http.Cookie{
		Name:     CookieSessionID,
		Value:    string(session.ID),
		MaxAge:   int(users.SessionDuration / time.Second),
		Expires:  session.Expiry,
		Domain:   GetDomain(),
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
		SameSite: cookieSameSite,
	})
}

func deleteSessionIDCookie(resp http.ResponseWriter) {
	http.SetCookie(resp, &http.Cookie{
		Name:     CookieSessionID,
		Value:    "",
		MaxAge:   -1,
		Domain:   GetDomain(),
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
		SameSite: cookieSameSite,
	})
}

// UserAdd adds a new user
func UserAdd(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.UserAdd",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Decode payload
	var reqData struct {
		Name     string `json:"name"`
		Password string `json:"password"`
	}
	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	db := database.Get()

	err := db.Users.AddUser(ctx, reqData.Name, reqData.Password)
	if err != nil {
		l.WithError(err).Error("Could not add new user")

		// For proper multi-user support, we should differentiate between types of errors here.
		// But for now, with only one user supported, no need.
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Login.
	session, err := db.Users.Login(ctx, reqData.Name, reqData.Password)
	if err != nil {
		l.WithError(err).Error("Could not login after creating user")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Set session cookie
	setSessionIDCookie(resp, session)

	resp.WriteHeader(http.StatusOK)
}

// UserLogin attempts to login a user
func UserLogin(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.UserLogin",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Decode payload
	var reqData struct {
		Name     string `json:"name"`
		Password string `json:"password"`
	}
	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	db := database.Get()

	// Login.
	session, err := db.Users.Login(ctx, reqData.Name, reqData.Password)
	if err != nil {
		l.WithError(err).Error("Could not login after creating user")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Set session cookie
	setSessionIDCookie(resp, session)

	resp.WriteHeader(http.StatusOK)
}

// UserLogout ends the active session
func UserLogout(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.UserLogout",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Extract the session id
	cookie, err := req.Cookie(CookieSessionID)
	if err == http.ErrNoCookie {
		resp.WriteHeader(http.StatusOK)
		return
	} else if err != nil {
		l.WithError(err).Warn("Could not extract session id cookie")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Try to end the session
	sessionID := docs.SessionID(cookie.Value)

	db := database.Get()
	err = db.Users.EndSession(ctx, sessionID)
	if err != nil {
		l.WithError(err).Warn("Could not end session")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Clean up step: delete all expired sessions
	err = db.Users.CleanupExpiredSessions(ctx)
	if err != nil {
		// Not a hard error
		l.WithError(err).Warn("Could not cleanup expired sessions")
	}

	// Delete session cookie
	deleteSessionIDCookie(resp)

	resp.WriteHeader(http.StatusOK)
}

// UserSessionRefresh refreshes the active session
func UserSessionRefresh(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.UserSessionRefresh",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Extract the session id
	cookie, err := req.Cookie(CookieSessionID)
	if err != nil {
		// No log message as this happens quite a bit and it's pretty harmless.
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Get the session to check if it is valid
	db := database.Get()

	sessionID := docs.SessionID(cookie.Value)
	session := db.Users.GetSession(ctx, sessionID)
	if session == nil {
		l.WithField("sessionID", sessionID).Error("Invalid session id")
		resp.WriteHeader(http.StatusBadRequest)
		deleteSessionIDCookie(resp)
		return
	}

	// Refresh the session
	err = db.Users.RefreshSession(ctx, sessionID)
	if err != nil {
		l.WithError(err).Error("Could not refresh session")
		resp.WriteHeader(http.StatusInternalServerError)
		deleteSessionIDCookie(resp)
		return
	}

	// Retrieve the updated session
	session = db.Users.GetSession(ctx, sessionID)
	if session == nil {
		l.Error("Could not retrieve updated session")
		resp.WriteHeader(http.StatusInternalServerError)
		deleteSessionIDCookie(resp)
		return
	}

	// Update the session cookie
	setSessionIDCookie(resp, session)

	resp.WriteHeader(http.StatusOK)
}

// GetSessionFromRequest tries to get the active session associated with a request. Uses the session cookie.
func GetSessionFromRequest(ctx context.Context, req *http.Request) (*docs.Session, error) {
	l := log.WithFields(log.Fields{
		"func": "handlers.GetSessionFromRequest",
		"url":  req.URL,
	})

	// Extract the session id from the cookie
	cookie, err := req.Cookie(CookieSessionID)
	if err == http.ErrNoCookie {
		return nil, nil
	} else if err != nil {
		l.WithError(err).Warn("Could not extract session id cookie")
		return nil, err
	}

	// Try to get the session
	db := database.Get()
	return db.Users.GetSession(ctx, docs.SessionID(cookie.Value)), nil
}

// IsAdminSession checks if the session associated with the request belongs to an admin user.
// If there is no session, fallbacks to indicating a non-admin session.
func IsAdminSession(ctx context.Context, req *http.Request) bool {
	l := log.WithFields(log.Fields{
		"func": "handlers.IsAdminSession",
		"url":  req.URL,
	})

	session, err := GetSessionFromRequest(ctx, req)
	if err != nil {
		// Not a fatal error, just treat as if not an admin session.
		l.WithError(err).Warn("Could not get session from request")
		return false
	} else if session == nil {
		// No session, not admin.
		return false
	}

	db := database.Get()
	return db.Users.IsAdmin(ctx, session.UserID)
}
