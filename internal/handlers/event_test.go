// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"net/http/httptest"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Events", func() {
	var db *database.Database
	var ctx context.Context

	var tokensAll = []string{docs.VisTokenAll}

	genPhotoMd := func() *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		db = database.Get()
	})

	Describe("EventAdd", func() {
		var photo1 string
		var photo2 string

		BeforeEach(func() {
			photo1 = string(*addPhotoAndMockResize(ctx, "a/b/photo1.jpg", genPhotoMd()).ID)
			photo2 = string(*addPhotoAndMockResize(ctx, "c/d/photo2.jpg", genPhotoMd()).ID)
			makeAllPhotosPublic(ctx)
		})

		It("Two photos events", func() {
			events := []apis.EventInfo{
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "home"},
				{Event: "photo/view/home", Object: "/photo/" + photo2, Context: "home"},
			}
			payload, _ := json.Marshal(events)
			request := httptest.NewRequest("POST", "/", strings.NewReader(string(payload)))
			respRec := httptest.NewRecorder()

			EventAdd(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			count, err := db.Events.CountOne(ctx, "photo/view/thumb", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

			count, err = db.Events.CountOne(ctx, "photo/view/home", nil, nil, nil, nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(count).To(Equal(1))

		})

		It("Event with date", func() {
			now := time.Now()
			events := []apis.EventInfo{
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "home", Date: &now},
			}
			payload, _ := json.Marshal(events)
			request := httptest.NewRequest("POST", "/", strings.NewReader(string(payload)))
			respRec := httptest.NewRecorder()

			EventAdd(respRec, request)

			// Dates are not allowed via the handler
			Expect(respRec.Code).To(Equal(400))
		})

	})

	Describe("EventCount", func() {
		var photo1 string
		var photo2 string

		makeDate := func(year int, month int, day int) *time.Time {
			t := time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
			return &t
		}

		BeforeEach(func() {
			photo1 = string(*addPhotoAndMockResize(ctx, "a/b/photo1.jpg", genPhotoMd()).ID)
			photo2 = string(*addPhotoAndMockResize(ctx, "c/d/photo2.jpg", genPhotoMd()).ID)
			makeAllPhotosPublic(ctx)

			err := db.Events.Add(ctx, []apis.EventInfo{
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "home", Date: makeDate(2022, 2, 1)},
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "home", Date: makeDate(2022, 2, 2)},
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "home", Date: makeDate(2022, 2, 3)},
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "recent", Date: makeDate(2022, 2, 2)},
				{Event: "photo/view/thumb", Object: "/photo/" + photo1, Context: "recent", Date: makeDate(2022, 2, 3)},
				{Event: "photo/view/lightbox", Object: "/photo/" + photo2, Context: "home/hlphoto/lightbox", Date: makeDate(2022, 4, 3)},
			}, tokensAll)
			Expect(err).ToNot(HaveOccurred())
		})

		It("Non-admin", func() {
			query := apis.EventCountQuery{
				Granularity: apis.EventByMonth,
			}

			payload, _ := json.Marshal(query)
			request := httptest.NewRequest("POST", "/", strings.NewReader(string(payload)))
			respRec := httptest.NewRecorder()

			EventCount(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Complex query", func() {
			// Admin session
			adminSession := createAdminUserAndSession(ctx)

			startDate := time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC)
			event := "^photo/view/"
			object := "^/photo/"
			query := apis.EventCountQuery{
				StartDate:      &startDate,
				Granularity:    apis.EventByMonth,
				Event:          &event,
				EventRegex:     true,
				Object:         &object,
				ObjectRegex:    true,
				GroupByEvent:   true,
				GroupByObject:  true,
				GroupByContext: true,
			}

			payload, _ := json.Marshal(query)
			request := addSessionToRequest(httptest.NewRequest("POST", "/", strings.NewReader(string(payload))), adminSession)
			respRec := httptest.NewRecorder()

			EventCount(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())

			Expect(response).To(HaveLen(3))
			Expect(response[0]).To(MatchAllKeys(Keys{
				"id": MatchAllKeys(Keys{
					"date":    Equal("2022-02-01T00:00:00Z"),
					"event":   Equal("photo/view/thumb"),
					"object":  Equal("/photo/" + photo1),
					"context": Equal("home"),
				}),
				"count": BeNumerically("==", 3),
			}))
			Expect(response[1]).To(MatchAllKeys(Keys{
				"id": MatchAllKeys(Keys{
					"date":    Equal("2022-02-01T00:00:00Z"),
					"event":   Equal("photo/view/thumb"),
					"object":  Equal("/photo/" + photo1),
					"context": Equal("recent"),
				}),
				"count": BeNumerically("==", 2),
			}))
			Expect(response[2]).To(MatchAllKeys(Keys{
				"id": MatchAllKeys(Keys{
					"date":    Equal("2022-04-01T00:00:00Z"),
					"event":   Equal("photo/view/lightbox"),
					"object":  Equal("/photo/" + photo2),
					"context": Equal("home/hlphoto/lightbox"),
				}),
				"count": BeNumerically("==", 1),
			}))
		})
	})

})
