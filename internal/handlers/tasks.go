// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"encoding/json"
	"fmt"

	cloudtasks "cloud.google.com/go/cloudtasks/apiv2"
	"gitlab.com/lookoutpoint/photos/internal/utils"
	taskspb "google.golang.org/genproto/googleapis/cloud/tasks/v2"
)

// Tasks is a wrapper interface around the Google Cloud Tasks client
type Tasks interface {
	CreateTask(ctx context.Context, req *taskspb.CreateTaskRequest) error
}

var theTasks Tasks

// SetTasks sets the task implementation to use
func SetTasks(tasks Tasks) {
	theTasks = tasks
}

// GetTasks returns the task implementation to use
func GetTasks() Tasks {
	return theTasks
}

type tasksImpl struct {
	client *cloudtasks.Client
}

func (tasks tasksImpl) CreateTask(ctx context.Context, req *taskspb.CreateTaskRequest) error {
	_, err := tasks.client.CreateTask(ctx, req)
	return err
}

// NewTasks creates a new real implementation for Tasks
func NewTasks() (Tasks, error) {
	client, err := cloudtasks.NewClient(context.Background())
	if err != nil {
		return nil, err
	}

	return &tasksImpl{client}, nil
}

// photo-notify task queue

var photoNotifyQueueLocationID = utils.GetEnv("PHOTO_NOTIFY_QUEUE_LOC_ID", "")
var photoNotifyQueueID = utils.GetEnv("PHOTO_NOTIFY_QUEUE_ID", "")

var photoNotifyQueuePath = fmt.Sprintf("projects/%s/locations/%s/queues/%s", utils.GetCloudProjectID(), photoNotifyQueueLocationID, photoNotifyQueueID)

type finalizePhotoTask struct {
	BucketID  string `json:"bucketId"`
	ObjectID  string `json:"objectId"`
	ObjectGen string `json:"objectGen"`
}

func createFinalizePhotoTask(bucketID string, objectID string, objectGen string) error {
	// Build the Task payload.
	req := &taskspb.CreateTaskRequest{
		Parent: photoNotifyQueuePath,
		Task: &taskspb.Task{
			MessageType: &taskspb.Task_AppEngineHttpRequest{
				AppEngineHttpRequest: &taskspb.AppEngineHttpRequest{
					HttpMethod: taskspb.HttpMethod_POST,
					// Send to photoadd service
					RelativeUri: "/s/photoadd/notify/finalize-photo",
				},
			},
		},
	}

	// Create the message
	payload := finalizePhotoTask{bucketID, objectID, objectGen}
	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	req.Task.GetAppEngineHttpRequest().Body = body

	// Create and add the task to the queue. Use the background context otherwise may get error about the
	// deadline being too far into the future.
	err = GetTasks().CreateTask(context.Background(), req)
	if err != nil {
		return err
	}

	return nil
}

func decodeFinalizePhotoTask(payload []byte) (*finalizePhotoTask, error) {
	var task finalizePhotoTask
	err := json.Unmarshal(payload, &task)
	if err != nil {
		return nil, err
	}
	return &task, nil
}

type deletePhotoTask struct {
	BucketID string `json:"bucketId"`
	ObjectID string `json:"objectId"`
}

func createDeletePhotoTask(bucketID string, objectID string) error {
	// Build the Task payload.
	req := &taskspb.CreateTaskRequest{
		Parent: photoNotifyQueuePath,
		Task: &taskspb.Task{
			MessageType: &taskspb.Task_AppEngineHttpRequest{
				AppEngineHttpRequest: &taskspb.AppEngineHttpRequest{
					HttpMethod:  taskspb.HttpMethod_POST,
					RelativeUri: "/s/photos/notify/delete-photo",
				},
			},
		},
	}

	// Create the message
	payload := deletePhotoTask{bucketID, objectID}
	body, err := json.Marshal(payload)
	if err != nil {
		return err
	}
	req.Task.GetAppEngineHttpRequest().Body = body

	// Create and add the task to the queue. Use the background context otherwise may get error about the
	// deadline being too far into the future.
	err = GetTasks().CreateTask(context.Background(), req)
	if err != nil {
		return err
	}

	return nil
}

func decodeDeletePhotoTask(payload []byte) (*deletePhotoTask, error) {
	var task deletePhotoTask
	err := json.Unmarshal(payload, &task)
	if err != nil {
		return nil, err
	}
	return &task, nil
}
