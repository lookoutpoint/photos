// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
)

// PagesCreates handles page group creation. Admin only.
func PageCreate(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.PageCreate",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		Name string `json:"name"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusOK)
		renderErrorToJSON(resp, err, l)
		return
	}

	l = l.WithField("req", reqData)
	db := database.Get()

	// Create
	id, err := db.Pages.Create(ctx, reqData.Name)
	if err != nil {
		l.WithError(err).Error("Could not create page")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// On success, render id as result
	renderJSON(resp, id, l)
}

// PageDelete deletes a page group. Admin only.
func PageDelete(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupDelete(resp, req, db.Pages)
}

// PageInfo returns info about the given page group
func PageInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Pages,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			if isAdminSession {
				keys = append(keys, "selfVisTokens")
			}

			return db.Pages.Get(ctx, id, keys, visTokens)
		},
		// No support for cover photo
		nil,
		nil,
	)
}

// PageChildren returns info about the children of the given page group
func PageChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.Pages, nil,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			return db.Pages.ListChildren(ctx, params)
		},
		stdBuildChildResponse,
		nil,
	)
}

// PageUpdate updates a page group. Must be admin.
func PageUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().Pages)
}

// PageVisibilityToken can add or delete a visibility token on a page. Admin only.
func PageVisibilityToken(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupVisibilityToken(resp, req, db.Pages)
}
