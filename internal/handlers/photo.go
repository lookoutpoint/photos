// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"io"
	"net/http"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	"gitlab.com/lookoutpoint/photos/internal/database/photos"
	"gitlab.com/lookoutpoint/photos/internal/database/visibility"
)

func addPhotoCacheHeaders(resp http.ResponseWriter, public bool) {
	if public {
		resp.Header().Add("cache-control", "public, max-age=604800") // 7 days
	} else {
		// Not public, so don't cache publicly. This must be done otherwise AppEngine may cache
		// and return it without coming to the service to check.
		resp.Header().Add("cache-control", "private, max-age=3600") // 1 hour
	}
}

// PhotoResize returns a resized version of a photo
func PhotoResize(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.PhotoResize",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "GET" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// The URL format is: **/<maxLength>/<photoID>/<version>.jpg
	urlParts := strings.Split(req.URL.Path, "/")
	if len(urlParts) < 3 {
		l.Error("Malformed URL")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Check referrer
	referrers := req.Header["Referer"]
	var referrer string
	if len(referrers) > 0 {
		referrer = referrers[0]
	}

	if !ReferrerRegex.MatchString(referrer) {
		l.WithField("referrer", referrer).Error("HOT LINK DETECTED: invalid referrer")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Parse URL
	ourParts := urlParts[len(urlParts)-3:]

	// Max length
	rawMaxLength, err := strconv.ParseUint(ourParts[0], 10, 32)
	if err != nil {
		l.WithError(err).Error("Malformed max length")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}
	maxLength := int(rawMaxLength)

	// Id
	id := docs.PhotoID(ourParts[1])

	// Version - don't actually care about the value, but check the extension
	version := ourParts[2]
	if !strings.HasSuffix(version, ".jpg") {
		l.WithError(err).Error("Invalid suffix")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Request visibility tokens
	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Get the cropped image
	db := database.Get()
	rImg, imgLen, isPublic, err := db.Photos.GetResizedImage(ctx, id, maxLength, visTokens)
	if err != nil {
		if err == photos.ErrResizeImageNotReady {
			// Use 503 as the response to indicate that the image is temporarily not available
			l.WithError(err).Warn("Resized image is not ready")
			resp.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		l.WithError(err).Error("Could not get cropped image")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}
	defer rImg.Close()

	// Form response header
	header := resp.Header()
	header.Add("content-type", "image/jpeg")
	header.Add("content-length", strconv.Itoa(imgLen))
	addPhotoCacheHeaders(resp, isPublic)

	// Copy the image data to the response writer
	_, err = io.Copy(resp, rImg)
	if err != nil {
		l.WithError(err).Error("Could not write image data")
	}
}

// PhotoGetMetadata returns metadata for a photo. For groups, returns both the id and the display string.
func PhotoGetMetadata(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.PhotoGetMetadata",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "GET" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// The URL format is: **/<photoID>
	urlParts := strings.Split(req.URL.Path, "/")
	if len(urlParts) < 1 {
		l.Error("Malformed URL")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Request vis tokens
	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Get photo metadata
	photoID := docs.PhotoID(urlParts[len(urlParts)-1])

	db := database.Get()
	photo, err := db.Photos.Get(ctx, photoID, []string{
		"id", "sortKey", "tsSortKey", "storeGen", "width", "height", "folder", "date", "title", "description", "keywords", "categories",
		"dateTime", "rating", "copyright", "usageTerms", "usageWebUrl", "locations", "gpsLocation", "gpsAltitude",
		"make", "model", "lens", "fNumber", "focalLength", "iso", "exposureTime", "exposureBias", "visTokens",
	}, visTokens)
	if err != nil {
		l.WithError(err).Error("Could not get photo metadata")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	if req.URL.RawQuery == *photo.StoreGen {
		// If the raw query is equal to the storeGen, allow caching
		addPhotoCacheHeaders(resp, visibility.IsPublic(photo.VisTokens))
	}

	type PhotoResponse struct {
		docs.Photo `json:",inline"`
		Folder     [2]string   `json:"folder"`
		Keywords   [][2]string `json:"keywords,omitempty"`
		Categories [][2]string `json:"categories,omitempty"`
		Locations  [][2]string `json:"locations,omitempty"`
		Public     bool        `json:"public"`
	}

	respPhoto := PhotoResponse{}

	// Vis tokens
	respPhoto.Public = visibility.IsPublic(photo.VisTokens)
	photo.VisTokens = nil // don't put this in the response

	// Folder
	folder, err := db.Folders.Get(ctx, *photo.Folder, []string{"display"}, visTokens)
	if err != nil {
		l.WithError(err).Error("Could not get folder")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}
	respPhoto.Folder = [2]string{*photo.Folder, *folder.Display}
	photo.Folder = nil

	// Keywords
	if len(photo.Keywords) > 0 {
		kws, err := db.Keywords.GetMulti(ctx, photo.Keywords, []string{"id", "display"}, visTokens)
		if err != nil || len(kws) != len(photo.Keywords) {
			l.WithError(err).Error("Could not get keywords")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}

		for _, kw := range kws {
			respPhoto.Keywords = append(respPhoto.Keywords, [2]string{*kw.ID, *kw.Display})
		}
		photo.Keywords = nil
	}

	// Categories
	if len(photo.Categories) > 0 {
		cvs, err := db.Categories.GetMulti(ctx, photo.Categories, []string{"id", "display"}, visTokens)
		if err != nil || len(cvs) != len(photo.Categories) {
			l.WithError(err).Error("Could not get categories")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}

		for _, cv := range cvs {
			respPhoto.Categories = append(respPhoto.Categories, [2]string{*cv.ID, *cv.Display})
		}
		photo.Categories = nil
	}

	// Locations
	if photo.Locations != nil {
		// Order matters, so query sequentially.
		for _, locID := range photo.Locations {
			loc, err := db.Locations.Get(ctx, locID, []string{"id", "display"}, visTokens)
			if err != nil {
				l.WithError(err).Error("Could not get location")
				resp.WriteHeader(http.StatusInternalServerError)
				return
			}

			respPhoto.Locations = append(respPhoto.Locations, [2]string{*loc.ID, *loc.Display})
		}

		photo.Locations = nil
	}

	respPhoto.Photo = *photo

	// Render the photo document as JSON
	renderJSON(resp, respPhoto, l)
}

func renderPhotoListResponse(ctx context.Context, resp http.ResponseWriter, photos []docs.Photo, visTokens []string, listKey apis.PhotoListKey, l *log.Entry) {
	db := database.Get()

	// Build response
	type PhotoResponse struct {
		docs.Photo `json:",inline"`
		Locations  [][2]string `json:"locations,omitempty"`
		Public     *bool       `json:"public,omitempty"`
	}

	respPhotos := make([]PhotoResponse, len(photos))
	for i, photo := range photos {
		// If there are no visTokens, then do not set public flag.
		if len(photo.VisTokens) > 0 {
			public := visibility.IsPublic(photo.VisTokens)
			respPhotos[i].Public = &public
			photo.VisTokens = nil // don't put this in the response
		}

		// Sort key. Either turn sortKey or tsSortKey.
		if listKey == apis.PhotoListKeyTimestamp {
			photo.SortKey = nil
		} else {
			photo.TsSortKey = nil
		}

		// Locations: only return the first location in lists.
		if len(photo.Locations) > 0 {
			loc, err := db.Locations.Get(ctx, photo.Locations[0], []string{"display"}, visTokens)
			if err != nil {
				l.WithError(err).Error("Could not get location")
				resp.WriteHeader(http.StatusInternalServerError)
				return
			}
			respPhotos[i].Locations = [][2]string{{photo.Locations[0], *loc.Display}}
			photo.Locations = nil
		}

		respPhotos[i].Photo = photo
	}

	renderJSON(resp, respPhotos, l)
}

// In mapping mode, returns photos with a subset of fields:
// - id
// - sortKey
// - storeGen
// - width
// - height
// - title
// - gpsLocation
var mappingPhotoListProjKeys = []string{"id", "sortKey", "storeGen", "width", "height", "title", "gpsLocation"}

// PhotoList responds with a listing of photos for the given parameters
// In default mode, returns photos with a subset of fields:
// - id
// - one of sortKey/tsSortKey
// - storeGen
// - width
// - height
// - title
// - locations: first location with [locationId, locationDisplay]
// - public
func PhotoList(resp http.ResponseWriter, req *http.Request) {
	const maxListPhotosCount = 1000

	l := log.WithFields(log.Fields{
		"func": "handlers.PhotoList",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Parse list parameters from payload. This is a subset of PhotoListParams.
	var params struct {
		Limit       int64                    `json:"limit,omitempty"`
		RefKey      apis.PhotoListKey        `json:"refKey,omitempty"` // defaults to ListKeySortKey
		RefKeyValue string                   `json:"refKeyValue,omitempty"`
		RefOp       list.RefOperator         `json:"refOp,omitempty"` // defaults to list.OpGt
		Filter      docs.PhotoListFilterExpr `json:"filter"`

		Mapping bool `json:"mapping,omitempty"`
	}

	if err := decodeJSONBody(req, &params); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("params", params)

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	db := database.Get()

	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Build list params
	listParams := apis.PhotoListParams{
		Limit:            params.Limit,
		RefKey:           params.RefKey,
		RefKeyValue:      params.RefKeyValue,
		RefOp:            params.RefOp,
		Filter:           params.Filter,
		VisibilityTokens: visTokens,
	}

	if params.Mapping {
		listParams.ProjectionKeys = mappingPhotoListProjKeys
		listParams.ExistKeys = []string{"gpsLocation"}
	} else {
		listParams.ProjectionKeys = []string{"id", "sortKey", "tsSortKey", "storeGen", "width", "height", "title", "locations", "visTokens"}
	}

	// If there is no limit set, set it to the max.
	if listParams.Limit == 0 {
		listParams.Limit = maxListPhotosCount
	}

	// The limit cannot exceed our capped max
	if listParams.Limit > maxListPhotosCount {
		l.WithField("limit", listParams.Limit).Warn("Request limit higher than cap")
		listParams.Limit = maxListPhotosCount
	}

	// List photos.
	photos, err := db.Photos.List(ctx, &listParams)
	if err != nil {
		l.WithError(err).Error("Could not list photos")
		if err == apis.ErrInvalidListParams {
			resp.WriteHeader(http.StatusBadRequest)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Render response
	renderPhotoListResponse(ctx, resp, photos, visTokens, listParams.RefKey, l)
}

// PhotoRandom responds with a random photo selection for the given parameters
func PhotoRandom(resp http.ResponseWriter, req *http.Request) {
	const maxListPhotosCount = 100

	l := log.WithFields(log.Fields{
		"func": "handlers.PhotoRandom",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Parse list parameters from payload. This is a subset of PhotoRandomListParams.
	var params struct {
		Count  int64                    `json:"count"`
		Filter docs.PhotoListFilterExpr `json:"filter"`
	}

	if err := decodeJSONBody(req, &params); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("params", params)

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	db := database.Get()

	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Build list params
	listParams := apis.PhotoRandomListParams{
		Count:            params.Count,
		Filter:           params.Filter,
		ProjectionKeys:   mappingPhotoListProjKeys,
		VisibilityTokens: visTokens,
	}

	// The limit cannot exceed our capped max
	if listParams.Count > maxListPhotosCount {
		l.WithField("count", listParams.Count).Warn("Request count higher than cap")
		listParams.Count = maxListPhotosCount
	}

	// List random photos in folder.
	photos, err := db.Photos.RandomList(ctx, &listParams)
	if err != nil {
		l.WithError(err).Error("Could not list photos")
		if err == apis.ErrInvalidListParams {
			resp.WriteHeader(http.StatusBadRequest)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Render response
	renderPhotoListResponse(ctx, resp, photos, visTokens, apis.PhotoListKeySortKey, l)
}

// PhotoRandomMappable responds with a random photo selection for the given parameters
// where each photo has a valid gps location. Photos with higher rating will be preferred.
func PhotoRandomMappable(resp http.ResponseWriter, req *http.Request) {
	const maxListPhotosCount = 1000

	l := log.WithFields(log.Fields{
		"func": "handlers.PhotoRandomMappable",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Parse list parameters from payload. This is a subset of PhotoRandomListParams.
	var params struct {
		Count  int64                    `json:"count"`
		Filter docs.PhotoListFilterExpr `json:"filter"`
	}

	if err := decodeJSONBody(req, &params); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("params", params)

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	db := database.Get()

	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Build list params
	baseListParams := apis.PhotoRandomListParams{
		Count:            params.Count,
		Filter:           params.Filter,
		ProjectionKeys:   []string{"id", "sortKey", "storeGen", "width", "height", "title", "gpsLocation", "visTokens"},
		ExistKeys:        []string{"gpsLocation"},
		VisibilityTokens: visTokens,
	}

	// The limit cannot exceed our capped max
	if baseListParams.Count > maxListPhotosCount {
		l.WithField("count", baseListParams.Count).Warn("Request count higher than cap")
		baseListParams.Count = maxListPhotosCount
	}

	// List random photos, starting from rating 5 down to 0 or until we reach the max limit count.
	var photos []docs.Photo
	for curRating := 5; curRating >= 0 && baseListParams.Count > 0; curRating-- {
		// Build list params for the current rating.
		listParams := baseListParams

		// Combine the base filter with the rating filter.
		ratingFilter := docs.PhotoListFilterExpr{
			Exprs:   []*docs.PhotoListFilterExpr{&baseListParams.Filter},
			Ratings: []docs.PhotoListFilterRating{{Value: curRating, CmpOp: list.CmpEq}},
		}
		listParams.Filter = ratingFilter

		rphotos, err := db.Photos.RandomList(ctx, &listParams)
		if err != nil {
			l.WithError(err).Error("Could not list photos")
			if err == apis.ErrInvalidListParams {
				resp.WriteHeader(http.StatusBadRequest)
			} else {
				resp.WriteHeader(http.StatusInternalServerError)
			}
			return
		}

		baseListParams.Count = baseListParams.Count - int64(len(rphotos))
		photos = append(photos, rphotos...)
	}

	// Render response
	renderPhotoListResponse(ctx, resp, photos, visTokens, apis.PhotoListKeySortKey, l)
}
