// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"

	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Pages", func() {
	var ctx context.Context

	tokensAll := []string{docs.VisTokenAll}

	createPage := func(name string) {
		_, err := db.Pages.Create(ctx, name)
		Expect(err).ToNot(HaveOccurred())
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("PageCreate", func() {
		var adminSession *docs.Session

		makeRequest := func(name string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "name": "%v" }`, name))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			request := makeRequest("/page/")
			respRec := httptest.NewRecorder()

			PageCreate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			_, err := db.Pages.Get(ctx, "/page/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
		})

		It("Create root page", func() {
			request := addSessionToRequest(makeRequest(""), adminSession)
			respRec := httptest.NewRecorder()

			PageCreate(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var id string
			Expect(json.Unmarshal(readResponseBody(respRec), &id)).ToNot(HaveOccurred())
			Expect(id).To(Equal("/"))

			Expect(db.Pages.Get(ctx, "/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("")),
			}))
		})

		It("Create page", func() {
			request := addSessionToRequest(makeRequest("/page/"), adminSession)
			respRec := httptest.NewRecorder()

			PageCreate(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var id string
			Expect(json.Unmarshal(readResponseBody(respRec), &id)).ToNot(HaveOccurred())
			Expect(id).To(Equal("/page/"))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("page")),
			}))
		})

	})

	Describe("PageDelete", func() {
		var adminSession *docs.Session

		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createPage("/page/")

			// Delete
			request := makeRequest("/page/")
			respRec := httptest.NewRecorder()

			PageDelete(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			_, err := db.Pages.Get(ctx, "/page/", nil, tokensAll)
			Expect(err).ToNot(HaveOccurred())
		})

		It("Delete", func() {
			createPage("/page/")

			// Delete
			request := addSessionToRequest(makeRequest("/page/"), adminSession)
			respRec := httptest.NewRecorder()

			PageDelete(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			_, err := db.Pages.Get(ctx, "/page/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
		})

	})

	Describe("PageInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		It("With rich text", func() {
			createPage("/page/")
			Expect(db.Pages.AddVisibilityToken(ctx, "/page/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.Update(ctx, "/page/", apis.NewGroupUpdate().SetRichText("Hello world"))).ToNot(HaveOccurred())

			request := makeRequest("/page/")
			respRec := httptest.NewRecorder()

			PageInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("page"),
				"public":      Equal(true),
				"richText":    Equal("Hello world"),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))

		})

	})

	Describe("PageChildren", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			createPage("/page/a/")
			createPage("/page/c/")
			createPage("/page/b/")
			Expect(db.Pages.AddVisibilityToken(ctx, "/page/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())

		})

		It("Non-public groups", func() {
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/page/b/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.Pages.DeleteVisibilityToken(ctx, "/page/a/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

			request := makeRequest("/page/")
			respRec := httptest.NewRecorder()

			PageChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/page/c/"),
				"display": Equal("page/c"),
				"public":  Equal(true),
			}))

		})

		It("All public groups", func() {
			request := makeRequest("/page/")
			respRec := httptest.NewRecorder()

			PageChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(3))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/page/a/"),
				"display": Equal("page/a"),
				"public":  Equal(true),
			}))
			Expect(response[1]).To(MatchAllKeys(Keys{
				"id":      Equal("/page/b/"),
				"display": Equal("page/b"),
				"public":  Equal(true),
			}))
			Expect(response[2]).To(MatchAllKeys(Keys{
				"id":      Equal("/page/c/"),
				"display": Equal("page/c"),
				"public":  Equal(true),
			}))

		})
	})

	Describe("Update", func() {
		var adminSession *docs.Session

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createPage("/page/")

			request := makeRequest("/page/", "Awesome Page")
			respRec := httptest.NewRecorder()

			PageUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("page"))}),
			)
		})

		It("Update display path", func() {
			createPage("/page/")

			// With admin
			request := addSessionToRequest(makeRequest("/page/", "Awesome Page"), adminSession)
			respRec := httptest.NewRecorder()

			PageUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("Awesome Page"))}),
			)
		})
	})

	Describe("VisibilityTokens", func() {
		var adminSession *docs.Session
		var tokenT string

		makeRequest := func(mode string, id string, token string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "mode": "%v", "id": "%v", "token": "%v" }`, mode, id, token))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)

			var err error
			tokenT, err = db.Visibility.Create(ctx, "t")
			Expect(err).ToNot(HaveOccurred())
		})

		It("No admin", func() {
			createPage("/page/")

			request := makeRequest("add", "/page/", tokenT)
			respRec := httptest.NewRecorder()

			PageVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
				}),
			)

		})

		It("Add and delete", func() {
			createPage("/page/")

			// With admin: add
			request := addSessionToRequest(makeRequest("add", "/page/", tokenT), adminSession)
			respRec := httptest.NewRecorder()

			PageVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, tokenT),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(tokenT),
				}),
			)

			// With admin: delete
			request = addSessionToRequest(makeRequest("delete", "/page/", tokenT), adminSession)
			respRec = httptest.NewRecorder()

			PageVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Pages.Get(ctx, "/page/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
				}),
			)

		})

	})

})
