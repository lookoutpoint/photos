// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
)

func EventAdd(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.EventAdd",
		"url":  req.URL,
	})

	db := database.Get()

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Request visibility tokens
	visTokens := getVisibilityTokensFromRequest(ctx, req)

	// Decode request
	var events []apis.EventInfo

	if err := decodeJSONBody(req, &events); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Dates are not allowed to be specified from this API endpoint.
	for _, event := range events {
		if event.Date != nil {
			l.Debugf("%+v", event)
			l.Error("Date provided with event")
			resp.WriteHeader(http.StatusBadRequest)
			return
		}
	}

	// Add events
	err := db.Events.Add(ctx, events, visTokens)
	if err != nil {
		l.WithError(err).Error("Could not add events")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusNoContent)
}

// Query event counts. Admin only.
func EventCount(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.EventCount",
		"url":  req.URL,
	})

	db := database.Get()

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var query apis.EventCountQuery

	if err := decodeJSONBody(req, &query); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Query
	res, err := db.Events.Count(ctx, query)
	if err != nil {
		l.WithError(err).Error("Could not count events")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	renderJSON(resp, res, l)
}
