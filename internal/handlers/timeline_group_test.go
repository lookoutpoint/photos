// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"

	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Timeline Groups", func() {
	var ctx context.Context

	tokensAll := []string{docs.VisTokenAll}

	createTimelineGroup := func(folderID string, name string, filter *docs.PhotoListFilterExpr) {
		_, err := db.TimelineGroups.Create(ctx, folderID, name, filter)
		Expect(err).ToNot(HaveOccurred())
	}

	genPhotoMd := func(title string, keywords []string, order int) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: fmt.Sprintf("2020:01:01 01:%02d:03", order),
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("TimelineGroupCreate", func() {
		var adminSession *docs.Session

		makeRequest := func(folder string, name string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "folder": "%v", "name": "%v" }`, folder, name))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			adminSession = createAdminUserAndSession(ctx)

			_, err := db.Folders.Create(ctx, "/folder/")
			Expect(err).ToNot(HaveOccurred())
		})

		It("No admin", func() {
			request := makeRequest("/folder/", "group")
			respRec := httptest.NewRecorder()

			TimelineGroupCreate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			_, err := db.TimelineGroups.Get(ctx, "/:folder:/group/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
		})

		It("Create group with no filter", func() {
			request := addSessionToRequest(makeRequest("/folder/", "group"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupCreate(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var id string
			Expect(json.Unmarshal(readResponseBody(respRec), &id)).ToNot(HaveOccurred())
			Expect(id).To(Equal("/:folder:/group/"))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/group/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.Display": PointTo(Equal("/folder/|group")),
			}))
		})

	})

	Describe("TimelineGroupSync", func() {
		var adminSession *docs.Session

		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "group", nil)

			// Sync
			request := makeRequest("/:folder:/group/")
			respRec := httptest.NewRecorder()

			TimelineGroupSync(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Sync", func() {
			createTimelineGroup("/folder/", "group", nil)

			addPhotoAndMockResize(ctx, "folder/photo1.jpg", genPhotoMd("title", []string{"zebra"}, 0))

			// Sync
			request := addSessionToRequest(makeRequest("/:folder:/group/"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupSync(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			date := time.Date(2020, 1, 1, 1, 0, 3, 0, time.UTC)
			Expect(db.TimelineGroups.Get(ctx, "/:folder:/group/", nil, tokensAll)).To(MatchGroupBasePtr(Fields{
				"GroupBase.SortOrder": PointTo(BeEquivalentTo(date.Unix() * 1000)),
				"StartDate":           PointTo(Equal(date)),
				"EndDate":             PointTo(Equal(date)),
			}))
		})

	})

	Describe("TimelineGroupDelete", func() {
		var adminSession *docs.Session

		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "group", nil)

			// Delete
			request := makeRequest("/:folder:/group/")
			respRec := httptest.NewRecorder()

			TimelineGroupDelete(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			_, err := db.TimelineGroups.Get(ctx, "/:folder:/group/", nil, tokensAll)
			Expect(err).ToNot(HaveOccurred())
		})

		It("Delete", func() {
			createTimelineGroup("/folder/", "group", nil)

			// Delete
			request := addSessionToRequest(makeRequest("/:folder:/group/"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupDelete(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			_, err := db.TimelineGroups.Get(ctx, "/:folder:/group/", nil, tokensAll)
			Expect(err).To(HaveOccurred())
		})

	})

	Describe("TimelineGroupInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Timeline group on folder", func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", []string{"animal: cat"}, 0))
			makeAllPhotosPublic(ctx)

			createTimelineGroup("/folder/", "", nil)
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

			// With cover photo
			request := makeRequestCoverPhoto("/:folder:/")
			respRec := httptest.NewRecorder()

			TimelineGroupInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("/folder/"),
				"public":  Equal(true),
				"filter": MatchAllKeys(Keys{
					"folders": ConsistOf(MatchAllKeys(Keys{
						"path":     Equal("/folder/"),
						"fullTree": Equal(true),
					})),
				}),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/:folder:/")
			respRec = httptest.NewRecorder()

			TimelineGroupInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("/folder/"),
				"public":  Equal(true),
				"filter": MatchAllKeys(Keys{
					"folders": ConsistOf(MatchAllKeys(Keys{
						"path":     Equal("/folder/"),
						"fullTree": Equal(true),
					})),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})

		It("Admin with no filter", func() {
			createTimelineGroup("/folder/", "", nil)

			// With admin
			adminSession := createAdminUserAndSession(ctx)
			request := addSessionToRequest(makeRequestCoverPhoto("/:folder:/"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("/folder/"),
				"public":  Equal(false),
				"filter": MatchAllKeys(Keys{
					"folders": ConsistOf(MatchAllKeys(Keys{
						"path":     Equal("/folder/"),
						"fullTree": Equal(true),
					})),
				}),
				"visTokens":       ConsistOf(docs.VisTokenAll),
				"sortOrder":       Equal("/folder/"),
				"unwrappedFilter": BeNil(),
				"hasChildren":     Equal(false),
				"hasPhotos":       BeEquivalentTo(0),
			}))
		})

		It("Admin with custom filter", func() {
			createTimelineGroup("/folder/", "", &docs.PhotoListFilterExpr{Keywords: []string{"/a/"}})

			// With admin
			adminSession := createAdminUserAndSession(ctx)
			request := addSessionToRequest(makeRequestCoverPhoto("/:folder:/"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("/folder/"),
				"public":  Equal(false),
				"filter": MatchAllKeys(Keys{
					"exprs": ConsistOf(
						MatchAllKeys(Keys{
							"folders": ConsistOf(MatchAllKeys(Keys{
								"path":     Equal("/folder/"),
								"fullTree": Equal(true),
							})),
						}),
						MatchAllKeys(Keys{
							"keywords": ConsistOf("/a/"),
						}),
					)}),
				"visTokens": ConsistOf(docs.VisTokenAll),
				"sortOrder": Equal("/folder/"),
				"unwrappedFilter": MatchAllKeys(Keys{
					"keywords": ConsistOf("/a/"),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))
		})

		It("With rich text", func() {
			createTimelineGroup("/folder/", "", nil)
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.Update(ctx, "/:folder:/", apis.NewGroupUpdate().SetRichText("Hello world"))).ToNot(HaveOccurred())

			request := makeRequest("/:folder:/")
			respRec := httptest.NewRecorder()

			TimelineGroupInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":  Equal("/folder/"),
				"public":   Equal(true),
				"richText": Equal("Hello world"),
				"filter": MatchAllKeys(Keys{
					"folders": ConsistOf(MatchAllKeys(Keys{
						"path":     Equal("/folder/"),
						"fullTree": Equal(true),
					})),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(0),
			}))

		})

	})

	Describe("TimelineGroupChildren", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Intentionally switch the order for testing
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", []string{"tree"}, 1))
			addPhotoAndMockResize(ctx, "folder/photo2.jpg", genPhotoMd("title", []string{"zebra"}, 0))
			makeAllPhotosPublic(ctx)

			createTimelineGroup("/folder/", "Trees", &docs.PhotoListFilterExpr{Keywords: []string{"/tree/"}})
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/trees/"))
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/trees/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

			createTimelineGroup("/folder/", "Zebras", &docs.PhotoListFilterExpr{Keywords: []string{"/zebra/"}})
			Expect(db.TimelineGroups.Sync(ctx, "/:folder:/zebras/"))
			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:folder:/zebras/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

		})

		It("Non-public groups", func() {
			Expect(db.TimelineGroups.DeleteVisibilityToken(ctx, "/:folder:/trees/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())
			Expect(db.TimelineGroups.DeleteVisibilityToken(ctx, "/:folder:/zebras/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

			request := makeRequest("/:folder:/")
			respRec := httptest.NewRecorder()

			TimelineGroupChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(0))

		})

		It("Public groups with cover photos", func() {
			request := makeRequest("/:folder:/")
			respRec := httptest.NewRecorder()

			TimelineGroupChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(2))

			date0 := time.Date(2020, 1, 1, 1, 0, 3, 0, time.UTC).Format(time.RFC3339)
			date1 := time.Date(2020, 1, 1, 1, 1, 3, 0, time.UTC).Format(time.RFC3339)

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":        Equal("/:folder:/zebras/"),
				"display":   Equal("/folder/|Zebras"),
				"public":    Equal(true),
				"startDate": Equal(date0),
				"endDate":   Equal(date0),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("fXfHtUbzuIEVeS-ZYrD4NAr6kjE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))
			Expect(response[1]).To(MatchAllKeys(Keys{
				"id":        Equal("/:folder:/trees/"),
				"display":   Equal("/folder/|Trees"),
				"public":    Equal(true),
				"startDate": Equal(date1),
				"endDate":   Equal(date1),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

		})
	})

	Describe("Update", func() {
		var adminSession *docs.Session

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "", nil)

			request := makeRequest("/:folder:/", "Awesome Timeline")
			respRec := httptest.NewRecorder()

			TimelineGroupUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("/folder/"))}),
			)
		})

		It("Update display path", func() {
			createTimelineGroup("/folder/", "", nil)

			// With admin
			request := addSessionToRequest(makeRequest("/:folder:/", "Awesome Timeline"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("Awesome Timeline"))}),
			)
		})
	})

	Describe("UpdateFilter", func() {
		var adminSession *docs.Session

		makeRequest := func(id string, filter *docs.PhotoListFilterExpr) *http.Request {
			var payload struct {
				ID     string                    `json:"id"`
				Filter *docs.PhotoListFilterExpr `json:"filter"`
			}
			payload.ID = id
			payload.Filter = filter

			jsonb, err := json.Marshal(payload)
			Expect(err).ToNot(HaveOccurred())

			return httptest.NewRequest("POST", "/", bytes.NewReader(jsonb))
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "", nil)

			filter := &docs.PhotoListFilterExpr{Keywords: []string{"/a/", "/b/"}}
			request := makeRequest("/:folder:/", filter)
			respRec := httptest.NewRecorder()

			TimelineGroupUpdateFilter(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
						"Exprs":   BeNil(),
						"Folders": HaveLen(1), // default filter
					})),
				}),
			)
		})

		It("Update filter", func() {
			createTimelineGroup("/folder/", "", nil)

			// With admin
			filter := &docs.PhotoListFilterExpr{Keywords: []string{"/a/", "/b/"}}
			request := addSessionToRequest(makeRequest("/:folder:/", filter), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupUpdateFilter(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"Filter": PointTo(MatchFields(IgnoreExtras, Fields{
						"Exprs": ConsistOf(
							Equal(filter),
							Ignore(), // default folder filter
						),
					})),
				}),
			)
		})

	})

	Describe("Update rich text", func() {
		var adminSession *docs.Session

		makeRequest := func(id string, richText string) *http.Request {
			var payload struct {
				ID       string `json:"id"`
				RichText string `json:"richText"`
			}
			payload.ID = id
			payload.RichText = richText

			jsonb, err := json.Marshal(payload)
			Expect(err).ToNot(HaveOccurred())

			return httptest.NewRequest("POST", "/", bytes.NewReader(jsonb))
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "", nil)

			request := makeRequest("/:folder:/", "Hello world")
			respRec := httptest.NewRecorder()

			TimelineGroupUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.RichText": BeNil(),
				}),
			)
		})

		It("Update rich text", func() {
			createTimelineGroup("/folder/", "", nil)

			// With admin
			request := addSessionToRequest(makeRequest("/:folder:/", "Hello world"), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.RichText": PointTo(Equal("Hello world")),
				}),
			)
		})

	})

	Describe("VisibilityTokens", func() {
		var adminSession *docs.Session
		var tokenT string

		makeRequest := func(mode string, id string, token string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "mode": "%v", "id": "%v", "token": "%v" }`, mode, id, token))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)

			var err error
			tokenT, err = db.Visibility.Create(ctx, "t")
			Expect(err).ToNot(HaveOccurred())
		})

		It("No admin", func() {
			createTimelineGroup("/folder/", "", nil)

			request := makeRequest("add", "/:folder:/", tokenT)
			respRec := httptest.NewRecorder()

			TimelineGroupVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
				}),
			)

		})

		It("Add and delete", func() {
			createTimelineGroup("/folder/", "", nil)

			// With admin: add
			request := addSessionToRequest(makeRequest("add", "/:folder:/", tokenT), adminSession)
			respRec := httptest.NewRecorder()

			TimelineGroupVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll, tokenT),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(tokenT),
				}),
			)

			// With admin: delete
			request = addSessionToRequest(makeRequest("delete", "/:folder:/", tokenT), adminSession)
			respRec = httptest.NewRecorder()

			TimelineGroupVisibilityToken(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.TimelineGroups.Get(ctx, "/:folder:/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{
					"GroupBase.VisTokens":                ConsistOf(docs.VisTokenAll),
					"SelfVisTokensWrapper.SelfVisTokens": ConsistOf(),
				}),
			)

		})

	})

})
