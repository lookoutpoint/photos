// SPDX-License-Identifier: MIT

package handlers_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = It("Referrer checks", func() {
	Expect(ReferrerRegex.MatchString("")).To(BeTrue())

	// photos.local is the test domain
	Expect(ReferrerRegex.MatchString("https://photos.local/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://photos.local/blogs/")).To(BeTrue())

	Expect(ReferrerRegex.MatchString("https://google.com/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://google.ca/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://google.ca")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://images.google.com/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://www.google.com/")).To(BeTrue())

	Expect(ReferrerRegex.MatchString("https://bing.com/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://yahoo.com/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("https://baidu.com/")).To(BeTrue())

	Expect(ReferrerRegex.MatchString("https://random.com/")).To(BeFalse())
	Expect(ReferrerRegex.MatchString("https://random.com/subpage/")).To(BeFalse())

	Expect(ReferrerRegex.MatchString("http://localhost/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("http://localhost:3000/")).To(BeTrue())
	Expect(ReferrerRegex.MatchString("http://localhost:3000/subpage/")).To(BeTrue())
})
