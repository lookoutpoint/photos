// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"encoding/xml"
	"io"
	"net/http"
	"sort"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
	dbutils "gitlab.com/lookoutpoint/photos/internal/database/utils"
	"gitlab.com/lookoutpoint/photos/internal/utils"
	"gitlab.com/lookoutpoint/storage"
)

type sitemapURL struct {
	XMLName xml.Name `xml:"url"`
	Loc     string   `xml:"loc"`
}

type sitemapUrlSet struct {
	XMLName xml.Name `xml:"urlset"`
	Xmlns   string   `xml:"xmlns,attr"`

	URLs []sitemapURL
}

var sitemapBucketID = utils.GetCloudProjectID() + ".appspot.com"

const sitemapObjectID = "sitemap.xml"

func newSiteMap() *sitemapUrlSet {
	return &sitemapUrlSet{
		Xmlns: "http://www.sitemaps.org/schemas/sitemap/0.9",
		URLs:  []sitemapURL{},
	}
}

func (sitemap *sitemapUrlSet) AddUrl(url string) {
	sitemap.URLs = append(sitemap.URLs, sitemapURL{Loc: "https://" + GetDomain() + "/" + url})
}

// Interface needed for sorting below. Intended to match GroupBase.
type GroupIntf interface {
	GetID() string
	GetRichText() *string
}

// SortByReverseId - sorter to sort documents by reverse ID order
type SortByReverseId[T GroupIntf] []T

func (s SortByReverseId[T]) Len() int {
	return len(s)
}
func (s SortByReverseId[T]) Less(i, j int) bool {
	return strings.Compare(s[j].GetID(), s[i].GetID()) < 0
}
func (s SortByReverseId[T]) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Returns the default view for the given doc at index `i“. It is assumed that `docs`
// is sorted by id in reverse order. If `doc[i]` has children, then the preceding document's
// id should start with the same id.
func getDefaultView[T GroupIntf](docs []T, i int) string {
	hasChildren := false
	if i > 0 {
		hasChildren = strings.HasPrefix(docs[i-1].GetID(), docs[i].GetID())
	}
	if hasChildren {
		return "children"
	} else {
		return "photos"
	}
}

// If the group has rich text, it has an info view.
func getInfoView[T GroupIntf](doc T) string {
	if doc.GetRichText() != nil {
		return "info"
	}
	return ""
}

// Add url with views. Skips views that are empty strings.
func addUrlWithViews(sitemap *sitemapUrlSet, url string, views ...string) {
	for _, view := range views {
		if len(view) == 0 {
			continue
		}

		sitemap.AddUrl(url + "@" + view + "/")
	}
}

// Generate the sitemap. The urls are based on the frontend urls.
func generateSitemapsImpl(ctx context.Context) error {
	l := log.WithField("func", "generateSitemapsImpl")

	db := database.Get()

	sitemap := newSiteMap()
	sitemap.AddUrl("")
	sitemap.AddUrl("collections/@children/") // frontend uses collections instead of folders
	sitemap.AddUrl("keywords/@children/")
	sitemap.AddUrl("categories/@children/")
	sitemap.AddUrl("locations/@children/")
	sitemap.AddUrl("dates/@children/")
	sitemap.AddUrl("recent/")
	sitemap.AddUrl("blogs/")

	// Folders
	folders, err := db.Folders.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id", "richText"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list folders")
		return err
	}

	sort.Sort(SortByReverseId[docs.Folder](folders))
	for i, folder := range folders {
		addUrlWithViews(sitemap, "collection"+*folder.ID, getDefaultView(folders, i), getInfoView(folder))

		// Photos (directly) in each folder
		photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
			Filter: docs.PhotoListFilterExpr{
				Folders: []docs.PhotoListFilterFolder{
					{Path: *folder.ID},
				},
				Ratings: []docs.PhotoListFilterRating{
					{Value: 4, CmpOp: list.CmpGte},
				},
			},
			ProjectionKeys:   []string{"id", "title"},
			VisibilityTokens: []string{docs.VisTokenPublic},
		})
		if err != nil {
			l.WithError(err).Error("Could not list folder photos")
			return err
		}

		for _, photo := range photos {
			photoUrl := "collection" + *folder.ID + "@photos/"
			if photo.Title != nil {
				photoUrl += dbutils.SlugifyToken(*photo.Title) + "/"
			}
			photoUrl += string(*photo.ID)

			sitemap.AddUrl(photoUrl)
		}
	}

	// Keywords
	keywords, err := db.Keywords.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id", "richText"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list keywords")
		return err
	}

	for _, kw := range keywords {
		addUrlWithViews(sitemap, "keyword"+*kw.ID, "photos", getInfoView(kw))
	}

	// Categories
	categories, err := db.Categories.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id", "richText"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list categories")
		return err
	}

	sort.Sort(SortByReverseId[docs.CategoryEntry](categories))
	for i, cat := range categories {
		addUrlWithViews(sitemap, "category"+*cat.ID, getDefaultView(categories, i), getInfoView(cat))
	}

	// Locations
	locations, err := db.Locations.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id", "richText"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list locations")
		return err
	}

	sort.Sort(SortByReverseId[docs.Location](locations))
	for i, loc := range locations {
		addUrlWithViews(sitemap, "location"+*loc.ID, getDefaultView(locations, i), getInfoView(loc))
	}

	// Dates
	dates, err := db.Dates.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id", "richText"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list dates")
		return err
	}

	sort.Sort(SortByReverseId[docs.Date](dates))
	for i, date := range dates {
		addUrlWithViews(sitemap, "date"+*date.ID, getDefaultView(dates, i), getInfoView(date))
	}

	// Timeline groups
	timelineGroups, err := db.TimelineGroups.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list timeline groups")
		return err
	}

	for _, group := range timelineGroups {
		// Get folder id
		id := *group.ID
		sep := strings.Index(id[1:], "/") + 1

		folderID := id[1:sep]
		folderID = strings.ReplaceAll(folderID, ":", "/")

		url := "collection" + folderID + "@timeline/" + id[sep+1:]
		sitemap.AddUrl(url)
	}

	// Pages
	pages, err := db.Pages.ListChildren(ctx, &apis.GroupListParams{
		ID:               "/",
		MaxRelDepth:      -1,
		ProjectionKeys:   []string{"id"},
		VisibilityTokens: []string{docs.VisTokenPublic},
	})
	if err != nil {
		l.WithError(err).Error("Could not list pages")
		return err
	}

	for _, page := range pages {
		id := *page.ID
		// Drop leading '/'
		sitemap.AddUrl(id[1:])
	}

	// Convert to XML
	sitemapBytes, err := xml.Marshal(sitemap)
	if err != nil {
		l.WithError(err).Error("Could not marshal sitemap to xml")
		return err
	}

	// Write output
	stg := storage.Get()
	w, err := stg.WriterForObject(ctx, sitemapBucketID, sitemapObjectID)
	if err != nil {
		l.WithError(err).Error("Could not get writer to sitemap")
		return err
	}
	defer w.Close()

	if _, err := w.Write([]byte(xml.Header)); err != nil {
		l.WithError(err).Error("Could not write sitemap xml header")
		return err
	}

	if _, err := w.Write(sitemapBytes); err != nil {
		l.WithError(err).Error("Could not write sitemap")
		return err
	}

	return nil
}

// SitemapGenerate generates the sitemap. Must be admin.
func SitemapGenerate(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.SitemapGenerate",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 5*time.Minute)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Generate sitemaps.
	if err := generateSitemapsImpl(ctx); err != nil {
		l.WithError(err).Error("Could not generate sitemap")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// No data on response
	resp.WriteHeader(http.StatusNoContent)
}

// SitemapGet retrieves the sitemap. Must have been generated.
func SitemapGet(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.SitemapGet",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "GET" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Get from storage
	stg := storage.Get()
	r, err := stg.ReaderForObject(ctx, sitemapBucketID, sitemapObjectID)
	if err != nil {
		l.WithError(err).Error("Could not get reader to sitemap")
		resp.WriteHeader(http.StatusNotFound)
		return
	}
	defer r.Close()

	resp.Header().Set("cachecontrol", "no-cache")

	_, err = io.Copy(resp, r)
	if err != nil {
		l.WithError(err).Error("Could not write sitemap")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

}
