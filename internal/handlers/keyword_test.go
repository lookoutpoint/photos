// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Keywords", func() {
	var ctx context.Context

	genPhotoMd := func(title string, keywords []string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:01 01:02:03",
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("KeywordInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Non-existent keyword", func() {
			request := makeRequest("/cat/")
			respRec := httptest.NewRecorder()

			KeywordInfo(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Keyword with no special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"cat"}))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/cat/")
			respRec := httptest.NewRecorder()

			KeywordInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("cat"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/cat/")
			respRec = httptest.NewRecorder()

			KeywordInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("cat"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Keyword with special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"special park"}))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/special-park/")
			respRec := httptest.NewRecorder()

			KeywordInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("special park"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/special-park/")
			respRec = httptest.NewRecorder()

			KeywordInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("special park"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

	})

	Describe("KeywordChildren", func() {
		makeRequest := func() *http.Request {
			payload := strings.NewReader(`{ "id": "/", "coverPhoto": true }`)
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", []string{"tree", "color", "special park"}))
		})

		It("Non-public keywords", func() {
			request := makeRequest()
			respRec := httptest.NewRecorder()

			KeywordChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(0))

		})

		It("Public keywords with cover photos", func() {
			makeAllPhotosPublic(ctx)

			request := makeRequest()
			respRec := httptest.NewRecorder()

			KeywordChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(3))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/color/"),
				"display": Equal("color"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))
			Expect(response[1]).To(MatchAllKeys(Keys{
				"id":      Equal("/special-park/"),
				"display": Equal("special park"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))
			Expect(response[2]).To(MatchAllKeys(Keys{
				"id":      Equal("/tree/"),
				"display": Equal("tree"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

		})
	})

	Describe("Update", func() {
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("Update keyword display path", func() {
			_, err := db.Keywords.Create(ctx, "mystery")
			Expect(err).ToNot(HaveOccurred())

			// With admin
			request := addSessionToRequest(makeRequest("/mystery/", "Mystery?!"), adminSession)
			respRec := httptest.NewRecorder()

			KeywordUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Keywords.Get(ctx, "/mystery/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("Mystery?!"))}),
			)
		})
	})
})
