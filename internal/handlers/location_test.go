// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Locations", func() {
	var ctx context.Context

	genPhotoMd := func(title string, location string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Location: location,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:01 01:02:03",
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()

	})

	Describe("LocationInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Single level location with no special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", "place"))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/place/")
			respRec := httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("place"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/place/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("place"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Multi level location with no special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", "hall, city, state, country"))
			makeAllPhotosPublic(ctx)

			locs := [][2]string{
				{"/country/", "country"},
				{"/country/state/", "country,state"},
				{"/country/state/city/", "country,state,city"},
				{"/country/state/city/hall/", "country,state,city,hall"},
			}

			for i, loc := range locs {
				// With cover photo
				request := makeRequestCoverPhoto(loc[0])
				respRec := httptest.NewRecorder()

				LocationInfo(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				var response map[string]interface{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(MatchAllKeys(Keys{
					"display": Equal(loc[1]),
					"public":  Equal(true),
					"coverPhoto": MatchAllKeys(Keys{
						"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
						"storeGen": Equal("AAAAAAAAAAA"),
						"width":    BeEquivalentTo(100),
						"height":   BeEquivalentTo(200),
					}),
					"type":        BeEquivalentTo(docs.LocationOther),
					"hasChildren": Equal(i < 3),
					"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
				}))

				// Without cover photo
				request = makeRequest(loc[0])
				respRec = httptest.NewRecorder()

				LocationInfo(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response = map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(MatchAllKeys(Keys{
					"display":     Equal(loc[1]),
					"public":      Equal(true),
					"type":        BeEquivalentTo(docs.LocationOther),
					"hasChildren": Equal(i < 3),
					"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
				}))
			}

		})

		It("Single level location with special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", "Special Place"))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/special-place/")
			respRec := httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("Special Place"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/special-place/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("Special Place"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Multi level location with special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", "Special Place, Mega City, country"))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/country/")
			respRec := httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("country"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequestCoverPhoto("/country/mega-city/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("country,Mega City"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequestCoverPhoto("/country/mega-city/special-place/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("country,Mega City,Special Place"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/country/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("country"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequest("/country/mega-city/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("country,Mega City"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequest("/country/mega-city/special-place/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("country,Mega City,Special Place"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationOther),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Multi level location with park and hike", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", "Hill, !hike: Trail, !park: Greenery, Abc"))
			makeAllPhotosPublic(ctx)

			// /abc/greenery/
			request := makeRequest("/abc/greenery/")
			respRec := httptest.NewRecorder()

			LocationInfo(respRec, request)
			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("Abc,!park:Greenery"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationPark),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// /abc/greenery/trail/
			request = makeRequest("/abc/greenery/trail/")
			respRec = httptest.NewRecorder()

			LocationInfo(respRec, request)
			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("Abc,!park:Greenery,!hike:Trail"),
				"public":      Equal(true),
				"type":        BeEquivalentTo(docs.LocationHike),
				"park":        Equal("/abc/greenery/"),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})
	})

	Describe("LocationChildren", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestWithType := func(id string, locType string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s", "coverPhoto": true, "maxRelDepth": -1, "extension": { "type": "%s" } }`, id, locType))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", "City, Country"))
		})

		It("Non-public categories", func() {
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			LocationChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(0))

		})

		It("Public categories with cover photos", func() {
			makeAllPhotosPublic(ctx)

			// Root
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			LocationChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/country/"),
				"display": Equal("Country"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type": BeEquivalentTo(docs.LocationOther),
			}))

			// Location
			request = makeRequest("/country/")
			respRec = httptest.NewRecorder()

			LocationChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/country/city/"),
				"display": Equal("Country,City"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type": BeEquivalentTo(docs.LocationOther),
			}))

		})

		It("Filter for parks", func() {
			addPhotoAndMockResize(ctx, "folder/park.jpg", genPhotoMd("title", "!park:Green, Country"))
			makeAllPhotosPublic(ctx)

			// Root
			request := makeRequestWithType("/", "park")
			respRec := httptest.NewRecorder()

			LocationChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/country/green/"),
				"display": Equal("Country,!park:Green"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("TdvEXDCnEmy6y0LsyPgtmHKyAt4"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type": BeEquivalentTo(docs.LocationPark),
			}))
		})

		It("Filter for hikes", func() {
			addPhotoAndMockResize(ctx, "folder/trail.jpg", genPhotoMd("title", "!hike:Trail, !park:Green, Country"))
			makeAllPhotosPublic(ctx)

			// Root
			request := makeRequestWithType("/", "hike")
			respRec := httptest.NewRecorder()

			LocationChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/country/green/trail/"),
				"display": Equal("Country,!park:Green,!hike:Trail"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("CLazkWzuBcbN-50zJ2R43dLjvbg"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"type": BeEquivalentTo(docs.LocationHike),
				"park": Equal("/country/green/"),
			}))
		})

		// Mimics real request when filtering by hikes
		Describe("Sort hikes by park and ignore depth", func() {
			makeCustomRequest := func(id string) *http.Request {
				payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s", "coverPhoto": false, "maxRelDepth": -1, "sortIgnoreDepth": true, "extension": { "type": "hike", "sort": "park" } }`, id))
				return httptest.NewRequest("POST", "/", payload)
			}

			It("Multi-hike, multi-park", func() {
				addPhotoAndMockResize(ctx, "1.jpg", genPhotoMd("title", "!hike:Abc Hike, !park:Xyz Park, Country"))
				addPhotoAndMockResize(ctx, "2.jpg", genPhotoMd("title", "!hike:Xyz Hike, !park:Abc Park, Country"))
				addPhotoAndMockResize(ctx, "3.jpg", genPhotoMd("title", "!hike:Mid Hike, sub-park, !park:Abc Park, Country"))

				request := makeCustomRequest("/")
				respRec := httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				var response []map[string]interface{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(3))

				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/abc-park/sub-park/mid-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/abc-park/"),
				}))
				Expect(response[1]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/abc-park/xyz-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/abc-park/"),
				}))
				Expect(response[2]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/xyz-park/abc-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/xyz-park/"),
				}))
			})

			It("Parks at different depths", func() {
				addPhotoAndMockResize(ctx, "1.jpg", genPhotoMd("title", "!hike:Abc Hike, !park:Xyz Park, Country"))
				addPhotoAndMockResize(ctx, "2.jpg", genPhotoMd("title", "!hike:Xyz Hike, !park:Abc Park, State, Country"))
				addPhotoAndMockResize(ctx, "3.jpg", genPhotoMd("title", "!hike:Mid Hike, sub-park, !park:Abc Park, State, Country"))
				addPhotoAndMockResize(ctx, "4.jpg", genPhotoMd("title", "!hike:Random Hike, !park:Half Park, ACountry"))

				request := makeCustomRequest("/")
				respRec := httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				var response []map[string]interface{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(4))

				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/state/abc-park/sub-park/mid-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/state/abc-park/"),
				}))
				Expect(response[1]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/state/abc-park/xyz-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/state/abc-park/"),
				}))
				Expect(response[2]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/acountry/half-park/random-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/acountry/half-park/"),
				}))
				Expect(response[3]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/xyz-park/abc-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/xyz-park/"),
				}))
			})

			It("Multi-stage listing", func() {
				makeRequest := func(refId string) *http.Request {
					payload := strings.NewReader(fmt.Sprintf(`{ "id": "/", "refId": "%s", "limit": 1, "coverPhoto": false, "maxRelDepth": -1, "sortIgnoreDepth": true, "extension": { "type": "hike", "sort": "park" } }`, refId))
					return httptest.NewRequest("POST", "/", payload)
				}

				addPhotoAndMockResize(ctx, "1.jpg", genPhotoMd("title", "!hike:Abc Hike, !park:Xyz Park, Country"))
				addPhotoAndMockResize(ctx, "2.jpg", genPhotoMd("title", "!hike:Xyz Hike, !park:Abc Park, State, Country"))
				addPhotoAndMockResize(ctx, "3.jpg", genPhotoMd("title", "!hike:Mid Hike, sub-park, !park:Abc Park, State, Country"))
				addPhotoAndMockResize(ctx, "4.jpg", genPhotoMd("title", "!hike:Random Hike, !park:Half Park, ACountry"))

				// Request 1
				request := makeRequest("")
				respRec := httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				var response []map[string]interface{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(1))

				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/state/abc-park/sub-park/mid-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/state/abc-park/"),
				}))

				// Request 2
				request = makeRequest(response[0]["id"].(string))
				respRec = httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response = []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(1))

				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/state/abc-park/xyz-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/state/abc-park/"),
				}))

				// Request 3
				request = makeRequest(response[0]["id"].(string))
				respRec = httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response = []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(1))
				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/acountry/half-park/random-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/acountry/half-park/"),
				}))

				// Request 4
				request = makeRequest(response[0]["id"].(string))
				respRec = httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response = []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(1))
				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/xyz-park/abc-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/xyz-park/"),
				}))

				// Request 5
				request = makeRequest(response[0]["id"].(string))
				respRec = httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response = []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(0))
			})

			It("Hike with no park", func() {
				addPhotoAndMockResize(ctx, "1.jpg", genPhotoMd("title", "!hike:Abc Hike, !park:Xyz Park, Country"))
				addPhotoAndMockResize(ctx, "2.jpg", genPhotoMd("title", "!hike:Xyz Hike, Country"))

				request := makeCustomRequest("/")
				respRec := httptest.NewRecorder()

				LocationChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				var response []map[string]interface{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(2))

				Expect(response[0]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/xyz-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
				}))
				Expect(response[1]).To(MatchKeys(IgnoreExtras, Keys{
					"id":   Equal("/country/xyz-park/abc-hike/"),
					"type": BeEquivalentTo(docs.LocationHike),
					"park": Equal("/country/xyz-park/"),
				}))
			})
		})
	})

	Describe("Update", func() {
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("Update location display path", func() {
			_, err := db.Locations.Create(ctx, "country")
			Expect(err).ToNot(HaveOccurred())

			// With admin
			request := addSessionToRequest(makeRequest("/country/", "Country!"), adminSession)
			respRec := httptest.NewRecorder()

			LocationUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Locations.Get(ctx, "/country/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("Country!"))}),
			)
		})
	})

})
