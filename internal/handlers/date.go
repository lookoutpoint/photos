// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/list"
)

func buildDateFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	if id == "/" {
		return &docs.PhotoListFilterExpr{}, nil
	}

	date := strings.ReplaceAll(id, "/", "")
	return &docs.PhotoListFilterExpr{
		Dates: []docs.PhotoListFilterDate{{Value: date, CmpOp: list.CmpEq}},
	}, nil
}

// DateInfo returns info about the given date
func DateInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Dates,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			return db.Dates.Get(ctx, id, keys, visTokens)
		},
		buildDateFilter,
		nil,
	)
}

// DateChildren returns info about the children of the given date
func DateChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()

	groupChildren(resp, req, db.Dates,
		func(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
			dateValue := strings.ReplaceAll(id, "/", "")
			return &docs.PhotoListFilterExpr{
				Dates: []docs.PhotoListFilterDate{
					{Value: dateValue, CmpOp: list.CmpEq},
				},
			}, nil
		},
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			return db.Dates.ListChildren(ctx, params)
		},
		stdBuildChildResponse,
		nil,
	)
}

func DateListEmpty(resp http.ResponseWriter, req *http.Request) {
	groupListEmpty(resp, req, database.Get().Dates.Collection())
}

func DateDeleteEmpty(resp http.ResponseWriter, req *http.Request) {
	groupDeleteEmpty(resp, req, database.Get().Dates.Collection())
}
