// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

func buildFolderFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	return &docs.PhotoListFilterExpr{
		Folders: []docs.PhotoListFilterFolder{
			{Path: id, FullTree: true},
		},
	}, nil
}

// FolderInfo returns info about the given folder path
func FolderInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Folders,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			// If admin, also return selfVisTokens
			if isAdminSession {
				keys = append(keys, "selfVisTokens")
			}
			return db.Folders.Get(ctx, id, keys, visTokens)
		},
		buildFolderFilter,
		nil,
	)
}

// FolderChildren returns info about the children of the given folder
func FolderChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.Folders, buildFolderFilter,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			return db.Folders.ListChildren(ctx, params)
		},
		stdBuildChildResponse,
		nil,
	)
}

// FolderUpdate updates a folder. Must be admin.
func FolderUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().Folders)
}

// FolderVisibilityToken can add or delete a visibility token on a folder. Admin only.
func FolderVisibilityToken(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.FolderVisibilityToken",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		Mode   string `json:"mode"`
		Folder string `json:"folder"`
		Target string `json:"target"`
		Token  string `json:"token"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)
	db := database.Get()

	var target apis.FolderVisibilityTokenTarget
	if reqData.Target == "folder" {
		target = apis.FolderTarget
	} else if reqData.Target == "photos" {
		target = apis.FolderTargetPhotos
	} else if reqData.Target == "fulltree" {
		target = apis.FolderTargetFullTree
	} else {
		l.Error("Invalid target")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validate folder
	if !db.Folders.Exists(ctx, reqData.Folder) {
		l.Error("Folder does not exist")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Validate token
	var token string
	if reqData.Token == string(docs.VisTokenPublic) {
		token = docs.VisTokenPublic
	} else {
		tokenDoc, err := db.Visibility.Get(ctx, reqData.Token, []string{"id"})
		if err != nil {
			l.WithError(err).Error("Could not get visibility token")
			resp.WriteHeader(http.StatusBadRequest)
			return
		}
		token = *tokenDoc.ID
	}

	// Do visibility token action.
	if reqData.Mode == "add" {
		if err := db.Folders.AddVisibilityToken(ctx, reqData.Folder, target, token); err != nil {
			l.WithError(err).Error("Could not add visibility token")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else if reqData.Mode == "delete" {
		if err := db.Folders.DeleteVisibilityToken(ctx, reqData.Folder, target, token); err != nil {
			l.WithError(err).Error("Could not delete visibility token")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	} else {
		l.Error("Invalid mode")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l.Info("Added visibility token")
	resp.WriteHeader(http.StatusNoContent)
}

func FolderListEmpty(resp http.ResponseWriter, req *http.Request) {
	groupListEmpty(resp, req, database.Get().Folders.Collection())
}

func FolderDeleteEmpty(resp http.ResponseWriter, req *http.Request) {
	groupDeleteEmpty(resp, req, database.Get().Folders.Collection())
}
