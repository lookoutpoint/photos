// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/database/testutils"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Categories", func() {
	var ctx context.Context

	genPhotoMd := func(title string, keywords []string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:01 01:02:03",
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("CategoryInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		makeRequestCoverPhoto := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Category with no special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"animal: cat"}))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/animal/")
			respRec := httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("animal"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequestCoverPhoto("/animal/cat/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("animal:cat"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/animal/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("animal"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequest("/animal/cat/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("animal:cat"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Category with special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"best part: fun"}))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/best-part/")
			respRec := httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("best part"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequestCoverPhoto("/best-part/fun/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("best part:fun"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/best-part/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("best part"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequest("/best-part/fun/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("best part:fun"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Category value with special display name", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"cat: fun place"}))
			makeAllPhotosPublic(ctx)

			// With cover photo
			request := makeRequestCoverPhoto("/cat/")
			respRec := httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("cat"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequestCoverPhoto("/cat/fun-place/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("cat:fun place"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("0PjYdmlOryG-E9jS4_dfv8N2CX8"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			// Without cover photo
			request = makeRequest("/cat/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("cat"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

			request = makeRequest("/cat/fun-place/")
			respRec = httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("cat:fun place"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))

		})

		It("Category value with related", func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"hike: walk", "park: park"}))
			makeAllPhotosPublic(ctx)

			Expect(db.Categories.UpdateRelated(ctx, "/hike/")).ToNot(HaveOccurred())

			// With cover photo
			request := makeRequest("/hike/walk/")
			respRec := httptest.NewRecorder()

			CategoryInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display": Equal("hike:walk"),
				"public":  Equal(true),
				"related": MatchAllKeys(Keys{
					"cat:park": ConsistOf("park"),
				}),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})

	})

	Describe("CategoryChildren", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%s", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", []string{"tree", "color: red"}))
		})

		It("Non-public categories", func() {
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			CategoryChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(0))

		})

		It("Public categories with cover photos", func() {
			makeAllPhotosPublic(ctx)

			// Root
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			CategoryChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/color/"),
				"display": Equal("color"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

			// Category
			request = makeRequest("/color/")
			respRec = httptest.NewRecorder()

			CategoryChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/color/red/"),
				"display": Equal("color:red"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

		})

		It("Public categories with relation", func() {
			addPhotoAndMockResize(ctx, "folder/photo2.jpg", genPhotoMd("title", []string{"tree", "color: red", "hike: h"}))
			makeAllPhotosPublic(ctx)

			Expect(db.Categories.UpdateRelated(ctx, "/hike/")).ToNot(HaveOccurred())

			// Category
			request := makeRequest("/hike/")
			respRec := httptest.NewRecorder()

			CategoryChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response := []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":         Equal("/hike/h/"),
				"display":    Equal("hike:h"),
				"public":     Equal(true),
				"coverPhoto": Ignore(),
				"related": MatchAllKeys(Keys{
					"cat:color": ConsistOf("red"),
				}),
			}))
		})

		Describe("Photos with park and hike and locations", func() {
			BeforeEach(func() {
				genPhotoMd := func(title string, location string, keywords []string) *photomd.Metadata {
					return &photomd.Metadata{
						Width:  100,
						Height: 200,
						Xmp: photomd.XmpData{
							Title:    title,
							Subjects: keywords,
							Location: location,
						},
						Exif: photomd.ExifData{
							photomd.ExifTagDateTimeOriginal: "2020:01:01 01:02:03",
						},
					}
				}

				addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", "la,country", []string{"park: pa", "hike: ha"}))
				addPhotoAndMockResize(ctx, "folder/photo2.jpg", genPhotoMd("title", "lb,country", []string{"park: pb", "hike: hb"}))
				addPhotoAndMockResize(ctx, "folder/photo3.jpg", genPhotoMd("title", "la,country", []string{"park: pa", "hike: hc"}))
				makeAllPhotosPublic(ctx)

				Expect(db.Categories.UpdateRelated(ctx, "/hike/")).ToNot(HaveOccurred())
				Expect(db.Categories.UpdateRelated(ctx, "/park/")).ToNot(HaveOccurred())
				Expect(db.Categories.UpdateCustomViews(ctx)).ToNot(HaveOccurred())
			})

			It("Sort by park", func() {
				makeRequest := func() *http.Request {
					payload := strings.NewReader(`{ "id": "/hike/", "extension": { "sortBy": "park" } }`)
					return httptest.NewRequest("POST", "/", payload)
				}

				// CategoryChildren with sort by park
				request := makeRequest()
				respRec := httptest.NewRecorder()

				CategoryChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(3))

				Expect(response[0]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/ha/@/park/pa/"),
					"display": Equal("hike:ha"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
				}))
				Expect(response[1]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/hc/@/park/pa/"),
					"display": Equal("hike:hc"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
				}))
				Expect(response[2]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/hb/@/park/pb/"),
					"display": Equal("hike:hb"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pb"),
						"location": ConsistOf("/country/lb/"),
					}),
				}))

			})

			It("Filter by park", func() {
				makeRequest := func(park string) *http.Request {
					payload := strings.NewReader(fmt.Sprintf(`{ "id": "/hike/", "extension": { "filterByPark": "%v" } }`, park))
					return httptest.NewRequest("POST", "/", payload)
				}

				// CategoryChildren with filter by park
				request := makeRequest("pa")
				respRec := httptest.NewRecorder()

				CategoryChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(2))

				Expect(response[0]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/ha/"),
					"display": Equal("hike:ha"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
				}))
				Expect(response[1]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/hc/"),
					"display": Equal("hike:hc"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
				}))

			})

			It("Filter hike by location", func() {
				makeRequest := func(loc string) *http.Request {
					payload := strings.NewReader(fmt.Sprintf(`{ "id": "/hike/", "extension": { "filterByLocation": "%v", "sortBy": "park" }, "coverPhoto": true }`, loc))
					return httptest.NewRequest("POST", "/", payload)
				}

				// CategoryChildren with filter by location
				request := makeRequest("/country/")
				respRec := httptest.NewRecorder()

				CategoryChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(3))

				Expect(response[0]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/ha/@/park/pa/"),
					"display": Equal("hike:ha"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
					"coverPhoto": Not(BeNil()),
				}))
				Expect(response[1]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/hc/@/park/pa/"),
					"display": Equal("hike:hc"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pa"),
						"location": ConsistOf("/country/la/"),
					}),
					"coverPhoto": Not(BeNil()),
				}))
				Expect(response[2]).To(MatchAllKeys(Keys{
					"id":      Equal("/hike/hb/@/park/pb/"),
					"display": Equal("hike:hb"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:park": ConsistOf("pb"),
						"location": ConsistOf("/country/lb/"),
					}),
					"coverPhoto": Not(BeNil()),
				}))

			})

			It("Filter park by location", func() {
				makeRequest := func(loc string) *http.Request {
					payload := strings.NewReader(fmt.Sprintf(`{ "id": "/park/", "extension": { "filterByLocation": "%v" } }`, loc))
					return httptest.NewRequest("POST", "/", payload)
				}

				// CategoryChildren with filter by location
				request := makeRequest("/country/")
				respRec := httptest.NewRecorder()

				CategoryChildren(respRec, request)

				Expect(respRec.Code).To(Equal(200))

				response := []map[string]interface{}{}
				Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
				Expect(response).To(HaveLen(2))

				Expect(response[0]).To(MatchAllKeys(Keys{
					"id":      Equal("/park/pa/"),
					"display": Equal("park:pa"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:hike": ConsistOf("ha", "hc"),
						"location": ConsistOf("/country/la/"),
					}),
				}))
				Expect(response[1]).To(MatchAllKeys(Keys{
					"id":      Equal("/park/pb/"),
					"display": Equal("park:pb"),
					"public":  Equal(true),
					"related": MatchAllKeys(Keys{
						"cat:hike": ConsistOf("hb"),
						"location": ConsistOf("/country/lb/"),
					}),
				}))

			})
		})

	})

	Describe("Update", func() {
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		makeRequest := func(id string, displayPath string) *http.Request {
			payload := strings.NewReader(
				fmt.Sprintf(`{ "id": "%v", "display": "%v" }`, id, displayPath))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("Without admin", func() {
			_, err := db.Categories.Create(ctx, "cat:value")
			Expect(err).ToNot(HaveOccurred())

			request := makeRequest("/cat/value/", "Why?")
			respRec := httptest.NewRecorder()

			CategoryUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Categories.Get(ctx, "/cat/value/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("cat:value"))}),
			)
		})

		It("Update category display path", func() {
			_, err := db.Categories.Create(ctx, "cat:value")
			Expect(err).ToNot(HaveOccurred())

			// With admin
			request := addSessionToRequest(makeRequest("/cat/value/", "Why?"), adminSession)
			respRec := httptest.NewRecorder()

			CategoryUpdate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Categories.Get(ctx, "/cat/value/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"GroupBase.Display": PointTo(Equal("cat:Why?"))}),
			)
		})
	})

	Describe("UpdateRelated", func() {
		var adminSession *docs.Session
		tokensAll := []string{docs.VisTokenAll}

		makeRequest := func(ids ...string) *http.Request {
			var payload struct {
				IDs []string `json:"ids,omitempty"`
			}
			payload.IDs = ids
			data, err := json.Marshal(payload)
			Expect(err).ToNot(HaveOccurred())

			return httptest.NewRequest("POST", "/", bytes.NewReader(data))
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "photo.jpg", genPhotoMd("title", []string{"hike: walk", "park: park"}))
			makeAllPhotosPublic(ctx)

			// Admin session
			adminSession = createAdminUserAndSession(ctx)
		})

		It("Without admin", func() {
			request := makeRequest()
			respRec := httptest.NewRecorder()

			CategoryUpdateRelated(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Categories.Get(ctx, "/hike/walk/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"Related": BeNil()}),
			)
		})

		It("With admin", func() {
			// With admin
			request := addSessionToRequest(makeRequest(), adminSession)
			respRec := httptest.NewRecorder()

			CategoryUpdateRelated(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Categories.Get(ctx, "/hike/walk/", nil, tokensAll)).To(
				MatchGroupBasePtr(Fields{"Related": MatchAllKeys(Keys{"cat:park": ConsistOf("park")})}),
			)
		})

	})

})
