// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Dates", func() {
	var ctx context.Context

	genPhotoMd := func(title string, dateTime string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title: title,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: dateTime,
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("DateInfo", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v" }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", "2020:11:03 00:00:00"))
			makeAllPhotosPublic(ctx)
		})

		It("root", func() {
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			DateInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal(""),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})

		It("year", func() {
			request := makeRequest("/2020/")
			respRec := httptest.NewRecorder()

			DateInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("2020"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})

		It("month", func() {
			request := makeRequest("/2020/11/")
			respRec := httptest.NewRecorder()

			DateInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("2020/11"),
				"public":      Equal(true),
				"hasChildren": Equal(true),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})

		It("day", func() {
			request := makeRequest("/2020/11/03/")
			respRec := httptest.NewRecorder()

			DateInfo(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(MatchAllKeys(Keys{
				"display":     Equal("2020/11/03"),
				"public":      Equal(true),
				"hasChildren": Equal(false),
				"hasPhotos":   BeEquivalentTo(HAS_PHOTOS),
			}))
		})
	})

	Describe("DateChildren", func() {
		makeRequest := func(id string) *http.Request {
			payload := strings.NewReader(fmt.Sprintf(`{ "id": "%v", "coverPhoto": true }`, id))
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			addPhotoAndMockResize(ctx, "folder/photo.jpg", genPhotoMd("title", "2020:11:03 00:00:00"))
		})

		It("Non-public dates", func() {
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			DateChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(0))

		})

		It("Public keywords with cover photos", func() {
			makeAllPhotosPublic(ctx)

			// Root
			request := makeRequest("/")
			respRec := httptest.NewRecorder()

			DateChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var response []map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/2020/"),
				"display": Equal("2020"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

			// Year
			request = makeRequest("/2020/")
			respRec = httptest.NewRecorder()

			DateChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/2020/11/"),
				"display": Equal("2020/11"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

			// Month
			request = makeRequest("/2020/11/")
			respRec = httptest.NewRecorder()

			DateChildren(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			response = []map[string]interface{}{}
			Expect(json.Unmarshal(readResponseBody(respRec), &response)).ToNot(HaveOccurred())
			Expect(response).To(HaveLen(1))

			Expect(response[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/2020/11/03/"),
				"display": Equal("2020/11/03"),
				"public":  Equal(true),
				"coverPhoto": MatchAllKeys(Keys{
					"id":       Equal("08K63D7sUd-b0GCfjX-TS2OFjVE"),
					"storeGen": Equal("AAAAAAAAAAA"),
					"width":    BeEquivalentTo(100),
					"height":   BeEquivalentTo(200),
				}),
			}))

		})
	})

})
