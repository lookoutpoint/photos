// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"go.mongodb.org/mongo-driver/bson"
)

func buildLocationFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	return &docs.PhotoListFilterExpr{
		Locations: []string{id},
	}, nil
}

type locationWithCoverPhoto struct {
	docs.Location `json:",inline"`
	Public        bool            `json:"public"`
	CoverPhoto    *coverPhotoInfo `json:"coverPhoto,omitempty"`
}

func locationBuildChildResponse(ctx context.Context, child interface{}, public bool, coverPhoto *coverPhotoInfo) (interface{}, error) {
	var response locationWithCoverPhoto
	response.Location = child.(docs.Location)
	response.Public = public
	response.CoverPhoto = coverPhoto

	return response, nil
}

// LocationInfo returns info about the given location slug
func LocationInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Locations,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			// Include Type and Park field.
			keysLocal := keys
			keysLocal = append(keysLocal, "type", "park")
			return db.Locations.Get(ctx, id, keysLocal, visTokens)
		},
		buildLocationFilter,
		nil,
	)
}

// LocationChildren returns info about the children of the given location
func LocationChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.Locations, buildLocationFilter,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			// Include Type and Park field.
			params.ProjectionKeys = append(params.ProjectionKeys, "type", "park")

			// Extension parameter: { type: "park" | "hike" }
			// We explicitly hardcode acceptable fields here to prevent undesired custom sort keys.
			if v, ok := extensionParams["type"]; ok {
				if v == "hike" {
					params.CustomFilter = bson.M{"type": docs.LocationHike}
				} else if v == "park" {
					params.CustomFilter = bson.M{"type": docs.LocationPark}
				} else {
					return nil, fmt.Errorf("invalid location type")
				}
			}

			// Extension parameter: { sort: "park" }
			// We explicitly hardcode acceptable fields here to prevent undesired custom sort keys.
			if v, ok := extensionParams["sort"]; ok {
				if v == "park" {
					// Sort by park name first, then park id
					params.CustomSortKeys = []string{"parkName", "park"}
				} else {
					return nil, fmt.Errorf("invalid sort type")
				}
			}

			return db.Locations.ListChildren(ctx, params)
		},
		locationBuildChildResponse,
		nil,
	)
}

// LocationUpdate updates a location. Must be admin.
func LocationUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().Locations)
}

func LocationListEmpty(resp http.ResponseWriter, req *http.Request) {
	groupListEmpty(resp, req, database.Get().Locations.Collection())
}

func LocationDeleteEmpty(resp http.ResponseWriter, req *http.Request) {
	groupDeleteEmpty(resp, req, database.Get().Locations.Collection())
}
