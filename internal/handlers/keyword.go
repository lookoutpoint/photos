// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

func buildKeywordFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	return &docs.PhotoListFilterExpr{
		Keywords: []string{id},
	}, nil
}

// KeywordInfo returns info about the given keyword slug
func KeywordInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Keywords,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			return db.Keywords.Get(ctx, id, keys, visTokens)
		},
		buildKeywordFilter,
		nil,
	)
}

// KeywordChildren returns info about the children of the given keyword
func KeywordChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.Keywords, buildKeywordFilter,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			return db.Keywords.ListChildren(ctx, params)
		},
		stdBuildChildResponse,
		nil,
	)
}

// KeywordUpdate updates a keyword. Must be admin.
func KeywordUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().Keywords)
}

func KeywordListEmpty(resp http.ResponseWriter, req *http.Request) {
	groupListEmpty(resp, req, database.Get().Keywords.Collection())
}

func KeywordDeleteEmpty(resp http.ResponseWriter, req *http.Request) {
	groupDeleteEmpty(resp, req, database.Get().Keywords.Collection())
}
