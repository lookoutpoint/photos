// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
	"gitlab.com/lookoutpoint/storage"
	teststorage "gitlab.com/lookoutpoint/storage/test"
)

var _ = Describe("NotifyMasterStorageUpdates", func() {
	var db *database.Database
	var ctx context.Context

	tokensAll := []string{docs.VisTokenAll}

	gcsEventAttrs := func(eventType string, bucketId string, objectId string, objectGeneration string) map[string]string {
		return map[string]string{
			"eventType":        eventType,
			"bucketId":         bucketId,
			"objectId":         objectId,
			"objectGeneration": objectGeneration,
		}
	}

	makeGcsNotifyRequest := func(attrs map[string]string) *http.Request {
		var payload struct {
			Message struct {
				Attributes map[string]string
			}
		}
		payload.Message.Attributes = attrs

		reqPayload, err := json.Marshal(payload)
		Expect(err).To(Succeed())

		return httptest.NewRequest("POST", "/", bytes.NewReader(reqPayload))
	}

	makeFinalizePhotoRequest := func(bucketId string, objectId string, objectGen string) *http.Request {
		payload := map[string]string{
			"bucketId":  bucketId,
			"objectId":  objectId,
			"objectGen": objectGen,
		}

		reqPayload, err := json.Marshal(payload)
		Expect(err).To(Succeed())

		req := httptest.NewRequest("POST", "/", bytes.NewReader(reqPayload))
		req.Header.Add("X-Appengine-Taskname", "task")
		return req
	}

	makeDeletePhotoRequest := func(bucketId string, objectId string) *http.Request {
		payload := map[string]string{
			"bucketId": bucketId,
			"objectId": objectId,
		}

		reqPayload, err := json.Marshal(payload)
		Expect(err).To(Succeed())

		req := httptest.NewRequest("POST", "/", bytes.NewReader(reqPayload))
		req.Header.Add("X-Appengine-Taskname", "task")
		return req
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		db = database.Get()
	})

	Context("No auth, no storage", func() {
		BeforeEach(func() {
			// Set up test authorizer that will fail auth checks
			SetAuth(NewTestAuth(false))
		})

		It("Request", func() {
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucket", "object", "12345"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})
	})

	Context("With auth, no storage", func() {
		BeforeEach(func() {
			// Set up test authorizer that will succeed auth checks
			SetAuth(NewTestAuth((true)))
		})

		It("Folder finalize", func() {
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucket", "folder/", "568623"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))
		})

		It("Folder delete", func() {
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventDelete, "bucket", "folder/", "484561631"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))
		})

		It("Nested folder finalize", func() {
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucket", "folder/a/b x/y/", "15489431"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))
		})

		It("Nested folder delete", func() {
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventDelete, "bucket", "folder/a/b x/y/", "24648"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))
		})
	})

	Context("With auth, with storage", func() {
		BeforeEach(func() {
			// Set up test authorizer that will succeed auth checks
			SetAuth(NewTestAuth((true)))

			storage.Set(teststorage.New())
		})

		It("nikon1.jpg", func() {
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/nikon1.jpg", "bucketA", "nikon1.jpg")).To(Succeed())

			// GCS notify
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "nikon1.jpg", "11894131"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeFinalizePhotoRequest("bucketA", "nikon1.jpg", "11894131")
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			Expect(db.Folders.Exists(ctx, "/")).To(BeTrue())

			photos, err := db.Photos.List(ctx, &apis.PhotoListParams{
				ProjectionKeys: []string{"fileSize"},
				Filter: docs.PhotoListFilterExpr{
					Folders: []docs.PhotoListFilterFolder{
						{Path: "/"},
					},
				},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(photos).To(HaveLen(1))
			Expect(photos[0].FileSize).To(PointTo(BeNumerically("==", 139882)))
		})

		It("a/b/nikon1.jpg", func() {
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/nikon1.jpg", "bucketA", "a/b/nikon1.jpg")).To(Succeed())

			// GCS notify
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "a/b/nikon1.jpg", "12315618489"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeFinalizePhotoRequest("bucketA", "a/b/nikon1.jpg", "12315618489")
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			Expect(db.Folders.Exists(ctx, "/a/b/")).To(BeTrue())
		})

		It("non-existent.jpg", func() {
			// GCS notify
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "non-existent.jpg", "7897892132"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeFinalizePhotoRequest("bucketA", "non-existent.jpg", "7897892132")
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(500))

		})

		It("not-a-photo.txt", func() {
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/not-a-photo.txt", "bucketA", "not-a-photo.txt")).To(Succeed())

			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "not-a-photo.txt", "378753153"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))
		})

		It("Add and then delete photo", func() {
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/nikon1.jpg", "bucketA", "a/b/nikon1.jpg")).To(Succeed())

			// GCS notify
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "a/b/nikon1.jpg", "851256489"))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeFinalizePhotoRequest("bucketA", "a/b/nikon1.jpg", "851256489")
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Check that the photo is there in the database
			p, err := db.Photos.List(ctx, &apis.PhotoListParams{
				Filter: docs.PhotoListFilterExpr{
					Folders: []docs.PhotoListFilterFolder{
						{Path: "/a/b/"},
					},
				},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(p).To(HaveLen(1))

			// Delete the photo
			Expect(storage.Get().DeleteObject(ctx, "bucketA", "a/b/nikon1.jpg")).To(Succeed())

			// GCS notify
			request = makeGcsNotifyRequest(gcsEventAttrs(storage.EventDelete, "bucketA", "a/b/nikon1.jpg", "851256489"))
			respRec = httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeDeletePhotoRequest("bucketA", "a/b/nikon1.jpg")
			respRec = httptest.NewRecorder()

			NotifyDeletePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Check that the photo is no longer in the database
			p, err = db.Photos.List(ctx, &apis.PhotoListParams{
				Filter: docs.PhotoListFilterExpr{
					Folders: []docs.PhotoListFilterFolder{
						{Path: "/a/b/"},
					},
				},
				VisibilityTokens: tokensAll,
			})
			Expect(err).ToNot(HaveOccurred())
			Expect(p).To(HaveLen(0))
		})

		It("Add and then replace photo", func() {
			Expect(teststorage.CopyLocalIntoStorage(ctx, "./fixtures/nikon1.jpg", "bucketA", "a/b/nikon1.jpg")).To(Succeed())

			const FIRST_GEN = "85126489"
			const SECOND_GEN = "85126490"

			// GCS notify
			request := makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "a/b/nikon1.jpg", FIRST_GEN))
			respRec := httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Task queue
			request = makeFinalizePhotoRequest("bucketA", "a/b/nikon1.jpg", FIRST_GEN)
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Check that the photo is there in the database
			listParams := apis.PhotoListParams{
				Filter: docs.PhotoListFilterExpr{
					Folders: []docs.PhotoListFilterFolder{
						{Path: "/a/b/"},
					},
				},
				VisibilityTokens: tokensAll,
			}

			p, err := db.Photos.List(ctx, &listParams)
			Expect(err).ToNot(HaveOccurred())
			Expect(p).To(HaveLen(1))

			// For replacement, send a delete event with overwrittenByGeneration set
			rAttrs := gcsEventAttrs(storage.EventDelete, "bucketA", "a/b/nikon1.jpg", FIRST_GEN)
			rAttrs["overwrittenByGeneration"] = SECOND_GEN
			request = makeGcsNotifyRequest(rAttrs)

			respRec = httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Check that the photo is still in the database (it shouldn't be deleted as we're expecting a finalize event)
			p, err = db.Photos.List(ctx, &listParams)
			Expect(err).ToNot(HaveOccurred())
			Expect(p).To(HaveLen(1))

			// Send finalize event for second gen
			request = makeGcsNotifyRequest(gcsEventAttrs(storage.EventFinalize, "bucketA", "a/b/nikon1.jpg", SECOND_GEN))
			respRec = httptest.NewRecorder()

			NotifyMasterStorageUpdates(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			request = makeFinalizePhotoRequest("bucketA", "a/b/nikon1.jpg", SECOND_GEN)
			respRec = httptest.NewRecorder()

			NotifyFinalizePhotoTask(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			// Check that the photo is still in the database (it shouldn't be deleted as we're expecting a finalize event)
			p, err = db.Photos.List(ctx, &listParams)
			Expect(err).ToNot(HaveOccurred())
			Expect(p).To(HaveLen(1))

		})

	})

})
