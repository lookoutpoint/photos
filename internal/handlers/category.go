// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	"gitlab.com/lookoutpoint/photos/internal/database/utils"
	"go.mongodb.org/mongo-driver/bson"
)

func buildCategoryFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	// Some ids may have /@/... (due to custom views). Remove the trailing @/... string.
	sep := strings.Index(id, "/@/")
	if sep > 0 {
		id = id[:sep+1] // keep trailing slash
	}

	return &docs.PhotoListFilterExpr{
		Categories: []string{id},
	}, nil
}

type categoryWithCoverPhoto struct {
	docs.CategoryEntry `json:",inline"`
	Public             bool            `json:"public"`
	CoverPhoto         *coverPhotoInfo `json:"coverPhoto,omitempty"`
}

func categoryBuildChildResponse(ctx context.Context, child interface{}, public bool, coverPhoto *coverPhotoInfo) (interface{}, error) {
	var response categoryWithCoverPhoto
	response.CategoryEntry = child.(docs.CategoryEntry)
	response.Public = public
	response.CoverPhoto = coverPhoto

	return response, nil
}

// CategoryInfo returns info about the given category or category-value slug
func CategoryInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.Categories,
		func(ctx context.Context, id string, inKeys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			// Return "related" data as well.
			var keys []string
			keys = append(keys, inKeys...)
			keys = append(keys, "related")

			return db.Categories.Get(ctx, id, keys, visTokens)
		},
		buildCategoryFilter,
		nil,
	)
}

// CategoryChildren returns info about the children of the given category
func CategoryChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.Categories, buildCategoryFilter,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			// Extension parameter: { sortBy: "park" }
			// We explicitly hardcode acceptable fields here to prevent undesired custom sort keys.
			if v, ok := extensionParams["sortBy"]; ok {
				if sortBy, ok := v.(string); !ok {
					return nil, fmt.Errorf("invalid sortBy param")
				} else {
					if sortBy == "park" {
						params.CustomSortKeys = []string{"related.cat:park"}
						params.CustomView = db.Categories.ViewByRelatedParkCollection().Name()
					} else {
						return nil, fmt.Errorf("invalid sortBy field name")
					}
				}
			}

			// Extension parameter: { filterByPark: "<park>" }
			if v, ok := extensionParams["filterByPark"]; ok {
				if park, ok := v.(string); !ok {
					return nil, fmt.Errorf("invalid filterByPark param")
				} else {
					// CustomFilter may already be specified
					if params.CustomFilter == nil {
						params.CustomFilter = bson.M{}
					}
					params.CustomFilter["related.cat:park"] = park
				}
			}

			// Extension parameter: { filterByLocation: "<location-id>" }
			if v, ok := extensionParams["filterByLocation"]; ok {
				if locID, ok := v.(string); !ok {
					return nil, fmt.Errorf("invalid filterByLocation param")
				} else if !utils.IsValidSlugPath(locID) {
					return nil, apis.ErrInvalidID
				} else {
					// CustomFilter may already be specified
					if params.CustomFilter == nil {
						params.CustomFilter = bson.M{}
					}
					params.CustomFilter["related.location"] = bson.M{"$regex": "^" + locID}
				}
			}

			// Return "related" data as well.
			params.ProjectionKeys = append(params.ProjectionKeys, "related")

			return db.Categories.ListChildren(ctx, params)
		},
		categoryBuildChildResponse,
		nil,
	)
}

// CategoryUpdate updates a category. Must be admin.
func CategoryUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().Categories)
}

// CategoryUpdateRelated updates the related categories. Must be admin.
func CategoryUpdateRelated(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.CategoryUpdateRelated",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, longTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// The path is in the JSON body payload.
	var reqData struct {
		IDs []string `json:"ids,omitempty"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	if len(reqData.IDs) == 0 {
		// Default is to update the related categories for hikes and parks
		reqData.IDs = []string{"/hike/", "/park/"}
	}

	l = l.WithField("req", reqData)
	db := database.Get()

	for _, catID := range reqData.IDs {
		if err := db.Categories.UpdateRelated(ctx, catID); err != nil {
			l.WithError(err).WithField("catID", catID).Error("Could not update related for category")
			resp.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	// Update custom views.
	if err := db.Categories.UpdateCustomViews(ctx); err != nil {
		l.WithError(err).Error("Could not update custom views")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusNoContent)
}

func CategoryListEmpty(resp http.ResponseWriter, req *http.Request) {
	groupListEmpty(resp, req, database.Get().Categories.Collection())
}

func CategoryDeleteEmpty(resp http.ResponseWriter, req *http.Request) {
	groupDeleteEmpty(resp, req, database.Get().Categories.Collection())
}
