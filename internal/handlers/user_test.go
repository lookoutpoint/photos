// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	"gitlab.com/lookoutpoint/photos/internal/database"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Users", func() {
	var db *database.Database
	var ctx context.Context

	getSessionID := func(respRec *httptest.ResponseRecorder) *string {
		cookies := respRec.Result().Cookies()
		for _, cookie := range cookies {
			if cookie.Name == CookieSessionID {
				return &cookie.Value
			}
		}
		return nil
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		db = database.Get()
	})

	It("Add user", func() {
		payload := strings.NewReader(`{ "name": "junior", "password": "pw" }`)
		request := httptest.NewRequest("POST", "/", payload)
		respRec := httptest.NewRecorder()

		UserAdd(respRec, request)

		Expect(respRec.Code).To(Equal(200))
		Expect(db.Users.GetUserCount(ctx)).To(BeEquivalentTo(1))
		Expect(respRec.Result().Cookies()).To(ConsistOf(
			PointTo(MatchFields(IgnoreExtras, Fields{
				"Name":   Equal(CookieSessionID),
				"Value":  Not(HaveLen(0)),
				"MaxAge": Equal(10800),
			})),
		))

		// Trying to add again should fail
		payload = strings.NewReader(`{ "name": "admin", "password": "pw" }`)
		request = httptest.NewRequest("POST", "/", payload)
		respRec = httptest.NewRecorder()

		UserAdd(respRec, request)

		Expect(respRec.Code).ToNot(Equal(200))
		Expect(db.Users.GetUserCount(ctx)).To(BeEquivalentTo(1))

	})

	Describe("Login flow", func() {
		BeforeEach(func() {
			Expect(db.Users.AddUser(ctx, "admin", "pass!")).ToNot(HaveOccurred())
		})

		It("Login with correct credentials", func() {
			payload := strings.NewReader(`{ "name": "admin", "password": "pass!" }`)
			request := httptest.NewRequest("POST", "/", payload)
			respRec := httptest.NewRecorder()

			UserLogin(respRec, request)

			Expect(respRec.Code).To(Equal(200))
			sessionID := getSessionID(respRec)
			Expect(sessionID).ToNot(BeNil())

			// Refresh session
			request = httptest.NewRequest("POST", "/", nil)
			request.Header.Set("Cookie", CookieSessionID+"="+*sessionID)
			respRec = httptest.NewRecorder()

			UserSessionRefresh(respRec, request)

			Expect(respRec.Code).To(Equal(200))
			sessionID = getSessionID(respRec)
			Expect(sessionID).ToNot(BeNil())

			// Logout
			request = httptest.NewRequest("POST", "/", nil)
			request.Header.Set("Cookie", CookieSessionID+"="+*sessionID)
			respRec = httptest.NewRecorder()

			UserLogout(respRec, request)

			Expect(respRec.Code).To(Equal(200))
			sessionID = getSessionID(respRec)
			Expect(sessionID).ToNot(BeNil())
			Expect(*sessionID).To(Equal(""))

		})

		It("Login with wrong password", func() {
			payload := strings.NewReader(`{ "name": "admin", "password": "pass!!" }`)
			request := httptest.NewRequest("POST", "/", payload)
			respRec := httptest.NewRecorder()

			UserLogin(respRec, request)

			Expect(respRec.Code).ToNot(Equal(200))
			Expect(getSessionID(respRec)).To(BeNil())

		})

		It("Login with wrong user name", func() {
			payload := strings.NewReader(`{ "name": "admn", "password": "pass!" }`)
			request := httptest.NewRequest("POST", "/", payload)
			respRec := httptest.NewRecorder()

			UserLogin(respRec, request)

			Expect(respRec.Code).ToNot(Equal(200))
			Expect(getSessionID(respRec)).To(BeNil())

		})

	})

})
