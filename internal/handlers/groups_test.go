// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/sirupsen/logrus"

	. "github.com/onsi/gomega/gstruct"

	photomd "gitlab.com/lookoutpoint/photometadata"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Groups", func() {
	var ctx context.Context

	genPhotoMd := func(title string, keywords []string, location string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
				Location: location,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: "2020:01:01 01:02:03",
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		SetHomeGroupCountThreshold(1)
	})

	Describe("HomeGroups", func() {
		makeRequest := func() *http.Request {
			payload := strings.NewReader(`{"recent": 2, "total": 4}`)
			return httptest.NewRequest("POST", "/", payload)
		}

		It("With no photos", func() {
			request := makeRequest()
			respRec := httptest.NewRecorder()

			HomeGroups(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"folders": MatchAllKeys(Keys{
					"recent": HaveLen(0),
					"other":  HaveLen(0),
				}),
				"locations": MatchAllKeys(Keys{
					"recent": HaveLen(0),
					"other":  HaveLen(0),
				}),
				"categories": MatchAllKeys(Keys{
					"recent": HaveLen(0),
					"other":  HaveLen(0),
				}),
				"keywords": MatchAllKeys(Keys{
					"recent": HaveLen(0),
					"other":  HaveLen(0),
				}),
			}))

		})

		It("With photos", func() {
			addPhotoAndMockResize(ctx, "folder1/photo.jpg", genPhotoMd("red tree", []string{"tree", "color: red"}, "place1"))
			addPhotoAndMockResize(ctx, "folder2/photo.jpg", genPhotoMd("orange fruit", []string{"fruit", "color: orange"}, "place2"))
			addPhotoAndMockResize(ctx, "folder3/photo.jpg", genPhotoMd("blue sky", []string{"sky", "color: blue"}, "place3"))
			addPhotoAndMockResize(ctx, "folder4/photo.jpg", genPhotoMd("green grass", []string{"grass", "color: green"}, "place4"))
			makeAllPhotosPublic(ctx)

			request := makeRequest()
			respRec := httptest.NewRecorder()

			HomeGroups(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp map[string]interface{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			logrus.Infof("resp %+v", resp)
			Expect(resp).To(MatchAllKeys(Keys{
				"folders": MatchAllKeys(Keys{
					"recent": HaveLen(2),
					"other":  HaveLen(2),
				}),
				"locations": MatchAllKeys(Keys{
					"recent": HaveLen(2),
					"other":  HaveLen(2),
				}),
				"categories": MatchAllKeys(Keys{
					"recent": HaveLen(2),
					"other":  HaveLen(2),
				}),
				"keywords": MatchAllKeys(Keys{
					"recent": HaveLen(2),
					"other":  HaveLen(2),
				}),
			}))
		})
	})

})
