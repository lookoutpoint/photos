// SPDX-License-Identifier: MIT

package handlers

import (
	"regexp"
	"strings"
)

func compileReferrerRegex() *regexp.Regexp {
	domains := []string{GetDomain(), "(google|yahoo|bing|baidu|yandex|duckduckgo)\\..+"}

	if AllowLocalhost() {
		domains = append(domains, "localhost(:\\d+)?")
	}

	// Allow:
	// - empty referrer
	// - domain names with arbitrary sub-domains and url
	return regexp.MustCompile("^(|(http(s)?://(.+\\.)?(" + strings.Join(domains, "|") + ")(/.*)?))$")
}

var ReferrerRegex = compileReferrerRegex()
