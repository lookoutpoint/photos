// SPDX-License-Identifier: MIT

package handlers

import (
	"os"

	log "github.com/sirupsen/logrus"
)

var domain = func() string {
	domain := os.Getenv("DOMAIN")
	if domain != "" {
		return domain
	}
	log.Warn("Env var DOMAIN not found")
	return "photos.local"
}()

// GetDomain returns the DOMAIN env var or a default
func GetDomain() string {
	return domain
}

var allowLocalhost = func() bool {
	// Allow localhost during tests (which have photos.local domain)
	val := os.Getenv("ALLOW_LOCALHOST")
	return val == "1" || domain == "photos.local"
}()

// AllowLocalhost returns the flag controlling if localhost is a valid origin
func AllowLocalhost() bool {
	return allowLocalhost
}
