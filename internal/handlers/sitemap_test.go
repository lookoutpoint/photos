// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
	"gitlab.com/lookoutpoint/storage"
	teststorage "gitlab.com/lookoutpoint/storage/test"
)

var _ = Describe("Sitemap", func() {
	var ctx context.Context

	genPhotoMd := func(title string, date string, location string, keywords []string) *photomd.Metadata {
		return &photomd.Metadata{
			Width:  100,
			Height: 200,
			Xmp: photomd.XmpData{
				Title:    title,
				Subjects: keywords,
				Location: location,
				Rating:   4,
			},
			Exif: photomd.ExifData{
				photomd.ExifTagDateTimeOriginal: date + " 01:02:03",
			},
		}
	}

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
	})

	Describe("SitemapGenerate", func() {
		BeforeEach(func() {
			storage.Set(teststorage.New())
		})

		makeRequest := func() *http.Request {
			return httptest.NewRequest("POST", "/", nil)
		}

		makeGetRequest := func() *http.Request {
			return httptest.NewRequest("GET", "/", nil)
		}

		It("Non-admin", func() {
			request := makeRequest()
			respRec := httptest.NewRecorder()

			SitemapGenerate(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Admin", func() {
			var err error
			addPhotoAndMockResize(ctx, "f/photo.jpg", genPhotoMd("hello world", "2020:01:02", "town, city", []string{"cat: value", "red"}))
			addPhotoAndMockResize(ctx, "f/g/photo2.jpg", genPhotoMd("what a photo!", "2020:02:03", "village, state", []string{"cat: value2", "yellow", "random: asdf"}))

			// Add rich text info to folder.
			err = db.Folders.Update(ctx, "/f/g/", apis.NewGroupUpdate().SetRichText("asdf"))
			Expect(err).ToNot(HaveOccurred())

			// Add rich text info to keyword.
			err = db.Keywords.Update(ctx, "/red/", apis.NewGroupUpdate().SetRichText("blah"))
			Expect(err).ToNot(HaveOccurred())

			// Add rich text info to category.
			err = db.Categories.Update(ctx, "/cat/value/", apis.NewGroupUpdate().SetRichText("text"))
			Expect(err).ToNot(HaveOccurred())

			// Add rich text info to location.
			err = db.Locations.Update(ctx, "/city/town/", apis.NewGroupUpdate().SetRichText("xyz"))
			Expect(err).ToNot(HaveOccurred())

			// Timeline group
			_, err = db.TimelineGroups.Create(ctx, "/f/g/", "", nil)
			Expect(err).ToNot(HaveOccurred())

			_, err = db.TimelineGroups.Create(ctx, "/f/g/", "sub-timeline", nil)
			Expect(err).ToNot(HaveOccurred())

			Expect(db.TimelineGroups.AddVisibilityToken(ctx, "/:f:g:/", docs.VisTokenPublic, true)).ToNot(HaveOccurred())

			// Page
			_, err = db.Pages.Create(ctx, "about")
			Expect(err).ToNot(HaveOccurred())

			Expect(db.Pages.AddVisibilityToken(ctx, "/about/", docs.VisTokenPublic, false)).ToNot(HaveOccurred())

			// Mark all photo as public.
			makeAllPhotosPublic(ctx)

			// Use admin
			adminSession := createAdminUserAndSession(ctx)

			request := addSessionToRequest(makeRequest(), adminSession)
			respRec := httptest.NewRecorder()

			SitemapGenerate(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			// Get sitemap
			request = makeGetRequest()
			respRec = httptest.NewRecorder()

			SitemapGet(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			body := respRec.Body.String()

			Expect(body).To(MatchRegexp("/collection/f/@children/"))
			Expect(body).To(MatchRegexp("/collection/f/g/@photos/"))
			Expect(body).To(MatchRegexp("/collection/f/g/@info/"))

			Expect(body).To(MatchRegexp("/keyword/red/@photos/"))
			Expect(body).To(MatchRegexp("/keyword/red/@info/"))

			Expect(body).To(MatchRegexp("/category/cat/@children/"))
			Expect(body).To(MatchRegexp("/category/cat/value/@photos/"))
			Expect(body).To(MatchRegexp("/category/cat/value/@info/"))
			Expect(body).To(MatchRegexp("/category/cat/value2/@photos/"))
			Expect(body).To(MatchRegexp("/category/random/@children/"))
			Expect(body).To(MatchRegexp("/category/random/asdf/@photos/"))

			Expect(body).To(MatchRegexp("/location/city/@children/"))
			Expect(body).To(MatchRegexp("/location/city/town/@photos/"))
			Expect(body).To(MatchRegexp("/location/city/town/@info/"))
			Expect(body).To(MatchRegexp("/location/state/@children/"))
			Expect(body).To(MatchRegexp("/location/state/village/@photos/"))

			Expect(body).To(MatchRegexp("/date/2020/@children/"))
			Expect(body).To(MatchRegexp("/date/2020/01/@children/"))
			Expect(body).To(MatchRegexp("/date/2020/01/02/@photos/"))
			Expect(body).To(MatchRegexp("/date/2020/02/@children/"))
			Expect(body).To(MatchRegexp("/date/2020/02/03/@photos/"))

			Expect(body).To(MatchRegexp("/date/2020/02/03/@photos/"))

			Expect(body).To(MatchRegexp("/collection/f/g/@timeline/"))
			Expect(body).To(MatchRegexp("/collection/f/g/@timeline/sub-timeline/"))

			Expect(body).To(MatchRegexp("/about/"))

		})

	})

})
