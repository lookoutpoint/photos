// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
)

// DbSchemaVersion returns info about the database schema version. Admin only.
func DbSchemaVersion(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.DbSchemaVersion",
		"url":  req.URL,
	})

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	db := database.Get()

	var respData struct {
		Current int `json:"current"`
		Latest  int `json:"latest"`
	}
	var err error

	respData.Current, respData.Latest, err = db.GetSchemaVersion(ctx)
	if err != nil {
		l.WithError(err).Error("Could not get schema version")
	}

	renderJSON(resp, respData, l)
}

// DbSchemaUpdate updates schema to latest version. Must be admin.
func DbSchemaUpdate(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.DbSchemaUpdate",
		"url":  req.URL,
	})

	// Set up context (no timeout)
	ctx := context.Background()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	db := database.Get()

	if err := db.UpdateSchemas(); err != nil {
		l.WithError(err).Error("Could not update schemas")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Success
	resp.WriteHeader(http.StatusNoContent)
}

func DbCheckAndFixVisTokenCounts(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.DbCheckAndFixVisTokenCounts",
		"url":  req.URL,
	})

	// Set up context (no timeout)
	ctx := context.Background()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Parse list parameters from payload.
	var params struct {
		CheckOnly bool `json:"checkOnly"`
	}

	if err := decodeJSONBody(req, &params); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("params", params)

	db := database.Get()

	errors, err := db.CheckAndFixVisTokenCounts(ctx, params.CheckOnly)
	if err != nil {
		l.WithError(err).Error("Could not run task")
		resp.WriteHeader(http.StatusInternalServerError)
	}

	renderJSON(resp, errors, l)
}
