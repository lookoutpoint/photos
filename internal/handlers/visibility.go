// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"net/http"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

func getVisibilityTokensFromRequest(ctx context.Context, req *http.Request) []string {
	var visTokens []string
	if IsAdminSession(ctx, req) {
		// Admin - see everything!
		visTokens = append(visTokens, docs.VisTokenAll)
	} else {
		// Public
		visTokens = append(visTokens, docs.VisTokenPublic)

		// Apply visibility token activation values. Validate them.
		visTokens = append(visTokens, extractAndValidateVisTokenValuesCookie(ctx, req)...)
	}

	return visTokens
}

// Cache of actValues -> (token id, update time)
// Used to make activation value validation and lookups more efficient instead of hitting db each time.
//
// This cache is local to the process. The process may be handling multiple http requests at the same time,
// so there is a RWMutex around the cache.
type actValueCacheEntry struct {
	tokenID    string
	updateTime time.Time
}

var actValueCache = map[string]actValueCacheEntry{}
var actValueCacheMutex sync.RWMutex

func extractAndValidateVisTokenValuesCookie(ctx context.Context, req *http.Request) []string {
	l := log.WithField("func", "extractAndValidateVisTokenValuesCookie")

	// Get cookie, if it's there
	cookie, err := req.Cookie(CookieVisTokenValues)
	if err != nil {
		return nil
	}

	db := database.Get()

	// The cookie has comma-separated activation values
	actValues := strings.Split(cookie.Value, ",")

	// Validate each one. Use a local memory cache to speed up the look up.
	visTokens := []string{}
	timeNow := time.Now()
	timeThreshold := timeNow.Add(-time.Hour) // can use up to hour old entries in the cache

	for _, actValue := range actValues {
		// Read from cache
		actValueCacheMutex.RLock()
		cache, ok := actValueCache[actValue]
		actValueCacheMutex.RUnlock()

		if ok && cache.updateTime.After(timeThreshold) {
			visTokens = append(visTokens, cache.tokenID)
		} else {
			// Not in cache or too old. Query database to map to token id.
			token, err := db.Visibility.GetByActivationValue(ctx, actValue, []string{"id"})
			if err != nil {
				// Error; skip this value
				l.WithError(err).Warnf("Activation value lookup failed (actValue=%v)", actValue)
				continue
			}

			visTokens = append(visTokens, *token.ID)

			// Put it in the cache.
			actValueCacheMutex.Lock()
			actValueCache[actValue] = actValueCacheEntry{
				tokenID:    *token.ID,
				updateTime: timeNow,
			}
			actValueCacheMutex.Unlock()
		}
	}

	return visTokens
}

func addVisTokenValueCookie(req *http.Request, resp http.ResponseWriter, actValue string) {
	// Build activation value set (to prevent duplicates)
	actValueSet := map[string]bool{}

	curCookie, _ := req.Cookie(CookieVisTokenValues)
	if curCookie != nil {
		actValues := strings.Split(curCookie.Value, ",")
		for _, av := range actValues {
			actValueSet[av] = true
		}
	}

	actValueSet[actValue] = true

	// Convert to a set of unique activation values
	actValues := make([]string, 0, len(actValueSet))
	for av := range actValueSet {
		actValues = append(actValues, av)
	}

	// Session cookie
	http.SetCookie(resp, &http.Cookie{
		Name:     CookieVisTokenValues,
		Value:    strings.Join(actValues, ","),
		Domain:   GetDomain(),
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
		SameSite: cookieSameSite,
	})
}

func clearVisTokenValueCookie(resp http.ResponseWriter) {
	http.SetCookie(resp, &http.Cookie{
		Name:     CookieVisTokenValues,
		Domain:   GetDomain(),
		MaxAge:   -1, // delete
		Path:     "/",
		Secure:   true,
		HttpOnly: true,
		SameSite: cookieSameSite,
	})
}

// VisibilityList returns a list of all visibility tokens. Admin only.
func VisibilityList(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityList",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be an admin.
	if !IsAdminSession(ctx, req) {
		l.Error("No admin session")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// There is no request data.
	var reqData struct{}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	db := database.Get()

	// List all tokens.
	tokens, err := db.Visibility.List(ctx, []string{"id", "display", "links"})
	if err != nil {
		l.WithError(err).Error("Could not list visibility tokens")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	renderJSON(resp, tokens, l)
}

// VisibilityCreate creates a new visibility token. Admin only.
func VisibilityCreate(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityCreate",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be an admin.
	if !IsAdminSession(ctx, req) {
		l.Error("No admin session")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Request data
	var reqData struct {
		Name string `json:"name"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	db := database.Get()

	// Create token
	tokenID, err := db.Visibility.Create(ctx, reqData.Name)
	if err != nil {
		l.WithError(err).Error("Could not create token")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Only response with id
	respToken := docs.VisibilityToken{
		GroupBase: docs.GroupBase{ID: &tokenID},
	}

	renderJSON(resp, respToken, l)
}

// VisibilityCreateLink creates a new link on a visibility token. Admin only.
func VisibilityCreateLink(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityCreateLink",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be an admin.
	if !IsAdminSession(ctx, req) {
		l.Error("No admin session")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Request data
	var reqData struct {
		ID       string  `json:"id"`
		Redirect *string `json:"redirect,omitempty"`
		Comment  *string `json:"comment,omitempty"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	db := database.Get()

	// Create link
	link, err := db.Visibility.CreateLink(ctx, reqData.ID, &docs.VisibilityTokenLink{
		Redirect: reqData.Redirect,
		Comment:  reqData.Comment,
	})
	if err != nil {
		l.WithError(err).Error("Could not create link")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	renderJSON(resp, link, l)
}

// VisibilityUpdateLink updates an existing visibility token link. Admin only.
func VisibilityUpdateLink(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityUpdateLink",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be an admin.
	if !IsAdminSession(ctx, req) {
		l.Error("No admin session")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Request data
	var reqData struct {
		ActValue string  `json:"actValue"`
		Redirect *string `json:"redirect,omitempty"`
		Comment  *string `json:"comment,omitempty"`
		Disabled *bool   `json:"disabled,omitempty"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	db := database.Get()

	// Update link
	err := db.Visibility.UpdateLink(ctx, reqData.ActValue, &docs.VisibilityTokenLink{
		Redirect: reqData.Redirect,
		Comment:  reqData.Comment,
		Disabled: reqData.Disabled,
	})
	if err != nil {
		l.WithError(err).Error("Could not update link")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusNoContent)
}

// VisibilityActivateLink activates a visibility token link.
func VisibilityActivateLink(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityActivateLink",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Request data
	var reqData struct {
		ActValue string `json:"actValue"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	db := database.Get()

	// Activate link
	token, err := db.Visibility.ActivateLink(ctx, reqData.ActValue)
	if err != nil {
		l.WithError(err).Error("Could not activate link")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Upon successful link activation, add the activation value to the visibility token cookie
	// and return the redirect.
	addVisTokenValueCookie(req, resp, reqData.ActValue)

	type ResponseData struct {
		Redirect string `json:"redirect"`
	}
	respData := ResponseData{}
	if len(token.Links) > 0 && token.Links[0].Redirect != nil {
		respData.Redirect = *token.Links[0].Redirect
	}

	if len(respData.Redirect) == 0 {
		// Default redirect
		respData.Redirect = "/"
	}

	renderJSON(resp, respData, l)
}

// VisibilityTokensClear clears any active tokens.
func VisibilityTokensClear(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityTokensClear",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	clearVisTokenValueCookie(resp)

	resp.WriteHeader(http.StatusNoContent)
}

// VisibilityHasTokens checks if the vis token value cookie has a value.
// Does not validate that the tokens are valid.
func VisibilityHasTokens(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.VisibilityHasTokens",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Get cookie, if it's there
	cookie, err := req.Cookie(CookieVisTokenValues)
	if err != nil || len(cookie.Value) == 0 {
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	resp.WriteHeader(http.StatusNoContent)
}
