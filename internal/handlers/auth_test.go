// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"net/http"

	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

// Test implementation of Auth that doesn't do any checking
type authTestImpl struct {
	allowAuth bool
}

func NewTestAuth(allowAuth bool) Auth {
	return &authTestImpl{allowAuth}
}

func (auth authTestImpl) CheckServiceAccountAuthorization(ctx context.Context, req *http.Request) bool {
	return auth.allowAuth
}
