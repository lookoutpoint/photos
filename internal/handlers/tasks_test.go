// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"

	. "gitlab.com/lookoutpoint/photos/internal/handlers"
	taskspb "google.golang.org/genproto/googleapis/cloud/tasks/v2"
)

// Test implementation of Tasks that doesn't do anything
type tasksTestImpl struct {
}

func NewTestTasks() (Tasks, error) {
	return &tasksTestImpl{}, nil
}

func (tasks tasksTestImpl) CreateTask(ctx context.Context, req *taskspb.CreateTaskRequest) error {
	return nil
}
