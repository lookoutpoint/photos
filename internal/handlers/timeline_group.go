// SPDX-License-Identifier: MIT

package handlers

import (
	"context"
	"fmt"
	"math"
	"net/http"
	"reflect"

	log "github.com/sirupsen/logrus"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
)

func buildTimelineGroupFilter(ctx context.Context, id string) (*docs.PhotoListFilterExpr, error) {
	db := database.Get()
	tg, err := db.TimelineGroups.Get(ctx, id, []string{"filter"}, []string{docs.VisTokenAll})
	if err != nil {
		return nil, err
	}
	return tg.Filter, nil
}

func getAspectRatioRange(ctx context.Context, doc interface{}) (float32, float32) {
	var group *docs.TimelineGroup
	var ok bool

	if group, ok = doc.(*docs.TimelineGroup); !ok {
		if groupVal, ok := doc.(docs.TimelineGroup); ok {
			group = &groupVal
		} else {
			// Timeline group is embedded in the doc
			group = reflect.ValueOf(doc).Elem().FieldByName("TimelineGroup").Addr().Interface().(*docs.TimelineGroup)
		}
	}

	if group.StartDate == nil || group.EndDate == nil {
		return 0, 0
	}

	diffDays := math.Ceil(group.EndDate.Sub(*group.StartDate).Hours() / 24)
	if diffDays < 1 {
		return 0, 0
	}

	// Use the number of days to set the aspect ratio range. Wider spans should require higher aspect ratios.
	// This accommodates how timeline groups are displayed.
	min := math.Min(diffDays, 3) * 0.9
	max := diffDays * 2

	return float32(min), float32(max)
}

// TimelineGroupCreate handles timeline group creation. Admin only.
func TimelineGroupCreate(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.TimelineGroupCreate",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		FolderID string                    `json:"folder"`
		Name     string                    `json:"name"`
		Filter   *docs.PhotoListFilterExpr `json:"filter,omitempty"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusOK)
		renderErrorToJSON(resp, err, l)
		return
	}

	// Validate filter.
	if reqData.Filter != nil {
		filterVal := reqData.Filter.Clone()
		filterVal.AssignDefaults()
		if err := filterVal.Validate(); err != nil {
			l.WithError(err).Error("Filter failed validation")
			resp.WriteHeader(http.StatusOK)
			renderErrorToJSON(resp, err, l)
			return
		}
	}

	l = l.WithField("req", reqData)
	db := database.Get()

	// Validate that folder exists
	if !db.Folders.Exists(ctx, reqData.FolderID) {
		l.Error("Folder does not exist")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	// Create
	id, err := db.TimelineGroups.Create(ctx, reqData.FolderID, reqData.Name, reqData.Filter)
	if err != nil {
		l.WithError(err).Error("Could not create timeline group")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// On success, render id as result
	renderJSON(resp, id, l)
}

// TimelineGroupDelete deletes a timeline group. Admin only.
func TimelineGroupDelete(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupDelete(resp, req, db.TimelineGroups)
}

// TimelineGroupSync syncs a timeline group. Admin only.
func TimelineGroupSync(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.TimelineGroupSync",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		ID string `json:"id"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusBadRequest)
		return
	}

	l = l.WithField("req", reqData)

	// Sync
	db := database.Get()
	err := db.TimelineGroups.Sync(ctx, reqData.ID)
	if err != nil {
		l.WithError(err).Error("Could not sync timeline group")
		resp.WriteHeader(http.StatusInternalServerError)
		return
	}

	// No data on response
	resp.WriteHeader(http.StatusNoContent)
}

// TimelineGroupInfo returns info about the given timeline group
func TimelineGroupInfo(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupInfo(resp, req, db.TimelineGroups,
		func(ctx context.Context, id string, keys []string, visTokens []string, isAdminSession bool) (interface{}, error) {
			keys = append(keys, "startDate", "endDate", "filter")
			if isAdminSession {
				keys = append(keys, "selfVisTokens")
			}

			group, err := db.TimelineGroups.Get(ctx, id, keys, visTokens)
			if err != nil {
				return nil, err
			}

			if !isAdminSession {
				return group, nil
			}

			// Provide the unwrapped filter (for editing).
			var adminGroup struct {
				docs.TimelineGroup
				UnwrappedFilter *docs.PhotoListFilterExpr `json:"unwrappedFilter"`
			}
			adminGroup.TimelineGroup = *group
			adminGroup.UnwrappedFilter = db.TimelineGroups.UnwrapFilter(group.Filter)

			return &adminGroup, nil
		},
		buildTimelineGroupFilter,
		getAspectRatioRange,
	)
}

// TimelineGroupChildren returns info about the children of the given timeline group
func TimelineGroupChildren(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupChildren(resp, req, db.TimelineGroups, buildTimelineGroupFilter,
		func(ctx context.Context, params *apis.GroupListParams, extensionParams map[string]interface{}) (interface{}, error) {
			// Also return startDate and endDate for each child
			newParams := *params
			newParams.ProjectionKeys = make([]string, 0, len(params.ProjectionKeys)+2)
			newParams.ProjectionKeys = append(newParams.ProjectionKeys, params.ProjectionKeys...)
			newParams.ProjectionKeys = append(newParams.ProjectionKeys, "startDate", "endDate")

			if v, ok := extensionParams["sortBy"]; ok {
				if sortBy, ok := v.(string); !ok {
					return nil, fmt.Errorf("invalid sortBy param")
				} else {
					if sortBy == "timestamp" {
						params.CustomSortKeys = []string{"timestamp"}
					} else {
						return nil, fmt.Errorf("invalid sortBy field name")
					}
				}
			}

			return db.TimelineGroups.ListChildren(ctx, &newParams)
		},
		func(ctx context.Context, child interface{}, public bool, coverPhoto *coverPhotoInfo) (interface{}, error) {
			var response struct {
				docs.TimelineGroup
				Public     bool            `json:"public"`
				CoverPhoto *coverPhotoInfo `json:"coverPhoto,omitempty"`
			}

			response.TimelineGroup = child.(docs.TimelineGroup)
			response.Public = public
			response.CoverPhoto = coverPhoto

			return response, nil
		},
		getAspectRatioRange,
	)
}

// TimelineGroupUpdate updates a timeline group. Must be admin.
func TimelineGroupUpdate(resp http.ResponseWriter, req *http.Request) {
	groupUpdate(resp, req, database.Get().TimelineGroups)
}

// TimelineGroupUpdateFilter updates the filter for a timeline group. Must be admin.
func TimelineGroupUpdateFilter(resp http.ResponseWriter, req *http.Request) {
	l := log.WithFields(log.Fields{
		"func": "handlers.TimelineGroupUpdateFilter",
		"url":  req.URL,
	})

	// Validate HTTP method
	if req.Method != "POST" {
		l.WithField("method", req.Method).Error("Unsupported HTTP method")
		resp.WriteHeader(http.StatusNotImplemented)
		return
	}

	// Set up context with timeout
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, shortTimeout)
	defer cancel()

	// Must be admin
	if !IsAdminSession(ctx, req) {
		l.Error("Not admin!")
		resp.WriteHeader(http.StatusNotFound)
		return
	}

	// Decode request
	var reqData struct {
		ID     string                   `json:"id"`
		Filter docs.PhotoListFilterExpr `json:"filter"`
	}

	if err := decodeJSONBody(req, &reqData); err != nil {
		l.WithError(err).Error("Could not decode body payload")
		resp.WriteHeader(http.StatusOK)
		renderErrorToJSON(resp, err, l)
		return
	}

	// Validate filter.
	filterVal := reqData.Filter.Clone()
	filterVal.AssignDefaults()
	if err := filterVal.Validate(); err != nil {
		l.WithError(err).Error("Filter failed validation")
		resp.WriteHeader(http.StatusOK)
		renderErrorToJSON(resp, err, l)
		return
	}

	l = l.WithField("req", reqData)

	// Do update
	db := database.Get()
	err := db.TimelineGroups.UpdateFilter(ctx, reqData.ID, &reqData.Filter)
	if err != nil {
		l.WithError(err).Error("Could not update timeline group filter")
		if err == apis.ErrInvalidID {
			resp.WriteHeader(http.StatusBadRequest)
		} else {
			resp.WriteHeader(http.StatusInternalServerError)
		}
		return
	}

	// Success
	resp.WriteHeader(http.StatusNoContent)
}

// TimelineGroupVisibilityToken can add or delete a visibility token on a timeline group. Admin only.
func TimelineGroupVisibilityToken(resp http.ResponseWriter, req *http.Request) {
	db := database.Get()
	groupVisibilityToken(resp, req, db.TimelineGroups)
}
