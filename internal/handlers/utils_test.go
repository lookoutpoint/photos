// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"

	. "github.com/onsi/gomega"
	photomd "gitlab.com/lookoutpoint/photometadata"
	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/apis"
	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

func readResponseBody(respRec *httptest.ResponseRecorder) []byte {
	body := respRec.Result().Body
	defer body.Close()

	data, err := ioutil.ReadAll(body)
	Expect(err).ToNot(HaveOccurred())

	return data
}

func makeAllPhotosPublic(ctx context.Context) {
	db := database.Get()
	Expect(db.Folders.AddVisibilityToken(ctx, "/", apis.FolderTargetFullTree, docs.VisTokenPublic)).ToNot(HaveOccurred())
}

func addPhotoAndMockResize(ctx context.Context, objectID string, md *photomd.Metadata) *docs.Photo {
	db := database.Get()

	photo, err := db.Photos.AddOrUpdate(ctx, "", objectID, 0, 100, md)
	Expect(err).ToNot(HaveOccurred())

	return photo
}

func createAdminUserAndSession(ctx context.Context) *docs.Session {
	db := database.Get()

	Expect(db.Users.AddUser(ctx, "admin", "password")).ToNot(HaveOccurred())
	session, err := db.Users.Login(ctx, "admin", "password")
	Expect(err).ToNot(HaveOccurred())

	return session
}

func addSessionToRequest(request *http.Request, session *docs.Session) *http.Request {
	if session != nil {
		request.Header.Add("cookie", CookieSessionID+"="+string(session.ID))
	}
	return request
}
