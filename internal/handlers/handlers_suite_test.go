// SPDX-License-Identifier: MIT

package handlers_test

import (
	"context"
	"os"
	"testing"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/database/testutils"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var db *database.Database

func TestHandlers(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Handlers Suite")
}

var _ = BeforeSuite(func() {
	// Force colors and no timestamps
	log.SetFormatter(&log.TextFormatter{
		ForceColors:   true,
		FullTimestamp: false,
	})

	tasks, _ := NewTestTasks()
	SetTasks(tasks)

	mdb := testutils.NewTestDb()
	db = database.NewWithDb(mdb)
	database.Set(db)

	clearTestDatabase()
	Expect(db.UpdateIndexes()).ToNot(HaveOccurred())

	// Log level (by default only panic-level messages are shown in test mode)
	log.SetLevel(log.PanicLevel)
	level, err := log.ParseLevel(os.Getenv("LOGLEVEL"))
	if err == nil {
		log.SetLevel(level)
	}
})

var _ = AfterEach(func() {
	Expect(db.CheckAndFixVisTokenCounts(context.Background(), true)).To(HaveLen(0))
})

func clearTestDatabase() {
	testutils.ClearTestDatabase(context.Background(), db)
}
