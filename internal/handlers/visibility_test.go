// SPDX-License-Identifier: MIT

package handlers_test

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	"gitlab.com/lookoutpoint/photos/internal/database/docs"
	. "gitlab.com/lookoutpoint/photos/internal/handlers"
)

var _ = Describe("Visibility", func() {
	var ctx context.Context
	var adminSession *docs.Session

	BeforeEach(func() {
		clearTestDatabase()

		ctx = context.Background()
		adminSession = createAdminUserAndSession(ctx)
	})

	Describe("Create", func() {
		type Response map[string]interface{}

		makeRequest := func(name string) *http.Request {
			data := fmt.Sprintf(`{ "name": "%s" }`, name)
			payload := strings.NewReader(data)
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Create with admin", func() {
			request := addSessionToRequest(makeRequest("My Special Token"), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityCreate(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"id": Equal("/my-special-token/"),
			}))
		})

		It("Create token with same name", func() {
			Expect(db.Visibility.Create(ctx, "asdf")).ToNot(BeNil())

			request := addSessionToRequest(makeRequest("asdf"), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityCreate(respRec, request)

			Expect(respRec.Code).To(Equal(500))
		})
	})

	Describe("List", func() {
		type Response []map[string]interface{}

		makeRequest := func() *http.Request {
			payload := strings.NewReader("{}")
			return httptest.NewRequest("POST", "/", payload)
		}

		It("Non-admin request", func() {
			request := makeRequest()
			respRec := httptest.NewRecorder()

			VisibilityList(respRec, request)

			Expect(respRec.Code).To(Equal(404))
		})

		It("Admin request with no tokens", func() {
			request := addSessionToRequest(makeRequest(), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(0))

		})

		It("Admin request with tokens", func() {
			Expect(db.Visibility.GetOrCreate(ctx, "c", nil)).ToNot(BeNil())
			Expect(db.Visibility.GetOrCreate(ctx, "X", nil)).ToNot(BeNil())
			Expect(db.Visibility.GetOrCreate(ctx, "a", nil)).ToNot(BeNil())

			request := addSessionToRequest(makeRequest(), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(3))
			Expect(resp).To(Equal(Response{{"id": "/a/", "display": "a"}, {"id": "/c/", "display": "c"}, {"id": "/x/", "display": "X"}}))
		})

		It("Admin request with token with links", func() {
			// Token
			Expect(db.Visibility.GetOrCreate(ctx, "a", nil)).ToNot(BeNil())

			// Link 1
			r := "/folder"
			link1, err := db.Visibility.CreateLink(ctx, "/a/", &docs.VisibilityTokenLink{Redirect: &r})
			Expect(err).ToNot(HaveOccurred())

			// Link 2
			r = "/folder2"
			c := "hello"
			link2, err := db.Visibility.CreateLink(ctx, "/a/", &docs.VisibilityTokenLink{Redirect: &r, Comment: &c})
			Expect(err).ToNot(HaveOccurred())

			d := true
			err = db.Visibility.UpdateLink(ctx, *link2.ActivationValue, &docs.VisibilityTokenLink{Disabled: &d})
			Expect(err).ToNot(HaveOccurred())

			// List
			request := addSessionToRequest(makeRequest(), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityList(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(HaveLen(1))
			Expect(resp[0]).To(MatchAllKeys(Keys{
				"id":      Equal("/a/"),
				"display": Equal("a"),
				"links": ConsistOf(
					MatchAllKeys(Keys{
						"activationValue": Equal(*link1.ActivationValue),
						"redirect":        Equal("/folder"),
					}),
					MatchAllKeys(Keys{
						"activationValue": Equal(*link2.ActivationValue),
						"redirect":        Equal("/folder2"),
						"comment":         Equal("hello"),
						"disabled":        BeTrue(),
					}),
				),
			}))
		})

	})

	Describe("CreateLink", func() {
		type RequestData struct {
			ID       string `json:"id"`
			Redirect string `json:"redirect,omitempty"`
			Comment  string `json:"comment,omitempty"`
		}
		type Response map[string]interface{}

		makeRequest := func(data *RequestData) *http.Request {
			b, err := json.Marshal(data)
			Expect(err).ToNot(HaveOccurred())

			payload := bytes.NewReader(b)
			return httptest.NewRequest("POST", "/", payload)
		}

		BeforeEach(func() {
			_, err := db.Visibility.GetOrCreate(ctx, "token", nil)
			Expect(err).ToNot(HaveOccurred())
		})

		It("CreateLink with admin", func() {
			request := addSessionToRequest(makeRequest(&RequestData{ID: "/token/", Redirect: "/tada", Comment: "hello world"}), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityCreateLink(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"activationValue": Ignore(),
				"redirect":        Equal("/tada"),
				"comment":         Equal("hello world"),
			}))

			token, err := db.Visibility.Get(ctx, "/token/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token.Links).To(ConsistOf(MatchFields(IgnoreExtras, Fields{
				"Redirect": PointTo(Equal("/tada")),
				"Comment":  PointTo(Equal("hello world")),
			})))

		})

		It("CreateLink without admin", func() {
			request := makeRequest(&RequestData{ID: "/token/", Redirect: "/tada"})
			respRec := httptest.NewRecorder()

			VisibilityCreateLink(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			token, err := db.Visibility.Get(ctx, "/token/", nil)
			Expect(err).ToNot(HaveOccurred())
			Expect(token.Links).To(HaveLen(0))

		})

	})

	Describe("UpdateLink", func() {
		type RequestData struct {
			ActValue string `json:"actValue"`
			Redirect string `json:"redirect,omitempty"`
			Comment  string `json:"comment,omitempty"`
			Disabled *bool  `json:"disabled,omitempty"`
		}

		makeRequest := func(data *RequestData) *http.Request {
			b, err := json.Marshal(data)
			Expect(err).ToNot(HaveOccurred())

			payload := bytes.NewReader(b)
			return httptest.NewRequest("POST", "/", payload)
		}

		var link1ActValue string
		var link2ActValue string

		BeforeEach(func() {
			_, err := db.Visibility.GetOrCreate(ctx, "token", nil)
			Expect(err).ToNot(HaveOccurred())

			link, err := db.Visibility.CreateLink(ctx, "/token/", nil)
			Expect(err).ToNot(HaveOccurred())
			link1ActValue = *link.ActivationValue

			link, err = db.Visibility.CreateLink(ctx, "/token/", nil)
			Expect(err).ToNot(HaveOccurred())
			link2ActValue = *link.ActivationValue

		})

		It("UpdateLink with admin", func() {
			// Update link 1
			d := false
			request := addSessionToRequest(
				makeRequest(&RequestData{ActValue: link1ActValue, Redirect: "/tada", Comment: "hello world", Disabled: &d}), adminSession)
			respRec := httptest.NewRecorder()

			VisibilityUpdateLink(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Visibility.Get(ctx, "/token/", nil)).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link1ActValue)),
						"Redirect":        PointTo(Equal("/tada")),
						"Comment":         PointTo(Equal("hello world")),
						"Disabled":        PointTo(BeFalse()),
					}),
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link2ActValue)),
						"Redirect":        BeNil(),
						"Comment":         BeNil(),
						"Disabled":        BeNil(),
					}),
				),
			})))

			// Update link 2
			d = true
			request = addSessionToRequest(
				makeRequest(&RequestData{ActValue: link2ActValue, Redirect: "/asdf", Comment: "xyz", Disabled: &d}), adminSession)
			respRec = httptest.NewRecorder()

			VisibilityUpdateLink(respRec, request)

			Expect(respRec.Code).To(Equal(204))

			Expect(db.Visibility.Get(ctx, "/token/", nil)).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link1ActValue)),
						"Redirect":        PointTo(Equal("/tada")),
						"Comment":         PointTo(Equal("hello world")),
						"Disabled":        PointTo(BeFalse()),
					}),
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link2ActValue)),
						"Redirect":        PointTo(Equal("/asdf")),
						"Comment":         PointTo(Equal("xyz")),
						"Disabled":        PointTo(BeTrue()),
					}),
				),
			})))

		})

		It("UpdateLink with no admin", func() {
			// Update link 1
			request := makeRequest(&RequestData{ActValue: link1ActValue, Redirect: "/tada", Comment: "hello world"})
			respRec := httptest.NewRecorder()

			VisibilityUpdateLink(respRec, request)

			Expect(respRec.Code).To(Equal(404))

			Expect(db.Visibility.Get(ctx, "/token/", nil)).To(PointTo(MatchFields(IgnoreExtras, Fields{
				"Links": ConsistOf(
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link1ActValue)),
						"Redirect":        BeNil(),
						"Comment":         BeNil(),
						"Disabled":        BeNil(),
					}),
					MatchFields(IgnoreExtras, Fields{
						"ActivationValue": PointTo(Equal(link2ActValue)),
						"Redirect":        BeNil(),
						"Comment":         BeNil(),
						"Disabled":        BeNil(),
					}),
				),
			})))
		})

	})

	Describe("ActivateLink", func() {
		type Response map[string]interface{}

		makeRequest := func(actValue string) *http.Request {
			data := fmt.Sprintf(`{ "actValue": "%s" }`, actValue)
			payload := strings.NewReader(data)
			return httptest.NewRequest("POST", "/", payload)
		}

		var link1ActValue string
		var link2ActValue string

		BeforeEach(func() {
			// Token 1
			_, err := db.Visibility.GetOrCreate(ctx, "token", nil)
			Expect(err).ToNot(HaveOccurred())

			link, err := db.Visibility.CreateLink(ctx, "/token/", nil)
			Expect(err).ToNot(HaveOccurred())
			link1ActValue = *link.ActivationValue

			// Token 2
			_, err = db.Visibility.GetOrCreate(ctx, "token2", nil)
			Expect(err).ToNot(HaveOccurred())

			r := "/path/to/folder"
			link, err = db.Visibility.CreateLink(ctx, "/token2/", &docs.VisibilityTokenLink{Redirect: &r})
			Expect(err).ToNot(HaveOccurred())
			link2ActValue = *link.ActivationValue

		})

		It("Activate links", func() {
			// Activate link 1
			request := makeRequest(link1ActValue)
			respRec := httptest.NewRecorder()

			VisibilityActivateLink(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			var resp Response
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"redirect": Equal("/"),
			}))

			Expect(respRec.Result().Cookies()).To(ConsistOf(
				PointTo(MatchFields(IgnoreExtras, Fields{
					"Name":  Equal(CookieVisTokenValues),
					"Value": Equal(link1ActValue),
				})),
			))

			// Activate link 2
			request = makeRequest(link2ActValue)
			request.Header.Set("cookie", CookieVisTokenValues+"="+link1ActValue)
			respRec = httptest.NewRecorder()

			VisibilityActivateLink(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"redirect": Equal("/path/to/folder"),
			}))

			cookies := respRec.Result().Cookies()
			Expect(cookies).To(ConsistOf(
				PointTo(MatchFields(IgnoreExtras, Fields{
					"Name": Equal(CookieVisTokenValues),
				})),
			))
			Expect(strings.Split(cookies[0].Value, ",")).To(ConsistOf(link1ActValue, link2ActValue))

			// Activate link 1 again (should have no duplicate cookie values)
			request = makeRequest(link1ActValue)
			request.Header.Set("cookie", CookieVisTokenValues+"="+cookies[0].Value)
			respRec = httptest.NewRecorder()

			VisibilityActivateLink(respRec, request)

			Expect(respRec.Code).To(Equal(200))

			resp = Response{}
			Expect(json.Unmarshal(readResponseBody(respRec), &resp)).ToNot(HaveOccurred())
			Expect(resp).To(MatchAllKeys(Keys{
				"redirect": Equal("/"),
			}))

			cookies = respRec.Result().Cookies()
			Expect(cookies).To(ConsistOf(
				PointTo(MatchFields(IgnoreExtras, Fields{
					"Name": Equal(CookieVisTokenValues),
				})),
			))
			Expect(strings.Split(cookies[0].Value, ",")).To(ConsistOf(link1ActValue, link2ActValue))

		})

		It("Activate disabled link", func() {
			d := true
			Expect(db.Visibility.UpdateLink(ctx, link1ActValue, &docs.VisibilityTokenLink{Disabled: &d})).ToNot(HaveOccurred())

			// Activate link 1
			request := makeRequest(link1ActValue)
			respRec := httptest.NewRecorder()

			VisibilityActivateLink(respRec, request)

			Expect(respRec.Code).To(Equal(400))
			Expect(respRec.Result().Cookies()).To(HaveLen(0))
		})

		It("Activate bad link", func() {
			// Activate link 1
			request := makeRequest("bad")
			respRec := httptest.NewRecorder()

			VisibilityActivateLink(respRec, request)

			Expect(respRec.Code).To(Equal(400))
			Expect(respRec.Result().Cookies()).To(HaveLen(0))
		})

	})

})
