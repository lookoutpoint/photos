// SPDX-License-Identifier: MIT

package main

import (
	"net/http"
	"net/url"

	"gitlab.com/lookoutpoint/photos/cmd/common"
	"gitlab.com/lookoutpoint/photos/internal/handlers"
)

func main() {
	common.Run(buildTopLevelMux())
}

func buildTopLevelMux() http.Handler {
	mux := buildPhotosRootMux()

	// Handle top-level dispatch url
	mux.Handle("/s/events/", http.StripPrefix("/s/events", buildEventsRootMux()))
	mux.Handle("/s/photos/", http.StripPrefix("/s/photos", buildPhotosRootMux()))
	mux.Handle("/s/users/", http.StripPrefix("/s/users", buildUsersRootMux()))

	return mux
}

func buildPhotosRootMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.Handle("/category/", http.StripPrefix("/category", buildPhotosCategoryMux()))
	// collection and folder are aliases
	mux.Handle("/collection/", http.StripPrefix("/collection", buildPhotosFolderMux()))
	mux.Handle("/date/", http.StripPrefix("/date", buildPhotosDateMux()))
	mux.Handle("/db/", http.StripPrefix("/db", buildPhotosDbMux()))
	mux.Handle("/folder/", http.StripPrefix("/folder", buildPhotosFolderMux()))
	mux.Handle("/group/", http.StripPrefix("/group", buildPhotosGroupMux()))
	mux.Handle("/keyword/", http.StripPrefix("/keyword", buildPhotosKeywordMux()))
	mux.Handle("/location/", http.StripPrefix("/location", buildPhotosLocationMux()))
	mux.Handle("/notify/", http.StripPrefix("/notify", buildPhotosNotifyMux()))
	mux.Handle("/page/", http.StripPrefix("/page", buildPhotosPageMux()))
	mux.Handle("/photo/", http.StripPrefix("/photo", buildPhotosPhotoMux()))
	mux.Handle("/sitemap/", http.StripPrefix("/sitemap", buildPhotosSitemapMux()))
	mux.Handle("/timeline-group/", http.StripPrefix("/timeline-group", buildPhotosTimelineGroupMux()))
	mux.Handle("/visibility/", http.StripPrefix("/visibility", buildPhotosVisibilityMux()))

	return mux
}

func buildPhotosCategoryMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.CategoryChildren))
	mux.HandleFunc("/empty/delete", corsHandler(handlers.CategoryDeleteEmpty))
	mux.HandleFunc("/empty/list", corsHandler(handlers.CategoryListEmpty))
	mux.HandleFunc("/info", corsHandler(handlers.CategoryInfo))
	mux.HandleFunc("/update", corsHandler(handlers.CategoryUpdate))
	mux.HandleFunc("/update-related", corsHandler(handlers.CategoryUpdateRelated))

	return mux
}

func buildPhotosDateMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.DateChildren))
	mux.HandleFunc("/empty/delete", corsHandler(handlers.DateDeleteEmpty))
	mux.HandleFunc("/empty/list", corsHandler(handlers.DateListEmpty))
	mux.HandleFunc("/info", corsHandler(handlers.DateInfo))
	return mux
}

func buildPhotosDbMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/schema/update", corsHandler(handlers.DbSchemaUpdate))
	mux.HandleFunc("/schema/version", corsHandler(handlers.DbSchemaVersion))
	mux.HandleFunc("/check-fix-vis-token-counts", corsHandler(handlers.DbCheckAndFixVisTokenCounts))

	return mux
}

func buildPhotosFolderMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.FolderChildren))
	mux.HandleFunc("/empty/delete", corsHandler(handlers.FolderDeleteEmpty))
	mux.HandleFunc("/empty/list", corsHandler(handlers.FolderListEmpty))
	mux.HandleFunc("/info", corsHandler(handlers.FolderInfo))
	mux.HandleFunc("/update", corsHandler(handlers.FolderUpdate))
	mux.HandleFunc("/visibility-token", corsHandler(handlers.FolderVisibilityToken))

	return mux
}

func buildPhotosGroupMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/home", corsHandler(handlers.HomeGroups))

	return mux
}

func buildPhotosKeywordMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.KeywordChildren))
	mux.HandleFunc("/empty/delete", corsHandler(handlers.KeywordDeleteEmpty))
	mux.HandleFunc("/empty/list", corsHandler(handlers.KeywordListEmpty))
	mux.HandleFunc("/info", corsHandler(handlers.KeywordInfo))
	mux.HandleFunc("/update", corsHandler(handlers.KeywordUpdate))

	return mux
}

func buildPhotosLocationMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.LocationChildren))
	mux.HandleFunc("/empty/delete", corsHandler(handlers.LocationDeleteEmpty))
	mux.HandleFunc("/empty/list", corsHandler(handlers.LocationListEmpty))
	mux.HandleFunc("/info", corsHandler(handlers.LocationInfo))
	mux.HandleFunc("/update", corsHandler(handlers.LocationUpdate))

	return mux
}

func buildPhotosNotifyMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/delete-photo", handlers.NotifyDeletePhotoTask)
	mux.HandleFunc("/master-storage-updates", handlers.NotifyMasterStorageUpdates)

	return mux
}

func buildPhotosPageMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.PageChildren))
	mux.HandleFunc("/create", corsHandler(handlers.PageCreate))
	mux.HandleFunc("/delete", corsHandler(handlers.PageDelete))
	mux.HandleFunc("/info", corsHandler(handlers.PageInfo))
	mux.HandleFunc("/update", corsHandler(handlers.PageUpdate))
	mux.HandleFunc("/visibility-token", corsHandler(handlers.PageVisibilityToken))

	return mux
}

func buildPhotosPhotoMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/list", corsHandler(handlers.PhotoList))
	mux.HandleFunc("/md/", corsHandler(handlers.PhotoGetMetadata))
	mux.HandleFunc("/random", corsHandler(handlers.PhotoRandom))
	mux.HandleFunc("/random-map", corsHandler(handlers.PhotoRandomMappable))
	mux.HandleFunc("/rz/", corsHandler(handlers.PhotoResize))

	return mux
}

func buildPhotosTimelineGroupMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/children", corsHandler(handlers.TimelineGroupChildren))
	mux.HandleFunc("/create", corsHandler(handlers.TimelineGroupCreate))
	mux.HandleFunc("/delete", corsHandler(handlers.TimelineGroupDelete))
	mux.HandleFunc("/info", corsHandler(handlers.TimelineGroupInfo))
	mux.HandleFunc("/sync", corsHandler(handlers.TimelineGroupSync))
	mux.HandleFunc("/update", corsHandler(handlers.TimelineGroupUpdate))
	mux.HandleFunc("/update-filter", corsHandler(handlers.TimelineGroupUpdateFilter))
	mux.HandleFunc("/visibility-token", corsHandler(handlers.TimelineGroupVisibilityToken))

	return mux
}

func buildPhotosSitemapMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/generate", corsHandler(handlers.SitemapGenerate))
	mux.HandleFunc("/sitemap.xml", corsHandler(handlers.SitemapGet))

	return mux
}

func buildPhotosVisibilityMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/clear", corsHandler(handlers.VisibilityTokensClear))
	mux.HandleFunc("/create", corsHandler(handlers.VisibilityCreate))
	mux.HandleFunc("/has-tokens", corsHandler(handlers.VisibilityHasTokens))
	mux.HandleFunc("/link/activate", corsHandler(handlers.VisibilityActivateLink))
	mux.HandleFunc("/link/create", corsHandler(handlers.VisibilityCreateLink))
	mux.HandleFunc("/link/update", corsHandler(handlers.VisibilityUpdateLink))
	mux.HandleFunc("/list", corsHandler(handlers.VisibilityList))

	return mux
}

func buildUsersRootMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.Handle("/add", corsHandler(handlers.UserAdd))
	mux.Handle("/login", corsHandler(handlers.UserLogin))
	mux.Handle("/logout", corsHandler(handlers.UserLogout))
	mux.Handle("/refresh", corsHandler(handlers.UserSessionRefresh))

	return mux
}

func buildEventsRootMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.Handle("/add", corsHandler(handlers.EventAdd))
	mux.Handle("/count", corsHandler(handlers.EventCount))

	return mux
}

func corsHandler(hf http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		origin := r.Header.Get("origin")
		originURL, _ := url.Parse(origin)

		var originHost string
		if originURL != nil {
			originHost = originURL.Hostname()
		}

		header := w.Header()
		// Due to CORS headers, caching should be done based on Origin.
		header.Add("vary", "Origin")
		if originHost == handlers.GetDomain() || (handlers.AllowLocalhost() && originHost == "localhost") {
			header.Add("access-control-allow-origin", origin)
			header.Add("access-control-allow-credentials", "true")
		}

		if r.Method == "OPTIONS" {
			header.Add("access-control-allow-headers", "accept, content-type")
			header.Add("access-control-allow-methods", "options, get, post")
			header.Add("access-control-max-age", "31536000") // 1 year
			w.WriteHeader(http.StatusNoContent)
		} else {
			hf(w, r)
		}
	}
}
