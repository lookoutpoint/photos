// SPDX-License-Identifier: MIT

package common

import (
	"math/rand"
	"net/http"
	"os"
	"time"

	"gitlab.com/lookoutpoint/photos/internal/database"
	"gitlab.com/lookoutpoint/photos/internal/handlers"
	"gitlab.com/lookoutpoint/storage"

	log "github.com/sirupsen/logrus"
)

// Run starts the http server with the given root handler
func Run(handler http.Handler) {
	// Seed.
	rand.Seed(time.Now().Unix())

	// Set up cloud storage
	store, err := storage.New()
	if err != nil {
		log.WithError(err).Fatal("Could not set up handler storage")
	}
	storage.Set(store)

	// Set up tasks
	tasks, err := handlers.NewTasks()
	if err != nil {
		log.WithError(err).Fatal("Could not set up tasks")
	}
	handlers.SetTasks(tasks)

	// Set up database
	db, err := database.New()
	if err != nil {
		log.WithError(err).Fatal("Could not set up handler database")
	}
	database.Set(db)

	// Database index updates. Not a blocking operation.
	go func() {
		if err := db.UpdateIndexes(); err != nil {
			// Just a warning
			log.WithError(err).Warn("Could not update database indexes")
		}
	}()

	// Start server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	log.Printf("Listening on port %s", port)
	if err := http.ListenAndServe(":"+port, handler); err != nil {
		log.Fatal(err)
	}
}
