// SPDX-License-Identifier: MIT

package main

import (
	"net/http"

	"gitlab.com/lookoutpoint/photos/cmd/common"
	"gitlab.com/lookoutpoint/photos/internal/handlers"
)

func main() {
	common.Run(buildTopLevelMux())
}

func buildTopLevelMux() http.Handler {
	mux := buildRootMux()

	// Handle top-level dispatch url
	mux.Handle("/s/photoadd/", http.StripPrefix("/s/photoadd", buildRootMux()))

	return mux
}

func buildRootMux() *http.ServeMux {
	mux := http.NewServeMux()

	mux.Handle("/notify/", http.StripPrefix("/notify", buildNotifyMux()))

	return mux
}

func buildNotifyMux() http.Handler {
	mux := http.NewServeMux()

	mux.HandleFunc("/finalize-photo", handlers.NotifyFinalizePhotoTask)

	return mux
}
