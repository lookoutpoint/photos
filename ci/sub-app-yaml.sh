#!/bin/sh
# SPDX-License-Identifier: MIT

set -e

APP_YAML=$1
MONGODB_URI=$2
DOMAIN=$3
ALLOW_LOCALHOST=$4
PHOTO_NOTIFY_QUEUE_ID=$5
PHOTO_NOTIFY_QUEUE_LOC_ID=$6

# Check args
if [ -z "$APP_YAML" ]; then
  echo "Error: APP_YAML is not defined"
  exit 1
fi

if [ -z "$MONGODB_URI" ]; then
  echo "Error: MONGODB_URI is not defined"
  exit 1
fi

if [ -z "$DOMAIN" ]; then
  echo "Error: DOMAIN is not defined"
  exit 1
fi

if [ -z "$ALLOW_LOCALHOST" ]; then
  echo "Error: ALLOW_LOCALHOST is not defined"
  exit 1
fi

if [ -z "$PHOTO_NOTIFY_QUEUE_ID" ]; then
  echo "Error: PHOTO_NOTIFY_QUEUE_ID is not defined"
  exit 1
fi

if [ -z "$PHOTO_NOTIFY_QUEUE_LOC_ID" ]; then
  echo "Error: PHOTO_NOTIFY_QUEUE_LOC_ID is not defined"
  exit 1
fi

# Check other env vars
if [ -z "$AUTH_AUDIENCE" ]; then
  echo "Error: AUTH_AUDIENCE is not defined"
  exit 1
fi

# This is horrible but if the URI has a "&" in it, it confuses the heck out of sed in CI
sed -i 's|<MONGODB_URI>|'"$(echo $MONGODB_URI | sed 's/&/\\&/')"'|' $APP_YAML
sed -i 's|<PHOTO_NOTIFY_QUEUE_ID>|'$PHOTO_NOTIFY_QUEUE_ID'|' $APP_YAML
sed -i 's|<PHOTO_NOTIFY_QUEUE_LOC_ID>|'$PHOTO_NOTIFY_QUEUE_LOC_ID'|' $APP_YAML
sed -i 's|<AUTH_AUDIENCE>|'$AUTH_AUDIENCE'|' $APP_YAML
sed -i 's|<DOMAIN>|'$DOMAIN'|' $APP_YAML
# For dev purposes, allow requests from localhost
sed -i 's|<ALLOW_LOCALHOST>|'$ALLOW_LOCALHOST'|' $APP_YAML
