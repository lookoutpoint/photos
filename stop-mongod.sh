#!/bin/sh
# SPDX-License-Identifier: MIT

echo "Stopping mongod..."
mongod --dbpath .mongo/data --shutdown

# Keep the log around
rm -rf .mongo/data
