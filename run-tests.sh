#!/bin/sh
# SPDX-License-Identifier: MIT

sh start-mongod.sh

# All tests share the same Mongod instance, can't run in parallel
go test -p 1 ./...
RV=$?

sh stop-mongod.sh

exit $RV
